package com.synapsis.cns.codensa.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.service.Service;
import com.synapsis.cns.codensa.model.LineaFactura;

/**
 * @author jhack
 */
public interface ReporteHelperService extends Service {
	public static final String NA = "N/A";
	public Integer getInteger(String intString); 
	public Double getDouble(String doubleString);
	public Long getLong(String longString);
	public BigDecimal getBigDecimal(String bigDecString);
	public Date getDate(String dateString);
//	public Date getShortDate(String dateString);
	public String getDato(String nombreDato, LineaFactura linea);
	public String getDatoVariable(String nombreDato, LineaFactura linea, String token);
	public String getDatoVariable(String nombreDato, LineaFactura linea, int inicio);
	public LineaFactura getDataLine(Date fecha, Long nroCuenta, Object reporte) throws ObjectNotFoundException;
	public Map findLineFormatFor(Object reporte);
	public int getCantServFinancAnexo(LineaFactura linea);
	public int getCantECOAnexo(LineaFactura linea);
	public int getInicioCamposECOAnexo(LineaFactura linea);
	public int getInicioCamposCCAAnexo(LineaFactura linea);
	public int getCCASize(LineaFactura linea);
	public int getECOSize(LineaFactura linea);
	public void setupLineaFactura(Object reporte, LineaFactura lineaFactura);
}
