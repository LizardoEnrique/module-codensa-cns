/**
 * $Id: ConsultaDetalleDeudaService.java,v 1.3 2007/12/05 20:00:13 syalri Exp $
 */
package com.synapsis.cns.codensa.service;

import java.util.List;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public interface ConsultaDetalleDeudaService {

	public List findByCriteria(QueryFilter filter)
			throws IllegalDAOAccessException;


	public int findByCriteriaToPageable(QueryFilter filter)
			throws IllegalDAOAccessException;	
	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException;

}