package com.synapsis.cns.codensa.service.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;
import com.synapsis.cns.codensa.model.CampoFactura;
import com.synapsis.cns.codensa.model.LineaFactura;
import com.synapsis.cns.codensa.model.ReporteAnexoFactura;
import com.synapsis.cns.codensa.model.ReporteFacturaComun;
import com.synapsis.cns.codensa.model.ReporteFacturaDuplicadoLegal;
import com.synapsis.cns.codensa.model.constants.AnexoFacturaConstants;
import com.synapsis.cns.codensa.model.constants.FacturaConstants;
import com.synapsis.cns.codensa.model.impl.CampoFacturaImpl;
import com.synapsis.cns.codensa.model.impl.ReporteAnexoFacturaImpl;
import com.synapsis.cns.codensa.service.ReporteHelperService;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

/**
 * @author jhack
 */
public class ReporteHelperServiceImpl extends SynergiaServiceImpl implements ReporteHelperService {
	private static Map registrosEspeciales = new HashMap();

	static {
		registrosEspeciales.put(FacturaConstants.FIELD_SUCURSAL_LECTURA, new RegistroClob(4, 480));
		registrosEspeciales.put(FacturaConstants.FIELD_ZONA_LECTURA, new RegistroClob(1, 485));
		registrosEspeciales.put(FacturaConstants.FIELD_CICLO_LECTURA, new RegistroClob(2, 486));
		registrosEspeciales.put(FacturaConstants.FIELD_GRUPO_LECTURA, new RegistroClob(3, 488));
		registrosEspeciales.put(FacturaConstants.FIELD_CORRELATIVO_LECTURA, new RegistroClob(4, 501));
		registrosEspeciales.put(FacturaConstants.FIELD_SUCURSAL_REPARTO, new RegistroClob(4, 103));
		registrosEspeciales.put(FacturaConstants.FIELD_ZONA_REPARTO, new RegistroClob(1, 108));
		registrosEspeciales.put(FacturaConstants.FIELD_CICLO_REPARTO, new RegistroClob(2, 109));
		registrosEspeciales.put(FacturaConstants.FIELD_GRUPO_REPARTO, new RegistroClob(3, 111));
		registrosEspeciales.put(FacturaConstants.FIELD_CORRELATIVO_REPARTO, new RegistroClob(4, 124));
		
		registrosEspeciales.put(FacturaConstants.FIELD_GRUPO_C, new RegistroClob(2, 4474));
		registrosEspeciales.put(FacturaConstants.FIELD_GRUPO_C2, new RegistroClob(2, 4478));
		registrosEspeciales.put(FacturaConstants.FIELD_GRUPO_C3, new RegistroClob(2, 4476));
		
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_INTERRUP, new RegistroClob(3, 4671));		
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_INTERRUP2, new RegistroClob(3, 4675));
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_INTERRUP3, new RegistroClob(3, 4673));
		
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_HORAS, new RegistroClob(3, 4712));
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_HORAS2, new RegistroClob(3, 4716));
		registrosEspeciales.put(FacturaConstants.FIELD_MAX_PERM_HORAS3, new RegistroClob(3, 4714));
		
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_INTERRUP_ACUM, new RegistroClob(3, 4534));		
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_INTERRUP_ACUM2, new RegistroClob(3, 4538));
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_INTERRUP_ACUM3, new RegistroClob(3, 4536));
		
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM, new RegistroClob(3, 4579));						
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM2, new RegistroClob(3, 4583));						
		registrosEspeciales.put(FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM3, new RegistroClob(3, 4581));						

		
		registrosEspeciales.put(FacturaConstants.FIELD_GTDCVPRRCUCF, new RegistroClob(96, 3900));
	}

	// le inyecto el servicio de busqueda de la linea de factura
	private CRUDService lineaService;

	private Map formatoDuplicadoLegal;

	public Integer getInteger(String intString) {
		try {
			return Integer.valueOf(intString.trim());
		}
		catch (NumberFormatException ex) {
			return new Integer(-1);
		}
	}

	public ReporteHelperServiceImpl() {
		super();
	}

	public ReporteHelperServiceImpl(Module module) {
		super(module);
	}

	public Double getDouble(String doubleString) {
		try {
			return Double.valueOf(doubleString.trim());
		}
		catch (NumberFormatException ex) {
			return new Double(-1);
		}
	}

	public BigDecimal getBigDecimal(String bigDecString) {
		try {
			return new BigDecimal(bigDecString.trim());
		}
		catch (NumberFormatException ex) {
			return new BigDecimal(-1);
		}
	}

	public Long getLong(String longString) {
		try {
			longString.replaceAll("$", "");
			return Long.valueOf(longString.trim());
		}
		catch (NumberFormatException ex) {
			return new Long(-1);
		}
	}

	/**
	 * formato de fecha, dado un string
	 */
	public Date getDate(String dateString) {
		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setLenient(false);
			date = dateFormat.parse(dateString.trim());
		}
		catch (ParseException ex) {
			date = new GregorianCalendar(1900, 0, 1).getTime();
		}
		return date;
	}

	/**
	 * Devuelve un dato de la factura
	 * 
	 * @param nombreDato es la clave �nica
	 * @param formato contiene la info de como obtenerlo del string
	 * @param linea contiene los datos
	 * @return dato de la factura
	 */
	public String getDato(String nombreDato, LineaFactura linea) {
		// Esto es una mierda pero los muchachos no quieren tocar el ProC y les esta generando %%
		// y produce un desplazamiento en la factura legal
		// Encima puede haber claves que no esten en la base por lo cual va primero esto, sino explota.		
		if (this.casoEspecial(nombreDato, linea)) {
			return this.procesarCasoEspecial(nombreDato, linea);
		}

		CampoFactura ubicacionDato = (CampoFactura) linea.getFormato().get(nombreDato);
		String lineaStr = "";

		// Factura o anexo
		if (linea.getReporte() instanceof ReporteAnexoFactura) {
			lineaStr = linea.getDatosAnexo();
		}
		else if (linea.getReporte() instanceof ReporteFacturaComun) {
			lineaStr = linea.getDatos();
		}

		String dato = "";
		if (linea.getDatos() != null) {
			// Real
			if (ubicacionDato != null) {
				int posInicio = ubicacionDato.getPosInicio().intValue();
				int posFin = ubicacionDato.getPosFin().intValue();
				try {
					dato = lineaStr.substring(posInicio - 1, posFin);
				}
				catch (StringIndexOutOfBoundsException ex) {
					dato = "N/A OOB";
				}
			}
			else {
				// System.out.println(" NOMBRE: "+ nombreDato + " - NO ESTA EN LA TABLA DE FORMATO ");
				// Real
				// dato = NA;
			}
		}
		return dato;
	}

	private boolean casoEspecial(String nombreDato, LineaFactura linea) {
		return (linea.getReporte() instanceof ReporteFacturaDuplicadoLegal && registrosEspeciales
			.containsKey(nombreDato));
	}

	private String procesarCasoEspecial(String nombreDato, LineaFactura linea) {
		RegistroClob registro = (RegistroClob) registrosEspeciales.get(nombreDato);
		try {
			return linea.getDatosClob().getSubString(registro.getPosicionInicial(), registro.getLongitud());	
		}
		catch (SQLException e) {
		}
		
		return "ERROR";
	}

	public String getDatoVariable(String nombreDato, LineaFactura linea, String token) {
		CampoFactura ubicacionDato = (CampoFactura) linea.getFormato().get(nombreDato);
		String dato;

		// Debug
		// if(false){
		// Real
		if (ubicacionDato != null) {
			int posInicio = ubicacionDato.getPosInicio().intValue();
			int posFin = ubicacionDato.getPosFin().intValue();
			try {
				dato = token.substring(posInicio - 1, posFin);
			}
			catch (StringIndexOutOfBoundsException ex) {
				dato = "N/A OOB";
			}
		}
		else {
			dato = "N/A";
		}
		return dato;
	}

	public String getDatoVariable(String nombreDato, LineaFactura linea, int offset) {
		CampoFactura ubicacionDato = (CampoFactura) linea.getFormato().get(nombreDato);
		String dato;
		if (ubicacionDato != null) {
			int posInicio = ubicacionDato.getPosInicio().intValue();
			int posFin = ubicacionDato.getPosFin().intValue();
			try {
				dato = linea.getDatos().substring(posInicio - 1 + offset, offset + posFin);
			}
			catch (StringIndexOutOfBoundsException ex) {
				dato = "N/A OOB";
			}
		}
		else {
			dato = "N/A";
		}
		return dato;
	}

	/**
	 * Devuelve una l�nea con la info de una factura o anexo
	 * 
	 * @throws ObjectNotFoundException
	 */
	public LineaFactura getDataLine(Date fecha, Long nroCuenta, Object reporte) throws ObjectNotFoundException {

		CompositeQueryFilter cqf = new CompositeQueryFilter();
		cqf.addFilter(new EqualQueryFilter("fechaFactura", fecha));
		cqf.addFilter(new EqualQueryFilter("nroCuenta", nroCuenta));

		LineaFactura lineaFactura = (LineaFactura) lineaService.findByCriteriaUnique(cqf);
		// lineaFactura = (LineaFactura) NasServiceHelper.getResponse("lineaFacturaService",
		// "findByCriteriaUnique", parameters);

		if (lineaFactura.getDatosAnexo() == null || lineaFactura.getDatosAnexo().trim().length() == 0) {
			lineaFactura.setDatosAnexo(this.getAnexoTestLine());
		}

		this.setupLineaFactura(reporte, lineaFactura);

		return lineaFactura;
	}

	public void setupLineaFactura(Object reporte, LineaFactura lineaFactura) {
		Map formato = this.findLineFormatFor(reporte);
		lineaFactura.setFormato(formato);
		lineaFactura.setReporte(reporte);

		/**
		 * ARREGLO ERROR DE DATOS, EN VEZ DE UN % APARECEN %%
		 */
		if (lineaFactura.getDatos() != null) {
			lineaFactura.setDatos(lineaFactura.getDatos().replaceAll("%%", "%"));
		}
		if (lineaFactura.getDatosAnexo() != null) {
			lineaFactura.setDatosAnexo(lineaFactura.getDatosAnexo().replaceAll("%%", "%"));
		}
	}

	/**
	 * Devuelve un Hash con la info de la estructura del archivo del anexo o factura Cada item es un par key:
	 * Constante con el nombre del campo (clave un�voca) value: Objeto CampoFactura con la info de un campo de
	 * la factura o anexo (nombre, desc, posicion inicial en el archivo, longitud, etc ...)
	 */
	public Map findLineFormatFor(Object reporte) {
		Map formato;
		// Factura o anexo
		if (reporte instanceof ReporteAnexoFacturaImpl) {
			formato = getFormatoAnexoFromMemory();
		}
		else {
			formato = this.getFormatoDuplicadoLegal();
		}
		return formato;
	}

	public static HashMap getFormatoFromMemory() {
		HashMap formato = new HashMap();

		addCampoFactura(FacturaConstants.FIELD_NOMBRE_CLIENTE, 11, 40, formato);
		addCampoFactura(FacturaConstants.FIELD_NUMERO_CUENTA, 3, 7, formato);
		addCampoFactura(FacturaConstants.FIELD_CODIGO_FACTURACION, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_FECHA_EXPEDICION, 0, 0, formato);
		addCampoFactura(FacturaConstants.FIELD_FECHA_PROCESO, 1088, 6, formato);
		addCampoFactura(FacturaConstants.FIELD_NIT, 51, 10, formato);
		// addCampoFactura(FacturaConstants.FIELD_DIR_OFIC_PPAL, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_NUMERO_FACTURA, 1094, 9, formato);
		addCampoFactura(FacturaConstants.FIELD_NUMERO_SERVICIO, 524, 9, formato);
		addCampoFactura(FacturaConstants.FIELD_PERIODO_FACTURADO_INICIO, 1066, 11, formato);
		addCampoFactura(FacturaConstants.FIELD_PERIODO_FACTURADO_FIN, 1077, 11, formato);
		// addCampoFactura(FacturaConstants.FIELD_TELEF_CLIENTE, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_DIRECC_CLIENTE, 917, 40, formato);
		addCampoFactura(FacturaConstants.FIELD_BARRIO_CLIENTE, 737, 40, formato);
		addCampoFactura(FacturaConstants.FIELD_MUNICIPIO_CLIENTE, 608, 40, formato);
		addCampoFactura(FacturaConstants.FIELD_INDIC_REPARTO_ESPEC, 99, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_TIPO_SERV_INFO_TEC, 998, 11, formato);
		addCampoFactura(FacturaConstants.FIELD_ZONA_LECTURA, 538, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_CICLO_LECTURA, 540, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_GRUPO_LECTURA, 542, 3, formato);
		addCampoFactura(FacturaConstants.FIELD_SUCURSAL_LECTURA, 534, 4, formato);
		addCampoFactura(FacturaConstants.FIELD_NRO_MEDIDOR_1, 1048, 9, formato);
		addCampoFactura(FacturaConstants.FIELD_NRO_MEDIDOR_2, 1057, 9, formato);
		addCampoFactura(FacturaConstants.FIELD_NRO_CIRCUITO, 4831, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_ESTRATO_SOCIO, 1020, 1, formato);
		addCampoFactura(FacturaConstants.FIELD_CARGA_CONTRATADA, 1038, 5, formato);
		addCampoFactura(FacturaConstants.FIELD_NIVEL_TENSION, 1009, 1, formato);
		addCampoFactura(FacturaConstants.FIELD_PERIODO_FACT_ACTUAL, 1088, 6, formato);
		addCampoFactura(FacturaConstants.FIELD_CONSUMO_PERIODO_ACTUAL, 2897, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_FEC_VENC_PAGO_OPORT, 3094, 11, formato);
		addCampoFactura(FacturaConstants.FIELD_FEC_VENC_AVISO_SUSP, 3119, 21, formato);
		addCampoFactura(FacturaConstants.FIELD_TOTAL_A_PAGAR, 3015, 14, formato);

		// CODIGO DE BARRAS
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_BEGIN, 3148, 1, formato);
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_NIEOCR, 3149, 7, formato);
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_DVNIE, 3156, 1, formato);
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_VALOROCR, 3157, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_FACTOCR, 3171, 9, formato);
		addCampoFactura(FacturaConstants.FIELD_COD_BARRAS_STOP, 3180, 1, formato);

		// INFORMACI�N DEL CONSUMO
		addCampoFactura(FacturaConstants.FIELD_TIPO_LECT_PER_ACTUAL, 4965, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_ANOMALIA, 4975, 22, formato);
		addCampoFactura(FacturaConstants.FIELD_MES_TARIFA_LIQ, 2897, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_PROM_ULT_MESES, 2394, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_COSTO_UNIT, 3642, 11, formato);
		// addCampoFactura(FacturaConstants.FIELD_ENERGIA_CONS, 2897, 10, formato);
		// addCampoFactura(FacturaConstants.FIELD_ENERGIA_FACT, 3022, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_TARIF_FACT_ACTUAL, 957, 30, formato);

		// addCampoFactura(FacturaConstants.FIELD_LECTURA_ACTUAL, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_LECTURA_ANTERIOR, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_DIF_ACT_ANT, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_FACTOR_LIQ, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_TOTAL_CONS_KWH, 2897, 10, formato);
		addCampoFactura(FacturaConstants.FIELD_VALOR_PROM_KWH, 2866, 9, formato);
		// addCampoFactura(FacturaConstants.FIELD_GENERACION, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_TRANSMISIO_REF, 3664, 11, formato);
		// addCampoFactura(FacturaConstants.FIELD_PERDIDAS, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_DISTRIBUCI_REF, 3675, 11, formato);
		// addCampoFactura(FacturaConstants.FIELD_OTROS, 1, 2, formato);
		addCampoFactura(FacturaConstants.FIELD_COMERCIALI_REF, 3686, 11, formato);

		// addCampoFactura(FacturaConstants.FIELD_MAX_PERM_INTERRUP, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_MAX_PERM_HORAS, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_NRO_INTERRUP_ACUM, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_NRO_HORAS_INTERRUP_ACUM, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_PERIODO_INTERRUP, 1, 2, formato);
		// addCampoFactura(FacturaConstants.FIELD_TRIMESTRE_INTERRUP, 1, 2, formato);

		// Consumos ultimos 6 meses
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_6MES_MES, 1104, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_6MES_CONS_KWH, 1247, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_5MES_MES, 1304, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_5MES_CONS_KWH, 1447, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_4MES_MES, 1504, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_4MES_CONS_KWH, 1647, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_3MES_MES, 1704, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_3MES_CONS_KWH, 1847, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_2MES_MES, 1904, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_2MES_CONS_KWH, 2047, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_1MES_MES, 2014, 23, formato);
		addCampoFactura(FacturaConstants.FIELD_CONS_ULT_1MES_CONS_KWH, 2247, 14, formato);

		// Factura Totales
		addCampoFactura(FacturaConstants.FIELD_SUBT_TOTAL_CARGOS, 4761, 14, formato);
		// addCampoFactura(FacturaConstants.FIELD_SUBT_CARGOS_CONS, 3195, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_SUBT_CARGOS_DESC, 3181, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_SUBT_CARGOS_SERV_PORT, 4747, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_SUBT_OTROS_CARGOS, 3195, 14, formato);
		addCampoFactura(FacturaConstants.FIELD_SUBT_TOTAL_FACTURA, 2897, 10, formato);

		// ultimo campo fijo
		addCampoFactura(FacturaConstants.FIELD_CANT_CARGOS, 5268, 2, formato);

		// CAMPOS VARIABLES (CARGOS) - Posiciones relativas dentro del token ...
		addCampoFactura(FacturaConstants.FIELD_VAR_CARGO_CODIGO, 50, 4, formato);
		addCampoFactura(FacturaConstants.FIELD_VAR_CARGO_CONCEPTO, 58, 44, formato);
		addCampoFactura(FacturaConstants.FIELD_VAR_CARGO_VALOR, 172, 14, formato);

		return formato;
	}

	private static void addCampoFactura(String nombre, int posInicio, int longitud, HashMap formato) {
		CampoFactura campo = new CampoFacturaImpl();
		int posFin = posInicio + (longitud - 1);

		campo.setNombre(nombre);
		campo.setDescripcion(nombre);
		campo.setPosInicio(new Long(posInicio));
		campo.setPosFin(new Long(posFin));
		campo.setLongitud(new Long(longitud));
		campo.setTipoDocumento("FACTURA");
		campo.setVersion(new Long(1));

		formato.put(campo.getNombre(), campo);
	}

	public static HashMap getFormatoAnexoFromMemory() {
		HashMap formato = new HashMap();

		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_numero_cuenta, 1, 7, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_dv_numero_cuenta, 8, 1, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_periodo_facturado, 9, 11, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_fecha_expedicion, 20, 11, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_direccion_reparto, 31, 75, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_manzana_reparto, 106, 10, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_sucursal_reparto, 116, 4, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_zona_reparto, 120, 1, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_ciclo_reparto, 121, 2, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_grupo_reparto, 123, 3, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_correlativo_reparto, 126, 4, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_localizacion_reparto, 130, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cod_depto_reparto, 170, 3, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_desc_depto_reparto, 173, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cod_munic_reparto, 213, 8, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_desc_munic_reparto, 221, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cod_localidad_reparto, 261, 2, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_desc_localidad_reparto, 263, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cod_upz_reparto, 303, 3, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_desc_upz_reparto, 306, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cod_barrio_reparto, 346, 4, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_desc_barrio_reparto, 350, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_tipo_dir_reparto, 390, 40, formato);
		addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_mensaje_anexo_factura, 430, 130, formato);

		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nro_servicio_financiero, 1, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nombre_titular, 11, 255, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nro_tarjeta, 266, 20, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_linea_negocio, 286, 100, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tipo_producto, 386, 100, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nombre_plan, 486, 100, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_precio_producto, 586, 14, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_valor_cuota_mensual, 600, 14, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_cuota_manejo, 614, 14, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_interes_cuota_manejo, 628, 14, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_serv_financiero, 642, 14, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_capital_facturado, 656, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_intereses_facturados, 666, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tasa_actual_reg_financ, 676, 25, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tasa_int_inicial, 701, 8, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_establecimiento_compra, 709, 8, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_fecha_creacion_convenio, 717, 11, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_fecha_compra, 728, 11, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_cupo_aprobado, 739, 2, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_cupo_disponible, 741, 2, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nro_total_cuotas, 743, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nro_cuotas_facturadas, 753, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_nro_cuotas_pendientes, 763, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tasa_interes_mora, 773, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_estado_convenio, 783, 10, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tasa_efectiva_anual, 793, 8, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_tasa_efectiva_mensual, 801, 8, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_periodo_compra, 809, 2, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_periodo_creacion_convenio, 811, 2, formato);
		addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_periodos_impagos, 813, 2, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_todos_servicios, 815, 14,
		// formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_servicios, 829, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_cedulas, 843, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_int_mora_cedula, 857, 14,
		// formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_linea_negocio, 871, 14,
		// formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_tipo_producto, 885, 14,
		// formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_linea_por_establecimiento,
		// 899, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_producto_por_establecimiento,
		// 913, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_total_dia_almacen_cuota, 927, 14,
		// formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_capital_mora_todos_servicios,
		// 941, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_interes_mora_todos_servicios,
		// 955, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_capital_mora_por_servicio,
		// 969, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_interes_mora_por_servicio,
		// 983, 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_capital_mora_por_cedula, 997,
		// 14, formato);
		// addCampoAnexoFactura("SERVFINANC", AnexoFacturaConstants.FIELD_saldo_interes_mora_por_cedula, 1011,
		// 14, formato);

		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_nro_eco, 1, 10, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_id_eco, 11, 10, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_titular_eco, 21, 255, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_linea_negocio_eco, 276, 100, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_socio_negocio_eco, 376, 100, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_tipo_producto_eco, 476, 100, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_plan_eco, 576, 100, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_valor_cuota_eco, 676, 14, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_perioricidad_cuota_eco, 690, 2, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_fecha_cargue_eco, 692, 11, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_nro_total_cuotas_eco, 703, 2, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_cuotas_facturadas_eco, 705, 2, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_nro_periodos_impagos_eco, 707, 2, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_cuotas_faltantes_eco, 709, 2, formato);
		addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_fecha_compra_eco, 711, 11, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_todos_servicios_eco, 722, 14,
		// formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_servicios_eco, 736, 14, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_cedulas_eco, 750, 14, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_int_mora_cedula_eco, 764, 14,
		// formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_linea_negocio_eco, 778, 14, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_tipo_producto_eco, 792, 14, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_linea_por_socio_eco, 806, 14,
		// formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_producto_por_socio_eco, 820, 14,
		// formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_total_dia_almacen_cuota_eco, 834, 14,
		// formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_saldo_capital_mora_todos_servicios_eco,
		// 848, 14, formato);
		// addCampoAnexoFactura("ECO", AnexoFacturaConstants.FIELD_saldo_interes_mora_todos_servicios_eco,
		// 862, 14, formato);

		// addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cant_serv_financ, 560, 2, formato);
		// addCampoAnexoFactura("GENERAL", AnexoFacturaConstants.FIELD_cant_eco, 562, 2, formato);

		return formato;
	}

	/**
	 * TODO Probar levantar el excel con la api POI de APACHE ...
	 * 
	 * @return
	 */
	/*
	 * public static HashMap getFormatoFromFile() { CampoFactura campo = null; HashMap formato = new
	 * HashMap();
	 * 
	 * try{ FileInputStream fis = new FileInputStream("camposAnexoFacturaImportBD"); Object obj = fis.read();
	 * 
	 * while(true){ String key = "codigo_facturacion"; // {posini, long, posfin}; Object[] values = {1, 2, 2};
	 * addCampoFactura(AnexoFacturaConstants.FIELD_VAR_CARGO_VALOR, 172, 14, formato); }
	 * }catch(FileNotFoundException ex){
	 * 
	 * }catch(IOException ex){ }
	 * 
	 * 
	 * return formato; }
	 */

	private static void addCampoAnexoFactura(String tipoInfo, String nombre, int posInicio, int longitud,
			HashMap formato) {
		CampoFactura campo = new CampoFacturaImpl();
		int posFin = posInicio + (longitud - 1);

		campo.setTipoInfo(tipoInfo);
		campo.setNombre(nombre);
		campo.setDescripcion(nombre);
		campo.setPosInicio(new Long(posInicio));
		campo.setPosFin(new Long(posFin));
		campo.setLongitud(new Long(longitud));
		campo.setTipoDocumento("ANEXO");
		campo.setVersion(new Long(1));

		formato.put(campo.getNombre(), campo);
	}

	public int getCantServFinancAnexo(LineaFactura linea) {
		// Anteultimos dos caracteres de todo el string son la cant de serv CCA appendeados al string ...
		// Cantidad de Serv Financ.
		int beginIndex = linea.getDatosAnexo().length() - 20;
		int endIndex = linea.getDatosAnexo().length() - 10;
		String cantServFinancStr = linea.getDatosAnexo().substring(beginIndex, endIndex);

		// String cantServFinancStr = this.getDato(AnexoFacturaConstants.FIELD_cant_serv_financ, linea);
		return Integer.parseInt(cantServFinancStr.trim());

	}

	public int getCantECOAnexo(LineaFactura linea) {
		// Ultimos dos caracteres de todo el string son la cant de serv ECO appendeados al string ...
		int beginIndex = linea.getDatosAnexo().length() - 10;
		int endIndex = linea.getDatosAnexo().length();
		String cantECOStr = linea.getDatosAnexo().substring(beginIndex, endIndex);

		// String cantECOStr = this.getDato(AnexoFacturaConstants.FIELD_cant_eco, linea);
		return Integer.parseInt(cantECOStr.trim());
	}

	public int getInicioCamposECOAnexo(LineaFactura linea) {
		int inicioECO = this.getInicioCamposCCAAnexo(linea) + (getCantServFinancAnexo(linea) * getCCASize(linea));
		return inicioECO;
	}

	public int getInicioCamposCCAAnexo(LineaFactura linea) {
		// ultimo campo fijo - inicio de los items de serv financieros CCA ...
		CampoFactura campoMensajeAnexo = (CampoFactura) linea.getFormato().get(
			AnexoFacturaConstants.FIELD_mensaje_anexo_factura);
		int inicioCamposCCA = campoMensajeAnexo.getPosFin().intValue();
		return inicioCamposCCA;
	}

	public int getCCASize(LineaFactura linea) {
		Collection camposAnexo = linea.getFormato().values();
		Iterator camposAnexoIter = camposAnexo.iterator();
		int camposAnexoCCASize = 0;
		while (camposAnexoIter.hasNext()) {
			CampoFactura campo = (CampoFactura) camposAnexoIter.next();
			if (campo.getTipoInfo().equals("SERVFINANC")) {
				camposAnexoCCASize = camposAnexoCCASize + campo.getLongitud().intValue();
			}
		}

		return camposAnexoCCASize;
		// return AnexoFacturaConstants.SERV_FINANC_CCA_SIZE;
	}

	public int getECOSize(LineaFactura linea) {
		Collection camposAnexo = linea.getFormato().values();
		Iterator camposAnexoIter = camposAnexo.iterator();
		int camposAnexoECOSize = 0;
		while (camposAnexoIter.hasNext()) {
			CampoFactura campo = (CampoFactura) camposAnexoIter.next();
			if (campo.getTipoInfo().equals("ECO")) {
				camposAnexoECOSize = camposAnexoECOSize + campo.getLongitud().intValue();
			}
		}

		return camposAnexoECOSize;
		// return AnexoFacturaConstants.ECO_SIZE;
	}

	private String getAnexoTestLine() {
		StringBuffer linea = new StringBuffer();

		addCamposFijos(linea);

		addCamposCCA(linea, "1");
		addCamposCCA(linea, "2");

		addCamposECO(linea, "1");
		addCamposECO(linea, "2");

		String cant_servicios_cca = "2         "; // 10
		linea.append(cant_servicios_cca);

		String cant_servicios_eco = "2         "; // 10
		linea.append(cant_servicios_eco);

		return linea.toString();

	}

	private void addCamposECO(StringBuffer linea, String nroEco) {
		String nro_eco = "123456789" + nroEco; // 10
		linea.append(nro_eco);

		String id_eco = "ID ECO 14 "; // 10
		linea.append(id_eco);

		String titular_eco = "TITULAR DEL ECO     1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345"; // 255
		linea.append(titular_eco);

		String linea_negocio_eco = "ASISTENCIA EXEQUIAL 12345678901234567890123456789012345678901234567890123456789012345678901234567890"; // 100
		linea.append(linea_negocio_eco);

		String socio_negocio_eco = "SOCIO DE NEGOCIO ECO12345678901234567890123456789012345678901234567890123456789012345678901234567890"; // 100
		linea.append(socio_negocio_eco);

		String tipo_producto_eco = "TIPO DE PRODUCTO ECO12345678901234567890123456789012345678901234567890123456789012345678901234567890"; // 100
		linea.append(tipo_producto_eco);

		String plan_eco = "SEGUROS CODENSA     12345678901234567890123456789012345678901234567890123456789012345678901234567890";// 100
		linea.append(plan_eco);

		String valor_cuota_eco = "123           "; // 14
		linea.append(valor_cuota_eco);

		String perioricidad_cuota_eco = "12"; // 2
		linea.append(perioricidad_cuota_eco);

		String fecha_cargue_eco = "25/12/2007 "; // 11
		linea.append(fecha_cargue_eco);

		String nro_total_cuotas_eco = "55";// 2
		linea.append(nro_total_cuotas_eco);

		String cuotas_facturadas_eco = "55";// 2
		linea.append(cuotas_facturadas_eco);

		String nro_periodos_impagos_eco = "55";// 2
		linea.append(nro_periodos_impagos_eco);

		String cuotas_faltantes_eco = "55";// 2
		linea.append(cuotas_faltantes_eco);

		String fecha_compra_eco = "25/12/2007 "; // 11
		linea.append(fecha_compra_eco);

	}

	private void addCamposCCA(StringBuffer linea, String nroCCA) {
		String nro_servicio_financiero = "799565870" + nroCCA;// 10
		linea.append(nro_servicio_financiero);

		String nombre_titular = "ANTONIO ABELLO      " + // 20
			"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345"; // 255-20=235
		linea.append(nombre_titular);

		String nro_tarjeta = "12345678901234567890";// 20
		linea.append(nro_tarjeta);

		String linea_negocio = "LINEA DE NEGOCIO 1  12345678901234567890123456789012345678901234567890123456789012345678901234567890";// 100
		linea.append(linea_negocio);

		String tipo_producto = "CREDITO FACIL CODENSA 345678901234567890123456789012345678901234567890123456789012345678901234567890";// 100
		linea.append(tipo_producto);

		String nombre_plan = "ELECTRO CARREFOUR   12345678901234567890123456789012345678901234567890123456789012345678901234567890";// 100
		linea.append(nombre_plan);

		String precio_producto = "155589.5545   ";// 14
		linea.append(precio_producto);

		String valor_cuota_mensual = "155589.5545   ";// 14
		linea.append(valor_cuota_mensual);

		String cuota_manejo = "155589.5545   ";// 14
		linea.append(cuota_manejo);

		String interes_cuota_manejo = "166666.5545   ";// 14
		linea.append(interes_cuota_manejo);

		String saldo_serv_financiero = "155589.5545   ";// 14
		linea.append(saldo_serv_financiero);

		String capital_facturado = "1555.55   ";// 10
		linea.append(capital_facturado);

		String intereses_facturados = "1555.55   ";// 10
		linea.append(intereses_facturados);

		String tasa_actual_reg_financ = "TASA DE INTERES ACTUAL   ";// 25
		linea.append(tasa_actual_reg_financ);

		String tasa_int_inicial = "159.58  ";// 8
		linea.append(tasa_int_inicial);

		String establecimiento_compra = "159     ";// 8
		linea.append(establecimiento_compra);

		String fecha_creacion_convenio = "25/12/2007 ";// 11
		linea.append(fecha_creacion_convenio);

		String fecha_compra = "25/12/2007 ";// 11
		linea.append(fecha_compra);

		String cupo_aprobado = "25";// 2
		linea.append(cupo_aprobado);

		String cupo_disponible = "25";// 2
		linea.append(cupo_disponible);

		String nro_total_cuotas = "12        ";// 10
		linea.append(nro_total_cuotas);

		String nro_cuotas_facturadas = "10        ";// 10
		linea.append(nro_cuotas_facturadas);

		String nro_cuotas_pendientes = "2         ";// 10
		linea.append(nro_cuotas_pendientes);

		String tasa_interes_mora = "10        ";// 10
		linea.append(tasa_interes_mora);

		String estado_convenio = "11        ";// 10
		linea.append(estado_convenio);

		String tasa_efectiva_anual = "10      ";// 8
		linea.append(tasa_efectiva_anual);

		String tasa_efectiva_mensual = "10      ";// 8
		linea.append(tasa_efectiva_mensual);

		String periodo_compra = "22";// 2
		linea.append(periodo_compra);

		String periodo_creacion_convenio = "22";// 2
		linea.append(periodo_creacion_convenio);

		String periodos_impagos = "22";// 2
		linea.append(periodos_impagos);

	}

	private void addCamposFijos(StringBuffer linea) {
		String numero_cuenta = "1234567"; // 7
		linea.append(numero_cuenta);

		String dv_numero_cuenta = "1"; // 1
		linea.append(dv_numero_cuenta);

		String periodo_facturado = "25/12/2007 "; // 11
		linea.append(periodo_facturado);

		String fecha_expedicion = "25/12/2007 "; // 11
		linea.append(fecha_expedicion);

		String direccion_reparto = "Direccion Reparto XXX        0123456789012345678901234567890123456789012345"; // 75
		linea.append(direccion_reparto);

		String manzana_reparto = "Manzana 1 "; // 10
		linea.append(manzana_reparto);

		String sucursal_reparto = "Suc1"; // 4
		linea.append(sucursal_reparto);

		String zona_reparto = "5"; // 1
		linea.append(zona_reparto);

		String ciclo_reparto = "C1"; // 2
		linea.append(ciclo_reparto);

		String grupo_reparto = "GR1"; // 3
		linea.append(grupo_reparto);

		String correlativo_reparto = "CO21"; // 4
		linea.append(correlativo_reparto);

		String localizacion_reparto = "Localizaci�n del cliente 678901234567890";// 40
		linea.append(localizacion_reparto);

		String cod_depto_reparto = "CR2";// 3
		linea.append(cod_depto_reparto);

		String desc_depto_reparto = "Descripci�n de departamento (reparto)   "; // 40
		linea.append(desc_depto_reparto);

		String cod_munic_reparto = "COD_MUNI";// 8
		linea.append(cod_munic_reparto);

		String desc_munic_reparto = "Descripci�n de municipio (reparto) 67890";// 40
		linea.append(desc_munic_reparto);

		String cod_localidad_reparto = "CL"; // 2
		linea.append(cod_localidad_reparto);

		String desc_localidad_reparto = "Descripci�n de localidad (reparto) 67890";// 40
		linea.append(desc_localidad_reparto);

		String cod_upz_reparto = "UPZ"; // 3
		linea.append(cod_upz_reparto);

		String desc_upz_reparto = "Descripci�n de u.p.z. (reparto) 34567890"; // 40
		linea.append(desc_upz_reparto);

		String cod_barrio_reparto = "BAR1";// 4
		linea.append(cod_barrio_reparto);

		String desc_barrio_reparto = "Descripci�n  de barrio o vereda reparto "; // 40
		linea.append(desc_barrio_reparto);

		String tipo_dir_reparto = "Tipo de direcci�n reparto urbana-rural  "; // 40
		linea.append(tipo_dir_reparto);

		String mensaje_anexo_factura = "Mensaje asignado en el parametrizador de mensajes.12345678901234567890123456789012345678901234567890123456789012345678901234567890"; // 130
		linea.append(mensaje_anexo_factura);
	}

	/**
	 * @return the lineaService
	 */
	public CRUDService getLineaService() {
		return lineaService;
	}

	/**
	 * @param lineaService the lineaService to set
	 */
	public void setLineaService(CRUDService lineaService) {
		this.lineaService = lineaService;
	}

	public Map getFormatoDuplicadoLegal() {
		if (formatoDuplicadoLegal == null) {
			formatoDuplicadoLegal = getFormatoDuplicadoLegalFromDB();
		}

		return formatoDuplicadoLegal;
	}

	private static HashMap getFormatoDuplicadoLegalFromDB() {
		HashMap formato = new HashMap();
		Map parameters = new HashMap();
		parameters.put("tipoDocumento", "FACTURA");
		parameters.put("version", new Long(1));
		List lista = (List) NasServiceHelper.getResponse("camposFacturaService", "findByCriteria", parameters);

		for (Iterator it = lista.iterator(); it.hasNext();) {
			CampoFactura campo = (CampoFactura) it.next();
			formato.put(campo.getNombre(), campo);
		}

		return formato;
	}

	private static class RegistroClob {
		private int longitud;
		private int posicionInicial;

		private RegistroClob(int longitud, int posicionInicial) {
			super();
			this.longitud = longitud;
			this.posicionInicial = posicionInicial;
		}

		public int getLongitud() {
			return longitud;
		}

		public void setLongitud(int longitud) {
			this.longitud = longitud;
		}

		public int getPosicionInicial() {
			return posicionInicial;
		}

		public void setPosicionInicial(int posicionInicial) {
			this.posicionInicial = posicionInicial;
		}

	}

}