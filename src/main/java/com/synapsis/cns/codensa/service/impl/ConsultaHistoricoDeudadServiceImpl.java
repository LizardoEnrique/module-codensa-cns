/**
 * 
 */
package com.synapsis.cns.codensa.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.suivant.arquitectura.core.dao.DAO;
import com.suivant.arquitectura.core.exception.IllegalDAOAccessException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.synapsis.cns.codensa.service.ConsultaDetalleDeudaService;
import com.synapsis.cns.codensa.service.ConsultaHistoricoDeudadService;
import com.synapsis.nuc.codensa.validation.NumeroCuentaQueryFilterValidation;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public class ConsultaHistoricoDeudadServiceImpl extends SynergiaServiceImpl
		implements ConsultaHistoricoDeudadService {

	public ConsultaHistoricoDeudadServiceImpl() {
		super();
	}

	public ConsultaHistoricoDeudadServiceImpl(Module module) {
		super(module);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.service.impl.ConsultaDetalleDeuda#findByCriteria(java.lang.Long,
	 *      java.lang.String)
	 */
	/*
	 * public BusinessObject findByCriteriaUnique(Long numeroCuenta) throws
	 * IllegalDAOAccessException {
	 * 
	 * Class klass = null; BusinessObject bo = null; EqualQueryFilter filter =
	 * new EqualQueryFilter("nroCuenta", numeroCuenta);
	 * filter.setQueryFilter(this.getEmpresaQueryFilter()); klass =
	 * com.synapsis.cns.codensa.model.impl.DeudaServicioImpl.class; if (klass !=
	 * null) try { bo = getDao(klass).findByCriteriaUnique(filter); } catch
	 * (ObjectNotFoundException e) { } return bo; }
	 */
	public List findByCriteria(Long numeroCuenta)
			throws IllegalDAOAccessException {

		Class klass = null;
		List results = null;
		EqualQueryFilter filter = new EqualQueryFilter("nroCuenta",
				numeroCuenta);

		NumeroCuentaQueryFilterValidation nc = new NumeroCuentaQueryFilterValidation(
				filter);
		if (nc.execute()) {
			filter.setQueryFilter(this.getEmpresaQueryFilter());
			klass = com.synapsis.cns.codensa.model.impl.DeudaServicioImpl.class;
			if (klass != null)
				results = getDao(klass).findByCriteria(filter);
			return results;
		}
		return null;
	}

	/**
	 * retorna el dao asociado
	 * 
	 * @return DAO
	 * @throws IllegalDAOAccessException
	 */
	public DAO getDao(Class klass) throws IllegalDAOAccessException {
		return getDAO(klass);
	}
}
