/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaRefacturacionAjusteItem extends SynergiaBusinessObject {

	public Long getCodigoItem() ;
	public Double getConsumoAjusteKW();
	public Double getConsumoAntesAjusteKW();
	public Double getConsumoDespuesAjusteKW();
	public String getDescripcionItem() ;
	public Date getFechaAjusteItem() ;
	public Long getIdAjuste() ;
	public Long getIdCuenta() ;
	public Long getNroAjuste();
	public Long getNroCuenta() ;
	public Double getValorAjusteItem();
	public Double getValorAntesAjusteItem() ;
	public Double getValorDespuesAjusteImpl();
}
