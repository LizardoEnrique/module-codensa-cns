package com.synapsis.cns.codensa.model;

import java.util.Collection;

/**
 * Interfaz del reporte del Duplicado Legal de Factura (Cobranza, no CNS!!!)
 * 
 * @author egrande
 */
public interface ReporteFacturaDuplicadoLegal extends ReporteFacturaComun {

	public String getFechaExpedicion();

	public void setFechaExpedicion(String fechaExpedicion);

	public String getFechaVencimientoAvisoSuspension();

	public void setFechaVencimientoAvisoSuspension(String fechaVencimientoAvisoSuspension);

	public String getFechaVencimientoPagoOportuno();

	public void setFechaVencimientoPagoOportuno(String fechaVencimientoPagoOportuno);

	public String getPeriodoFacturadoFin();

	public void setPeriodoFacturadoFin(String periodoFacturadoFin);

	public String getPeriodoFacturadoInicio();

	public void setPeriodoFacturadoInicio(String periodoFacturadoInicio);

	public String getFechaProceso();

	public void setFechaProceso(String fechaProceso);

	public String getAnomaliaLecturaPeriodoActual();

	public void setAnomaliaLecturaPeriodoActual(String anomaliaLecturaPeriodoActual);

	public String getConsumoPromedioUltimosMeses();

	public void setConsumoPromedioUltimosMeses(String consumoPromedioUltimosMeses);

	public String getFactorLiquidacion();

	public void setFactorLiquidacion(String factorLiquidacion);

	public String getMesTarifaParaLiquidacion();

	public void setMesTarifaParaLiquidacion(String mesTarifaParaLiquidacion);

	public String getTarifaParaFacturacionActual();

	public void setTarifaParaFacturacionActual(String tarifaParaFacturacionActual);

	public String getTipoLecturaPeriodoActual();

	public void setTipoLecturaPeriodoActual(String tipoLecturaPeriodoActual);

	public String getTotalConsumoKwh();

	public void setTotalConsumoKwh(String totalConsumoKwh);

	public String getValorPromedioKwh();

	public void setValorPromedioKwh(String valorPromedioKwh);

	public String getComercializacion();

	public void setComercializacion(String comercializacion);

	public String getCostoUnitario();

	public void setCostoUnitario(String costoUnitario);
	
	public String getCostoUnitarioFijo();

	public void setCostoUnitarioFijo(String costoUnitario);
	
	public String getRestricciones();

	public void setRestricciones(String restricciones);

	public String getDistribucion();

	public void setDistribucion(String distribucion);

	public String getGeneracion();

	public void setGeneracion(String generacion);

	public String getMaximoPermitidoHoras();

	public void setMaximoPermitidoHoras(String maximoPermitidoHoras);

	public String getMaximoPermitidoInterrupciones();

	public void setMaximoPermitidoInterrupciones(String maximoPermitidoInterrupciones);

	public String getNumeroHorasInterrupcionesAcumuladas();

	public void setNumeroHorasInterrupcionesAcumuladas(String numeroHorasInterrupcionesAcumuladas);

	public String getNumeroInterrupcionesAcumuladas();

	public void setNumeroInterrupcionesAcumuladas(String numeroInterrupcionesAcumuladas);

	public String getOtros();

	public void setOtros(String otros);

	public String getPerdidas();

	public void setPerdidas(String perdidas);

	public String getPeriodoInterrupciones();

	public void setPeriodoInterrupciones(String periodoInterrupciones);

	public String getTransmision();

	public void setTransmision(String transmision);

	public String getTrimestreInterrupciones();

	public void setTrimestreInterrupciones(String trimestreInterrupciones);

	public String getDiferenciaEntreLecturaActualYAnterior();

	public void setDiferenciaEntreLecturaActualYAnterior(String diferenciaEntreLecturaActualYAnterior);

	public String getEnergiaConsumida();

	public void setEnergiaConsumida(String energiaConsumida);

	public String getEnergiaFacturada();

	public void setEnergiaFacturada(String energiaFacturada);

	public String getLecturaActual();

	public void setLecturaActual(String lecturaActual);

	public String getLecturaAnterior();

	public void setLecturaAnterior(String lecturaAnterior);

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public String getSubtCargosCons();

	public void setSubtCargosCons(String subtCargosCons);

	public String getSubtOtrosCargos();

	public void setSubtOtrosCargos(String subtOtrosCargos);

	public String getSubtCargosDesc();

	public void setSubtCargosDesc(String subtCargosDesc);

	public String getSubtTotCargosEne();

	public void setSubtTotCargosEne(String subtTotCargosEne);

	public String getSubtCargosSrvPort();

	public void setSubtCargosSrvPort(String subtCargosSrvPort);

	public String getTotalFactura();

	public void setTotalFactura(String totalFactura);
	
	public String getTotalAPagar();

	public void setTotalAPagar(String totalAPagar);

	public void setDireccionSuministro(String direccionSuministro);
	public String getDireccionSuministro();

	public void setDireccionReparto(String direccionReparto);
	public String getDireccionReparto();
	
	public void setMunicipioReparto(String municipioReparto);
	public String getMunicipioReparto();
	
	public void setBarrioReparto(String barrioReparto);
	public String getBarrioReparto();
	
	public Collection getCargos();

	public void setCargos(Collection cargos);

	public Collection getCargosDescuento();

	public void setCargosDescuento(Collection cargosDescuento);

	public Collection getCargosOtrosServPort();

	public void setCargosOtrosServPort(Collection cargosOtrosServPort);

	public Collection getOtrosCargos();

	public void setOtrosCargos(Collection otrosCargos);

	public Collection getConsumosUltimos6Meses();

	public void setConsumosUltimos6Meses(Collection consumosUltimos6Meses);

	public Collection getCargosServicioElectrico();

	public void setCargosServicioElectrico(Collection cargosServicioElectrico);

	public Collection getConsumos();

	public void setConsumos(Collection consumos);
	
	public String getRutaReparto();
	
	public void setRutaReparto(String rutaReparto);
	
	public String getManzanaLectura();
	
	public void setManzanaLectura(String manzanaLectura);

	public String getManzanaReparto();

	public void setManzanaReparto(String manzanaReparto);
	
	public String getProximaLectura();

	public void setProximaLectura(String proximaLectura);
}