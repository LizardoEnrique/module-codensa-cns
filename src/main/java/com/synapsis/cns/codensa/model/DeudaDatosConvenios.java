package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaDatosConvenios extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getDeudaConvenio();

	public void setDeudaConvenio(String deudaConvenio);

	public Double getAntiguedadDeudaConvenio();

	public void setAntiguedadDeudaConvenio(Double antiguedadDeudaConvenio);

	public Double getSaldosEnergia();

	public void setSaldosEnergia(Double saldosEnergia);

	public Double getTotalCuotasOtorgadas();

	public void setTotalCuotasOtorgadas(Double totalCuotasOtorgadas);

	public Double getTotalCuotasFacturadas();

	public void setTotalCuotasFacturadas(Double totalCuotasFacturadas);

	public String getValorDeudaCondonada();

	public void setValorDeudaCondonada(String valorDeudaCondonada);

	public String getIndicadorDeudaPendiente();

	public void setIndicadorDeudaPendiente(String indicadorDeudaPendiente);

	public String getEstadoCobranza();

	public void setEstadoCobranza(String estadoCobranza);

}