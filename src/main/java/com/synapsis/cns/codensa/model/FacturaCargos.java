package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface FacturaCargos extends SynergiaBusinessObject {

	public String getNroFactura();

	public void setNroFactura(String nroFactura);

	public String getCodigo();

	public void setCodigo(String codigo);

	public String getConcepto();

	public void setConcepto(String concepto);

	public Double getValor();

	public void setValor(Double valor);

}