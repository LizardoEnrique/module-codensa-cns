package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDatosTransformador extends SynergiaBusinessObject {
	public String getCircuito(); 
	public void setCircuito(String circuito); 
	public String getConexion(); 
	public void setConexion(String conexion);
	public String getEstado();
	public void setEstado(String estado);
	public String getPropiedad();
	public void setPropiedad(String propiedad);
	public String getRed();
	public void setRed(String red);
	public String getTension();
	public void setTension(String tension);
	public String getTransformador();
	public void setTransformador(String transformador);
	public String getAlumbrado();
	public void setAlumbrado(String alumbrado);
	public Double getCantidadServicios();
	public void setCantidadServicios(Double cantidadServicios);
	public String getPeriodoFact();
	public void setPeriodoFact(String periodoFact);
	public Date getFechaProceso();
	public void setFechaProceso(Date fechaProceso);
	
}