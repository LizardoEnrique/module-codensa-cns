/**
 * $Id: ComboTipoOrden.java,v 1.2 2007/02/06 22:30:58 ar29261698 Exp $
 */

package com.synapsis.cns.codensa.model.combo;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ComboTipoOrden extends SynergiaBusinessObject{

	public abstract String getDescripcion();
	public abstract void setDescripcion(String descripcion);

	public String getCodigo();
	public void setCodigo(String codigo);
	
	
}
