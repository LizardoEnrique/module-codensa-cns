/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public interface CaracteristicasGeneral extends SynergiaBusinessObject {
	
	public String getCircuito() ;
	public void setCircuito(String circuito) ;
	public String getDescripcionVoltaje();
	public void setDescripcionVoltaje(String descripcionVoltaje);
	public Date getFechaConexion();
	public void setFechaConexion(Date fechaConexion);
	public String getNivelTension();
	public void setNivelTension(String nivelTension);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta) ;
	public Long getNroServicio() ;
	public void setNroServicio(Long nroServicio);
	public String getNumeroOrden() ;
	public void setNumeroOrden(String numeroOrden);
	public String getPuntoTransformacion() ;
	public void setPuntoTransformacion(String puntoTransformacion);
	public String getTarifa();
	public void setTarifa(String tarifa);

}
