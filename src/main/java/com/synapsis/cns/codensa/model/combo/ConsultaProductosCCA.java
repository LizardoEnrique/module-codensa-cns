package com.synapsis.cns.codensa.model.combo;

public interface ConsultaProductosCCA {

	/**
	 * @return the descripcion
	 */
	public String getDescripcion();

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);

	public Long getIdProducto();

	public void setIdProducto(Long idProducto);

}