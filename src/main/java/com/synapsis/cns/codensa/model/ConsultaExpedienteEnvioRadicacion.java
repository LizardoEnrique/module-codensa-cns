package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteEnvioRadicacion extends SynergiaBusinessObject {

	/**
	 * @return the decision
	 */
	public String getDecision();

	/**
	 * @param decision the decision to set
	 */
	public void setDecision(String decision);

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision();

	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision);

	/**
	 * @return the fechaRadicacion
	 */
	public Date getFechaRadicacion();

	/**
	 * @param fechaRadicacion the fechaRadicacion to set
	 */
	public void setFechaRadicacion(Date fechaRadicacion);

	/**
	 * @return the fechaResolucion
	 */
	public Date getFechaResolucion();

	/**
	 * @param fechaResolucion the fechaResolucion to set
	 */
	public void setFechaResolucion(Date fechaResolucion);

	/**
	 * @return the numeroRadicacion
	 */
	public Long getNumeroRadicacion();

	/**
	 * @param numeroRadicacion the numeroRadicacion to set
	 */
	public void setNumeroRadicacion(Long numeroRadicacion);

	/**
	 * @return the numeroResolucion
	 */
	public Long getNumeroResolucion();

	/**
	 * @param numeroResolucion the numeroResolucion to set
	 */
	public void setNumeroResolucion(Long numeroResolucion);

	/**
	 * @return the valorFinal
	 */
	public Double getValorFinal();

	/**
	 * @param valorFinal the valorFinal to set
	 */
	public void setValorFinal(Double valorFinal);
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente();
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente);

}