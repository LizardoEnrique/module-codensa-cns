/**
 * $Id: ConsultaFacturaDetalleValoresConsumoImpl.java,v 1.8 2010/11/05 16:51:56 ar29302553 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaFacturaDetalleValoresConsumo;

/**
 * @author ar30557486
 *
 */
public class ConsultaFacturaDetalleValoresConsumoImpl extends
	SynergiaBusinessObjectImpl implements ConsultaFacturaDetalleValoresConsumo {
	
	private static final long serialVersionUID = 1L;
	
	private String periodo;	
	private Date fechaFacturacion;	
	private Date fechaLectura;	
	private String valorConsumoActiva;	
	private String valorConsumoReactiva;	
	private String valorSubsidio;	
	private String valorContribucion;
	private Long nroCuenta;
	
	private Double demandaNormal;
	private Double demandaHp;
	private Double alumbradoPublico;
	private Double potenciaGeneradaHp;
	private Double potenciaGeneradaFp;
	private Double mantMedidor;	
	private Double cargoFijo;
	private Double energiaActivaHp;
	
	private String caracteristica082;
	private Date vigenciaTarifa;

	
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getFechaLectura()
	 */
	public Date getFechaLectura() {
		return fechaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setFechaLectura(java.util.Date)
	 */
	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getPeriodo()
	 */
	public String getPeriodo() {
		return periodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setPeriodo(java.util.Date)
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getValorConsumoActiva()
	 */
	public String getValorConsumoActiva() {
		return valorConsumoActiva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setValorConsumoActiva(java.lang.Double)
	 */
	public void setValorConsumoActiva(String valorConsumoActiva) {
		this.valorConsumoActiva = valorConsumoActiva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getValorConsumoReactiva()
	 */
	public String getValorConsumoReactiva() {
		return valorConsumoReactiva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setValorConsumoReactiva(java.lang.Double)
	 */
	public void setValorConsumoReactiva(String valorConsumoReactiva) {
		this.valorConsumoReactiva = valorConsumoReactiva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getValorContribucion()
	 */
	public String getValorContribucion() {
		return valorContribucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setValorContribucion(java.lang.Double)
	 */
	public void setValorContribucion(String valorContribucion) {
		this.valorContribucion = valorContribucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#getValorSubsidio()
	 */
	public String getValorSubsidio() {
		return valorSubsidio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturaDetalleValoresConsumo#setValorSubsidio(java.lang.Double)
	 */
	public void setValorSubsidio(String valorSubsidio) {
		this.valorSubsidio = valorSubsidio;
	}
	public Double getAlumbradoPublico() {
		return alumbradoPublico;
	}
	public void setAlumbradoPublico(Double alumbradoPublico) {
		this.alumbradoPublico = alumbradoPublico;
	}
	public Double getCargoFijo() {
		return cargoFijo;
	}
	public void setCargoFijo(Double cargoFijo) {
		this.cargoFijo = cargoFijo;
	}
	public Double getDemandaHp() {
		return demandaHp;
	}
	public void setDemandaHp(Double demandaHp) {
		this.demandaHp = demandaHp;
	}
	public Double getDemandaNormal() {
		return demandaNormal;
	}
	public void setDemandaNormal(Double demandaNormal) {
		this.demandaNormal = demandaNormal;
	}
	public Double getEnergiaActivaHp() {
		return energiaActivaHp;
	}
	public void setEnergiaActivaHp(Double energiaActivaHp) {
		this.energiaActivaHp = energiaActivaHp;
	}
	public Double getMantMedidor() {
		return mantMedidor;
	}
	public void setMantMedidor(Double mantMedidor) {
		this.mantMedidor = mantMedidor;
	}
	public Double getPotenciaGeneradaFp() {
		return potenciaGeneradaFp;
	}
	public void setPotenciaGeneradaFp(Double potenciaGeneradaFp) {
		this.potenciaGeneradaFp = potenciaGeneradaFp;
	}
	public Double getPotenciaGeneradaHp() {
		return potenciaGeneradaHp;
	}
	public void setPotenciaGeneradaHp(Double potenciaGeneradaHp) {
		this.potenciaGeneradaHp = potenciaGeneradaHp;
	}
	/**
	 * @return the caracteristica082
	 */
	public String getCaracteristica082() {
		return caracteristica082;
	}
	/**
	 * @param caracteristica082 the caracteristica082 to set
	 */
	public void setCaracteristica082(String caracteristica082) {
		this.caracteristica082 = caracteristica082;
	}
	/**
	 * @return the vigenciaTarifa
	 */
	public Date getVigenciaTarifa() {
		return vigenciaTarifa;
	}
	/**
	 * @param vigenciaTarifa the vigenciaTarifa to set
	 */
	public void setVigenciaTarifa(Date vigenciaTarifa) {
		this.vigenciaTarifa = vigenciaTarifa;
	}
}