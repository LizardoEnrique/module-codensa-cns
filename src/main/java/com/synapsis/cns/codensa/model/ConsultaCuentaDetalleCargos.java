package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaDetalleCargos extends SynergiaBusinessObject {
	
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	
	/**
	 * @return the area
	 */
	public String getArea();

	/**
	 * @param area the area to set
	 */
	public void setArea(String area);

	/**
	 * @return the cargo
	 */
	public String getCargo();

	/**
	 * @param cargo the cargo to set
	 */
	public void setCargo(String cargo);

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the consumo
	 */
	public Long getConsumo();

	/**
	 * @param consumo the consumo to set
	 */
	public void setConsumo(Long consumo);

	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso();

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso);

	/**
	 * @return the idConfigFactura
	 */
	public Long getIdConfigFact();

	/**
	 * @param idConfigFactura the idConfigFactura to set
	 */
	public void setIdConfigFact(Long idConfigFact);

	/**
	 * @return the idDetalle
	 */
	public Long getIdDetalle();

	/**
	 * @param idDetalle the idDetalle to set
	 */
	public void setIdDetalle(Long idDetalle);

	/**
	 * @return the tipoCargo
	 */
	public String getTipoCargo();

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public void setTipoCargo(String tipoCargo);

	/**
	 * @return the unidadCobro
	 */
	public String getUnidadCobro();

	/**
	 * @param unidadCobro the unidadCobro to set
	 */
	public void setUnidadCobro(String unidadCobro);

	/**
	 * @return the valorCargo
	 */
	public Long getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(Long valorCargo);
	
	/**
	 * @return the vigenciaTarifa
	 */
	public Date getVigenciaTarifa();
	/**
	 * @param vigenciaTarifa the vigenciaTarifa to set
	 */
	public void setVigenciaTarifa(Date vigenciaTarifa);

}