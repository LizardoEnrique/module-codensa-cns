/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;


import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteImpl extends SynergiaBusinessObjectImpl implements ConsultaExpediente 
 {
	   private Long nroCuenta;             
	   private Long nroExpediente;         
	   private String tipoExpediente;      
	   private String estadoExpediente;    
	   private String buzonExpediente;     
	   private String metodoLiquidacion;   
	   private Double diasPermanencia;     
	   private String infoSancion;         
	   private Double totalLiquidado;      
	   private String estadoLiquidacion;   
	   private String obsLiquidacion;
	   private String nombreSusc;
	   private String calidad;
	
	   
	 /**
	 * @return the buzonExpediente
	 */
	public String getBuzonExpediente() {
		return buzonExpediente;
	}
	/**
	 * @param buzonExpediente the buzonExpediente to set
	 */
	public void setBuzonExpediente(String buzonExpediente) {
		this.buzonExpediente = buzonExpediente;
	}
	/**
	 * @return the diasPermanencia
	 */
	public Double getDiasPermanencia() {
		return diasPermanencia;
	}
	/**
	 * @param diasPermanencia the diasPermanencia to set
	 */
	public void setDiasPermanencia(Double diasPermanencia) {
		this.diasPermanencia = diasPermanencia;
	}
	/**
	 * @return the estadoExpediente
	 */
	public String getEstadoExpediente() {
		return estadoExpediente;
	}
	/**
	 * @param estadoExpediente the estadoExpediente to set
	 */
	public void setEstadoExpediente(String estadoExpediente) {
		this.estadoExpediente = estadoExpediente;
	}
	/**
	 * @return the estadoLiquidacion
	 */
	public String getEstadoLiquidacion() {
		return estadoLiquidacion;
	}
	/**
	 * @param estadoLiquidacion the estadoLiquidacion to set
	 */
	public void setEstadoLiquidacion(String estadoLiquidacion) {
		this.estadoLiquidacion = estadoLiquidacion;
	}
	/**
	 * @return the infoSancion
	 */
	public String getInfoSancion() {
		return infoSancion;
	}
	/**
	 * @param infoSancion the infoSancion to set
	 */
	public void setInfoSancion(String infoSancion) {
		this.infoSancion = infoSancion;
	}
	/**
	 * @return the metodoLiquidacion
	 */
	public String getMetodoLiquidacion() {
		return metodoLiquidacion;
	}
	/**
	 * @param metodoLiquidacion the metodoLiquidacion to set
	 */
	public void setMetodoLiquidacion(String metodoLiquidacion) {
		this.metodoLiquidacion = metodoLiquidacion;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	}
	/**
	 * @return the obsLiquidacion
	 */
	public String getObsLiquidacion() {
		return obsLiquidacion;
	}
	/**
	 * @param obsLiquidacion the obsLiquidacion to set
	 */
	public void setObsLiquidacion(String obsLiquidacion) {
		this.obsLiquidacion = obsLiquidacion;
	}
	/**
	 * @return the tipoExpediente
	 */
	public String getTipoExpediente() {
		return tipoExpediente;
	}
	/**
	 * @param tipoExpediente the tipoExpediente to set
	 */
	public void setTipoExpediente(String tipoExpediente) {
		this.tipoExpediente = tipoExpediente;
	}
	/**
	 * @return the totalLiquidado
	 */
	public Double getTotalLiquidado() {
		return totalLiquidado;
	}
	/**
	 * @param totalLiquidado the totalLiquidado to set
	 */
	public void setTotalLiquidado(Double totalLiquidado) {
		this.totalLiquidado = totalLiquidado;
	}
	/**
	 * @return the nombreSusc
	 */
	public String getNombreSusc() {
		return nombreSusc;
	}
	/**
	 * @param nombreSusc the nombreSusc to set
	 */
	public void setNombreSusc(String nombreSusc) {
		this.nombreSusc = nombreSusc;
	}
	/**
	 * @return the calidad
	 */
	public String getCalidad() {
		return calidad;
	}
	/**
	 * @param calidad the calidad to set
	 */
	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}      
}
