/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaRefacturacionCongelacionOperacion extends
		SynergiaBusinessObject {
	
	public String getCodigoItem() ;
	public Double getConsumoKW() ;
	public String getDescripcionItem();
	public Double getEnergiaKW() ;
	public Double getEnergiaKWAclaracion();
	public Date getFechaFacturacion() ;
	public Long getIdCuenta() ;
	public String getIdentificacionItem();
	public Long getIdOperacion() ;
	public Long getNroCuenta() ;
	public Long getNroOperacion();
	public String getObservaciones();
	public Double getValorItem() ;
	public Double getValorItemAclaracion();
	public Double getValorItemAntesYDespues();

}
