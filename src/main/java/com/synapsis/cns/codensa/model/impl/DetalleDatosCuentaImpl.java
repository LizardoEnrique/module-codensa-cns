package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DetalleDatosCuenta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleDatosCuentaImpl extends SynergiaBusinessObjectImpl 
	implements DetalleDatosCuenta {

	private Long nroCuenta;
	private String cuentaCorporativa;
	private String codigoEntidadComercial;
	private String descripcionEntidadComercial;
	private String numeroCuentaPadre;
	private String indicadorPresentaExpediente;
	private String indicadorDeudaCastigada;
	private String indicadorPresentaDeudaCondonadaPendiente;
	private String indicadorClienteVip;
	private String servicioElectricoEmpresarial;
	private String perteneceOtroComercializador;
	private String cuentaOficial;	
	private String procesoFacturacion;
	private String codigoRetencion;
	private String rutaFacturacion;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getCodigoEntidadComercial() {
		return codigoEntidadComercial;
	}
	public void setCodigoEntidadComercial(String codigoEntidadComercial) {
		this.codigoEntidadComercial = codigoEntidadComercial;
	}
	public String getCodigoRetencion() {
		return codigoRetencion;
	}
	public void setCodigoRetencion(String codigoRetencion) {
		this.codigoRetencion = codigoRetencion;
	}
	public String getCuentaCorporativa() {
		return cuentaCorporativa;
	}
	public void setCuentaCorporativa(String cuentaCorporativa) {
		this.cuentaCorporativa = cuentaCorporativa;
	}
	public String getCuentaOficial() {
		return cuentaOficial;
	}
	public void setCuentaOficial(String cuentaOficial) {
		this.cuentaOficial = cuentaOficial;
	}
	public String getDescripcionEntidadComercial() {
		return descripcionEntidadComercial;
	}
	public void setDescripcionEntidadComercial(String descripcionEntidadComercial) {
		this.descripcionEntidadComercial = descripcionEntidadComercial;
	}
	public String getIndicadorClienteVip() {
		return indicadorClienteVip;
	}
	public void setIndicadorClienteVip(String indicadorClienteVip) {
		this.indicadorClienteVip = indicadorClienteVip;
	}
	public String getIndicadorDeudaCastigada() {
		return indicadorDeudaCastigada;
	}
	public void setIndicadorDeudaCastigada(String indicadorDeudaCastigada) {
		this.indicadorDeudaCastigada = indicadorDeudaCastigada;
	}
	public String getIndicadorPresentaDeudaCondonadaPendiente() {
		return indicadorPresentaDeudaCondonadaPendiente;
	}
	public void setIndicadorPresentaDeudaCondonadaPendiente(
			String indicadorPresentaDeudaCondonadaPendiente) {
		this.indicadorPresentaDeudaCondonadaPendiente = indicadorPresentaDeudaCondonadaPendiente;
	}
	public String getIndicadorPresentaExpediente() {
		return indicadorPresentaExpediente;
	}
	public void setIndicadorPresentaExpediente(String indicadorPresentaExpediente) {
		this.indicadorPresentaExpediente = indicadorPresentaExpediente;
	}
	public String getNumeroCuentaPadre() {
		return numeroCuentaPadre;
	}
	public void setNumeroCuentaPadre(String numeroCuentaPadre) {
		this.numeroCuentaPadre = numeroCuentaPadre;
	}
	public String getPerteneceOtroComercializador() {
		return perteneceOtroComercializador;
	}
	public void setPerteneceOtroComercializador(String perteneceOtroComercializador) {
		this.perteneceOtroComercializador = perteneceOtroComercializador;
	}
	public String getProcesoFacturacion() {
		return procesoFacturacion;
	}
	public void setProcesoFacturacion(String procesoFacturacion) {
		this.procesoFacturacion = procesoFacturacion;
	}
	public String getRutaFacturacion() {
		return rutaFacturacion;
	}
	public void setRutaFacturacion(String rutaFacturacion) {
		this.rutaFacturacion = rutaFacturacion;
	}
	public String getServicioElectricoEmpresarial() {
		return servicioElectricoEmpresarial;
	}
	public void setServicioElectricoEmpresarial(String servicioElectricoEmpresarial) {
		this.servicioElectricoEmpresarial = servicioElectricoEmpresarial;
	}
}
