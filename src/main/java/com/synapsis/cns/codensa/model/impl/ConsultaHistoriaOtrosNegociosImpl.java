/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoriaOtrosNegocios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaHistoriaOtrosNegociosImpl extends SynergiaBusinessObjectImpl 
implements ConsultaHistoriaOtrosNegocios {
	private String tipoServicio;
	private String tipoIdentificacionTitular;
	private String lineaNegocio;
	private Date fechaInicialProducto;
	private Date FechaFinalProducto;
	private Long nroServicio;
	private Long nroIdentificacionTitular;
	private String producto;
	private Long nroIdentificadorCodensaHogar;
	private String nombreTitularProducto;
	private String plan;
	private Long nroCuenta;
	private String detalleProducto;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getFechaFinalProducto()
	 */
	public Date getFechaFinalProducto() {
		return FechaFinalProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setFechaFinalProducto(java.util.Date)
	 */
	public void setFechaFinalProducto(Date fechaFinalProducto) {
		FechaFinalProducto = fechaFinalProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getFechaInicialProducto()
	 */
	public Date getFechaInicialProducto() {
		return fechaInicialProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setFechaInicialProducto(java.util.Date)
	 */
	public void setFechaInicialProducto(Date fechaInicialProducto) {
		this.fechaInicialProducto = fechaInicialProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getLineaNegocio()
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setLineaNegocio(java.lang.String)
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getNombreTitularProducto()
	 */
	public String getNombreTitularProducto() {
		return nombreTitularProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setNombreTitularProducto(java.lang.String)
	 */
	public void setNombreTitularProducto(String nombreTitularProducto) {
		this.nombreTitularProducto = nombreTitularProducto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getNroIdentificacionTitular()
	 */
	public Long getNroIdentificacionTitular() {
		return nroIdentificacionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setNroIdentificacionTitular(java.lang.Long)
	 */
	public void setNroIdentificacionTitular(Long nroIdentificacionTitular) {
		this.nroIdentificacionTitular = nroIdentificacionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getNroIdentificadorCodensaHogar()
	 */
	public Long getNroIdentificadorCodensaHogar() {
		return nroIdentificadorCodensaHogar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setNroIdentificadorCodensaHogar(java.lang.Long)
	 */
	public void setNroIdentificadorCodensaHogar(Long nroIdentificadorCodensaHogar) {
		this.nroIdentificadorCodensaHogar = nroIdentificadorCodensaHogar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getPlan()
	 */
	public String getPlan() {
		return plan;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setPlan(java.lang.String)
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getProducto()
	 */
	public String getProducto() {
		return producto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setProducto(java.lang.String)
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getTipoIdentificacionTitular()
	 */
	public String getTipoIdentificacionTitular() {
		return tipoIdentificacionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setTipoIdentificacionTitular(java.lang.String)
	 */
	public void setTipoIdentificacionTitular(String tipoIdentificacionTitular) {
		this.tipoIdentificacionTitular = tipoIdentificacionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#getTipoServicio()
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoriaOtrosNegocios#setTipoServicio(java.lang.String)
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the detalleProducto
	 */
	public String getDetalleProducto() {
		return detalleProducto;
	}
	/**
	 * @param detalleProducto the detalleProducto to set
	 */
	public void setDetalleProducto(String detalleProducto) {
		this.detalleProducto = detalleProducto;
	}

}
