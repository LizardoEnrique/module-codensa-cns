package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author ccamba (Vista: CNS_CONS_CLI_PREF_V)
 * 
 */
public interface ConsultaClientePreferencial extends SynergiaBusinessObject {
 
	public Long getNroCuenta();

	public Long getNroCuentaAnterior();
		
	public String getTipoCliente();
	
}
