package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaHistoriaOtrosNegocios {

	/**
	 * @return the fechaFinalProducto
	 */
	public Date getFechaFinalProducto();

	/**
	 * @param fechaFinalProducto the fechaFinalProducto to set
	 */
	public void setFechaFinalProducto(Date fechaFinalProducto);

	/**
	 * @return the fechaInicialProducto
	 */
	public Date getFechaInicialProducto();

	/**
	 * @param fechaInicialProducto the fechaInicialProducto to set
	 */
	public void setFechaInicialProducto(Date fechaInicialProducto);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the nombreTitularProducto
	 */
	public String getNombreTitularProducto();

	/**
	 * @param nombreTitularProducto the nombreTitularProducto to set
	 */
	public void setNombreTitularProducto(String nombreTitularProducto);

	/**
	 * @return the nroIdentificacionTitular
	 */
	public Long getNroIdentificacionTitular();

	/**
	 * @param nroIdentificacionTitular the nroIdentificacionTitular to set
	 */
	public void setNroIdentificacionTitular(
			Long nroIdentificacionTitular);

	/**
	 * @return the nroIdentificadorCodensaHogar
	 */
	public Long getNroIdentificadorCodensaHogar();

	/**
	 * @param nroIdentificadorCodensaHogar the nroIdentificadorCodensaHogar to set
	 */
	public void setNroIdentificadorCodensaHogar(
			Long nroIdentificadorCodensaHogar);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the tipoIdentificacionTitular
	 */
	public String getTipoIdentificacionTitular();

	/**
	 * @param tipoIdentificacionTitular the tipoIdentificacionTitular to set
	 */
	public void setTipoIdentificacionTitular(
			String tipoIdentificacionTitular);

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the detalleProducto
	 */
	public String getDetalleProducto();
	/**
	 * @param detalleProducto the detalleProducto to set
	 */
	public void setDetalleProducto(String detalleProducto);

}