package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaComponenteMedidor {

	/**
	 * @return the factor
	 */
	public Double getFactor();

	/**
	 * @param factor the factor to set
	 */
	public void setFactor(Double factor);

	/**
	 * @return the fase
	 */
	public String getFase();

	/**
	 * @param fase the fase to set
	 */
	public void setFase(String fase);

	/**
	 * @return the fechaInstalacion
	 */
	public Date getFechaInstalacion();

	/**
	 * @param fechaInstalacion the fechaInstalacion to set
	 */
	public void setFechaInstalacion(Date fechaInstalacion);

	/**
	 * @return the marca
	 */
	public String getMarca();

	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca);

	/**
	 * @return the nroComponente
	 */
	public String getNroComponente();

	/**
	 * @param nroComponente the nroComponente to set
	 */
	public void setNroComponente(String nroComponente);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);

}