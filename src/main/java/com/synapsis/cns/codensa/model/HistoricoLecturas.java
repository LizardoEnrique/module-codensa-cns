package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface HistoricoLecturas extends SynergiaBusinessObject {

	public Long getConsumoActivaFP();

	public void setConsumoActivaFP(Long consumoActivaFP);

	public Long getConsumoActivoHP();

	public void setConsumoActivoHP(Long consumoActivoHP);

	public Long getConsumoActivoXP();

	public void setConsumoActivoXP(Long consumoActivoXP);

	public Long getConsumoReactivoCalculadoFP();

	public void setConsumoReactivoCalculadoFP(Long consumoReactivoCalculadoFP);

	public Long getConsumoReactivoCalculadoHP();

	public void setConsumoReactivoCalculadoHP(Long consumoReactivoCalculadoHP);

	public Long getConsumoReactivoCalculadoXP();

	public void setConsumoReactivoCalculadoXP(Long consumoReactivoCalculadoXP);

	public Long getConsumoReactivoFacturadoFP();

	public void setConsumoReactivoFacturadoFP(Long consumoReactivoFacturadoFP);

	public Long getConsumoReactivoFacturadoHP();

	public void setConsumoReactivoFacturadoHP(Long consumoReactivoFacturadoHP);

	public Long getConsumoReactivoFacturadoXP();

	public void setConsumoReactivoFacturadoXP(Long consumoReactivoFacturadoXP);

	public Date getFechaLectura();

	public void setFechaLectura(Date fechaLectura);

	public Long getIdMedidor();

	public void setIdMedidor(Long idMedidor);

	public Long getLecturaActivaFP();

	public void setLecturaActivaFP(Long lecturaActivaFP);

	public Long getLecturaActivaXP();

	public void setLecturaActivaXP(Long lecturaActivaXP);

	public Long getLecturaReactivaFP();

	public void setLecturaReactivaFP(Long lecturaReactivaFP);

	public Long getLecturaReactivaHP();

	public void setLecturaReactivaHP(Long lecturaReactivaHP);

	public Long getLecturaReactivaXP();

	public void setLecturaReactivaXP(Long lecturaReactivaXP);

	public Long getLecturaActivaHP();

	public void setLecturaActivaHP(Long lecturaActivaHP);

}