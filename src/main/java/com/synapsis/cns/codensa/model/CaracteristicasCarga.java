/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public interface CaracteristicasCarga extends SynergiaBusinessObject {
	
	public String getCargaAlumbrado();
	public void setCargaAlumbrado(String cargaAlumbrado);
	public String getCargaAlumbradoInstalada() ;
	public void setCargaAlumbradoInstalada(String cargaAlumbradoInstalada);
	public String getCargaCalefaccion() ;
	public void setCargaCalefaccion(String cargaCalefaccion) ;
	public String getCargaCalefaccionInstalada() ;
	public void setCargaCalefaccionInstalada(String cargaCalefaccionInstalada);
	public String getCargaFuerza();
	public void setCargaFuerza(String cargaFuerza) ;
	public String getCargaFuerzaInstalada() ;
	public void setCargaFuerzaInstalada(String cargaFuerzaInstalada);
	public Long getNroCuenta() ;
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	public String getTotalCarga();
	public void setTotalCarga(String totalCarga);
	public String getTotalCargaInstalada();
	public void setTotalCargaInstalada(String totalCargaInstalada);
}
