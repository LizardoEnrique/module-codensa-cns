package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaDeudaConveniosConceptos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaDeudaConveniosConceptosImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDeudaConveniosConceptos {

	/**
	 * Comment for <code>indicador</code>
	 */
	private String indicador;

	/**
	 * Comment for <code>saldo</code>
	 */
	private Double saldo;

	/**
	 * @return the indicador
	 */
	public String getIndicador() {
		return this.indicador;
	}

	/**
	 * @param indicador
	 *            the indicador to set
	 */
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}

	/**
	 * @return the saldo
	 */
	public Double getSaldo() {
		return this.saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

}