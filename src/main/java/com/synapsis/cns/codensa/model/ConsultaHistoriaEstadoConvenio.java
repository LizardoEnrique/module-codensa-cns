package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaHistoriaEstadoConvenio {

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion();

	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the nroSolicitante
	 */
	public String getNroSolicitante();

	/**
	 * @param nroSolicitante the nroSolicitante to set
	 */
	public void setNroSolicitante(String nroSolicitante);

	/**
	 * @return the saldoServicio
	 */
	public Double getSaldoServicio();

	/**
	 * @param saldoServicio the saldoServicio to set
	 */
	public void setSaldoServicio(Double saldoServicio);

	/**
	 * @return the valorOriginal
	 */
	public Double getValorOriginal();

	/**
	 * @param valorOriginal the valorOriginal to set
	 */
	public void setValorOriginal(Double valorOriginal);

}