package com.synapsis.cns.codensa.model.impl;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * @author m3.MarioRoss - 19/12/2006
 *
 */
public class FechaParser {
	private static DateFormat df = new SimpleDateFormat("MMMMM", Locale.getDefault());
	private Calendar cal = Calendar.getInstance();
	
	public FechaParser(Date fecha)
	{
		cal.setTime(fecha);
	}
	
	public Integer getDia() {
		return new Integer(cal.get(Calendar.DAY_OF_MONTH));
	}

	public String getMes() {
		return df.format(cal.getTime());
	}

	public Integer getAnio() {
		return new Integer(cal.get(Calendar.YEAR));
	}

	public Date getFecha()
	{
		return cal.getTime();
	}
}
