package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ResumenDeudaServicio extends SynergiaBusinessObject {

	/**
	 * @return the antiguedadDeuda
	 */
	public abstract Double getAntiguedadDeuda();

	/**
	 * @param antiguedadDeuda the antiguedadDeuda to set
	 */
	public abstract void setAntiguedadDeuda(Double antiguedadDeuda);

	/**
	 * @return the deudaConvenida
	 */
	public abstract String getDeudaConvenida();

	/**
	 * @param deudaConvenida the deudaConvenida to set
	 */
	public abstract void setDeudaConvenida(String deudaConvenida);

//	/**
//	 * @return the deudaCorte
//	 */
//	public abstract String getDeudaCorte();
//
//	/**
//	 * @param deudaCorte the deudaCorte to set
//	 */
//	public abstract void setDeudaCorte(String deudaCorte);

	/**
	 * @return the deudaDisputa
	 */
	public abstract String getDeudaDisputa();

	/**
	 * @param deudaDisputa the deudaDisputa to set
	 */
	public abstract void setDeudaDisputa(String deudaDisputa);

	/**
	 * @return the deudaEncargoCobranza
	 */
	public abstract String getDeudaEncargoCobranza();

	/**
	 * @param deudaEncargoCobranza the deudaEncargoCobranza to set
	 */
	public abstract void setDeudaEncargoCobranza(String deudaEncargoCobranza);

	/**
	 * @return the deudaEnergiaActual
	 */
	public abstract String getDeudaEnergiaActual();

	/**
	 * @param deudaEnergiaActual the deudaEnergiaActual to set
	 */
	public abstract void setDeudaEnergiaActual(String deudaEnergiaActual);

	/**
	 * @return the deudaPagar
	 */
	public abstract String getDeudaPagar();

	/**
	 * @param deudaPagar the deudaPagar to set
	 */
	public abstract void setDeudaPagar(String deudaPagar);

	/**
	 * @return the deudaServicioFinanciero
	 */
	public abstract String getDeudaServicioFinanciero();

	/**
	 * @param deudaServicioFinanciero the deudaServicioFinanciero to set
	 */
	public abstract void setDeudaServicioFinanciero(
			String deudaServicioFinanciero);

	/**
	 * @return the deudaTotal
	 */
	public abstract String getDeudaTotal();

	/**
	 * @param deudaTotal the deudaTotal to set
	 */
	public abstract void setDeudaTotal(String deudaTotal);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

}