package com.synapsis.cns.codensa.model.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.synapsis.cns.codensa.model.ReporteFacturaDuplicadoLegal;
import com.synapsis.cns.codensa.model.impl.AbstractReporteFactura;

public class ReporteFacturaDuplicadoLegalImpl extends AbstractReporteFactura implements ReporteFacturaDuplicadoLegal {
	private String fechaExpedicion;
	private String fechaProceso;
	private String periodoFacturadoInicio;
	private String periodoFacturadoFin;
	private String fechaVencimientoPagoOportuno;
	private String fechaVencimientoAvisoSuspension;

	private String tipoLecturaPeriodoActual;
	private String tarifaParaFacturacionActual;
	private String factorLiquidacion;
	private String totalConsumoKwh;
	private String valorPromedioKwh;
	private String anomaliaLecturaPeriodoActual;
	private String mesTarifaParaLiquidacion;
	private String consumoPromedioUltimosMeses;

	private String maximoPermitidoInterrupciones;
	private String maximoPermitidoHoras;
	private String numeroInterrupcionesAcumuladas;
	private String numeroHorasInterrupcionesAcumuladas;
	private String periodoInterrupciones;
	private String trimestreInterrupciones;
	private String generacion;
	private String transmision;
	private String distribucion;
	private String comercializacion;
	private String perdidas;
	private String restricciones;	
	private String costoUnitario;
	private String costoUnitarioFijo;
	

	private String lecturaActual;
	private String lecturaAnterior;
	private String diferenciaEntreLecturaActualYAnterior;
	private String energiaConsumida;
	private String energiaFacturada;

	private Long nroFactura;

	private String subtCargosCons;

	private String subtOtrosCargos;

	private String subtCargosDesc;

	private String subtTotCargosEne;

	private String subtCargosSrvPort;

	private String totalFactura;

	private String totalAPagar;

	private String direccionSuministro;
	private String direccionReparto;
	private String municipioReparto;
	private String barrioReparto;

	// EVOLUCI�N DEL CONSUMO (�ltimos 6 meses)
	private Collection consumosUltimos6Meses;

	// DETALLES DE LA CUENTA
	// Colecciones de tipo DetalleCuenta
	private Collection cargos;
	private Collection otrosCargos;
	private Collection cargosDescuento;
	private Collection cargosOtrosServPort;
	private Collection cargosServicioElectrico;
	private Collection consumos;

	private String rutaReparto;
	private String manzanaLectura;
	private String manzanaReparto;
	private String proximaLectura;
	
	
	public ReporteFacturaDuplicadoLegalImpl() {
		this.setConsumos(new ArrayList());
		this.setConsumosUltimos6Meses(new ArrayList());
		this.setCargos(new ArrayList());
		this.setOtrosCargos(new ArrayList());
		this.setCargosDescuento(new ArrayList());
		this.setCargosOtrosServPort(new ArrayList());
		this.setCargosServicioElectrico(new ArrayList());
	}

	public String getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(String fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public String getFechaVencimientoAvisoSuspension() {
		return fechaVencimientoAvisoSuspension;
	}

	public void setFechaVencimientoAvisoSuspension(String fechaVencimientoAvisoSuspension) {
		this.fechaVencimientoAvisoSuspension = fechaVencimientoAvisoSuspension;
	}

	public String getFechaVencimientoPagoOportuno() {
		return fechaVencimientoPagoOportuno;
	}

	public void setFechaVencimientoPagoOportuno(String fechaVencimientoPagoOportuno) {
		this.fechaVencimientoPagoOportuno = fechaVencimientoPagoOportuno;
	}

	public String getPeriodoFacturadoFin() {
		return periodoFacturadoFin;
	}

	public void setPeriodoFacturadoFin(String periodoFacturadoFin) {
		this.periodoFacturadoFin = periodoFacturadoFin;
	}

	public String getPeriodoFacturadoInicio() {
		return periodoFacturadoInicio;
	}

	public void setPeriodoFacturadoInicio(String periodoFacturadoInicio) {
		this.periodoFacturadoInicio = periodoFacturadoInicio;
	}

	public String getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getAnomaliaLecturaPeriodoActual() {
		return anomaliaLecturaPeriodoActual;
	}

	public void setAnomaliaLecturaPeriodoActual(String anomaliaLecturaPeriodoActual) {
		this.anomaliaLecturaPeriodoActual = anomaliaLecturaPeriodoActual;
	}

	public String getConsumoPromedioUltimosMeses() {
		return consumoPromedioUltimosMeses;
	}

	public void setConsumoPromedioUltimosMeses(String consumoPromedioUltimosMeses) {
		this.consumoPromedioUltimosMeses = consumoPromedioUltimosMeses;
	}

	public String getFactorLiquidacion() {
		return factorLiquidacion;
	}

	public void setFactorLiquidacion(String factorLiquidacion) {
		this.factorLiquidacion = factorLiquidacion;
	}

	public String getMesTarifaParaLiquidacion() {
		return mesTarifaParaLiquidacion;
	}

	public void setMesTarifaParaLiquidacion(String mesTarifaParaLiquidacion) {
		this.mesTarifaParaLiquidacion = mesTarifaParaLiquidacion;
	}

	public String getTarifaParaFacturacionActual() {
		return tarifaParaFacturacionActual;
	}

	public void setTarifaParaFacturacionActual(String tarifaParaFacturacionActual) {
		this.tarifaParaFacturacionActual = tarifaParaFacturacionActual;
	}

	public String getTipoLecturaPeriodoActual() {
		return tipoLecturaPeriodoActual;
	}

	public void setTipoLecturaPeriodoActual(String tipoLecturaPeriodoActual) {
		this.tipoLecturaPeriodoActual = tipoLecturaPeriodoActual;
	}

	public String getTotalConsumoKwh() {
		return totalConsumoKwh;
	}

	public void setTotalConsumoKwh(String totalConsumoKwh) {
		this.totalConsumoKwh = totalConsumoKwh;
	}

	public String getValorPromedioKwh() {
		return valorPromedioKwh;
	}

	public void setValorPromedioKwh(String valorPromedioKwh) {
		this.valorPromedioKwh = valorPromedioKwh;
	}

	public String getComercializacion() {
		return comercializacion;
	}

	public void setComercializacion(String comercializacion) {
		this.comercializacion = comercializacion;
	}

	public String getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(String costoUnitario) {
		this.costoUnitario = costoUnitario;
	}
	
	public String getCostoUnitarioFijo() {
		return costoUnitarioFijo;
	}

	public void setCostoUnitarioFijo(String costoUnitarioFijo) {
		this.costoUnitarioFijo = costoUnitarioFijo;
	}
	
	public String getRestricciones() {
		return restricciones;
	}

	public void setRestricciones(String restricciones) {
		this.restricciones = restricciones;
	}
	

	public String getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(String distribucion) {
		this.distribucion = distribucion;
	}

	public String getGeneracion() {
		return generacion;
	}

	public void setGeneracion(String generacion) {
		this.generacion = generacion;
	}

	public String getMaximoPermitidoHoras() {
		return maximoPermitidoHoras;
	}

	public void setMaximoPermitidoHoras(String maximoPermitidoHoras) {
		this.maximoPermitidoHoras = maximoPermitidoHoras;
	}

	public String getMaximoPermitidoInterrupciones() {
		return maximoPermitidoInterrupciones;
	}

	public void setMaximoPermitidoInterrupciones(String maximoPermitidoInterrupciones) {
		this.maximoPermitidoInterrupciones = maximoPermitidoInterrupciones;
	}

	public String getNumeroHorasInterrupcionesAcumuladas() {
		return numeroHorasInterrupcionesAcumuladas;
	}

	public void setNumeroHorasInterrupcionesAcumuladas(String numeroHorasInterrupcionesAcumuladas) {
		this.numeroHorasInterrupcionesAcumuladas = numeroHorasInterrupcionesAcumuladas;
	}

	public String getNumeroInterrupcionesAcumuladas() {
		return numeroInterrupcionesAcumuladas;
	}

	public void setNumeroInterrupcionesAcumuladas(String numeroInterrupcionesAcumuladas) {
		this.numeroInterrupcionesAcumuladas = numeroInterrupcionesAcumuladas;
	}

	public String getPerdidas() {
		return perdidas;
	}

	public void setPerdidas(String perdidas) {
		this.perdidas = perdidas;
	}

	public String getPeriodoInterrupciones() {
		return periodoInterrupciones;
	}

	public void setPeriodoInterrupciones(String periodoInterrupciones) {
		this.periodoInterrupciones = periodoInterrupciones;
	}

	public String getTransmision() {
		return transmision;
	}

	public void setTransmision(String transmision) {
		this.transmision = transmision;
	}

	public String getTrimestreInterrupciones() {
		return trimestreInterrupciones;
	}

	public void setTrimestreInterrupciones(String trimestreInterrupciones) {
		this.trimestreInterrupciones = trimestreInterrupciones;
	}

	public String getDiferenciaEntreLecturaActualYAnterior() {
		return diferenciaEntreLecturaActualYAnterior;
	}

	public void setDiferenciaEntreLecturaActualYAnterior(String diferenciaEntreLecturaActualYAnterior) {
		this.diferenciaEntreLecturaActualYAnterior = diferenciaEntreLecturaActualYAnterior;
	}

	public String getEnergiaConsumida() {
		return energiaConsumida;
	}

	public void setEnergiaConsumida(String energiaConsumida) {
		this.energiaConsumida = energiaConsumida;
	}

	public String getEnergiaFacturada() {
		return energiaFacturada;
	}

	public void setEnergiaFacturada(String energiaFacturada) {
		this.energiaFacturada = energiaFacturada;
	}

	public String getLecturaActual() {
		return lecturaActual;
	}

	public void setLecturaActual(String lecturaActual) {
		this.lecturaActual = lecturaActual;
	}

	public String getLecturaAnterior() {
		return lecturaAnterior;
	}

	public void setLecturaAnterior(String lecturaAnterior) {
		this.lecturaAnterior = lecturaAnterior;
	}

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getSubtCargosCons() {
		return subtCargosCons;
	}

	public void setSubtCargosCons(String subtCargosCons) {
		this.subtCargosCons = subtCargosCons;
	}

	public String getSubtOtrosCargos() {
		return subtOtrosCargos;
	}

	public void setSubtOtrosCargos(String subtOtrosCargos) {
		this.subtOtrosCargos = subtOtrosCargos;
	}

	public String getSubtCargosDesc() {
		return subtCargosDesc;
	}

	public void setSubtCargosDesc(String subtCargosDesc) {
		this.subtCargosDesc = subtCargosDesc;
	}

	public String getSubtTotCargosEne() {
		return subtTotCargosEne;
	}

	public void setSubtTotCargosEne(String subtTotCargosEne) {
		this.subtTotCargosEne = subtTotCargosEne;
	}

	public String getSubtCargosSrvPort() {
		return subtCargosSrvPort;
	}

	public void setSubtCargosSrvPort(String subtCargosSrvPort) {
		this.subtCargosSrvPort = subtCargosSrvPort;
	}

	public String getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(String totalFactura) {
		this.totalFactura = totalFactura;
	}

	public String getTotalAPagar() {
		return totalAPagar;
	}

	public void setTotalAPagar(String totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

	public String getBarrioReparto() {
		return barrioReparto;
	}

	public void setBarrioReparto(String barrioReparto) {
		this.barrioReparto = barrioReparto;
	}

	public String getDireccionReparto() {
		return direccionReparto;
	}

	public void setDireccionReparto(String direccionReparto) {
		this.direccionReparto = direccionReparto;
	}

	public String getMunicipioReparto() {
		return municipioReparto;
	}

	public void setMunicipioReparto(String municipioReparto) {
		this.municipioReparto = municipioReparto;
	}

	public String getDireccionSuministro() {
		return direccionSuministro;
	}

	public void setDireccionSuministro(String direccionSuministro) {
		this.direccionSuministro = direccionSuministro;
	}

	public Collection getCargos() {
		return cargos;
	}

	public void setCargos(Collection cargos) {
		this.cargos = cargos;
	}

	public Collection getCargosDescuento() {
		return cargosDescuento;
	}

	public void setCargosDescuento(Collection cargosDescuento) {
		this.cargosDescuento = cargosDescuento;
	}

	public Collection getCargosServicioElectrico() {
		return cargosServicioElectrico;
	}

	public void setCargosServicioElectrico(Collection cargosServicioElectrico) {
		this.cargosServicioElectrico = cargosServicioElectrico;
	}

	public Collection getCargosOtrosServPort() {
		return cargosOtrosServPort;
	}

	public void setCargosOtrosServPort(Collection cargosOtrosServPort) {
		this.cargosOtrosServPort = cargosOtrosServPort;
	}

	public Collection getOtrosCargos() {
		return otrosCargos;
	}

	public void setOtrosCargos(Collection otrosCargos) {
		this.otrosCargos = otrosCargos;
	}

	public Collection getConsumosUltimos6Meses() {
		return consumosUltimos6Meses;
	}

	public void setConsumosUltimos6Meses(Collection consumosUltimos6Meses) {
		this.consumosUltimos6Meses = consumosUltimos6Meses;
	}

	public Collection getConsumos() {
		return consumos;
	}

	public void setConsumos(Collection consumos) {
		this.consumos = consumos;
	}

	public String getRutaReparto() {
		return rutaReparto;
	}

	public void setRutaReparto(String rutaReparto) {
		this.rutaReparto = rutaReparto;
	}

	public String getManzanaLectura() {
		return manzanaLectura;
	}

	public void setManzanaLectura(String manzanaLectura) {
		this.manzanaLectura = manzanaLectura;
	}

	public String getManzanaReparto() {
		return manzanaReparto;
	}

	public void setManzanaReparto(String manzanaReparto) {
		this.manzanaReparto = manzanaReparto;
	}

	public String getProximaLectura() {
		return proximaLectura;
	}

	public void setProximaLectura(String proximaLectura) {
		this.proximaLectura = proximaLectura;
	}

	public String getOtros() {
		// No aplica para duplicado legal
		return null;
	}

	public void setOtros(String otros) {
		//No aplica para duplicado legal	
	}
}