package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.CongAprob;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongAprobImpl extends SynergiaBusinessObjectImpl implements
		CongAprob {

	private Long nroCuenta;

	private Long nroSaldo;

	private Date fecCreacion;

	private String usuarioCreador;

	private Date fechaAprobacion;

	private String usuarioAprobador;

	private Double montoTotal;

	private Double energiaSd;

	private String tipoOperacion;

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroSaldo() {
		return nroSaldo;
	}

	public void setNroSaldo(Long nroSaldo) {
		this.nroSaldo = nroSaldo;
	}

	public Date getFecCreacion() {
		return fecCreacion;
	}

	public void setFecCreacion(Date fecCreacion) {
		this.fecCreacion = fecCreacion;
	}

	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public String getUsuarioAprobador() {
		return usuarioAprobador;
	}

	public void setUsuarioAprobador(String usuarioAprobador) {
		this.usuarioAprobador = usuarioAprobador;
	}

	public Double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Double getEnergiaSd() {
		return energiaSd;
	}

	public void setEnergiaSd(Double energiaSd) {
		this.energiaSd = energiaSd;
	}
}