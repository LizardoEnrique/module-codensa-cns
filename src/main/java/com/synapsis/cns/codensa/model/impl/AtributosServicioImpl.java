package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.AtributosServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class AtributosServicioImpl extends SynergiaBusinessObjectImpl implements AtributosServicio 
	{
	
	private Long nroCuenta;
	private String tieneConvenio;
	private String tieneSaldosDisputa;
	private String reincorporado;
	private String castigado;
	private String tieneEncargoCobranza;
	private String tieneCNR;
	private String centralRiesgo;
	private String clienteVIP;
	private String reconexionAnulada;
	private String chequeDevuelto;
	private String tieneLeyArrendamiento;
	private String tieneServicioFinanciero;
	
	private String deudaCorregida;
	private String medicionProceso;
	private String mantPreventivo;
	private Date fechaRetiro;
	private String clienteRegular;
	
	private String condensador;
	private String interHorario;
	private String blockPrueba;
	private String celdaFoto;
	
	
	public String getCastigado() {
		return castigado;
	}
	public void setCastigado(String castigado) {
		this.castigado = castigado;
	}
	public String getCentralRiesgo() {
		return centralRiesgo;
	}
	public void setCentralRiesgo(String centralRiesgo) {
		this.centralRiesgo = centralRiesgo;
	}
	public String getChequeDevuelto() {
		return chequeDevuelto;
	}
	public void setChequeDevuelto(String chequeDevuelto) {
		this.chequeDevuelto = chequeDevuelto;
	}
	public String getClienteVIP() {
		return clienteVIP;
	}
	public void setClienteVIP(String clienteVIP) {
		this.clienteVIP = clienteVIP;
	}
	public String getDeudaCorregida() {
		return deudaCorregida;
	}
	public void setDeudaCorregida(String deudaCorregida) {
		this.deudaCorregida = deudaCorregida;
	}
	public Date getFechaRetiro() {
		return fechaRetiro;
	}
	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}
	public String getMantPreventivo() {
		return mantPreventivo;
	}
	public void setMantPreventivo(String mantPreventivo) {
		this.mantPreventivo = mantPreventivo;
	}
	public String getMedicionProceso() {
		return medicionProceso;
	}
	public void setMedicionProceso(String medicionProceso) {
		this.medicionProceso = medicionProceso;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getReconexionAnulada() {
		return reconexionAnulada;
	}
	public void setReconexionAnulada(String reconexionAnulada) {
		this.reconexionAnulada = reconexionAnulada;
	}
	public String getReincorporado() {
		return reincorporado;
	}
	public void setReincorporado(String reincorporado) {
		this.reincorporado = reincorporado;
	}
	public String getTieneCNR() {
		return tieneCNR;
	}
	public void setTieneCNR(String tieneCNR) {
		this.tieneCNR = tieneCNR;
	}
	public String getTieneConvenio() {
		return tieneConvenio;
	}
	public void setTieneConvenio(String tieneConvenio) {
		this.tieneConvenio = tieneConvenio;
	}
	public String getTieneEncargoCobranza() {
		return tieneEncargoCobranza;
	}
	public void setTieneEncargoCobranza(String tieneEncargoCobranza) {
		this.tieneEncargoCobranza = tieneEncargoCobranza;
	}
	public String getTieneLeyArrendamiento() {
		return tieneLeyArrendamiento;
	}
	public void setTieneLeyArrendamiento(String tieneLeyArrendamiento) {
		this.tieneLeyArrendamiento = tieneLeyArrendamiento;
	}
	public String getTieneSaldosDisputa() {
		return tieneSaldosDisputa;
	}
	public void setTieneSaldosDisputa(String tieneSaldosDisputa) {
		this.tieneSaldosDisputa = tieneSaldosDisputa;
	}
	public String getTieneServicioFinanciero() {
		return tieneServicioFinanciero;
	}
	public void setTieneServicioFinanciero(String tieneServicioFinanciero) {
		this.tieneServicioFinanciero = tieneServicioFinanciero;
	}
	public String getClienteRegular() {
		return clienteRegular;
	}
	public void setClienteRegular(String clienteRegular) {
		this.clienteRegular = clienteRegular;
	}
	/**
	 * @return the blockPrueba
	 */
	public String getBlockPrueba() {
		return blockPrueba;
	}
	/**
	 * @param blockPrueba the blockPrueba to set
	 */
	public void setBlockPrueba(String blockPrueba) {
		this.blockPrueba = blockPrueba;
	}
	/**
	 * @return the celdaFoto
	 */
	public String getCeldaFoto() {
		return celdaFoto;
	}
	/**
	 * @param celdaFoto the celdaFoto to set
	 */
	public void setCeldaFoto(String celdaFoto) {
		this.celdaFoto = celdaFoto;
	}
	/**
	 * @return the condensador
	 */
	public String getCondensador() {
		return condensador;
	}
	/**
	 * @param condensador the condensador to set
	 */
	public void setCondensador(String condensador) {
		this.condensador = condensador;
	}
	/**
	 * @return the interHorario
	 */
	public String getInterHorario() {
		return interHorario;
	}
	/**
	 * @param interHorario the interHorario to set
	 */
	public void setInterHorario(String interHorario) {
		this.interHorario = interHorario;
	}
	

}