package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaNroPCR extends SynergiaBusinessObject {

	/**
	 * @return the nroPCR
	 */
	public String getNroPCR();

	/**
	 * @param nroPCR the nroPCR to set
	 */
	public void setNroPCR(String nroPCR);

}