/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleCargos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class DetalleCargosImpl extends SynergiaBusinessObjectImpl implements DetalleCargos {

	private static final long serialVersionUID = 1L;

	private String codigoCargo; 			
	private String descripcionCargo;			
	private Long valorCargo; 				
	private String correlativoFacturacion;	
	private Date fechaFacturacion; 	
	private String periodoFacturacion;	
	private Long nroDocumentoAssoc;
	private String tipoDocumento;
	private Long nroCuenta;
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getCorrelativoFacturacion()
	 */
	public String getCorrelativoFacturacion() {
		return correlativoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setCorrelativoFacturacion(java.lang.String)
	 */
	public void setCorrelativoFacturacion(String correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getDescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setDescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getNroDocumentoAssoc()
	 */
	public Long getNroDocumentoAssoc() {
		return nroDocumentoAssoc;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setNroDocumentoAssoc(java.lang.Long)
	 */
	public void setNroDocumentoAssoc(Long nroDocumentoAssoc) {
		this.nroDocumentoAssoc = nroDocumentoAssoc;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getPeriodoFacturacion()
	 */
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setPeriodoFacturacion(java.lang.String)
	 */
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#getValorCargo()
	 */
	public Long getValorCargo() {
		return valorCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DetalleCargos#setValorCargo(java.lang.String)
	 */
	public void setValorCargo(Long valorCargo) {
		this.valorCargo = valorCargo;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	
}