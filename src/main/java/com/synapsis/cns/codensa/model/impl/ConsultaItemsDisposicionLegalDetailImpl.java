package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaItemsDisposicionLegalDetail;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaItemsDisposicionLegalDetailImpl extends SynergiaBusinessObjectImpl implements ConsultaItemsDisposicionLegalDetail {

	private Long id;
	private Empresa empresa;
	private String cargoDispLegal;
	private Long idCargo;
	private Long codigoItem;
	private String descItem;
	private String fecPeriodo;
	private Double valorBeforeAjuste;
	private Double valorAfterAjuste;
	private Double valorAjuste;
	private Double consumoKW;
	
	
	public ConsultaItemsDisposicionLegalDetailImpl() {
		
	}
	
	public Long getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(Long codigoItem) {
		this.codigoItem = codigoItem;
	}
	public Double getConsumoKW() {
		return consumoKW;
	}
	public void setConsumoKW(Double consumoKW) {
		this.consumoKW = consumoKW;
	}
	public String getDescItem() {
		return descItem;
	}
	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getFecPeriodo() {
		return fecPeriodo;
	}
	public void setFecPeriodo(String fecPeriodo) {
		this.fecPeriodo = fecPeriodo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}
	public String getCargoDispLegal() {
		return cargoDispLegal;
	}
	public void setCargoDispLegal(String cargoDispLegal) {
		this.cargoDispLegal = cargoDispLegal;
	}
	public Double getValorAfterAjuste() {
		return valorAfterAjuste;
	}
	public void setValorAfterAjuste(Double valorAfterAjuste) {
		this.valorAfterAjuste = valorAfterAjuste;
	}
	public Double getValorAjuste() {
		return valorAjuste;
	}
	public void setValorAjuste(Double valorAjuste) {
		this.valorAjuste = valorAjuste;
	}
	public Double getValorBeforeAjuste() {
		return valorBeforeAjuste;
	}
	public void setValorBeforeAjuste(Double valorBeforeAjuste) {
		this.valorBeforeAjuste = valorBeforeAjuste;
	}

}
