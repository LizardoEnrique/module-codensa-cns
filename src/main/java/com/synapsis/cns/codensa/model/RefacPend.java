package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacPend extends SynergiaBusinessObject {

	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
	
	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public Long getNroOrdenRefa();

	public void setNroOrdenRefa(Long nroOrdenRefa);

	public Long getServAjustados();

	public void setServAjustados(Long servAjustados);

	public Double getValorTotalAjustado();

	public void setValorTotalAjustado(Double valorTotalAjustado);

	public Double getCantKwhAjust();

	public void setCantKwhAjust(Double cantKwhAjust);

	public Double getValorTotalPagar();

	public void setValorTotalPagar(Double valorTotalPagar);

	public Date getFecVencPagoAjuste();

	public void setFecVencPagoAjuste(Date fecVencPagoAjuste);

	public Date getFecHorRealizAjuste();

	public void setFecHorRealizAjuste(Date fecHorRealizAjuste);

	public String getUsuarioRealizAjuste();

	public void setUsuarioRealizAjuste(String usuarioRealizAjuste);

	public String getNombreUsuarioRealizAjuste();

	public void setNombreUsuarioRealizAjuste(String nombreUsuarioRealizAjuste);

	public String getTipoOperacion() ;

	public void setTipoOperacion(String tipoOperacion) ;

}