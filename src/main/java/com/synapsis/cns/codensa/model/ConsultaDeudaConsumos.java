package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaDeudaConsumos extends SynergiaBusinessObject {

	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda();

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda);

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza();

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza);

	/**
	 * @return the fechaFacturaUltimoConsumo
	 */
	public Date getFechaFacturaUltimoConsumo();

	/**
	 * @param fechaFacturaUltimoConsumo
	 *            the fechaFacturaUltimoConsumo to set
	 */
	public void setFechaFacturaUltimoConsumo(Date fechaFacturaUltimoConsumo);

	/**
	 * @return the promedioConsumoUltimosMeses
	 */
	public Integer getPromedioConsumoUltimosMeses();

	/**
	 * @param promedioConsumoUltimosMeses
	 *            the promedioConsumoUltimosMeses to set
	 */
	public void setPromedioConsumoUltimosMeses(Integer promedioConsumoUltimosMeses);

	/**
	 * @return the ultimoValorConsumoKw
	 */
	public Long getUltimoValorConsumoKw();

	/**
	 * @param ultimoValorConsumoKw
	 *            the ultimoValorConsumoKw to set
	 */
	public void setUltimoValorConsumoKw(Long ultimoValorConsumoKw);

	/**
	 * @return the ultimoValorConsumoPesos
	 */
	public Double getUltimoValorConsumoPesos();

	/**
	 * @param ultimoValorConsumoPesos
	 *            the ultimoValorConsumoPesos to set
	 */
	public void setUltimoValorConsumoPesos(Double ultimoValorConsumoPesos);

}