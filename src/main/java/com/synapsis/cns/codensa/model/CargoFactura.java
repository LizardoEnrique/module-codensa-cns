package com.synapsis.cns.codensa.model;

/**
 * Impresión duplicado factura - Cargos de la factura
 * 
 * @author adambrosio
 */
public interface CargoFactura extends CargoFacturaComun {
	public Long getValor();

	public void setValor(Long valor);
}
