/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * 
 *
 * @author Paola Attadio
 * @version $Id: CertificadoLegalidad.java,v 1.3 2007/02/06 22:30:58 ar29261698 Exp $
 */
public interface CertificadoLegalidad extends SynergiaBusinessObject {
	
	public Long getNroCuenta() ;
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicioElectrico();
	public void setNroServicioElectrico(Long nroServicioElectrico) ;
	public String getIdSolicitante();
	public void setIdSolicitante(String idSolicitante);
	public Date getFechaSolicitudGarantia();
	public void setFechaSolicitudGarantia(Date fechaSolicitudGarantia);	
	public Date getFechaGeneracionDocEmitido();
	public void setFechaGeneracionDocEmitido(Date fechaGeneracionDocEmitido);	
	public Long getNroDocEmitido() ;
	public void setNroDocEmitido(Long nroDocEmitido);
	public Date getFechaImpresionDocEmitido();
	public void setFechaImpresionDocEmitido(Date fechaImpresionDocEmitido);
	public String getNroOrdenGarantia() ;
	public void setNroOrdenGarantia(String nroOrdenGarantia);

}
