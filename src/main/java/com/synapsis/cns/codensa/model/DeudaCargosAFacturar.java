package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaCargosAFacturar extends SynergiaBusinessObject {
		
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getIdCobroAdic();

	public void setIdCobroAdic(Long idCobroAdic);

	public String getCodigoCargo();

	public void setCodigoCargo(String codigoCargo);

	public String getDescripcionCargo();

	public void setDescripcionCargo(String descripcionCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public String getTipoServicioCargo();

	public void setTipoServicioCargo(String tipoServicioCargo);

	public Long getNroServicioCargo();

	public void setNroServicioCargo(Long nroServicioCargo);

	public String getUsuarioIngresoCargo();

	public void setUsuarioIngresoCargo(String usuarioIngresoCargo);
	
	public Date getFechaIngresoCargo();

	public void setFechaIngresoCargo(Date fechaIngresoCargo);

	public String getNombreIngresoCargo();

	public void setNombreIngresoCargo(String nombreIngresoCargo);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

}