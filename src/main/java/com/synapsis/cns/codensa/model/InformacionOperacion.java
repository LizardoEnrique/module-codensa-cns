package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio - 29/11/2006 
 */
public interface InformacionOperacion extends SynergiaBusinessObject {

	public String getContratista() ;
	
	public void setContratista(String contratista);
	
	public Date getFechaCumplimiento();
	
	public void setFechaCumplimiento(Date fechaCumplimiento) ;
	
	public Date getFechaGeneracion() ;
	
	public void setFechaGeneracion(Date fechaGeneracion);
	
	public String getNombreUsuarioCreador() ;
	
	public void setNombreUsuarioCreador(String nombreUsuarioCreador);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta) ;
	
	public String getObservaciones() ;
	
	public void setObservaciones(String observaciones);
	
	public String getResultadoOperacion() ;
	
	public void setResultadoOperacion(String resultadoOperacion);
	
	public String getSituacionEncontrada() ;
	
	public void setSituacionEncontrada(String situacionEncontrada) ;
	
	public String getSubtipoOperacion() ;
	
	public void setSubtipoOperacion(String subtipoOperacion) ;
	
	public String getTipoOperacion() ;
	
	public void setTipoOperacion(String tipoOperacion);
	
	public String getUsuarioCreador();
	
	public void setUsuarioCreador(String usuarioCreador) ;

	public String getEstadoOperacion() ;

	public void setEstadoOperacion(String estadoOperacion);

	public String getMotivoGeneracion() ;

	public void setMotivoGeneracion(String motivoGeneracion) ;

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public Long getIdServicio();

	public void setIdServicio(Long idServicio);

	public Long getNroServicio();

	public void setNroServicio(Long nroServicio) ;
	
    public Long getNroOrden();
	public void setNroOrden(Long nroOrden);
}