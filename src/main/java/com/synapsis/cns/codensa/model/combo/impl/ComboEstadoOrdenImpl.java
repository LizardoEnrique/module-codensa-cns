package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ComboEstadoOrden;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ComboEstadoOrdenImpl extends SynergiaBusinessObjectImpl
		implements ComboEstadoOrden {
		
	private static final long serialVersionUID = 1L;

	private String codigo;
	private String descripcion;
	private String codigoOrden;	
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return this.codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the codigoOrden
	 */
	public String getCodigoOrden() {
		return this.codigoOrden;
	}
	/**
	 * @param codigoOrden the codigoOrden to set
	 */
	public void setCodigoOrden(String codigoOrden) {
		this.codigoOrden = codigoOrden;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
		
	
}