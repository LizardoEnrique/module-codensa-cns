/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaExpedienteRadicaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteRadicacionesImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteRadicaciones {
	private Long numeroExpediente;
	private String radicaciones;
	private String notificaciones;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#getNotificaciones()
	 */
	public String getNotificaciones() {
		return notificaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#setNotificaciones(java.lang.String)
	 */
	public void setNotificaciones(String notificaciones) {
		this.notificaciones = notificaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#getRadicaciones()
	 */
	public String getRadicaciones() {
		return radicaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#setRadicaciones(java.lang.String)
	 */
	public void setRadicaciones(String radicaciones) {
		this.radicaciones = radicaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#getNumeroExpediente()
	 */
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicaciones#setNumeroExpediente(java.lang.Long)
	 */
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

}
