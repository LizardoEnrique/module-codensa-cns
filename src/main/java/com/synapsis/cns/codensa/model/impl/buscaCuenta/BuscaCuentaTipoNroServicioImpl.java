package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaTipoNroServicio;

public class BuscaCuentaTipoNroServicioImpl extends BuscaCuentaImpl implements BuscaCuentaTipoNroServicio {
	private String tipoServicio;
	private Long nroServicio;
	private Long idClienteTelem;

	public Long getNroServicio() {
		return nroServicio;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public Long getIdClienteTelem() {
		return this.idClienteTelem;
	}

	public void setIdClienteTelem(Long idClienteTelem) {
		this.idClienteTelem = idClienteTelem;
	}

}
