package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DetalleDeuda;
/**
 * 
 * @author ar30557486
 *
 */
public class DetalleDeudaImpl extends SynergiaBusinessObjectImpl 
	implements DetalleDeuda {
	
	//DETALLE DEL MOVIMIENTO
	
	private Long totalDeuda;
	private Long saldoSrvElectrico;
	private Long saldoSrvOtros;
	private Long saldoInteres;
	private Long saldoSancion;
	private Long deudaConvenioSrvElectrico;
	private Long deudaConvenioSrvOtros;
	private Long deudaExigible;
	private Long valorDisputa;
	private Long valorCastigo;
	private Long valorCondonadoConvenio;
	private Long idMovimiento;
	
	public Long getIdMovimiento() {
		return idMovimiento;
	}
	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}
	public Long getDeudaConvenioSrvElectrico() {
		return deudaConvenioSrvElectrico;
	}
	public void setDeudaConvenioSrvElectrico(Long deudaConvenioSrvElectrico) {
		this.deudaConvenioSrvElectrico = deudaConvenioSrvElectrico;
	}
	public Long getDeudaConvenioSrvOtros() {
		return deudaConvenioSrvOtros;
	}
	public void setDeudaConvenioSrvOtros(Long deudaConvenioSrvOtros) {
		this.deudaConvenioSrvOtros = deudaConvenioSrvOtros;
	}
	public Long getDeudaExigible() {
		return deudaExigible;
	}
	public void setDeudaExigible(Long deudaExigible) {
		this.deudaExigible = deudaExigible;
	}
	public Long getSaldoInteres() {
		return saldoInteres;
	}
	public void setSaldoInteres(Long saldoInteres) {
		this.saldoInteres = saldoInteres;
	}
	public Long getSaldoSancion() {
		return saldoSancion;
	}
	public void setSaldoSancion(Long saldoSancion) {
		this.saldoSancion = saldoSancion;
	}
	public Long getSaldoSrvElectrico() {
		return saldoSrvElectrico;
	}
	public void setSaldoSrvElectrico(Long saldoSrvElectrico) {
		this.saldoSrvElectrico = saldoSrvElectrico;
	}
	public Long getSaldoSrvOtros() {
		return saldoSrvOtros;
	}
	public void setSaldoSrvOtros(Long saldoSrvOtros) {
		this.saldoSrvOtros = saldoSrvOtros;
	}
	public Long getTotalDeuda() {
		return totalDeuda;
	}
	public void setTotalDeuda(Long totalDeuda) {
		this.totalDeuda = totalDeuda;
	}
	public Long getValorCastigo() {
		return valorCastigo;
	}
	public void setValorCastigo(Long valorCastigo) {
		this.valorCastigo = valorCastigo;
	}
	public Long getValorCondonadoConvenio() {
		return valorCondonadoConvenio;
	}
	public void setValorCondonadoConvenio(Long valorCondonadoConvenio) {
		this.valorCondonadoConvenio = valorCondonadoConvenio;
	}
	public Long getValorDisputa() {
		return valorDisputa;
	}
	public void setValorDisputa(Long valorDisputa) {
		this.valorDisputa = valorDisputa;
	}
	
			
	
}