package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetalleCongelaciones {

	public Double getValorInicial() ;
	
	public void setValorInicial(Double valorInicial);
	
	public Double getValorFinal();
	
	public void setValorFinal(Double valorFinal);
	
	public void setNumeroSaldo(Long numeroSaldo);
	
	public Long getNumeroSaldo();
	
	/**
	 * @return the codItemAclaracion
	 */
	public String getCodItemAclaracion();

	/**
	 * @param codItemAclaracion the codItemAclaracion to set
	 */
	public void setCodItemAclaracion(String codItemAclaracion);

	/**
	 * @return the codItemsCongDes
	 */
	public String getCodItemsCongDes();

	/**
	 * @param codItemsCongDes the codItemsCongDes to set
	 */
	public void setCodItemsCongDes(String codItemsCongDes);

	/**
	 * @return the conusmoCongdesc
	 */
	public Long getConusmoCongdesc();

	/**
	 * @param conusmoCongdesc the conusmoCongdesc to set
	 */
	public void setConusmoCongdesc(Long conusmoCongdesc);

	/**
	 * @return the descItemAclaracion
	 */
	public String getDescItemAclaracion();

	/**
	 * @param descItemAclaracion the descItemAclaracion to set
	 */
	public void setDescItemAclaracion(String descItemAclaracion);

	/**
	 * @return the descripItemCongDes
	 */
	public String getDescripItemCongDes();

	/**
	 * @param descripItemCongDes the descripItemCongDes to set
	 */
	public void setDescripItemCongDes(String descripItemCongDes);

	/**
	 * @return the energiaAclaracion
	 */
	public Long getEnergiaAclaracion();

	/**
	 * @param energiaAclaracion the energiaAclaracion to set
	 */
	public void setEnergiaAclaracion(Long energiaAclaracion);

	/**
	 * @return the energiaConge
	 */
	public Long getEnergiaConge();

	/**
	 * @param energiaConge the energiaConge to set
	 */
	public void setEnergiaConge(Long energiaConge);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento();

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento);

	/**
	 * @return the perFacAclaracion
	 */
	public Date getPerFacAclaracion();

	/**
	 * @param perFacAclaracion the perFacAclaracion to set
	 */
	public void setPerFacAclaracion(Date perFacAclaracion);

	/**
	 * @return the perFacCong
	 */
	public Date getPerFacCong();

	/**
	 * @param perFacCong the perFacCong to set
	 */
	public void setPerFacCong(Date perFacCong);

	/**
	 * @return the saldoAntes
	 */
	public Double getSaldoAntes();

	/**
	 * @param saldoAntes the saldoAntes to set
	 */
	public void setSaldoAntes(Double saldoAntes);

	/**
	 * @return the saldoDespues
	 */
	public Long getSaldoDespues();

	/**
	 * @param saldoDespues the saldoDespues to set
	 */
	public void setSaldoDespues(Long saldoDespues);

	/**
	 * @return the saldoEnergia
	 */
	public Long getSaldoEnergia();

	/**
	 * @param saldoEnergia the saldoEnergia to set
	 */
	public void setSaldoEnergia(Long saldoEnergia);

	/**
	 * @return the saldoEnergiaDespuesCong
	 */
	public Long getSaldoEnergiaDespuesCong();

	/**
	 * @param saldoEnergiaDespuesCong the saldoEnergiaDespuesCong to set
	 */
	public void setSaldoEnergiaDespuesCong(Long saldoEnergiaDespuesCong);

	/**
	 * @return the saldoOtrosNegociosantes
	 */
	public Long getSaldoOtrosNegociosantes();

	/**
	 * @param saldoOtrosNegociosantes the saldoOtrosNegociosantes to set
	 */
	public void setSaldoOtrosNegociosantes(Long saldoOtrosNegociosantes);

	/**
	 * @return the saldoOtrosNegociosdespues
	 */
	public Long getSaldoOtrosNegociosdespues();

	/**
	 * @param saldoOtrosNegociosdespues the saldoOtrosNegociosdespues to set
	 */
	public void setSaldoOtrosNegociosdespues(Long saldoOtrosNegociosdespues);

	/**
	 * @return the valorAclaracion
	 */
	public Double getValorAclaracion();

	/**
	 * @param valorAclaracion the valorAclaracion to set
	 */
	public void setValorAclaracion(Double valorAclaracion);

	/**
	 * @return the valorEnergiaCongdesc
	 */
	public Long getValorEnergiaCongdesc();

	/**
	 * @param valorEnergiaCongdesc the valorEnergiaCongdesc to set
	 */
	public void setValorEnergiaCongdesc(Long valorEnergiaCongdesc);

	/**
	 * @return the valorItemsCongDes
	 */
	public Double getValorItemsCongDes();

	/**
	 * @param valorItemsCongDes the valorItemsCongDes to set
	 */
	public void setValorItemsCongDes(Double valorItemsCongDes);

	/**
	 * @return the valorOtrosCongdes
	 */
	public Long getValorOtrosCongdes();

	/**
	 * @param valorOtrosCongdes the valorOtrosCongdes to set
	 */
	public void setValorOtrosCongdes(Long valorOtrosCongdes);
	
	public Double getValorDisputado();
	
	public Long getConsumoInicial();
	public Long getConsumoFinal();
	public void setConsumoFinal(Long consumoFinal);

}