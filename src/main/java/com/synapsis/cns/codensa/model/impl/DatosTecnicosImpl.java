package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DatosTecnicos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosTecnicosImpl extends SynergiaBusinessObjectImpl implements DatosTecnicos {

	private Long nroCuenta;
	private String nivelTension;
	private String conexion;
	private String cadenaElectrica;
	private Date fechaIncorporacion;
	private String sistemaMedida;

	/**
	 * @return the cadenaElectrica
	 */
	public String getCadenaElectrica() {
		return cadenaElectrica;
	}

	/**
	 * @param cadenaElectrica the cadenaElectrica to set
	 */
	public void setCadenaElectrica(String cadenaElectrica) {
		this.cadenaElectrica = cadenaElectrica;
	}

	/**
	 * @return the conexion
	 */
	public String getConexion() {
		return conexion;
	}

	/**
	 * @param conexion the conexion to set
	 */
	public void setConexion(String conexion) {
		this.conexion = conexion;
	}

	/**
	 * @return the fechaIncorporacion
	 */
	public Date getFechaIncorporacion() {
		return fechaIncorporacion;
	}

	/**
	 * @param fechaIncorporacion the fechaIncorporacion to set
	 */
	public void setFechaIncorporacion(Date fechaIncorporacion) {
		this.fechaIncorporacion = fechaIncorporacion;
	}

	/**
	 * @return the nivelTension
	 */
	public String getNivelTension() {
		return nivelTension;
	}

	/**
	 * @param nivelTension the nivelTension to set
	 */
	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getSistemaMedida() {
		return this.sistemaMedida;
	}

	public void setSistemaMedida(String sistemaMedida) {
		this.sistemaMedida = sistemaMedida;
	}

}