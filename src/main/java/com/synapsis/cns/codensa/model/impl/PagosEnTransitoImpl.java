package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.PagosEnTransito;
/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public class PagosEnTransitoImpl extends SynergiaBusinessObjectImpl 
	implements PagosEnTransito {
	
	private Long nroCuenta;
	private Date fechaPago;	
	private Double valor;	
	private String entidadRecaudadora;	
	private Date fechaHoraAlmacenamiento;	
	private String procedencia;	
	private String estadoArchivo;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#getEntidadRecaudadora()
	 */
	public String getEntidadRecaudadora() {
		return entidadRecaudadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#getEstadoArchivo()
	 */
	public String getEstadoArchivo() {
		return estadoArchivo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#getFechaPago()
	 */
	public Date getFechaPago() {
		return fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#getProcedencia()
	 */
	public String getProcedencia() {
		return procedencia;
	}

	/**
	 * @return the valor
	 */
	public Double getValor() {
		return this.valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Double valor) {
		this.valor = valor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#setEntidadRecaudadora(java.lang.String)
	 */
	public void setEntidadRecaudadora(String entidadRecaudadora) {
		this.entidadRecaudadora = entidadRecaudadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#setEstadoArchivo(java.lang.String)
	 */
	public void setEstadoArchivo(String estadoArchivo) {
		this.estadoArchivo = estadoArchivo;
	}

	/**
	 * @return the fechaHoraAlmacenamiento
	 */
	public Date getFechaHoraAlmacenamiento() {
		return this.fechaHoraAlmacenamiento;
	}
	/**
	 * @param fechaHoraAlmacenamiento the fechaHoraAlmacenamiento to set
	 */
	public void setFechaHoraAlmacenamiento(Date fechaHoraAlmacenamiento) {
		this.fechaHoraAlmacenamiento = fechaHoraAlmacenamiento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#setFechaPago(java.lang.String)
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosPagosEnTransito#setProcedencia(java.lang.String)
	 */
	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}

	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}	
}