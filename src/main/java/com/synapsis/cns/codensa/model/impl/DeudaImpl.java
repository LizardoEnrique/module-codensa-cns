/**
 * $Id: DeudaImpl.java,v 1.7 2010/05/26 18:32:07 ar29302553 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

/**
 * @author Paola Attadio
 * @version $Revision: 1.7 $
 */
import java.util.Date;

import com.synapsis.cns.codensa.model.Deuda;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaImpl extends SynergiaBusinessObjectImpl implements
		Deuda {

	private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;
	private Long idItemDoc;
	private String codCargo;
	private String descCargo;
	private Double valorCargo;
	private Double saldoCargo;
	private String correlativoFacturacion;
	private Date fecFactura;
	private String periodoFacturacion;
	private Long nroDocumento;
	private String origenDeuda;
	private String tipoDocumento;
	private Long nroServicio;
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	

	public String getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getIdItemDoc() {
		return idItemDoc;
	}

	public void setIdItemDoc(Long idItemDoc) {
		this.idItemDoc = idItemDoc;
	}

	public String getCodCargo() {
		return codCargo;
	}

	public void setCodCargo(String codCargo) {
		this.codCargo = codCargo;
	}

	public String getDescCargo() {
		return descCargo;
	}

	public void setDescCargo(String descCargo) {
		this.descCargo = descCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public Double getSaldoCargo() {
		return saldoCargo;
	}

	public void setSaldoCargo(Double saldoCargo) {
		this.saldoCargo = saldoCargo;
	}

	public String getCorrelativoFacturacion() {
		return correlativoFacturacion;
	}

	public void setCorrelativoFacturacion(String correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}

	public Date getFecFactura() {
		return fecFactura;
	}

	public void setFecFactura(Date fecFactura) {
		this.fecFactura = fecFactura;
	}

	

	public Long getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	/**
	 * @return the origenDeuda
	 */
	public String getOrigenDeuda() {
		return origenDeuda;
	}

	/**
	 * @param origenDeuda the origenDeuda to set
	 */
	public void setOrigenDeuda(String origenDeuda) {
		this.origenDeuda = origenDeuda;
	}

	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	
}