package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaRefactCongelaciones {

	public Long getNroOrden();
	
	public void setNroOrden(Long nroOrden);
	/**
	 * @return the fecAprobacion
	 */
	public Date getFecAprobacion();

	/**
	 * @param fecAprobacion the fecAprobacion to set
	 */
	public void setFecAprobacion(Date fecAprobacion);

	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion();

	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion);

	/**
	 * @return the valor
	 */
	public String getValor();

	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor);

	/**
	 * @return the valorTotalAntes
	 */
	public String getValorTotalAntes();

	/**
	 * @param valorTotalAntes the valorTotalAntes to set
	 */
	public void setValorTotalAntes(String valorTotalAntes);

	/**
	 * @return the valorTotalDespues
	 */
	public String getValorTotalDespues();

	/**
	 * @param valorTotalDespues the valorTotalDespues to set
	 */
	public void setValorTotalDespues(String valorTotalDespues);

	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the nroDocumento
	 */
	public String getNroDocumento();
	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(String nroDocumento);

}