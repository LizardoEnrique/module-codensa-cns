package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteEvaluacionLaboratorio extends SynergiaBusinessObject {


	/**
	 * @return the fechaRealizacion
	 */
	public abstract Date getFechaRealizacion();

	/**
	 * @param fechaRealizacion the fechaRealizacion to set
	 */
	public abstract void setFechaRealizacion(Date fechaRealizacion);

	/**
	 * @return the indicadorEnergia
	 */
	public abstract String getIndicadorEnergia();

	/**
	 * @param indicadorEnergia the indicadorEnergia to set
	 */
	public abstract void setIndicadorEnergia(String indicadorEnergia);

	/**
	 * @return the nombrePresencio
	 */
	public abstract String getNombrePresencio();

	/**
	 * @param nombrePresencio the nombrePresencio to set
	 */
	public abstract void setNombrePresencio(String nombrePresencio);
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente();
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente);

}