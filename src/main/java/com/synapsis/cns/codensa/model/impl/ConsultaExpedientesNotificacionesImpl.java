/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedientesNotificaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedientesNotificacionesImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedientesNotificaciones {
	private String tipoNotificacion; 	
	private Date fechaNotificacion;	
	private String nombreNotifico;	
	private String tipoDocumento;	
	private String numeroDocumento;	
	private String calidadActua;	
	private Date fechaFijacionEdicto;	
	private Date fechaDesfijacionEdicto;
	private Long numeroExpediente;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getCalidadActua()
	 */
	public String getCalidadActua() {
		return calidadActua;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setCalidadActua(java.lang.String)
	 */
	public void setCalidadActua(String calidadActua) {
		this.calidadActua = calidadActua;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getFechaDesfijacionEdicto()
	 */
	public Date getFechaDesfijacionEdicto() {
		return fechaDesfijacionEdicto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setFechaDesfijacionEdicto(java.util.Date)
	 */
	public void setFechaDesfijacionEdicto(Date fechaDesfijacionEdicto) {
		this.fechaDesfijacionEdicto = fechaDesfijacionEdicto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getFechaFijacionEdicto()
	 */
	public Date getFechaFijacionEdicto() {
		return fechaFijacionEdicto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setFechaFijacionEdicto(java.util.Date)
	 */
	public void setFechaFijacionEdicto(Date fechaFijacionEdicto) {
		this.fechaFijacionEdicto = fechaFijacionEdicto;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getFechaNotificacion()
	 */
	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setFechaNotificacion(java.util.Date)
	 */
	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getNombreNotifico()
	 */
	public String getNombreNotifico() {
		return nombreNotifico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setNombreNotifico(java.lang.String)
	 */
	public void setNombreNotifico(String nombreNotifico) {
		this.nombreNotifico = nombreNotifico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getNumeroDocumento()
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setNumeroDocumento(java.lang.String)
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setTipoDocumento(java.lang.String)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#getTipoNotificacion()
	 */
	public String getTipoNotificacion() {
		return tipoNotificacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedientesNotificaciones#setTipoNotificacion(java.lang.String)
	 */
	public void setTipoNotificacion(String tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}


	
}
