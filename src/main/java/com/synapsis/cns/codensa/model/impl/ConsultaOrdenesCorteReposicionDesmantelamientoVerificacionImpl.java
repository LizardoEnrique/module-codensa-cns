package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOrdenesCorteReposicionDesmantelamientoVerificacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Consulta de Órdenes de Corte, Reposición, Desmantelamiento y Verificación<br>
 * (Vista XCNS_EDE_COR_ORDEN)
 * 
 * @author egrande
 * 
 */
public class ConsultaOrdenesCorteReposicionDesmantelamientoVerificacionImpl
		extends SynergiaBusinessObjectImpl implements
		ConsultaOrdenesCorteReposicionDesmantelamientoVerificacion {
	private Long nroCuenta;

	private Long nroServicio;

	private String nroOrden;

	private String tipo;

	private String subtipo;

	private Date fechaVigencia;

	private Date fechaEmision;

	private String usuarioCreador;

	private String sitEncontrada1;

	private String sitEncontrada2;

	private String accRealizada;

	private Date fechaEjecucion;

	private Date fechaReal;

	private String contratista;

	private String ejecutor;

	private String motivo;

	private String cargo;

	private String estadoCargo;

	private Double valorCargo;

	private String estado;

	private String esMasiva;

	private String observacion;

	private Long lecturas;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public String getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubtipo() {
		return subtipo;
	}

	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public String getSitEncontrada1() {
		return sitEncontrada1;
	}

	public void setSitEncontrada1(String sitEncontrada1) {
		this.sitEncontrada1 = sitEncontrada1;
	}

	public String getSitEncontrada2() {
		return sitEncontrada2;
	}

	public void setSitEncontrada2(String sitEncontrada2) {
		this.sitEncontrada2 = sitEncontrada2;
	}

	public String getAccRealizada() {
		return accRealizada;
	}

	public void setAccRealizada(String accRealizada) {
		this.accRealizada = accRealizada;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public String getContratista() {
		return contratista;
	}

	public void setContratista(String contratista) {
		this.contratista = contratista;
	}

	public String getEjecutor() {
		return ejecutor;
	}

	public void setEjecutor(String ejecutor) {
		this.ejecutor = ejecutor;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getEstadoCargo() {
		return estadoCargo;
	}

	public void setEstadoCargo(String estadoCargo) {
		this.estadoCargo = estadoCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEsMasiva() {
		return esMasiva;
	}

	public void setEsMasiva(String esMasiva) {
		this.esMasiva = esMasiva;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getLecturas() {
		return lecturas;
	}

	public void setLecturas(Long lecturas) {
		this.lecturas = lecturas;
	}
}
