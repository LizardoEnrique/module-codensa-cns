package com.synapsis.cns.codensa.model;

public interface ConsultaCuotaManejo {
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getNroServicio();

	public void setNroServicio(Long nroServicio);

	public String getTipoSrv();

	public void setTipoSrv(String tipoSrv);

	public String getNombreTit();

	public void setNombreTit(String nombreTit);

	public String getTipoDocTitular();

	public void setTipoDocTitular(String tipoDocTitular);

	public String getNroDocTitular();

	public void setNroDocTitular(String nroDocTitular);

	public Double getValorSrv();

	public void setValorSrv(Double valorSrv);

	public Double getValorFacImpago();

	public void setValorFacImpago(Double valorFacImpago);

}
