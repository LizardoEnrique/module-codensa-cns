package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaNumeroTransformador extends SynergiaBusinessObject {

	/**
	 * @return the nroTransformador
	 */
	public String getNroTransformador();

	/**
	 * @param nroTransformador the nroTransformador to set
	 */
	public void setNroTransformador(String nroTransformador);

}