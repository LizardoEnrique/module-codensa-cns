/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public interface Restricciones extends SynergiaBusinessObject {
	
	public Date getFechaCreacion();
	public void setFechaCreacion(Date fechaCreacion);
	public Date getFechaDesactivacion() ;
	public void setFechaDesactivacion(Date fechaDesactivacion);
	public Date getFechaFinal();
	public void setFechaFinal(Date fechaFinal);
	public Date getFechaInicial();
	public void setFechaInicial(Date fechaInicial) ;
	public String getIdSolicitante();
	public void setIdSolicitante(String idSolicitante);
	public String getMotivo();
	public void setMotivo(String motivo);
	public String getNombreSolicitante() ;
	public void setNombreSolicitante(String nombreSolicitante);
	public String getNombreUsuarioDesactiva() ;
	public void setNombreUsuarioDesactiva(String nombreUsuarioDesactiva);
	public String getNombreUsuarioGenerador() ;
	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador);
	public Long getNroCuenta() ;
	public void setNroCuenta(Long nroCuenta);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio) ;
	public String getObservacionesCreacion() ;
	public void setObservacionesCreacion(String observacionesCreacion);
	public String getObservacionesDesactivacion() ;
	public void setObservacionesDesactivacion(String observacionesDesactivacion);
	public String getObservacionesIngreso() ;
	public void setObservacionesIngreso(String observacionesIngreso) ;
	public String getSolicitante() ;
	public void setSolicitante(String solicitante);
	public String getTipoOperacionRestringida() ;
	public void setTipoOperacionRestringida(String tipoOperacionRestringida);
	public String getTipoServicioAfectado() ;
	public void setTipoServicioAfectado(String tipoServicioAfectado);
	public String getUsuarioDesactiva();
	public void setUsuarioDesactiva(String usuarioDesactiva) ;
	public String getUsuarioGenerador() ;
	public void setUsuarioGenerador(String usuarioGenerador);
	public String getTipoServicio();
	public void setTipoServicio(String tipoServicio);
}
