package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface EventosSello extends SynergiaBusinessObject {

	public String getColorSello();
	
	public void setColorSello(String colorSello);
	
	public String getEstadoInstalacionSello();
	
	public void setEstadoInstalacionSello(String estadoInstalacionSello);
	
	public String getEstadoSello();
	
	public void setEstadoSello(String estadoSello);
	
	public Date getFechaInstalacion();
	
	public void setFechaInstalacion(Date fechaInstalacion);
	
	public Date getFechaRetiro();
	
	public void setFechaRetiro(Date fechaRetiro);
	
	public Long getIdComponente();
	
	public void setIdComponente(Long idComponente);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public String getLocalizacionSello();
	
	public void setLocalizacionSello(String localizacionSello);
	
	public String getMarcaMedidor();
	
	public void setMarcaMedidor(String marcaMedidor);
	
	public String getModeloMedidor();
	
	public void setModeloMedidor(String modeloMedidor);
	
	public String getNombreArea();
	
	public void setNombreArea(String nombreArea);
	
	public String getNombreContratistaInstalador();
	
	public void setNombreContratistaInstalador(String nombreContratistaInstalador);
	
	public String getNombreContratistaRetiro();
	
	public void setNombreContratistaRetiro(String nombreContratistaRetiro);
	
	public String getNombreInstalador();
	
	public void setNombreInstalador(String nombreInstalador);
	
	public String getNombreRetiro();
	
	public void setNombreRetiro(String nombreRetiro);
	
	public Long getNroSello();
	
	public void setNroSello(Long nroSello);
	
	public String getNumeroMedidor();
	
	public void setNumeroMedidor(String numeroMedidor);
	
	public String getTipoSello();
	
	public void setTipoSello(String tipoSello);
	
	public String getUsuario();
	
	public void setUsuario(String usuario);

}