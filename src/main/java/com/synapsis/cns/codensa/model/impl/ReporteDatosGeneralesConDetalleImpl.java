package com.synapsis.cns.codensa.model.impl;
import java.util.Collection;
import java.util.Vector;

import com.synapsis.cns.codensa.model.ReporteDatosGeneralesConDetalle;


/**
 * @author m3.MarioRoss - 18/12/2006
 */
public class ReporteDatosGeneralesConDetalleImpl extends ReporteDatosGeneralesImpl implements ReporteDatosGeneralesConDetalle {
	private Collection detalles = new Vector();
	
	public Collection getDetalles() {
		return detalles;
	}
	public void setDetalles(Collection detalles) {
		this.detalles = detalles;
	}
}
