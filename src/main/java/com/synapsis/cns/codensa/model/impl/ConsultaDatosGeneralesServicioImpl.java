/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDatosGeneralesServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDatosGeneralesServicioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDatosGeneralesServicio {
	   private Double valInteresMora;      
	   private Long nroServicio;           
	   private String lineaNegocio;        
	   private Date fechaCompra;           
	   private String socioNegocio;        
	   private String sucursalSocio;       
	   private String tipoProducto;        
	   private String marcaProducto;       
	   private Double valorServicio;       
	   private Long nroCuotas;             
	   private Double tasaIntOrigen;       
	   private Double cantCuotFact;        
	   private Double cantCuotRestantes;   
	   private Double tasaIntActual;       
	   private Double saldoCapital;        
	   private Double saldoIntCorriente;   
	   private Double saldoIntMora;        
	   private Double periodosAtrasados;   
	   private Double diasEnMora;          
	   private Double valCapitalMora;
	/**
	 * @return the cantCuotFact
	 */
	public Double getCantCuotFact() {
		return cantCuotFact;
	}
	/**
	 * @param cantCuotFact the cantCuotFact to set
	 */
	public void setCantCuotFact(Double cantCuotFact) {
		this.cantCuotFact = cantCuotFact;
	}
	/**
	 * @return the cantCuotRestantes
	 */
	public Double getCantCuotRestantes() {
		return cantCuotRestantes;
	}
	/**
	 * @param cantCuotRestantes the cantCuotRestantes to set
	 */
	public void setCantCuotRestantes(Double cantCuotRestantes) {
		this.cantCuotRestantes = cantCuotRestantes;
	}
	/**
	 * @return the diasEnMora
	 */
	public Double getDiasEnMora() {
		return diasEnMora;
	}
	/**
	 * @param diasEnMora the diasEnMora to set
	 */
	public void setDiasEnMora(Double diasEnMora) {
		this.diasEnMora = diasEnMora;
	}
	/**
	 * @return the fechaCompra
	 */
	public Date getFechaCompra() {
		return fechaCompra;
	}
	/**
	 * @param fechaCompra the fechaCompra to set
	 */
	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	/**
	 * @return the marcaProducto
	 */
	public String getMarcaProducto() {
		return marcaProducto;
	}
	/**
	 * @param marcaProducto the marcaProducto to set
	 */
	public void setMarcaProducto(String marcaProducto) {
		this.marcaProducto = marcaProducto;
	}
	/**
	 * @return the nroCuotas
	 */
	public Long getNroCuotas() {
		return nroCuotas;
	}
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the periodosAtrasados
	 */
	public Double getPeriodosAtrasados() {
		return periodosAtrasados;
	}
	/**
	 * @param periodosAtrasados the periodosAtrasados to set
	 */
	public void setPeriodosAtrasados(Double periodosAtrasados) {
		this.periodosAtrasados = periodosAtrasados;
	}
	/**
	 * @return the saldoCapital
	 */
	public Double getSaldoCapital() {
		return saldoCapital;
	}
	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}
	/**
	 * @return the saldoIntCorriente
	 */
	public Double getSaldoIntCorriente() {
		return saldoIntCorriente;
	}
	/**
	 * @param saldoIntCorriente the saldoIntCorriente to set
	 */
	public void setSaldoIntCorriente(Double saldoIntCorriente) {
		this.saldoIntCorriente = saldoIntCorriente;
	}
	/**
	 * @return the saldoIntMora
	 */
	public Double getSaldoIntMora() {
		return saldoIntMora;
	}
	/**
	 * @param saldoIntMora the saldoIntMora to set
	 */
	public void setSaldoIntMora(Double saldoIntMora) {
		this.saldoIntMora = saldoIntMora;
	}
	/**
	 * @return the socioNegocio
	 */
	public String getSocioNegocio() {
		return socioNegocio;
	}
	/**
	 * @param socioNegocio the socioNegocio to set
	 */
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}
	/**
	 * @return the sucursalSocio
	 */
	public String getSucursalSocio() {
		return sucursalSocio;
	}
	/**
	 * @param sucursalSocio the sucursalSocio to set
	 */
	public void setSucursalSocio(String sucursalSocio) {
		this.sucursalSocio = sucursalSocio;
	}
	/**
	 * @return the tasaIntActual
	 */
	public Double getTasaIntActual() {
		return tasaIntActual;
	}
	/**
	 * @param tasaIntActual the tasaIntActual to set
	 */
	public void setTasaIntActual(Double tasaIntActual) {
		this.tasaIntActual = tasaIntActual;
	}
	/**
	 * @return the tasaIntOrigen
	 */
	public Double getTasaIntOrigen() {
		return tasaIntOrigen;
	}
	/**
	 * @param tasaIntOrigen the tasaIntOrigen to set
	 */
	public void setTasaIntOrigen(Double tasaIntOrigen) {
		this.tasaIntOrigen = tasaIntOrigen;
	}
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}
	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	/**
	 * @return the valCapitalMora
	 */
	public Double getValCapitalMora() {
		return valCapitalMora;
	}
	/**
	 * @param valCapitalMora the valCapitalMora to set
	 */
	public void setValCapitalMora(Double valCapitalMora) {
		this.valCapitalMora = valCapitalMora;
	}
	/**
	 * @return the valInteresMora
	 */
	public Double getValInteresMora() {
		return valInteresMora;
	}
	/**
	 * @param valInteresMora the valInteresMora to set
	 */
	public void setValInteresMora(Double valInteresMora) {
		this.valInteresMora = valInteresMora;
	}
	/**
	 * @return the valorServicio
	 */
	public Double getValorServicio() {
		return valorServicio;
	}
	/**
	 * @param valorServicio the valorServicio to set
	 */
	public void setValorServicio(Double valorServicio) {
		this.valorServicio = valorServicio;
	}      

}
