package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaOtrosNegocios extends SynergiaBusinessObject {
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getId2();

	public void setId2(Long id2);

	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public Long getIdItemDoc();

	public void setIdItemDoc(Long idItemDoc);

	public String getCodCargo();

	public void setCodCargo(String codCargo);

	public String getDesCargo();

	public void setDesCargo(String desCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public Double getSaldoCargo();

	public void setSaldoCargo(Double saldoCargo);

	public String getCorrelativoFacturacion();

	public void setCorrelativoFacturacion(String correlativoFacturacion);

	public Date getFecFactura();

	public void setFecFactura(Date fecFactura);

	public String getPeriodoFacturacion();

	public void setPeriodoFacturacion(String periodoFacturacion);

	public String getTipoDocumento();

	public void setTipoDocumento(String tipoDocumento);

	public Long getNroDocumento();

	public void setNroDocumento(Long nroDocumento);

	public String getOrigenDeuda();

	public void setOrigenDeuda(String origenDeuda);
}
