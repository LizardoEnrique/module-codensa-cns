package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaConvenioTraslado extends SynergiaBusinessObject {

	/**
	 * @return the fechaTraslado
	 */
	public Date getFechaTraslado();

	/**
	 * @param fechaTraslado
	 *            the fechaTraslado to set
	 */
	public void setFechaTraslado(Date fechaTraslado);

	/**
	 * @return the nombreUsuarioTraslado
	 */
	public String getNombreUsuarioTraslado();

	/**
	 * @param nombreUsuarioTraslado
	 *            the nombreUsuarioTraslado to set
	 */
	public void setNombreUsuarioTraslado(String nombreUsuarioTraslado);

	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones
	 *            the observaciones to set
	 */
	public void setObservaciones(String observaciones);

	/**
	 * @return the tipoConvenioAnterior
	 */
	public String getTipoConvenioAnterior();

	/**
	 * @param tipoConvenioAnterior
	 *            the tipoConvenioAnterior to set
	 */
	public void setTipoConvenioAnterior(String tipoConvenioAnterior);

	/**
	 * @return the usuarioTraslado
	 */
	public String getUsuarioTraslado();

	/**
	 * @param usuarioTraslado
	 *            the usuarioTraslado to set
	 */
	public void setUsuarioTraslado(String usuarioTraslado);

	public Long getNroCuentaAnterior();

	public void setNroCuentaAnterior(Long nroCuentaAnterior);

	public Double getSaldoTraslado();

	public void setSaldoTraslado(Double saldoTraslado);

	public Long getNroConvenio();

	public void setNroConvenio(Long nroConvenio);

	public String getLinea();

	public void setLinea(String linea);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

}