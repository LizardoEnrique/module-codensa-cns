package com.synapsis.cns.codensa.model;

public interface ConsultaCompPagoDetalleCargos {
	public Long getNroCuenta();
	/**
	 * @return the codCargo
	 */
	public String getCodCargo();

	/**
	 * @param codCargo the codCargo to set
	 */
	public void setCodCargo(String codCargo);

	/**
	 * @return the descripciónCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripciónCargo the descripciónCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the periodoCargo
	 */
	public String getPeriodoCargo();

	/**
	 * @param periodoCargo the periodoCargo to set
	 */
	public void setPeriodoCargo(String periodoCargo);

	/**
	 * @return the tipoCargo
	 */
	public String getTipoCargo();

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public void setTipoCargo(String tipoCargo);

	/**
	 * @return the valorCargo
	 */
	public double getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(double valorCargo);
	
	public long getIdDocumento(); 
	/**
	 * @param idDocumento the idDocumento to set
	 */
	public void setIdDocumento(long idDocumento);
	
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);
	
}