package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCastigoCartera;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public class ConsultaCastigoCarteraImpl extends SynergiaBusinessObjectImpl
		implements ConsultaCastigoCartera {

	/**
	 * Comment for <code>fechaGeneracionDeuda</code>
	 */
	private Date fechaGeneracionDeuda;

	/**
	 * Comment for <code>usuarioNombreIngresoDeuda</code>
	 */
	private String usuarioNombreIngresoDeuda;

	/**
	 * Comment for <code>valorTotalCastigado</code>
	 */
	private Double valorTotalCastigado;

	/**
	 * @return the fechaGeneracionDeuda
	 */
	public Date getFechaGeneracionDeuda() {
		return this.fechaGeneracionDeuda;
	}

	/**
	 * @param fechaGeneracionDeuda
	 *            the fechaGeneracionDeuda to set
	 */
	public void setFechaGeneracionDeuda(Date fechaGeneracionDeuda) {
		this.fechaGeneracionDeuda = fechaGeneracionDeuda;
	}

	/**
	 * @return the usuarioNombreIngresoDeuda
	 */
	public String getUsuarioNombreIngresoDeuda() {
		return this.usuarioNombreIngresoDeuda;
	}

	/**
	 * @param usuarioNombreIngresoDeuda
	 *            the usuarioNombreIngresoDeuda to set
	 */
	public void setUsuarioNombreIngresoDeuda(String usuarioNombreIngresoDeuda) {
		this.usuarioNombreIngresoDeuda = usuarioNombreIngresoDeuda;
	}

	/**
	 * @return the valorTotalCastigado
	 */
	public Double getValorTotalCastigado() {
		return this.valorTotalCastigado;
	}

	/**
	 * @param valorTotalCastigado
	 *            the valorTotalCastigado to set
	 */
	public void setValorTotalCastigado(Double valorTotalCastigado) {
		this.valorTotalCastigado = valorTotalCastigado;
	}

}