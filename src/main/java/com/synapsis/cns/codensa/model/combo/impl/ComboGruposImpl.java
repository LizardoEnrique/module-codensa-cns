/**
 * 
 */
package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.Combo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ComboGruposImpl extends SynergiaBusinessObjectImpl implements Combo {

	private static final long serialVersionUID = 1L;

	private String codigo;

	private String descripcion;
	
	private Long idCiclo;
	
	
	
	/**
	 * @return the idCiclo
	 */
	public Long getIdCiclo() {
		return idCiclo;
	}

	/**
	 * @param idCiclo the idCiclo to set
	 */
	public void setIdCiclo(Long idCiclo) {
		this.idCiclo = idCiclo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#getCodigo()
	 */
	public String getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#setCodigo(java.lang.String)
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.combo.impl.ComboDetalle#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
