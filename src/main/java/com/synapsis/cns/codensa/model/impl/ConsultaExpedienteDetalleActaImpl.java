/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteDetalleActa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteDetalleActaImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteDetalleActa {
	
	private Long numeroExpediente;
	private Long numeroActa;
	private Date fechaActa;	
	private Double valorInicialActa;	
	private Double valorFinalActa;	
	private String tipoExpediente;	
	private String indicadorConvenio;	
	private Long numeroCuotas;	
	private String indicadorCondonacion;	
	private Double valorCondonacion;	
	private String estadoActa;	
	private String nombreSuscribioActaEmpresa;	
	private String nombreSuscribioActaCliente;	
	private String tipoIdentificacionCliente;	
	private String numeroIdentificacionCliente;	
	private String calidadActua;
	
	
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getCalidadActua()
	 */
	public String getCalidadActua() {
		return calidadActua;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setCalidadActua(java.lang.String)
	 */
	public void setCalidadActua(String calidadActua) {
		this.calidadActua = calidadActua;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getEstadoActa()
	 */
	public String getEstadoActa() {
		return estadoActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setEstadoActa(java.lang.String)
	 */
	public void setEstadoActa(String estadoActa) {
		this.estadoActa = estadoActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getFechaActa()
	 */
	public Date getFechaActa() {
		return fechaActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setFechaActa(java.util.Date)
	 */
	public void setFechaActa(Date fechaActa) {
		this.fechaActa = fechaActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getIndicadorCondonacion()
	 */
	public String getIndicadorCondonacion() {
		return indicadorCondonacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setIndicadorCondonacion(java.lang.String)
	 */
	public void setIndicadorCondonacion(String indicadorCondonacion) {
		this.indicadorCondonacion = indicadorCondonacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getIndicadorConvenio()
	 */
	public String getIndicadorConvenio() {
		return indicadorConvenio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setIndicadorConvenio(java.lang.String)
	 */
	public void setIndicadorConvenio(String indicadorConvenio) {
		this.indicadorConvenio = indicadorConvenio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getNombreSuscribioActaCliente()
	 */
	public String getNombreSuscribioActaCliente() {
		return nombreSuscribioActaCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setNombreSuscribioActaCliente(java.lang.String)
	 */
	public void setNombreSuscribioActaCliente(String nombreSuscribioActaCliente) {
		this.nombreSuscribioActaCliente = nombreSuscribioActaCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getNombreSuscribioActaEmpresa()
	 */
	public String getNombreSuscribioActaEmpresa() {
		return nombreSuscribioActaEmpresa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setNombreSuscribioActaEmpresa(java.lang.String)
	 */
	public void setNombreSuscribioActaEmpresa(String nombreSuscribioActaEmpresa) {
		this.nombreSuscribioActaEmpresa = nombreSuscribioActaEmpresa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getNumeroActa()
	 */
	public Long getNumeroActa() {
		return numeroActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setNumeroActa(java.lang.Long)
	 */
	public void setNumeroActa(Long numeroActa) {
		this.numeroActa = numeroActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getNumeroCuotas()
	 */
	public Long getNumeroCuotas() {
		return numeroCuotas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setNumeroCuotas(java.lang.Long)
	 */
	public void setNumeroCuotas(Long numeroCuotas) {
		this.numeroCuotas = numeroCuotas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getNumeroIdentificacionCliente()
	 */
	public String getNumeroIdentificacionCliente() {
		return numeroIdentificacionCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setNumeroIdentificacionCliente(java.lang.String)
	 */
	public void setNumeroIdentificacionCliente(String numeroIdentificacionCliente) {
		this.numeroIdentificacionCliente = numeroIdentificacionCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getTipoExpediente()
	 */
	public String getTipoExpediente() {
		return tipoExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setTipoExpediente(java.lang.String)
	 */
	public void setTipoExpediente(String tipoExpediente) {
		this.tipoExpediente = tipoExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getTipoIdentificacionCliente()
	 */
	public String getTipoIdentificacionCliente() {
		return tipoIdentificacionCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setTipoIdentificacionCliente(java.lang.String)
	 */
	public void setTipoIdentificacionCliente(String tipoIdentificacionCliente) {
		this.tipoIdentificacionCliente = tipoIdentificacionCliente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getValorCondonacion()
	 */
	public Double getValorCondonacion() {
		return valorCondonacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setValorCondonacion(java.lang.Double)
	 */
	public void setValorCondonacion(Double valorCondonacion) {
		this.valorCondonacion = valorCondonacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getValorFinalActa()
	 */
	public Double getValorFinalActa() {
		return valorFinalActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setValorFinalActa(java.lang.Double)
	 */
	public void setValorFinalActa(Double valorFinalActa) {
		this.valorFinalActa = valorFinalActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#getValorInicialActa()
	 */
	public Double getValorInicialActa() {
		return valorInicialActa;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteDetalleActa#setValorInicialActa(java.lang.Double)
	 */
	public void setValorInicialActa(Double valorInicialActa) {
		this.valorInicialActa = valorInicialActa;
	}
}
