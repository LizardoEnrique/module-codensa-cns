package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Interfaz del item de Seguro Codensa, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ItemClasificados extends ItemECO{
	
	
}
