/**
 *
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaLecturaConsumoMedidor extends SynergiaBusinessObject {

	public Long getIdCuenta() ;
	public Long getNroCuenta() ;
	public String getAnioFabricacionMedidor() ;
	public String getEstadoMedidor() ;
	public String getFactorMedidor() ;
	public String getMarcaMedidor() ;
	public String getModeloMedidor();
	public String getNivelTension() ;
	public String getNumeroMedidor();
	public String getTipoMedida() ;
	public String getTipoMedidor();
	public Long getCargaContratada() ;
	public Long getConstanteDemanda() ;
	public Long getConstanteEnergia() ;
	public Long getConstanteReactivaHoraPunta();
	public Long getNumeroDecimalesMedidor() ;
	public Long getNumeroEnteroMedidor() ;
	public Long getNumeroEnterosDemanda() ;

}
