package com.synapsis.cns.codensa.model.impl;



import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DetalleAnomalia;

public class DetalleAnomaliaImpl extends SynergiaBusinessObjectImpl 
	implements DetalleAnomalia {
    // Fields    
	
	private String codigoAnomalia;
	private String descripcionAnomalia;

    private Long idComponente;

	public String getCodigoAnomalia() {
		return codigoAnomalia;
	}

	public void setCodigoAnomalia(String codigoAnomalia) {
		this.codigoAnomalia = codigoAnomalia;
	}

	public String getDescripcionAnomalia() {
		return descripcionAnomalia;
	}

	public void setDescripcionAnomalia(String descripcionAnomalia) {
		this.descripcionAnomalia = descripcionAnomalia;
	}

	public Long getIdComponente() {
		return idComponente;
	}

	public void setIdComponente(Long idComponente) {
		this.idComponente = idComponente;
	}
   
}
