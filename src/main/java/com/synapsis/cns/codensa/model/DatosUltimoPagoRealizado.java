package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosUltimoPagoRealizado extends SynergiaBusinessObject{

	/**
	 * @return the codCajero
	 */
	public abstract String getCodCajero();

	/**
	 * @param codCajero the codCajero to set
	 */
	public abstract void setCodCajero(String codCajero);

	/**
	 * @return the fechaPago
	 */
	public abstract Date getFechaPago();

	/**
	 * @param fechaPago the fechaPago to set
	 */
	public abstract void setFechaPago(Date fechaPago);

	/**
	 * @return the montoTotal
	 */
	public abstract String getMontoTotal();

	/**
	 * @param montoTotal the montoTotal to set
	 */
	public abstract void setMontoTotal(String montoTotal);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the tipoCargo
	 */
	public abstract String getTipoCargo();

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public abstract void setTipoCargo(String tipoCargo);
	
	/**
	 * @return the oficina
	 */
	public String getOficina();
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(String oficina);

}