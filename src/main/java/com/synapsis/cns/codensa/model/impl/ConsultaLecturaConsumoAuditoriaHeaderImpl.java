/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaLecturaConsumoAuditoriaHeader;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 006
 */
public class ConsultaLecturaConsumoAuditoriaHeaderImpl extends
		SynergiaBusinessObjectImpl implements ConsultaLecturaConsumoAuditoriaHeader{


	private Long idCuenta;
	private Long nroCuenta;
	private Long idServicio;
	private Long nroServicio;
	private String sucursal;
	private String ciclo;
	private String zona;
	
	public String getCiclo() {
		return ciclo;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public String getSucursal() {
		return sucursal;
	}
	public String getZona() {
		return zona;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	
	
	
}
