package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ResumenDeuda;

public class ResumenDeudaImpl extends SynergiaBusinessObjectImpl 
	implements ResumenDeuda{


    private Long nroCuenta;
	private String deudaTotalActual;
    private String deudaPagar;
    private String saldoTotalConvenioVigente;
    private String saldoDeudaEnergiaActual;
    private String saldoDeudaServicioActual;
    private String valorTotalUltimaFactura;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}    

   public String getDeudaPagar() {
		return deudaPagar;
	}

	public void setDeudaPagar(String deudaPagar) {
		this.deudaPagar = deudaPagar;
	}

	public String getDeudaTotalActual() {
		return deudaTotalActual;
	}

	public void setDeudaTotalActual(String deudaTotalActual) {
		this.deudaTotalActual = deudaTotalActual;
	}

	public String getSaldoDeudaEnergiaActual() {
		return saldoDeudaEnergiaActual;
	}

	public void setSaldoDeudaEnergiaActual(String saldoDeudaEnergiaActual) {
		this.saldoDeudaEnergiaActual = saldoDeudaEnergiaActual;
	}

	public String getSaldoDeudaServicioActual() {
		return saldoDeudaServicioActual;
	}

	public void setSaldoDeudaServicioActual(String saldoDeudaServicioActual) {
		this.saldoDeudaServicioActual = saldoDeudaServicioActual;
	}

	public String getSaldoTotalConvenioVigente() {
		return saldoTotalConvenioVigente;
	}

	public void setSaldoTotalConvenioVigente(String saldoTotalConvenioVigente) {
		this.saldoTotalConvenioVigente = saldoTotalConvenioVigente;
	}

	public String getValorTotalUltimaFactura() {
		return valorTotalUltimaFactura;
	}

	public void setValorTotalUltimaFactura(String valorTotalUltimaFactura) {
		this.valorTotalUltimaFactura = valorTotalUltimaFactura;
	}

}
