package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.EstadoServicio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class EstadoServicioImpl extends SynergiaBusinessObjectImpl implements EstadoServicio {
	private String convenioRestringido;
	private String cobranzaExterna;
	private String accionLegal;    
	private Long nroCuenta;  
	private Long nroServicio;  
	private String suministro;
	private String corteRestringido;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getAccionLegal()
	 */
	public String getAccionLegal() {
		return accionLegal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setAccionLegal(java.lang.String)
	 */
	public void setAccionLegal(String accionLegal) {
		this.accionLegal = accionLegal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getCobranzaExterna()
	 */
	public String getCobranzaExterna() {
		return cobranzaExterna;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setCobranzaExterna(java.lang.String)
	 */
	public void setCobranzaExterna(String cobranzaExterna) {
		this.cobranzaExterna = cobranzaExterna;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getConvenioRestringido()
	 */
	public String getConvenioRestringido() {
		return convenioRestringido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setConvenioRestringido(java.lang.String)
	 */
	public void setConvenioRestringido(String convenioRestringido) {
		this.convenioRestringido = convenioRestringido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getCorteRestringido()
	 */
	public String getCorteRestringido() {
		return corteRestringido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setCorteRestringido(java.lang.String)
	 */
	public void setCorteRestringido(String corteRestringido) {
		this.corteRestringido = corteRestringido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#getSuministro()
	 */
	public String getSuministro() {
		return suministro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.EstadoServicio#setSuministro(java.lang.String)
	 */
	public void setSuministro(String suministro) {
		this.suministro = suministro;
	}
}
