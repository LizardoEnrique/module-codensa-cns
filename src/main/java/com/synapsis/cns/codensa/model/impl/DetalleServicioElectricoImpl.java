/**
 * $Id: DetalleServicioElectricoImpl.java,v 1.2 2008/05/16 15:01:47 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DetalleServicioElectrico;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleServicioElectricoImpl extends SynergiaBusinessObjectImpl
		implements DetalleServicioElectrico {

	private static final long serialVersionUID = 1L;

	private String acreditacionBeneficiario;
	private String chip;
	private String contadorGradual;
	private String estadoCobranza;
	private String manzana;
	private Long nroCuenta;
	private Long numeroServicio;
	private String tipoCliente;

	public String getAcreditacionBeneficiario() {
		return acreditacionBeneficiario;
	}

	public void setAcreditacionBeneficiario(String acreditacionBeneficiario) {
		this.acreditacionBeneficiario = acreditacionBeneficiario;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public String getContadorGradual() {
		return contadorGradual;
	}

	public void setContadorGradual(String contadorGradual) {
		this.contadorGradual = contadorGradual;
	}

	public String getEstadoCobranza() {
		return estadoCobranza;
	}

	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNumeroServicio() {
		return numeroServicio;
	}

	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
}