package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosMedidor;

public class DatosMedidorImpl extends SynergiaBusinessObjectImpl 
	implements DatosMedidor {
    // Fields    
	
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroMedidor;

    private String tipoMedidor;
    private Date fechaDescarga;

	private Long lecturaActivaFP;
	private Long lecturaActivaHP;
	private Long lecturaActivaXP;
	private Long lecturaReactivaFP;
	private Long lecturaReactivaHP;
	private Long lecturaReactivaXP;
	
	private Long idOrden;

    
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getFechaDescarga()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getFechaDescarga()
	 */
	public Date getFechaDescarga() {
		return fechaDescarga;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setFechaDescarga(java.util.Date)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setFechaDescarga(java.util.Date)
	 */
	public void setFechaDescarga(Date fechaDescarga) {
		this.fechaDescarga = fechaDescarga;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getMarcaMedidor()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getMarcaMedidor()
	 */
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setMarcaMedidor(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setMarcaMedidor(java.lang.String)
	 */
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getModeloMedidor()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getModeloMedidor()
	 */
	public String getModeloMedidor() {
		return modeloMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setModeloMedidor(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setModeloMedidor(java.lang.String)
	 */
	public void setModeloMedidor(String modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getNumeroMedidor()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getNumeroMedidor()
	 */
	public String getNumeroMedidor() {
		return numeroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setNumeroMedidor(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setNumeroMedidor(java.lang.String)
	 */
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getTipoMedidor()
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getTipoMedidor()
	 */
	public String getTipoMedidor() {
		return tipoMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setTipoMedidor(java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setTipoMedidor(java.lang.String)
	 */
	public void setTipoMedidor(String tipoMedidor) {
		this.tipoMedidor = tipoMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaActivaFP()
	 */
	public Long getLecturaActivaFP() {
		return lecturaActivaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaActivaFP(java.lang.Long)
	 */
	public void setLecturaActivaFP(Long lecturaActivaFP) {
		this.lecturaActivaFP = lecturaActivaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaActivaHP()
	 */
	public Long getLecturaActivaHP() {
		return lecturaActivaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaActivaHP(java.lang.Long)
	 */
	public void setLecturaActivaHP(Long lecturaActivaHP) {
		this.lecturaActivaHP = lecturaActivaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaActivaXP()
	 */
	public Long getLecturaActivaXP() {
		return lecturaActivaXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaActivaXP(java.lang.Long)
	 */
	public void setLecturaActivaXP(Long lecturaActivaXP) {
		this.lecturaActivaXP = lecturaActivaXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaReactivaFP()
	 */
	public Long getLecturaReactivaFP() {
		return lecturaReactivaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaReactivaFP(java.lang.Long)
	 */
	public void setLecturaReactivaFP(Long lecturaReactivaFP) {
		this.lecturaReactivaFP = lecturaReactivaFP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaReactivaHP()
	 */
	public Long getLecturaReactivaHP() {
		return lecturaReactivaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaReactivaHP(java.lang.Long)
	 */
	public void setLecturaReactivaHP(Long lecturaReactivaHP) {
		this.lecturaReactivaHP = lecturaReactivaHP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#getLecturaReactivaXP()
	 */
	public Long getLecturaReactivaXP() {
		return lecturaReactivaXP;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.DatosMedidor#setLecturaReactivaXP(java.lang.Long)
	 */
	public void setLecturaReactivaXP(Long lecturaReactivaXP) {
		this.lecturaReactivaXP = lecturaReactivaXP;
	}
	/**
	 * @return the idOrden
	 */
	public Long getIdOrden() {
		return idOrden;
	}
	/**
	 * @param idOrden the idOrden to set
	 */
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}
}
