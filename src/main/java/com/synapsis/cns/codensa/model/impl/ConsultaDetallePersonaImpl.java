/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetallePersona;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaDetallePersonaImpl extends SynergiaBusinessObjectImpl implements ConsultaDetallePersona {
	
	private Long nroCuenta;
	private String nombre;
	private String direComercial;
	private Date fechaNacimiento;
	private String sexo;
	private String descTipoPersona;
	private String giro;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getDescTipoPersona()
	 */
	public String getDescTipoPersona() {
		return descTipoPersona;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setDescTipoPersona(java.lang.String)
	 */
	public void setDescTipoPersona(String descTipoPersona) {
		this.descTipoPersona = descTipoPersona;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getDireComercial()
	 */
	public String getDireComercial() {
		return direComercial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setDireComercial(java.lang.String)
	 */
	public void setDireComercial(String direComercial) {
		this.direComercial = direComercial;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getFechaNacimiento()
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setFechaNacimiento(java.util.Date)
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getGiro()
	 */
	public String getGiro() {
		return giro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setGiro(java.lang.String)
	 */
	public void setGiro(String giro) {
		this.giro = giro;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getNombre()
	 */
	public String getNombre() {
		return nombre;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setNombre(java.lang.String)
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#getSexo()
	 */
	public String getSexo() {
		return sexo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.ConsultaDetallePersona#setSexo(java.lang.String)
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

}
