package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaDetTotalValorCastigado extends SynergiaBusinessObject {
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public String getCodCargo();

	public void setCodCargo(String codCargo);

	public String getDescCargo();

	public void setDescCargo(String descCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public Long getCorrelativoFacturacion();

	public void setCorrelativoFacturacion(Long correlativoFacturacion);

	public String getUsuarioCreador();

	public void setUsuarioCreador(String usuarioCreador);

	public String getNombreUsuarioCreador();

	public void setNombreUsuarioCreador(String nombreUsuarioCreador);

	public Date getFechaCastigo();

	public void setFechaCastigo(Date fechaCastigo);

	public String getMotivoCastigo();

	public void setMotivoCastigo(String motivoCastigo);

	public String getTipoCastigo();

	public void setTipoCastigo(String tipoCastigo);

	public String getCargoOriginal();

	public void setCargoOriginal(String cargoOriginal);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

}