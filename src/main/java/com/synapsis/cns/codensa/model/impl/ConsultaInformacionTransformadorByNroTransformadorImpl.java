package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaInformacionTransformadorByNroTransformador;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaInformacionTransformadorByNroTransformadorImpl extends SynergiaBusinessObjectImpl implements ConsultaInformacionTransformadorByNroTransformador {

	private String transformador;
	private String circuito;
	private String propiedadTra;
	private String conexion;
	private String tension;
	
	public String getCircuito() {
		return circuito;
	}
	public void setCircuito(String circuito) {
		this.circuito = circuito;
	}
	public String getConexion() {
		return conexion;
	}
	public void setConexion(String conexion) {
		this.conexion = conexion;
	}
	public String getPropiedadTra() {
		return propiedadTra;
	}
	public void setPropiedadTra(String propiedadTra) {
		this.propiedadTra = propiedadTra;
	}
	public String getTension() {
		return tension;
	}
	public void setTension(String tension) {
		this.tension = tension;
	}
	public String getTransformador() {
		return transformador;
	}
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}
	
	
	
}
