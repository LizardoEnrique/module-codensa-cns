package com.synapsis.cns.codensa.model;

public interface DetalleECO {

	public Long getAntiguedadDeuda();

	public void setAntiguedadDeuda(Long antiguedadDeuda);

	public String getCiclo();

	public void setCiclo(String ciclo);

	public Long getDiaMora();

	public void setDiaMora(Long diaMora);

	public String getNombreTitular();

	public void setNombreTitular(String nombreTitular);

	public Long getNumeroCuotasFacturadas();

	public void setNumeroCuotasFacturadas(Long numeroCuotasFacturadas);

	public Long getNumeroCuotasFacturar();

	public void setNumeroCuotasFacturar(Long numeroCuotasFacturar);

	public Long getSaldoInteresesMora();

	public void setSaldoInteresesMora(Long saldoInteresesMora);

	public Long getSaldoSocio();

	public void setSaldoSocio(Long saldoSocio);

	public String getSocio();

	public void setSocio(String socio);

	public Long getValorCuota();

	public void setValorCuota(Long valorCuota);

	public Long getValorCuotaMora();

	public void setValorCuotaMora(Long valorCuotaMora);

}