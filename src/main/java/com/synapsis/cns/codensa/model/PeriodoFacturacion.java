package com.synapsis.cns.codensa.model;

import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface PeriodoFacturacion extends SynergiaBusinessObject {
	
	public CalendarDate getFechaProceso();
	
	public void setFechaProceso(CalendarDate fechaProceso);
	
	public String getPeriodoFacturacion();
	
	public void setPeriodoFacturacion(String periodoFacturacion);

}
