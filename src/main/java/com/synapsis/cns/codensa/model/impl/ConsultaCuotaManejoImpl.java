package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaCuotaManejo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaCuotaManejoImpl extends SynergiaBusinessObjectImpl
		implements ConsultaCuotaManejo {
	private Long nroCuenta;
	private Long nroServicio;
	private String tipoSrv;
	private String nombreTit;
	private String tipoDocTitular;
	private String nroDocTitular;
	private Double valorSrv;
	private Double valorFacImpago;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public String getTipoSrv() {
		return tipoSrv;
	}

	public void setTipoSrv(String tipoSrv) {
		this.tipoSrv = tipoSrv;
	}

	public String getNombreTit() {
		return nombreTit;
	}

	public void setNombreTit(String nombreTit) {
		this.nombreTit = nombreTit;
	}

	public String getTipoDocTitular() {
		return tipoDocTitular;
	}

	public void setTipoDocTitular(String tipoDocTitular) {
		this.tipoDocTitular = tipoDocTitular;
	}

	public String getNroDocTitular() {
		return nroDocTitular;
	}

	public void setNroDocTitular(String nroDocTitular) {
		this.nroDocTitular = nroDocTitular;
	}

	public Double getValorSrv() {
		return valorSrv;
	}

	public void setValorSrv(Double valorSrv) {
		this.valorSrv = valorSrv;
	}

	public Double getValorFacImpago() {
		return valorFacImpago;
	}

	public void setValorFacImpago(Double valorFacImpago) {
		this.valorFacImpago = valorFacImpago;
	}

}
