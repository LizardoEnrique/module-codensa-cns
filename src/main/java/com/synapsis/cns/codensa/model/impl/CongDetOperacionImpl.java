package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.CongDetOperacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongDetOperacionImpl extends SynergiaBusinessObjectImpl implements
		CongDetOperacion {
    
	private Long nroCuenta;
	private Long nroSaldo;
	private String periodoFacturacion;
	private String codigoItem;
	private String descItem;
	private Long valorInicial;
	private Long valorCongelado;
	private Long valorAclaracion;
	private Long valorFinal;
	private Long consumoInicial;
	private Long consumoCongelado;
	private Long consumoAclaracion;
	private Long consumoFinal;
	private Long valorDisputado;
	private Long consumoDisputado;
	
	/**
	 * @return the periodoFacturacion
	 */
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	/**
	 * @return the codigoItem
	 */
	public String getCodigoItem() {
		return codigoItem;
	}
	/**
	 * @param codigoItem the codigoItem to set
	 */
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	/**
	 * @return the descItem
	 */
	public String getDescItem() {
		return descItem;
	}
	/**
	 * @param descItem the descItem to set
	 */
	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}
	/**
	 * @return the valorInicial
	 */
	public Long getValorInicial() {
		return valorInicial;
	}
	/**
	 * @param valorInicial the valorInicial to set
	 */
	public void setValorInicial(Long valorInicial) {
		this.valorInicial = valorInicial;
	}
	/**
	 * @return the valorCongelado
	 */
	public Long getValorCongelado() {
		return valorCongelado;
	}
	/**
	 * @param valorCongelado the valorCongelado to set
	 */
	public void setValorCongelado(Long valorCongelado) {
		this.valorCongelado = valorCongelado;
	}
	/**
	 * @return the valorAclaracion
	 */
	public Long getValorAclaracion() {
		return valorAclaracion;
	}
	/**
	 * @param valorAclaracion the valorAclaracion to set
	 */
	public void setValorAclaracion(Long valorAclaracion) {
		this.valorAclaracion = valorAclaracion;
	}
	/**
	 * @return the valorFinal
	 */
	public Long getValorFinal() {
		return valorFinal;
	}
	/**
	 * @param valorFinal the valorFinal to set
	 */
	public void setValorFinal(Long valorFinal) {
		this.valorFinal = valorFinal;
	}
	/**
	 * @return the consumoInicial
	 */
	public Long getConsumoInicial() {
		return consumoInicial;
	}
	/**
	 * @param consumoInicial the consumoInicial to set
	 */
	public void setConsumoInicial(Long consumoInicial) {
		this.consumoInicial = consumoInicial;
	}
	/**
	 * @return the consumoCongelado
	 */
	public Long getConsumoCongelado() {
		return consumoCongelado;
	}
	/**
	 * @param consumoCongelado the consumoCongelado to set
	 */
	public void setConsumoCongelado(Long consumoCongelado) {
		this.consumoCongelado = consumoCongelado;
	}
	/**
	 * @return the consumoAclaracion
	 */
	public Long getConsumoAclaracion() {
		return consumoAclaracion;
	}
	/**
	 * @param consumoAclaracion the consumoAclaracion to set
	 */
	public void setConsumoAclaracion(Long consumoAclaracion) {
		this.consumoAclaracion = consumoAclaracion;
	}
	/**
	 * @return the consumoFinal
	 */
	public Long getConsumoFinal() {
		return consumoFinal;
	}
	/**
	 * @param consumoFinal the consumoFinal to set
	 */
	public void setConsumoFinal(Long consumoFinal) {
		this.consumoFinal = consumoFinal;
	}
	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado() {
		return valorDisputado;
	}
	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado) {
		this.valorDisputado = valorDisputado;
	}
	/**
	 * @return the consumoDisputado
	 */
	public Long getConsumoDisputado() {
		return consumoDisputado;
	}
	/**
	 * @param consumoDisputado the consumoDisputado to set
	 */
	public void setConsumoDisputado(Long consumoDisputado) {
		this.consumoDisputado = consumoDisputado;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo() {
		return nroSaldo;
	}
	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo) {
		this.nroSaldo = nroSaldo;
	}
	
}