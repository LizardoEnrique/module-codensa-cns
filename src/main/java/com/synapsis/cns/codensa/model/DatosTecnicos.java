package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosTecnicos extends SynergiaBusinessObject {
		
	/**
	 * @return the cadenaElectrica
	 */
	public String getCadenaElectrica();
	/**
	 * @param cadenaElectrica the cadenaElectrica to set
	 */
	public void setCadenaElectrica(String cadenaElectrica);
	/**
	 * @return the conexion
	 */
	public String getConexion();
	/**
	 * @param conexion the conexion to set
	 */
	public void setConexion(String conexion);
	/**
	 * @return the fechaIncorporacion
	 */
	public Date getFechaIncorporacion();
	/**
	 * @param fechaIncorporacion the fechaIncorporacion to set
	 */
	public void setFechaIncorporacion(Date fechaIncorporacion);
	/**
	 * @return the nivelTension
	 */
	public String getNivelTension();
	/**
	 * @param nivelTension the nivelTension to set
	 */
	public void setNivelTension(String nivelTension);
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);	
	
	public String getSistemaMedida();

	public void setSistemaMedida(String sistemaMedida);

}