package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DetalleInventarioInicialInfraestructura extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public Long getNroServicio();

	public Long getIdClienteTelem();

	public Long getPeriodo();

	public Long getNroProyecto();

	public String getZona();

	public String getProducto();

	public String getSubProducto();

	public Date getFechaViabilidad();

	public BigDecimal getCantidad();

	public Date getFechaFactura();

	public Date getFechaPeriodoGracia();

	public Integer getDiasCobro();

	public BigDecimal getCostoPeriodo();

	public BigDecimal getTarifaAplicada();

	public String getEstadoFacturacion();

	public void setNroCuenta(Long nroCuenta);

	public void setNroServicio(Long nroServicio);

	public void setIdClienteTelem(Long idClienteTelem);

	public void setPeriodo(Long periodo);

	public void setNroProyecto(Long nroProyecto);

	public void setZona(String zona);

	public void setProducto(String producto);

	public void setSubProducto(String subProducto);

	public void setFechaViabilidad(Date fechaViabilidad);

	public void setCantidad(BigDecimal cantidad);

	public void setFechaFactura(Date fechaFactura);

	public void setFechaPeriodoGracia(Date fechaPeriodoGracia);

	public void setDiasCobro(Integer diasCobro);

	public void setCostoPeriodo(BigDecimal costoPeriodo);

	public void setTarifaAplicada(BigDecimal tarifaAplicada);

	public void setEstadoFacturacion(String estadoFacturacion);

}
