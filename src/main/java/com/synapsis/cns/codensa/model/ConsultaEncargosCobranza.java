package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaEncargosCobranza {

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion();

	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion);

	/**
	 * @return the fechaDesactivacion
	 */
	public Date getFechaDesactivacion();

	/**
	 * @param fechaDesactivacion the fechaDesactivacion to set
	 */
	public void setFechaDesactivacion(Date fechaDesactivacion);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the nombreTit
	 */
	public String getNombreTit();

	/**
	 * @param nombreTit the nombreTit to set
	 */
	public void setNombreTit(String nombreTit);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroEco
	 */
	public Long getNroEco();

	/**
	 * @param nroEco the nroEco to set
	 */
	public void setNroEco(Long nroEco);

	/**
	 * @return the nroIdEco
	 */
	public String getNroIdEco();

	/**
	 * @param nroIdEco the nroIdEco to set
	 */
	public void setNroIdEco(String nroIdEco);

	/**
	 * @return the nroIdTitular
	 */
	public String getNroIdTitular();

	/**
	 * @param nroIdTitular the nroIdTitular to set
	 */
	public void setNroIdTitular(String nroIdTitular);

	/**
	 * @return the nroSrvFin
	 */
	public Long getNroSrvFin();

	/**
	 * @param nroSrvFin the nroSrvFin to set
	 */
	public void setNroSrvFin(Long nroSrvFin);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the saldoEco
	 */
	public Double getSaldoEco();

	/**
	 * @param saldoEco the saldoEco to set
	 */
	public void setSaldoEco(Double saldoEco);

	/**
	 * @return the tipoIdTitular
	 */
	public String getTipoIdTitular();

	/**
	 * @param tipoIdTitular the tipoIdTitular to set
	 */
	public void setTipoIdTitular(String tipoIdTitular);

	/**
	 * @return the tipoSrvFin
	 */
	public String getTipoSrvFin();

	/**
	 * @param tipoSrvFin the tipoSrvFin to set
	 */
	public void setTipoSrvFin(String tipoSrvFin);

	/**
	 * @return the valorCargado
	 */
	public Double getValorCargado();

	/**
	 * @param valorCargado the valorCargado to set
	 */
	public void setValorCargado(Double valorCargado);
	
	public Long getIdTipoProducto();
	
	public void setIdTipoProducto(Long idTipoProducto);	

	public String getNumDocTitular();
	
	public void setNumDocTitular(String numDocTitular);
	
	public String getTipoDocTitular();
	
	public void setTipoDocTitular(String tipoDocTitular);
	
	public String getIndicadorSaldoAnterior();
	
	public void setIndicadorSaldoAnterior(String indicadorSaldoAnterior);
	
}