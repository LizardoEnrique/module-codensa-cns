package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.DeudaDatosConvenios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaDatosConveniosImpl extends SynergiaBusinessObjectImpl
		implements DeudaDatosConvenios {

	private Long nroCuenta;

	private String deudaConvenio;

	private Double antiguedadDeudaConvenio;

	private Double saldosEnergia;

	private Double totalCuotasOtorgadas;

	private Double totalCuotasFacturadas;

	private String valorDeudaCondonada;

	private String indicadorDeudaPendiente;

	private String estadoCobranza;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getDeudaConvenio() {
		return deudaConvenio;
	}

	public void setDeudaConvenio(String deudaConvenio) {
		this.deudaConvenio = deudaConvenio;
	}

	public Double getAntiguedadDeudaConvenio() {
		return antiguedadDeudaConvenio;
	}

	public void setAntiguedadDeudaConvenio(Double antiguedadDeudaConvenio) {
		this.antiguedadDeudaConvenio = antiguedadDeudaConvenio;
	}

	public Double getSaldosEnergia() {
		return saldosEnergia;
	}

	public void setSaldosEnergia(Double saldosEnergia) {
		this.saldosEnergia = saldosEnergia;
	}

	public Double getTotalCuotasOtorgadas() {
		return totalCuotasOtorgadas;
	}

	public void setTotalCuotasOtorgadas(Double totalCuotasOtorgadas) {
		this.totalCuotasOtorgadas = totalCuotasOtorgadas;
	}

	public Double getTotalCuotasFacturadas() {
		return totalCuotasFacturadas;
	}

	public void setTotalCuotasFacturadas(Double totalCuotasFacturadas) {
		this.totalCuotasFacturadas = totalCuotasFacturadas;
	}

	public String getValorDeudaCondonada() {
		return valorDeudaCondonada;
	}

	public void setValorDeudaCondonada(String valorDeudaCondonada) {
		this.valorDeudaCondonada = valorDeudaCondonada;
	}

	public String getIndicadorDeudaPendiente() {
		return indicadorDeudaPendiente;
	}

	public void setIndicadorDeudaPendiente(String indicadorDeudaPendiente) {
		this.indicadorDeudaPendiente = indicadorDeudaPendiente;
	}

	public String getEstadoCobranza() {
		return estadoCobranza;
	}

	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}
}