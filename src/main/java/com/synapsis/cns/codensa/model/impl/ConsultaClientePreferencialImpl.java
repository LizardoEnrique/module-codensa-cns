package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaClientePreferencial;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaClientePreferencialImpl extends SynergiaBusinessObjectImpl implements ConsultaClientePreferencial {
	private Long nroCuenta;
	private Long nroCuentaAnterior;
	private String tipoCliente;

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public String getTipoCliente() {
		return this.tipoCliente;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public Long getNroCuentaAnterior() {
		return this.nroCuentaAnterior;
	}

	public void setNroCuentaAnterior(Long nroCuentaAnterior) {
		this.nroCuentaAnterior = nroCuentaAnterior;
	}

}
