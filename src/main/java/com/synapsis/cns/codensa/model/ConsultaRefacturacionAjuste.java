/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 016
 */
public interface ConsultaRefacturacionAjuste extends SynergiaBusinessObject {

	public Double getCantidadKWAjustados() ;
	public Date getFechaEliminacion() ;
	public Date getFechaVencimientoPagoAjuste();
	public Date getFechaYHoraAprobacion() ;
	public Date getFechaYHoraRealizacion() ;
	public Long getIdCuenta();
	public String getNombresUsuariosAprobadores() ;
	public String getNombreUsuarioEliminador() ;
	public String getNombreUsuarioRealizador() ;
	public Long getNroAjuste();
	public Long getNroCuenta() ;
	public Long getNroOrdenRefacturacion();
	public String getObservacionesEliminacion();
	public String getServicioAjustado() ;
	public String getUsuarioEliminador() ;
	public String getUsuarioRealizador() ;
	public String getUsuariosAprobadores();
	public Double getValorAPagarDespuesReajuste();
	public Double getValorTotalAjustado() ;
	public String getAreaResponsable() ;
	public String getAreaSolicitante() ;
	public String getClasificacionAjuste();
	public String getSubmotivoAjuste() ;
	public Double getTiempoTranscurridoHastaAprobacion() ;
}
