package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaFacturacionInformacionGeneral;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class ConsultaFacturacionInformacionGeneralImpl extends SynergiaBusinessObjectImpl implements
		ConsultaFacturacionInformacionGeneral {

	private String periodo;
	private Date fechaFacturacion;
	private Date fechaProceso;
	private Date fechaLectura;
	private String totalFactura;
	private Integer numeroFactura;
	private Date fechaVencimiento;
	private Long nroCuenta;

	private String totalConsumo;
	private String totalInteres;
	private String totalFinanciado;
	private String totalSrvVentas;
	private String totalCodensaHogar;
	private String totalEncargoCobranzas;
	private String totalConReconnecion;
	private String totalOpAdic;
	private String saldoAnterior;
	private String nroDocCli;
	private String tipoLiquidacion = "Mensual";
	private String tipoEsquemaFacturacion = "Tradicional";

	/**
	 * @return the nroDocCli
	 */
	public String getNroDocCli() {
		return nroDocCli;
	}

	/**
	 * @param nroDocCli the nroDocCli to set
	 */
	public void setNroDocCli(String nroDocCli) {
		this.nroDocCli = nroDocCli;
	}

	/**
	 * @return the saldoAnterior
	 */
	public String getSaldoAnterior() {
		return saldoAnterior;
	}

	/**
	 * @param saldoAnterior the saldoAnterior to set
	 */
	public void setSaldoAnterior(String saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	/**
	 * @return the totalCodensaHogar
	 */
	public String getTotalCodensaHogar() {
		return totalCodensaHogar;
	}

	/**
	 * @param totalCodensaHogar the totalCodensaHogar to set
	 */
	public void setTotalCodensaHogar(String totalCodensaHogar) {
		this.totalCodensaHogar = totalCodensaHogar;
	}

	/**
	 * @return the totalConReconnecion
	 */
	public String getTotalConReconnecion() {
		return totalConReconnecion;
	}

	/**
	 * @param totalConReconnecion the totalConReconnecion to set
	 */
	public void setTotalConReconnecion(String totalConReconnecion) {
		this.totalConReconnecion = totalConReconnecion;
	}

	/**
	 * @return the totalConsumo
	 */
	public String getTotalConsumo() {
		return totalConsumo;
	}

	/**
	 * @param totalConsumo the totalConsumo to set
	 */
	public void setTotalConsumo(String totalConsumo) {
		this.totalConsumo = totalConsumo;
	}

	/**
	 * @return the totalEncargoCobranzas
	 */
	public String getTotalEncargoCobranzas() {
		return totalEncargoCobranzas;
	}

	/**
	 * @param totalEncargoCobranzas the totalEncargoCobranzas to set
	 */
	public void setTotalEncargoCobranzas(String totalEncargoCobranzas) {
		this.totalEncargoCobranzas = totalEncargoCobranzas;
	}

	/**
	 * @return the totalFinanciado
	 */
	public String getTotalFinanciado() {
		return totalFinanciado;
	}

	/**
	 * @param totalFinanciado the totalFinanciado to set
	 */
	public void setTotalFinanciado(String totalFinanciado) {
		this.totalFinanciado = totalFinanciado;
	}

	/**
	 * @return the totalInteres
	 */
	public String getTotalInteres() {
		return totalInteres;
	}

	/**
	 * @param totalInteres the totalInteres to set
	 */
	public void setTotalInteres(String totalInteres) {
		this.totalInteres = totalInteres;
	}

	/**
	 * @return the totalOpAdic
	 */
	public String getTotalOpAdic() {
		return totalOpAdic;
	}

	/**
	 * @param totalOpAdic the totalOpAdic to set
	 */
	public void setTotalOpAdic(String totalOpAdic) {
		this.totalOpAdic = totalOpAdic;
	}

	/**
	 * @return the totalSrvVentas
	 */
	public String getTotalSrvVentas() {
		return totalSrvVentas;
	}

	/**
	 * @param totalSrvVentas the totalSrvVentas to set
	 */
	public void setTotalSrvVentas(String totalSrvVentas) {
		this.totalSrvVentas = totalSrvVentas;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaLectura()
	 */
	public Date getFechaLectura() {
		return fechaLectura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getFechaVencimiento()
	 */
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getNumeroFactura()
	 */
	public Integer getNumeroFactura() {
		return numeroFactura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getPeriodo()
	 */
	public String getPeriodo() {
		return periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#getTotalFactura()
	 */
	public String getTotalFactura() {
		return totalFactura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaLectura(java.util.Date)
	 */
	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setFechaVencimiento(java.util.Date)
	 */
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setNumeroFactura(java.lang.Integer)
	 */
	public void setNumeroFactura(Integer numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setPeriodo(java.util.Date)
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionInformacionGeneral#setTotalFactura(java.lang.String)
	 */
	public void setTotalFactura(String totalFactura) {
		this.totalFactura = totalFactura;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getTipoEsquemaFacturacion() {
		return tipoEsquemaFacturacion;
	}

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion) {
		this.tipoEsquemaFacturacion = tipoEsquemaFacturacion;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

}