package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface DetalleAnomalia extends SynergiaBusinessObject {

	public String getCodigoAnomalia();

	public void setCodigoAnomalia(String codigoAnomalia);

	public String getDescripcionAnomalia();

	public void setDescripcionAnomalia(String descripcionAnomalia);

	public Long getIdComponente();

	public void setIdComponente(Long idComponente);

}