/**
 * $Id: DetalleServicioElectrico.java,v 1.2 2008/05/16 15:01:47 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;



public interface DetalleServicioElectrico  extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);	
	
	public Long getNumeroServicio();

	public void setNumeroServicio(Long numeroServicio);	
	
	public String getAcreditacionBeneficiario();
	
	public void setAcreditacionBeneficiario(String acreditacionBeneficiario);

	public String getContadorGradual();
	
	public void setContadorGradual(String contadorGradual);
	
	public String getManzana();
	
	public void setManzana(String manzana);
	
	public String getEstadoCobranza(); 

	public void setEstadoCobranza(String estadoCobranza);

	public String getChip();

	public void setChip(String chip);

	public String getTipoCliente();
	
	public void setTipoCliente(String tipoCliente);
}