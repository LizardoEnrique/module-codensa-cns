package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.TitularCuenta;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class TitularCuentaImpl extends SynergiaBusinessObjectImpl implements TitularCuenta {
	private Long nroCuenta;
	private Long nroCuentaAnterior;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String tipoDocumento;
	private String descripcionTipoDocumento;
	private String numeroDocumento;
	private String telefono;
	private String telefonoAuxiliar;
	private String telefonoMovil;
	private String email;
	private String digitoVerificador;

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroCuentaAnterior() {
		return nroCuentaAnterior;
	}

	public void setNroCuentaAnterior(Long nroCuentaAnterior) {
		this.nroCuentaAnterior = nroCuentaAnterior;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDescripcionTipoDocumento() {
		return this.descripcionTipoDocumento;
	}

	public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
		this.descripcionTipoDocumento = descripcionTipoDocumento;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefonoAuxiliar() {
		return this.telefonoAuxiliar;
	}

	public void setTelefonoAuxiliar(String telefonoAuxiliar) {
		this.telefonoAuxiliar = telefonoAuxiliar;
	}

	public String getTelefonoMovil() {
		return this.telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

}
