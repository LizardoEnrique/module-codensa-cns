package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosCuenta;

public class DatosCuentaImpl extends SynergiaBusinessObjectImpl 
	implements DatosCuenta {

    private Long nroCuenta;
    private String descripcionSucursal;
    private Boolean direccionRepartoEspecial ;
    private String codigoEntidadComercial;
    private String descripcionEntidadComercial;
    private String numeroCuentaPadre;
    private Date fechaIncorporacion;    
    private Boolean presentaConvenioActivo;
    private Boolean presentaExpediente;
    private Boolean presentaDeudaCastigada;
    private Boolean presentaDeudaCondonada;
    private Boolean clienteVIP;    
    private String estadoCliente;
    private Boolean servicioElectricoEmpresarial;
    private Boolean otroComercializador;
    private Boolean cuentaCorporativa;
    private Boolean cuentaOficial;
    private Boolean denunciaLeyArrendamiento;   
    private Boolean enFacturacion;
    private String codigoRetencion;
    
    private Boolean indicadorPropietario;
    private Boolean indicadorArrendatario;


	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Boolean getClienteVIP() {
		return clienteVIP;
	}

	public void setClienteVIP(Boolean clienteVIP) {
		this.clienteVIP = clienteVIP;
	}

	public String getCodigoEntidadComercial() {
		return codigoEntidadComercial;
	}

	public void setCodigoEntidadComercial(String codigoEntidadComercial) {
		this.codigoEntidadComercial = codigoEntidadComercial;
	}

	public Boolean getCuentaCorporativa() {
		return cuentaCorporativa;
	}

	public void setCuentaCorporativa(Boolean cuentaCorporativa) {
		this.cuentaCorporativa = cuentaCorporativa;
	}

	public Boolean getCuentaOficial() {
		return cuentaOficial;
	}

	public void setCuentaOficial(Boolean cuentaOficial) {
		this.cuentaOficial = cuentaOficial;
	}

	public Boolean getDenunciaLeyArrendamiento() {
		return denunciaLeyArrendamiento;
	}

	public void setDenunciaLeyArrendamiento(Boolean denunciaLeyArrendamiento) {
		this.denunciaLeyArrendamiento = denunciaLeyArrendamiento;
	}

	public String getDescripcionEntidadComercial() {
		return descripcionEntidadComercial;
	}

	public void setDescripcionEntidadComercial(String descripcionEntidadComercial) {
		this.descripcionEntidadComercial = descripcionEntidadComercial;
	}

	public String getDescripcionSucursal() {
		return descripcionSucursal;
	}

	public void setDescripcionSucursal(String descripcionSucursal) {
		this.descripcionSucursal = descripcionSucursal;
	}

	public Boolean getDireccionRepartoEspecial() {
		return direccionRepartoEspecial;
	}

	public void setDireccionRepartoEspecial(Boolean direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public String getEstadoCliente() {
		return estadoCliente;
	}

	public void setEstadoCliente(String estadoCliente) {
		this.estadoCliente = estadoCliente;
	}

	public Date getFechaIncorporacion() {
		return fechaIncorporacion;
	}

	public void setFechaIncorporacion(Date fechaIncorporacion) {
		this.fechaIncorporacion = fechaIncorporacion;
	}

	public String getNumeroCuentaPadre() {
		return numeroCuentaPadre;
	}

	public void setNumeroCuentaPadre(String numeroCuentaPadre) {
		this.numeroCuentaPadre = numeroCuentaPadre;
	}

	public Boolean getServicioElectricoEmpresarial() {
		return servicioElectricoEmpresarial;
	}

	public void setServicioElectricoEmpresarial(Boolean servicioElectricoEmpresarial) {
		this.servicioElectricoEmpresarial = servicioElectricoEmpresarial;
	}
	
	public Boolean getOtroComercializador() {
		return otroComercializador;
	}

	public void setOtroComercializador(Boolean otroComercializador) {
		this.otroComercializador = otroComercializador;
	}

	public Boolean getPresentaConvenioActivo() {
		return presentaConvenioActivo;
	}

	public void setPresentaConvenioActivo(Boolean presentaConvenioActivo) {
		this.presentaConvenioActivo = presentaConvenioActivo;
	}

	public Boolean getPresentaDeudaCastigada() {
		return presentaDeudaCastigada;
	}

	public void setPresentaDeudaCastigada(Boolean presentaDeudaCastigada) {
		this.presentaDeudaCastigada = presentaDeudaCastigada;
	}

	public Boolean getPresentaDeudaCondonada() {
		return presentaDeudaCondonada;
	}

	public void setPresentaDeudaCondonada(Boolean presentaDeudaCondonada) {
		this.presentaDeudaCondonada = presentaDeudaCondonada;
	}

	public Boolean getPresentaExpediente() {
		return presentaExpediente;
	}

	public void setPresentaExpediente(Boolean presentaExpediente) {
		this.presentaExpediente = presentaExpediente;
	}

	public String getCodigoRetencion() {
		return codigoRetencion;
	}

	public void setCodigoRetencion(String codigoRetencion) {
		this.codigoRetencion = codigoRetencion;
	}

	public Boolean getEnFacturacion() {
		return enFacturacion;
	}

	public void setEnFacturacion(Boolean enFacturacion) {
		this.enFacturacion = enFacturacion;
	}

	public Boolean getIndicadorArrendatario() {
		return indicadorArrendatario;
	}

	public void setIndicadorArrendatario(Boolean indicadorArrendatario) {
		this.indicadorArrendatario = indicadorArrendatario;
	}

	public Boolean getIndicadorPropietario() {
		return indicadorPropietario;
	}

	public void setIndicadorPropietario(Boolean indicadorPropietario) {
		this.indicadorPropietario = indicadorPropietario;
	}

    
}
