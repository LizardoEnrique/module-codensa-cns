package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.InformacionPago;

public class InformacionPagoImpl extends SynergiaBusinessObjectImpl implements InformacionPago {

	private Long nroCuenta;
	private Date fechaIngreso;
	private String tipoDocumento;
	private Long nroDocumento;
	private String valorPago;
	private String valorPagoFormat;
	private String medioPago;
	private String sucursalRecaudo;
	private String entidadRecaudadora;
	private Date fechaPagoEntidad;
	private String procedenciaPago;
	private String estadoPago;
	private Boolean chequeDevuelto;
	
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getEntidadRecaudadora()
	 */
	public String getEntidadRecaudadora() {
		return entidadRecaudadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getEstadoPago()
	 */
	public String getEstadoPago() {
		return estadoPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getFechaIngreso()
	 */
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getFechaPagoEntidad()
	 */
	public Date getFechaPagoEntidad() {
		return fechaPagoEntidad;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getMedioPago()
	 */
	public String getMedioPago() {
		return medioPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getNroDocumento()
	 */
	public Long getNroDocumento() {
		return nroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getProcedenciaPago()
	 */
	public String getProcedenciaPago() {
		return procedenciaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getSucursalRecaudo()
	 */
	public String getSucursalRecaudo() {
		return sucursalRecaudo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#getValorPago()
	 */
	public String getValorPago() {
		return valorPago;
	}
	
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Boolean getChequeDevuelto() {
		return chequeDevuelto;
	}

	public void setChequeDevuelto(Boolean chequeDevuelto) {
		this.chequeDevuelto = chequeDevuelto;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setEntidadRecaudadora(java.lang.String)
	 */
	public void setEntidadRecaudadora(String entidadRecaudadora) {
		this.entidadRecaudadora = entidadRecaudadora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setEstadoPago(java.lang.String)
	 */
	public void setEstadoPago(String estadoPago) {
		this.estadoPago = estadoPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setFechaIngreso(java.lang.String)
	 */
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setFechaPagoEntidad(java.lang.String)
	 */
	public void setFechaPagoEntidad(Date fechaPagoEntidad) {
		this.fechaPagoEntidad = fechaPagoEntidad;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setMedioPago(java.lang.String)
	 */
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setNroDocumento(java.lang.String)
	 */
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setProcedenciaPago(java.lang.String)
	 */
	public void setProcedenciaPago(String procedenciaPago) {
		this.procedenciaPago = procedenciaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setSucursalRecaudo(java.lang.String)
	 */
	public void setSucursalRecaudo(String sucursalRecaudo) {
		this.sucursalRecaudo = sucursalRecaudo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setTipoDocumento(java.lang.String)
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosHeader#setValorPago(java.lang.Long)
	 */
	public void setValorPago(String valorPago) {
		this.valorPago = valorPago;
	}
	public String getValorPagoFormat() {
		return valorPagoFormat;
	}
	public void setValorPagoFormat(String valorPagoFormat) {
		this.valorPagoFormat = valorPagoFormat;
	}		
}
