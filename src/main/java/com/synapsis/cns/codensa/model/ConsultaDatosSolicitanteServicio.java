package com.synapsis.cns.codensa.model;

public interface ConsultaDatosSolicitanteServicio {

	/**
	 * @return the causalNegacion
	 */
	public String getCausalNegacion();

	/**
	 * @param causalNegacion the causalNegacion to set
	 */
	public void setCausalNegacion(String causalNegacion);

	/**
	 * @return the cicloFacturacion
	 */
	public Double getCicloFacturacion();

	/**
	 * @param cicloFacturacion the cicloFacturacion to set
	 */
	public void setCicloFacturacion(Double cicloFacturacion);

	/**
	 * @return the estrato
	 */
	public String getEstrato();

	/**
	 * @param estrato the estrato to set
	 */
	public void setEstrato(String estrato);

	/**
	 * @return the fechaSolicitudCred
	 */
	public String getFechaSolicitudCred();

	/**
	 * @param fechaSolicitudCred the fechaSolicitudCred to set
	 */
	public void setFechaSolicitudCred(String fechaSolicitudCred);

	/**
	 * @return the montoAprobado
	 */
	public Long getMontoAprobado();

	/**
	 * @param montoAprobado the montoAprobado to set
	 */
	public void setMontoAprobado(Long montoAprobado);

	/**
	 * @return the montoDisponible
	 */
	public Long getMontoDisponible();

	/**
	 * @param montoDisponible the montoDisponible to set
	 */
	public void setMontoDisponible(Long montoDisponible);

	/**
	 * @return the nombreSolicitante
	 */
	public String getNombreSolicitante();

	/**
	 * @param nombreSolicitante the nombreSolicitante to set
	 */
	public void setNombreSolicitante(String nombreSolicitante);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the nroSolicitante
	 */
	public String getNroSolicitante();

	/**
	 * @param nroSolicitante the nroSolicitante to set
	 */
	public void setNroSolicitante(String nroSolicitante);

	/**
	 * @return the resultadoEstudio
	 */
	public String getResultadoEstudio();

	/**
	 * @param resultadoEstudio the resultadoEstudio to set
	 */
	public void setResultadoEstudio(String resultadoEstudio);

}