package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.EventosComponentes;

public class EventosComponentesImpl extends SynergiaBusinessObjectImpl 
	implements EventosComponentes {
    // Fields    
	
    private static final long serialVersionUID = 1L;

	private Long nroCuenta;
	private String marcaComponente;
	private String modeloComponente;
	private String numeroComponente;
    private String tipoComponente;
    private String claseComponente;
    private String relacionTransformacion;
    private Date fechaInstalacion;
    private Date fechaRetiro;

    private Long idComponente; //este luego no deberia el id
    private Long idMedidor;

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getClaseComponente() {
		return claseComponente;
	}

	public void setClaseComponente(String claseComponente) {
		this.claseComponente = claseComponente;
	}

	public Date getFechaInstalacion() {
		return fechaInstalacion;
	}

	public void setFechaInstalacion(Date fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}

	public Date getFechaRetiro() {
		return fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Long getIdComponente() {
		return idComponente;
	}

	public void setIdComponente(Long idComponente) {
		this.idComponente = idComponente;
	}

	public String getMarcaComponente() {
		return marcaComponente;
	}

	public void setMarcaComponente(String marcaComponente) {
		this.marcaComponente = marcaComponente;
	}

	public String getModeloComponente() {
		return modeloComponente;
	}

	public void setModeloComponente(String modeloComponente) {
		this.modeloComponente = modeloComponente;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNumeroComponente() {
		return numeroComponente;
	}

	public void setNumeroComponente(String numeroComponente) {
		this.numeroComponente = numeroComponente;
	}

	public String getRelacionTransformacion() {
		return relacionTransformacion;
	}

	public void setRelacionTransformacion(String relacionTransformacion) {
		this.relacionTransformacion = relacionTransformacion;
	}

	public String getTipoComponente() {
		return tipoComponente;
	}

	public void setTipoComponente(String tipoComponente) {
		this.tipoComponente = tipoComponente;
	}

   
}
