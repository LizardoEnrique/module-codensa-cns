package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.FacturaTiposServicios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class FacturaTiposServiciosImpl extends SynergiaBusinessObjectImpl
		implements FacturaTiposServicios {

	private Long nroFactura;

	private Long nroServicio;

	private String tipoServicio;

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
}