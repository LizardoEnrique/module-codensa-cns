package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaOperacionesAdicionales {

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the descripCargo
	 */
	public String getDescripCargo();

	/**
	 * @param descripCargo the descripCargo to set
	 */
	public void setDescripCargo(String descripCargo);

	/**
	 * @return the estado
	 */
	public String getEstado();

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado);

	/**
	 * @return the fechaCotizacion
	 */
	public Date getFechaCotizacion();

	/**
	 * @param fechaCotizacion the fechaCotizacion to set
	 */
	public void setFechaCotizacion(Date fechaCotizacion);

	/**
	 * @return the observaciones
	 */
	public String getObservaciones();

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);

	/**
	 * @return the tipoCargo
	 */
	public String getTipoCargo();

	/**
	 * @param tipoCargo the tipoCargo to set
	 */
	public void setTipoCargo(String tipoCargo);

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda();

	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda);

	/**
	 * @return the tipoOperAdicional
	 */
	public String getTipoOperAdicional();

	/**
	 * @param tipoOperAdicional the tipoOperAdicional to set
	 */
	public void setTipoOperAdicional(String tipoOperAdicional);

	/**
	 * @return the usuarioRegistro
	 */
	public String getUsuarioRegistro();

	/**
	 * @param usuarioRegistro the usuarioRegistro to set
	 */
	public void setUsuarioRegistro(String usuarioRegistro);

	/**
	 * @return the valor
	 */
	public Double getValor();

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Double valor);

}