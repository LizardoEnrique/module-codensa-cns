package com.synapsis.cns.codensa.model;

import java.sql.Clob;
import java.util.Date;
import java.util.Map;

import com.synapsis.nuc.codensa.model.Cuenta;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author jhack
 */
public interface LineaFactura extends SynergiaBusinessObject {
	public Date getFechaFactura();
	public void setFechaFactura(Date fechaFactura);
	public String getDatos();
	public String getDatosAnexo();
	public void setDatos(String datosFactura);
	public void setDatosAnexo(String datosAnexo);
	public Map getFormato();
	public void setFormato(Map formato);
	public Object getReporte();
	public void setReporte(Object reporte);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	public Clob getDatosClob();
	public void setDatosClob(Clob datosClob);
	public Clob getDatosAnexoClob();
	public void setDatosAnexoClob(Clob datosAnexoClob);

}
