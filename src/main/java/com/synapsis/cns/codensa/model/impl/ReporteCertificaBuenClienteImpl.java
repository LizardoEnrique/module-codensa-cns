package com.synapsis.cns.codensa.model.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.synapsis.cns.codensa.model.ReporteCertificaBuenCliente;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author m3.MarioRoss - 12/12/2006
 *
 */
public class ReporteCertificaBuenClienteImpl extends SynergiaBusinessObjectImpl implements ReporteCertificaBuenCliente {
	// atributos necesarios para obtener cada campo (dia, mes, a�o) de la fechaExpedicion
	private Calendar cal = Calendar.getInstance();
	private static DateFormat df = new SimpleDateFormat("MMMMM", Locale.getDefault());
	
	private Long nroCuenta;
	private Date fechaExpedicion;

	private String nombreSolicitante;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
	
	private String nombreTitularCuenta;
	private String apellidoTitularCuenta;
	private String direccion;
	private String localizacion;
	private String municipio;
	private String barrio;
	private String telefono;
	private String direccionRepartoEspecial;
	
	private String usuarioSistema;
	
	private Long nroDocumentoTitularCuenta;

	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getDiaExpedicion()
	 */
	public Integer getDiaExpedicion(){
		return new Integer(cal.get(Calendar.DAY_OF_MONTH));
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getMesExpedicion()
	 */
	public String getMesExpedicion(){
		return df.format(getFechaExpedicion());
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getAnioExpedicion()
	 */
	public Integer getAnioExpedicion(){
		return new Integer(cal.get(Calendar.YEAR));
	}
	/**
	 * Devuelve un String tal como est� si no es null. Si es null devuelve un String vac�o
	 */
	private String nullToEmpty(String string) {
		return string != null ? string : "";
	}
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getBarrio()
	 */
	public String getBarrio() {
		return nullToEmpty(barrio);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setBarrio(java.lang.String)
	 */
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getDireccion()
	 */
	public String getDireccion() {
		return nullToEmpty(direccion);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setDireccion(java.lang.String)
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getDireccionRepartoEspecial()
	 */
	public String getDireccionRepartoEspecial() {
		return nullToEmpty(direccionRepartoEspecial);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setDireccionRepartoEspecial(java.lang.String)
	 */
	public void setDireccionRepartoEspecial(String direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setFechaExpedicion(java.util.Date)
	 */
	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
		cal.setTime(fechaExpedicion);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getLocalizacion()
	 */
	public String getLocalizacion() {
		return nullToEmpty(localizacion);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setLocalizacion(java.lang.String)
	 */
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getMunicipio()
	 */
	public String getMunicipio() {
		return nullToEmpty(municipio);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setMunicipio(java.lang.String)
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getNombreSolicitante()
	 */
	public String getNombreSolicitante() {
		return nullToEmpty(nombreSolicitante);
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setNombreSolicitante(java.lang.String)
	 */
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getNombreTitularCuenta()
	 */
	public String getNombreTitularCuenta() {
		return nombreTitularCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setNombreTitularCuenta(java.lang.String)
	 */
	public void setNombreTitularCuenta(String nombreTitularCuenta) {
		this.nombreTitularCuenta = nombreTitularCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getNumeroIdentificacion()
	 */
	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setNumeroIdentificacion(java.lang.String)
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getTelefono()
	 */
	public String getTelefono() {
		return telefono;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setTelefono(java.lang.String)
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getTipoIdentificacion()
	 */
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setTipoIdentificacion(java.lang.String)
	 */
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getUsuarioSistema()
	 */
	public String getUsuarioSistema() {
		return usuarioSistema;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setUsuarioSistema(java.lang.String)
	 */
	public void setUsuarioSistema(String usuarioSistema) {
		this.usuarioSistema = usuarioSistema;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#getApellidoTitularCuenta()
	 */
	public String getApellidoTitularCuenta() {
		return nullToEmpty(apellidoTitularCuenta);
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ReporteCertificaBuenCliente#setApellidoTitularCuenta(java.lang.String)
	 */
	public void setApellidoTitularCuenta(String apellidoTitularCuenta) {
		this.apellidoTitularCuenta = apellidoTitularCuenta;
	}
	public Long getNroDocumentoTitularCuenta() {
		return nroDocumentoTitularCuenta;
	}
	public void setNroDocumentoTitularCuenta(Long nroDocumentoTitularCuenta) {
		this.nroDocumentoTitularCuenta = nroDocumentoTitularCuenta;
	}

}
