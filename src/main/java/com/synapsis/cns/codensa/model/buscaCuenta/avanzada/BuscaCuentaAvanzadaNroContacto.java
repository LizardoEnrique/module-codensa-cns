package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaNroContacto extends SynergiaBusinessObject {

	/**
	 * @return the nroContacto
	 */
	public Integer getNroContacto();

	/**
	 * @param nroContacto the nroContacto to set
	 */
	public void setNroContacto(Integer nroContacto);

}