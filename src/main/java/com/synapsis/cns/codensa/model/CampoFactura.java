package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author jhack
 */
public interface CampoFactura extends SynergiaBusinessObject {
	
	public String getTipoDocumento() ;
	public void setTipoDocumento(String tipoDocumento);
	public String getNombre();
	public void setNombre(String nombre);
	public String getDescripcion();
	public void setDescripcion(String descripcion);
	public Long getPosInicio();
	public void setPosInicio(Long posInicio);
	public Long getLongitud();
	public void setLongitud(Long longitud);
	public Long getPosFin();
	public void setPosFin(Long posFin);
	public Long getVersion();
	public void setVersion(Long version);
	public String getTipoInfo();
	public void setTipoInfo(String tipoInfo);
}
