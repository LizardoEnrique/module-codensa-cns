package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author jhv
 */

public interface FacturaInfoConsumos extends SynergiaBusinessObject {
	

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public String getTarifa();

	public void setTarifa(String tarifa);

	public Double getLectActual();

	public void setLectActual(Double lectActual);

	public Double getLectAnterior();

	public void setLectAnterior(Double lectAnterior);

	public Double getDifLecturas();

	public void setDifLecturas(Double difLecturas);

	public Double getFactorLiq();

	public void setFactorLiq(Double factorLiq);

	public Double getTotalConsKwh();

	public void setTotalConsKwh(Double totalConsKwh);

	public Double getEnergiaCons();

	public void setEnergiaCons(Double energiaCons);

	public Double getEnergiaFac();

	public void setEnergiaFac(Double energiaFac);

	public Double getValorPromKwh();

	public void setValorPromKwh(Double valorPromKwh);

	public String getMesTarifa();

	public void setMesTarifa(String mesTarifa);

	public Double getHrsIntrPermMax();

	public void setHrsIntrPermMax(Double hrsIntrPermMax);

	public Double getHrsIntrAcum();

	public void setHrsIntrAcum(Double hrsIntrAcum);

	public Double getNroIntrPermMax();

	public void setNroIntrPermMax(Double nroIntrPermMax);

	public Double getNroIntrAcum();

	public void setNroIntrAcum(Double nroIntrAcum);

	public Double getConsPromUlt6();

	public void setConsPromUlt6(Double consPromUlt6);

	public Double getGeneracion();

	public void setGeneracion(Double generacion);

	public Double getTransmision();

	public void setTransmision(Double transmision);

	public Double getPerdidas();

	public void setPerdidas(Double perdidas);

	public Double getDistribucion();

	public void setDistribucion(Double distribucion);

	public Double getOtros();

	public void setOtros(Double otros);

	public Double getComercializacion();

	public void setComercializacion(Double comercializacion);

	public Double getCostoUnitario();

	public void setCostoUnitario(Double costoUnitario);

	public String getTipoLectActual();

	public void setTipoLectActual(String tipoLectActual);

	public String getAnomaliaLectActual();

	public void setAnomaliaLectActual(String anomaliaLectActual);

}