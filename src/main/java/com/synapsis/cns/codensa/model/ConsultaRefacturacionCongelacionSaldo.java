/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 016
 */
public interface ConsultaRefacturacionCongelacionSaldo extends
		SynergiaBusinessObject {
	
	public Long getIdCuenta() ;
	public Long getIdOperacion();
	public Long getNroCuenta() ;
	public Long getNroOperacion();
	public Double getSaldoEnergiaAntes();
	public Double getSaldoEnergiaDespues();
	public Double getSaldoOtrosNegociosAntes();
	public Double getSaldoOtrosNegociosDespues();
	public Double getSaldoTotalAntes() ;
	public Double getSaldoTotalDespues();
	public Double getValorEnergia() ;
	public Double getValorOtrosNegocios();

}
