package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.5 $ $Date: 2009/10/29 18:52:01 $
 *
 */
public interface EstadoCobranza extends SynergiaBusinessObject {

	
	public Long getNumeroCartera();
	
	public void setNumeroCartera(Long numeroCartera);
	
	
	/**
	 * @return the antiguedad
	 */
	public Long getAntiguedad() ;
	/**
	 * @param antiguedad the antiguedad to set
	 */
	public void setAntiguedad(Long antiguedad);
	/**
	 * @return the caracterContacto
	 */
	public String getCaracterContacto();
	/**
	 * @param caracterContacto the caracterContacto to set
	 */
	public void setCaracterContacto(String caracterContacto);
	/**
	 * @return the deuda
	 */
	public String getDeuda();
	/**
	 * @param deuda the deuda to set
	 */
	public void setDeuda(String deuda);
	/**
	 * @return the estadoAnterior
	 */
	public String getEstadoAnterior();
	/**
	 * @param estadoAnterior the estadoAnterior to set
	 */
	public void setEstadoAnterior(String estadoAnterior);
	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza();
	/**
	 * @param estadoCobranza the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza);
	/**
	 * @return the fechaDemanda
	 */
	public Date getFechaDemanda();
	/**
	 * @param fechaDemanda the fechaDemanda to set
	 */
	public void setFechaDemanda(Date fechaDemanda);
	/**
	 * @return the fechaHonorarioJuridico
	 */
	public Date getFechaHonorarioJuridico();
	/**
	 * @param fechaHonorarioJuridico the fechaHonorarioJuridico to set
	 */
	public void setFechaHonorarioJuridico(Date fechaHonorarioJuridico);
	/**
	 * @return the fechaHonorarioPrejuridico
	 */
	public Date getFechaHonorarioPrejuridico();
	/**
	 * @param fechaHonorarioPrejuridico the fechaHonorarioPrejuridico to set
	 */
	public void setFechaHonorarioPrejuridico(Date fechaHonorarioPrejuridico);
	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion();
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion);
	/**
	 * @return the firmaAbogado
	 */
	public String getFirmaAbogado();
	/**
	 * @param firmaAbogado the firmaAbogado to set
	 */
	public void setFirmaAbogado(String firmaAbogado);
	/**
	 * @return the honorarioJuridico
	 */
	public String getHonorarioJuridico();
	/**
	 * @param honorarioJuridico the honorarioJuridico to set
	 */
	public void setHonorarioJuridico(String honorarioJuridico);
	/**
	 * @return the honorarioPrejuridico
	 */
	public String getHonorarioPrejuridico();
	/**
	 * @param honorarioPrejuridico the honorarioPrejuridico to set
	 */
	public void setHonorarioPrejuridico(String honorarioPrejuridico);
	/**
	 * @return the nombre
	 */
	public String getNombre();
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre);
	/**
	 * @return the nombreContacto
	 */
	public String getNombreContacto();
	/**
	 * @param nombreContacto the nombreContacto to set
	 */
	public void setNombreContacto(String nombreContacto);
	/**
	 * @return the nombreEmpresaProcEsp
	 */
	public String getNombreEmpresaProcEsp();
	/**
	 * @param nombreEmpresaProcEsp the nombreEmpresaProcEsp to set
	 */
	public void setNombreEmpresaProcEsp(String nombreEmpresaProcEsp);
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);
	/**
	 * @return the observacion
	 */
	public String getObservacion();
	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion);
	/**
	 * @return the restriccionConvenio
	 */
	public String getRestriccionConvenio();
	/**
	 * @param restriccionConvenio the restriccionConvenio to set
	 */
	public void setRestriccionConvenio(String restriccionConvenio);
	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();
	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);
	/**
	 * @return the ubicacionJuridicional
	 */
	public String getUbicacionJuridicional();
	/**
	 * @param ubicacionJuridicional the ubicacionJuridicional to set
	 */
	public void setUbicacionJuridicional(String ubicacionJuridicional);
	/**
	 * @return the usuario
	 */
	public String getUsuario();
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario);
	
	
	public String getEtapaCobranza();
	public void setEtapaCobranza(String etapaCobranza);

}