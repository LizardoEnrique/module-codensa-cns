package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaExpedienteDetalleActa {

	public Long getNumeroExpediente();
	public void setNumeroExpediente(Long numeroExpediente);
	
	/**
	 * @return the calidadActua
	 */
	public String getCalidadActua();

	/**
	 * @param calidadActua the calidadActua to set
	 */
	public void setCalidadActua(String calidadActua);

	/**
	 * @return the estadoActa
	 */
	public String getEstadoActa();

	/**
	 * @param estadoActa the estadoActa to set
	 */
	public void setEstadoActa(String estadoActa);

	/**
	 * @return the fechaActa
	 */
	public Date getFechaActa();

	/**
	 * @param fechaActa the fechaActa to set
	 */
	public void setFechaActa(Date fechaActa);

	/**
	 * @return the indicadorCondonacion
	 */
	public String getIndicadorCondonacion();

	/**
	 * @param indicadorCondonacion the indicadorCondonacion to set
	 */
	public void setIndicadorCondonacion(String indicadorCondonacion);

	/**
	 * @return the indicadorConvenio
	 */
	public String getIndicadorConvenio();

	/**
	 * @param indicadorConvenio the indicadorConvenio to set
	 */
	public void setIndicadorConvenio(String indicadorConvenio);

	/**
	 * @return the nombreSuscribioActaCliente
	 */
	public String getNombreSuscribioActaCliente();

	/**
	 * @param nombreSuscribioActaCliente the nombreSuscribioActaCliente to set
	 */
	public void setNombreSuscribioActaCliente(String nombreSuscribioActaCliente);

	/**
	 * @return the nombreSuscribioActaEmpresa
	 */
	public String getNombreSuscribioActaEmpresa();

	/**
	 * @param nombreSuscribioActaEmpresa the nombreSuscribioActaEmpresa to set
	 */
	public void setNombreSuscribioActaEmpresa(String nombreSuscribioActaEmpresa);

	/**
	 * @return the numeroActa
	 */
	public Long getNumeroActa();

	/**
	 * @param numeroActa the numeroActa to set
	 */
	public void setNumeroActa(Long numeroActa);

	/**
	 * @return the numeroCuotas
	 */
	public Long getNumeroCuotas();

	/**
	 * @param numeroCuotas the numeroCuotas to set
	 */
	public void setNumeroCuotas(Long numeroCuotas);

	/**
	 * @return the numeroIdentificacionCliente
	 */
	public String getNumeroIdentificacionCliente();

	/**
	 * @param numeroIdentificacionCliente the numeroIdentificacionCliente to set
	 */
	public void setNumeroIdentificacionCliente(String numeroIdentificacionCliente);

	/**
	 * @return the tipoExpediente
	 */
	public String getTipoExpediente();

	/**
	 * @param tipoExpediente the tipoExpediente to set
	 */
	public void setTipoExpediente(String tipoExpediente);

	/**
	 * @return the tipoIdentificacionCliente
	 */
	public String getTipoIdentificacionCliente();

	/**
	 * @param tipoIdentificacionCliente the tipoIdentificacionCliente to set
	 */
	public void setTipoIdentificacionCliente(String tipoIdentificacionCliente);

	/**
	 * @return the valorCondonacion
	 */
	public Double getValorCondonacion();

	/**
	 * @param valorCondonacion the valorCondonacion to set
	 */
	public void setValorCondonacion(Double valorCondonacion);

	/**
	 * @return the valorFinalActa
	 */
	public Double getValorFinalActa();

	/**
	 * @param valorFinalActa the valorFinalActa to set
	 */
	public void setValorFinalActa(Double valorFinalActa);

	/**
	 * @return the valorInicialActa
	 */
	public Double getValorInicialActa();

	/**
	 * @param valorInicialActa the valorInicialActa to set
	 */
	public void setValorInicialActa(Double valorInicialActa);

}