package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.synapsis.cns.codensa.model.ItemServicioFinancieroCCA;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Itenm del Servicio Financiero CCA para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ItemServicioFinancieroCCAImpl extends SynergiaBusinessObjectImpl implements ItemServicioFinancieroCCA{
	private static final long serialVersionUID = 1L;

	// Datos comunes 
	private String titular;
	private Long nroServicio;
	private String tipoProducto;
	
	// Datos para agrupar y mostrar por servicio CCA ... 
	private BigDecimal pagoMinimo;
	private BigDecimal saldoEnMora;
	private BigDecimal interesPorMora;
	private BigDecimal pagoTotal;
	private Double tasaInteres;
	private Long cupoTotal;
	private Long cupoDisponible;

	// Datos de cada plan ...
	private Date fecha;
	private String descripcion;
	private Long cuotaActual;
	private Long totalDeCuotas;
	private BigDecimal valorCompra;
	private BigDecimal cuotaACapital;
	private BigDecimal cuotaAInteres;
	private BigDecimal saldoAPagar;
	private BigDecimal valorCuota;
	
	
	
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public BigDecimal getPagoMinimo() {
		return pagoMinimo;
	}
	public void setPagoMinimo(BigDecimal pagoMinimo) {
		this.pagoMinimo = pagoMinimo;
	}
	public BigDecimal getSaldoEnMora() {
		return saldoEnMora;
	}
	public void setSaldoEnMora(BigDecimal saldoEnMora) {
		this.saldoEnMora = saldoEnMora;
	}
	public BigDecimal getInteresPorMora() {
		return interesPorMora;
	}
	public void setInteresPorMora(BigDecimal interesPorMora) {
		this.interesPorMora = interesPorMora;
	}
	public BigDecimal getPagoTotal() {
		return pagoTotal;
	}
	public void setPagoTotal(BigDecimal pagoTotal) {
		this.pagoTotal = pagoTotal;
	}
	public Double getTasaInteres() {
		return tasaInteres;
	}
	public void setTasaInteres(Double tasaInteres) {
		this.tasaInteres = tasaInteres;
	}
	public Long getCupoTotal() {
		return cupoTotal;
	}
	public void setCupoTotal(Long cupoTotal) {
		this.cupoTotal = cupoTotal;
	}
	public Long getCupoDisponible() {
		return cupoDisponible;
	}
	public void setCupoDisponible(Long cupoDisponible) {
		this.cupoDisponible = cupoDisponible;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getCuotaActual() {
		return cuotaActual;
	}
	public void setCuotaActual(Long cuotaActual) {
		this.cuotaActual = cuotaActual;
	}
	public Long getTotalDeCuotas() {
		return totalDeCuotas;
	}
	public void setTotalDeCuotas(Long totalDeCuotas) {
		this.totalDeCuotas = totalDeCuotas;
	}
	public BigDecimal getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(BigDecimal valorCompra) {
		this.valorCompra = valorCompra;
	}
	public BigDecimal getCuotaACapital() {
		return cuotaACapital;
	}
	public void setCuotaACapital(BigDecimal cuotaACapital) {
		this.cuotaACapital = cuotaACapital;
	}
	public BigDecimal getCuotaAInteres() {
		return cuotaAInteres;
	}
	public void setCuotaAInteres(BigDecimal cuotaAInteres) {
		this.cuotaAInteres = cuotaAInteres;
	}
	public BigDecimal getSaldoAPagar() {
		return saldoAPagar;
	}
	public void setSaldoAPagar(BigDecimal saldoAPagar) {
		this.saldoAPagar = saldoAPagar;
	}
	public BigDecimal getValorCuota() {
		return valorCuota;
	}
	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	

}