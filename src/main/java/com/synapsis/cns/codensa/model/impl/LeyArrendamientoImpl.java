package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.LeyArrendamiento;

public class LeyArrendamientoImpl extends SynergiaBusinessObjectImpl 
	implements LeyArrendamiento {

    private Long nroCuenta;
    private Boolean contratoVigente ;
    private String tipoResidente;
    private Boolean propiedadMedidorResidente;
    private Boolean facturaConsumo;
    private Boolean emiteFactura;
    private String numeroCuentaInquilino;
    private String nombreInquilino;
    private String ApellidoInquilino;
    private String numeroContrato;
    private Date fechaVencimientoContrato;

    private Long numeroCuentaArrendatario;
    private Long numeroCuentaPropietario;

	public Long getNumeroCuentaArrendatario() {
		return numeroCuentaArrendatario;
	}

	public void setNumeroCuentaArrendatario(Long numeroCuentaArrendatario) {
		this.numeroCuentaArrendatario = numeroCuentaArrendatario;
	}

	public Long getNumeroCuentaPropietario() {
		return numeroCuentaPropietario;
	}

	public void setNumeroCuentaPropietario(Long numeroCuentaPropietario) {
		this.numeroCuentaPropietario = numeroCuentaPropietario;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
    
    
	public String getApellidoInquilino() {
		return ApellidoInquilino;
	}

	public void setApellidoInquilino(String apellidoInquilino) {
		ApellidoInquilino = apellidoInquilino;
	}

	public Boolean getContratoVigente() {
		return contratoVigente;
	}

	public void setContratoVigente(Boolean contratoVigente) {
		this.contratoVigente = contratoVigente;
	}


	public Boolean getFacturaConsumo() {
		return facturaConsumo;
	}

	public void setFacturaConsumo(Boolean facturaConsumo) {
		this.facturaConsumo = facturaConsumo;
	}

	public Boolean getEmiteFactura() {
		return emiteFactura;
	}

	public void setEmiteFactura(Boolean emiteFactura) {
		this.emiteFactura = emiteFactura;
	}

	public Date getFechaVencimientoContrato() {
		return fechaVencimientoContrato;
	}

	public void setFechaVencimientoContrato(Date fechaVencimientoContrato) {
		this.fechaVencimientoContrato = fechaVencimientoContrato;
	}

	public String getNombreInquilino() {
		return nombreInquilino;
	}

	public void setNombreInquilino(String nombreInquilino) {
		this.nombreInquilino = nombreInquilino;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getNumeroCuentaInquilino() {
		return numeroCuentaInquilino;
	}

	public void setNumeroCuentaInquilino(String numeroCuentaInquilino) {
		this.numeroCuentaInquilino = numeroCuentaInquilino;
	}

	public Boolean getPropiedadMedidorResidente() {
		return propiedadMedidorResidente;
	}

	public void setPropiedadMedidorResidente(Boolean propiedadMedidorResidente) {
		this.propiedadMedidorResidente = propiedadMedidorResidente;
	}

	public String getTipoResidente() {
		return tipoResidente;
	}

	public void setTipoResidente(String tipoResidente) {
		this.tipoResidente = tipoResidente;
	}

}
