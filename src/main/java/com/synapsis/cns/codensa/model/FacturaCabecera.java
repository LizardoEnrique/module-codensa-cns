package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface FacturaCabecera extends SynergiaBusinessObject {

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public String getNit();

	public void setNit(String nit);

	public String getOfPpalCod();

	public void setOfPpalCod(String ofPpalCod);

	public Date getFechaExpedicion();

	public void setFechaExpedicion(Date fechaExpedicion);

	public Date getPeriodoFacIni();

	public void setPeriodoFacIni(Date periodoFacIni);

	public Date getPeriodoFacFin();

	public void setPeriodoFacFin(Date periodoFacFin);

	public String getClienteNombre();

	public void setClienteNombre(String clienteNombre);

	public String getClienteDireccion();

	public void setClienteDireccion(String clienteDireccion);

	public String getClienteTel();

	public void setClienteTel(String clienteTel);

	public String getClienteBarrio();

	public void setClienteBarrio(String clienteBarrio);

	public String getEstratoSocioeco();

	public void setEstratoSocioeco(String estratoSocioeco);

	public String getTipoSrv();

	public void setTipoSrv(String tipoSrv);

	public String getRutaLectura();

	public void setRutaLectura(String rutaLectura);

	public String getCargaContratada();

	public void setCargaContratada(String cargaContratada);

	public Double getNivelTension();

	public void setNivelTension(Double nivelTension);

	public String getNroMedidor_1();

	public void setNroMedidor_1(String nroMedidor_1);

	public String getNroMedidor_2();

	public void setNroMedidor_2(String nroMedidor_2);

	public String getNroCircuito();

	public void setNroCircuito(String nroCircuito);

	public Date getVtoPagoOportuno();

	public void setVtoPagoOportuno(Date vtoPagoOportuno);

	public Date getVtoAvisoSuspension();

	public void setVtoAvisoSuspension(Date vtoAvisoSuspension);

	public String getCodigoBarras();

	public void setCodigoBarras(String codigoBarras);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
}