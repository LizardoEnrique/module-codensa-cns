package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DatosUltimaFacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosUltimaFacturacionImpl extends SynergiaBusinessObjectImpl implements DatosUltimaFacturacion {
	private static final long serialVersionUID = 1L;

	private Long nroCuenta;
	private Long nroServicio;
	private Date fechaFactura;
	private Date fechaVencimiento;
	private Long factura;
	private String saldoAnterior;
	private String totalDocumento;
	private String tipoLiquidacion = "Mensual";
	private String tipoEsquemaFacturacion = "Tradicional";

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getFactura() {
		return factura;
	}

	public void setFactura(Long factura) {
		this.factura = factura;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public String getSaldoAnterior() {
		return saldoAnterior;
	}

	public void setSaldoAnterior(String saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public String getTotalDocumento() {
		return totalDocumento;
	}

	public void setTotalDocumento(String totalDocumento) {
		this.totalDocumento = totalDocumento;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public String getTipoEsquemaFacturacion() {
		return tipoEsquemaFacturacion;
	}

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion) {
		this.tipoEsquemaFacturacion = tipoEsquemaFacturacion;
	}

}
