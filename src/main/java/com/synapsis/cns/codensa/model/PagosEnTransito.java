package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public interface PagosEnTransito extends SynergiaBusinessObject {

	/**
	 * @return the entidadRecaudadora
	 */
	public String getEntidadRecaudadora();

	/**
	 * @return the estadoArchivo
	 */
	public String getEstadoArchivo();

	/**
	 * @return the fechaPago
	 */
	public Date getFechaPago();

	/**
	 * @return the procedencia
	 */
	public String getProcedencia();

	/**
	 * @param entidadRecaudadora the entidadRecaudadora to set
	 */
	public void setEntidadRecaudadora(String entidadRecaudadora);

	/**
	 * @param estadoArchivo the estadoArchivo to set
	 */
	public void setEstadoArchivo(String estadoArchivo);

	/**
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago);

	/**
	 * @param procedencia the procedencia to set
	 */
	public void setProcedencia(String procedencia);

	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);	

	public Double getValor();
	
	public void setValor(Double valor);

	public Date getFechaHoraAlmacenamiento();
	public void setFechaHoraAlmacenamiento(Date fechaHoraAlmacenamiento);	
}