/**
 *
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaLecturaConsumoMedidor;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 * CU 006
 */
public class ConsultaLecturaConsumoMedidorImpl extends SynergiaBusinessObjectImpl implements ConsultaLecturaConsumoMedidor{

	private Long idCuenta;
	private Long nroCuenta;
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroMedidor;
	private String anioFabricacionMedidor;
	private String tipoMedida;
	private String tipoMedidor;
	private String nivelTension;
	private String estadoMedidor;
	private String factorMedidor;
	private Long numeroEnteroMedidor;
	private Long numeroDecimalesMedidor;
	private Long cargaContratada;
	private Long constanteEnergia;
	private Long constanteDemanda;
	private Long constanteReactivaHoraPunta;
	private Long numeroEnterosDemanda;

	private String evento;
	private Date fechaEvento;
	private String usuarioEvento;
	private String nombreUsuarioEvento;


	public Long getIdCuenta() {
		return idCuenta;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getAnioFabricacionMedidor() {
		return anioFabricacionMedidor;
	}
	public void setAnioFabricacionMedidor(String anioFabricacionMedidor) {
		this.anioFabricacionMedidor = anioFabricacionMedidor;
	}
	public String getEstadoMedidor() {
		return estadoMedidor;
	}
	public void setEstadoMedidor(String estadoMedidor) {
		this.estadoMedidor = estadoMedidor;
	}
	public String getFactorMedidor() {
		return factorMedidor;
	}
	public void setFactorMedidor(String factorMedidor) {
		this.factorMedidor = factorMedidor;
	}
	public String getMarcaMedidor() {
		return marcaMedidor;
	}
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	public String getModeloMedidor() {
		return modeloMedidor;
	}
	public void setModeloMedidor(String modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}
	public String getNivelTension() {
		return nivelTension;
	}
	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}
	public String getNumeroMedidor() {
		return numeroMedidor;
	}
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	public String getTipoMedida() {
		return tipoMedida;
	}
	public void setTipoMedida(String tipoMedida) {
		this.tipoMedida = tipoMedida;
	}
	public String getTipoMedidor() {
		return tipoMedidor;
	}
	public void setTipoMedidor(String tipoMedidor) {
		this.tipoMedidor = tipoMedidor;
	}
	public Long getCargaContratada() {
		return cargaContratada;
	}
	public void setCargaContratada(Long cargaContratada) {
		this.cargaContratada = cargaContratada;
	}
	public Long getConstanteDemanda() {
		return constanteDemanda;
	}
	public void setConstanteDemanda(Long constanteDemanda) {
		this.constanteDemanda = constanteDemanda;
	}
	public Long getConstanteEnergia() {
		return constanteEnergia;
	}
	public void setConstanteEnergia(Long constanteEnergia) {
		this.constanteEnergia = constanteEnergia;
	}
	public Long getConstanteReactivaHoraPunta() {
		return constanteReactivaHoraPunta;
	}
	public void setConstanteReactivaHoraPunta(Long constanteReactivaHoraPunta) {
		this.constanteReactivaHoraPunta = constanteReactivaHoraPunta;
	}
	public Long getNumeroDecimalesMedidor() {
		return numeroDecimalesMedidor;
	}
	public void setNumeroDecimalesMedidor(Long numeroDecimalesMedidor) {
		this.numeroDecimalesMedidor = numeroDecimalesMedidor;
	}
	public Long getNumeroEnteroMedidor() {
		return numeroEnteroMedidor;
	}
	public void setNumeroEnteroMedidor(Long numeroEnteroMedidor) {
		this.numeroEnteroMedidor = numeroEnteroMedidor;
	}
	public Long getNumeroEnterosDemanda() {
		return numeroEnterosDemanda;
	}
	public void setNumeroEnterosDemanda(Long numeroEnterosDemanda) {
		this.numeroEnterosDemanda = numeroEnterosDemanda;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public Date getFechaEvento() {
		return fechaEvento;
	}
	public void setFechaEvento(Date fechaEvento) {
		this.fechaEvento = fechaEvento;
	}
	public String getUsuarioEvento() {
		return usuarioEvento;
	}
	public void setUsuarioEvento(String usuarioEvento) {
		this.usuarioEvento = usuarioEvento;
	}
	public String getNombreUsuarioEvento() {
		return nombreUsuarioEvento;
	}
	public void setNombreUsuarioEvento(String nombreUsuarioEvento) {
		this.nombreUsuarioEvento = nombreUsuarioEvento;
	}

}
