package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteRadicacionesDetalle extends SynergiaBusinessObject{

	/**
	 * @return the fechaRadicacion
	 */
	public Date getFechaRadicacion();

	/**
	 * @param fechaRadicacion the fechaRadicacion to set
	 */
	public void setFechaRadicacion(Date fechaRadicacion);

	/**
	 * @return the numeroRadicacion
	 */
	public Long getNumeroRadicacion();

	/**
	 * @param numeroRadicacion the numeroRadicacion to set
	 */
	public void setNumeroRadicacion(Long numeroRadicacion);

	/**
	 * @return the radicacion
	 */
	public Date getRadicacion();

	/**
	 * @param radicacion the radicacion to set
	 */
	public void setRadicacion(Date radicacion);

	/**
	 * @return the tipoRadicacion
	 */
	public String getTipoRadicacion();

	/**
	 * @param tipoRadicacion the tipoRadicacion to set
	 */
	public void setTipoRadicacion(String tipoRadicacion);
	
	public Long getNumeroExpediente();
	
	public void setNumeroExpediente(Long numeroExpediente);

}