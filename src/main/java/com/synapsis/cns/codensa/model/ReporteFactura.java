package com.synapsis.cns.codensa.model;

import java.util.Date;

/**
 * Interfaz del reporte de duplicado factura (CNS-003)
 * 
 * @author adambrosio
 */
public interface ReporteFactura extends ReporteFacturaComun {

	/**
	 * Este Mensaje se lo envia un Cliente al objeto ReporteFactura para poder cargar la manija.
	 * 
	 * Saludos.
	 */
	public void cargarLaManija();

	public Date getFechaExpedicion();

	public void setFechaExpedicion(Date fechaExpedicion);

	public Date getFechaVencimientoAvisoSuspension();

	public void setFechaVencimientoAvisoSuspension(Date fechaVencimientoAvisoSuspension);

	public Date getFechaVencimientoPagoOportuno();

	public void setFechaVencimientoPagoOportuno(Date fechaVencimientoPagoOportuno);

	public Date getPeriodoFacturadoFin();

	public void setPeriodoFacturadoFin(Date periodoFacturadoFin);

	public Date getPeriodoFacturadoInicio();

	public void setPeriodoFacturadoInicio(Date periodoFacturadoInicio);

	public Date getFechaProceso();

	public void setFechaProceso(Date fechaProceso);

	public InformacionConsumos getInfoCons();

	public void setInfoCons(InformacionConsumos infoCons);

	public FacturaTotales getTotalesFactura();

	public void setTotalesFactura(FacturaTotales totalesFactura);

	public Long getTotalAPagar();

	public void setTotalAPagar(Long totalAPagar);
}
