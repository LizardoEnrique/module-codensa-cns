package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.AuditoriaFacturacion;
import com.synapsis.cns.codensa.model.Motivo;
import com.synapsis.synergia.core.model.impl.SynergiaPersistentObjectImpl;

/**
 * @author adambrosio
 *
 */
public class AuditoriaFacturacionImpl extends SynergiaPersistentObjectImpl
		implements AuditoriaFacturacion {
	private String usuarioNombre;
	private Date fecha;
	private Long cantCopias;
	private Long nroCuenta;
	private Long nroFactura;
	private String observacion;
	private Long idServicio;
	private Long idCuenta;
	private String periodo;
	private Long nroServicio;
	private MotivoImpl motivo;
	
	/**
	 * @return the motivo
	 */
	public Motivo getMotivo() {
		return motivo;
	}
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(Motivo motivo) {
		this.motivo = (MotivoImpl)motivo;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/**
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}
	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	/**
	 * @return the idServicio
	 */
	public Long getIdServicio() {
		return idServicio;
	}
	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public Long getCantCopias() {
		return this.cantCopias;
	}
	public void setCantCopias(Long cantCopias) {
		this.cantCopias = cantCopias;
	}
	public Date getFecha() {
		return this.fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Long getNroFactura() {
		return this.nroFactura;
	}
	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}
	public String getObservacion() {
		return this.observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getEntityName() {
		return "auditoriaFacturacion";
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the usuarioNombre
	 */
	public String getUsuarioNombre() {
		return usuarioNombre;
	}
	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	
}
