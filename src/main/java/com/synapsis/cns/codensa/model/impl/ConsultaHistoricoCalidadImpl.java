package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoricoCalidad;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
/**
 * 
 * @author lcanas
 */
public class ConsultaHistoricoCalidadImpl extends SynergiaBusinessObjectImpl implements ConsultaHistoricoCalidad {

	private Empresa empresa;
	private Long idCalidadIndiv;
	private String referencia;
	private  Date fechaIncidencia;
	private String nroTransformador;
	private Long duracion;
	
	
	//_______________________________________ SETTERS && GETTERS ______________________________
	public String getNroTransformador() {
		return nroTransformador;
	}
	public void setNroTransformador(String nroTransformador) {
		this.nroTransformador = nroTransformador;
	}
	public Long getDuracion() {
		return duracion;
	}
	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}
	public Date getFechaIncidencia() {
		return fechaIncidencia;
	}
	public void setFechaIncidencia(Date fechaIncidencia) {
		this.fechaIncidencia = fechaIncidencia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Long getIdCalidadIndiv() {
		return idCalidadIndiv;
	}
	public void setIdCalidadIndiv(Long idCalidadIndiv) {
		this.idCalidadIndiv = idCalidadIndiv;
	}


}
