package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosTitular extends SynergiaBusinessObject{

	/**
	 * @return the dirPredio
	 */
	public abstract String getDirPredio();

	/**
	 * @param dirPredio the dirPredio to set
	 */
	public abstract void setDirPredio(String dirPredio);

	/**
	 * @return the distrito
	 */
	public abstract String getDistrito();

	/**
	 * @param distrito the distrito to set
	 */
	public abstract void setDistrito(String distrito);

	/**
	 * @return the documentoB
	 */
	public abstract String getDocumentoB();

	/**
	 * @param documentoB the documentoB to set
	 */
	public abstract void setDocumentoB(String documentoB);

	/**
	 * @return the email
	 */
	public abstract String getEmail();

	/**
	 * @param email the email to set
	 */
	public abstract void setEmail(String email);

	/**
	 * @return the nombreB
	 */
	public abstract String getNombreB();

	/**
	 * @param nombreB the nombreB to set
	 */
	public abstract void setNombreB(String nombreB);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the rutaLec
	 */
	public abstract String getRutaLec();

	/**
	 * @param rutaLec the rutaLec to set
	 */
	public abstract void setRutaLec(String rutaLec);

	/**
	 * @return the telefonoB
	 */
	public abstract String getTelefonoB();

	/**
	 * @param telefonoB the telefonoB to set
	 */
	public abstract void setTelefonoB(String telefonoB);

	/**
	 * @return the dirDomicilio
	 */
	public String getDirDomicilio();
	/**
	 * @param dirDomicilio the dirDomicilio to set
	 */
	public void setDirDomicilio(String dirDomicilio);
	
	
	/**
	 * @return the idPersona
	 */
	public abstract Long getIdPersona() ;

	/**
	 * @param idPersona the idPersona to set
	 */
	public abstract void setIdPersona(Long idPersona) ;
	
	public String getBarrioLocalizacion();

	public void setBarrioLocalizacion(String barrioLocalizacion);
	
}