package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOtrosNegocios;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaOtrosNegociosImpl extends SynergiaBusinessObjectImpl
		implements ConsultaOtrosNegocios {
	private Long nroCuenta;
	private Long id2;
	private Long idCuenta;
	private Long idItemDoc;
	private String codCargo;
	private String desCargo;
	private Double valorCargo;
	private Double saldoCargo;
	private String correlativoFacturacion;
	private Date fecFactura;
	private String periodoFacturacion;
	private String tipoDocumento;
	private Long nroDocumento;
	private String origenDeuda;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getId2() {
		return id2;
	}

	public void setId2(Long id2) {
		this.id2 = id2;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Long getIdItemDoc() {
		return idItemDoc;
	}

	public void setIdItemDoc(Long idItemDoc) {
		this.idItemDoc = idItemDoc;
	}

	public String getCodCargo() {
		return codCargo;
	}

	public void setCodCargo(String codCargo) {
		this.codCargo = codCargo;
	}

	public String getDesCargo() {
		return desCargo;
	}

	public void setDesCargo(String desCargo) {
		this.desCargo = desCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public Double getSaldoCargo() {
		return saldoCargo;
	}

	public void setSaldoCargo(Double saldoCargo) {
		this.saldoCargo = saldoCargo;
	}

	public String getCorrelativoFacturacion() {
		return correlativoFacturacion;
	}

	public void setCorrelativoFacturacion(String correlativoFacturacion) {
		this.correlativoFacturacion = correlativoFacturacion;
	}

	public Date getFecFactura() {
		return fecFactura;
	}

	public void setFecFactura(Date fecFactura) {
		this.fecFactura = fecFactura;
	}

	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Long getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getOrigenDeuda() {
		return origenDeuda;
	}

	public void setOrigenDeuda(String origenDeuda) {
		this.origenDeuda = origenDeuda;
	}

}
