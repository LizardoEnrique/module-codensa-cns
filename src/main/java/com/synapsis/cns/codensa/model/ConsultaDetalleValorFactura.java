package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaDetalleValorFactura {

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion();

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion);

	/**
	 * @return the nombreCargo
	 */
	public String getNombreCargo();

	/**
	 * @param nombreCargo the nombreCargo to set
	 */
	public void setNombreCargo(String nombreCargo);

	/**
	 * @return the nombreGenerador
	 */
	public String getNombreGenerador();

	/**
	 * @param nombreGenerador the nombreGenerador to set
	 */
	public void setNombreGenerador(String nombreGenerador);

	/**
	 * @return the numeroDocumento
	 */
	public Long getNumeroDocumento();

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(Long numeroDocumento);

	/**
	 * @return the numeroUnidades
	 */
	public Long getNumeroUnidades();

	/**
	 * @param numeroUnidades the numeroUnidades to set
	 */
	public void setNumeroUnidades(Long numeroUnidades);

	/**
	 * @return the unidad
	 */
	public String getUnidad();

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad);

	/**
	 * @return the usuarioGenerador
	 */
	public String getUsuarioGenerador();

	/**
	 * @param usuarioGenerador the usuarioGenerador to set
	 */
	public void setUsuarioGenerador(String usuarioGenerador);

	/**
	 * @return the valorCargo
	 */
	public Double getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(Double valorCargo);

	public void setIdCuenta(Long idCuenta);
	
	public Long getIdCuenta();

}