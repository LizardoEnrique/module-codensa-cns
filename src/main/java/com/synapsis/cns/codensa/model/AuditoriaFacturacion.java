package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.suivant.arquitectura.core.security.model.User;
import com.synapsis.nuc.codensa.model.Cuenta;
import com.synapsis.nuc.codensa.model.Servicio;
import com.synapsis.synergia.core.model.SynergiaPersistentObject;


/**
 * @author adambrosio
 */
public interface AuditoriaFacturacion extends SynergiaPersistentObject {

	public Long getCantCopias();

	public void setCantCopias(Long cantCopias);

	public Date getFecha();

	public void setFecha(Date fecha);

	public Motivo getMotivo();

	public void setMotivo(Motivo motivo);

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public String getObservacion();

	public void setObservacion(String observacion);

	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	public String getUsuarioNombre();
	public void setUsuarioNombre(String usuarioNombre);
	public Long getIdServicio();
	public void setIdServicio(Long idServicio);
	public Long getIdCuenta();
	public void setIdCuenta(Long idCuenta);
	public String getPeriodo();
	public void setPeriodo(String periodo);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
}