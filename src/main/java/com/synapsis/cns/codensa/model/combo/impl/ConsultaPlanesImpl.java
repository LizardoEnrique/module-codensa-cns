/**
 * 
 */
package com.synapsis.cns.codensa.model.combo.impl;

import com.synapsis.cns.codensa.model.combo.ConsultaPlanes;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaPlanesImpl extends SynergiaBusinessObjectImpl implements ConsultaPlanes {
	   private String descripcion;
	   private Long idPlan;

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(Long idPlan) {
		this.idPlan = idPlan;
	}
	
	
	
}
