package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongDetOperacion extends SynergiaBusinessObject {
	

	/**
	 * @return the periodoFacturacion
	 */
	public String getPeriodoFacturacion() ;
	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(String periodoFacturacion);
	/**
	 * @return the codigoItem
	 */
	public String getCodigoItem();
	/**
	 * @param codigoItem the codigoItem to set
	 */
	public void setCodigoItem(String codigoItem);
	/**
	 * @return the descItem
	 */
	public String getDescItem();
	/**
	 * @param descItem the descItem to set
	 */
	public void setDescItem(String descItem) ;
	/**
	 * @return the valorInicial
	 */
	public Long getValorInicial();
	/**
	 * @param valorInicial the valorInicial to set
	 */
	public void setValorInicial(Long valorInicial);
	/**
	 * @return the valorCongelado
	 */
	public Long getValorCongelado();
	/**
	 * @param valorCongelado the valorCongelado to set
	 */
	public void setValorCongelado(Long valorCongelado) ;
	/**
	 * @return the valorAclaracion
	 */
	public Long getValorAclaracion() ;
	/**
	 * @param valorAclaracion the valorAclaracion to set
	 */
	public void setValorAclaracion(Long valorAclaracion) ;
	/**
	 * @return the valorFinal
	 */
	public Long getValorFinal();
	/**
	 * @param valorFinal the valorFinal to set
	 */
	public void setValorFinal(Long valorFinal);
	/**
	 * @return the consumoInicial
	 */
	public Long getConsumoInicial();
	/**
	 * @param consumoInicial the consumoInicial to set
	 */
	public void setConsumoInicial(Long consumoInicial);
	/**
	 * @return the consumoCongelado
	 */
	public Long getConsumoCongelado();
	/**
	 * @param consumoCongelado the consumoCongelado to set
	 */
	public void setConsumoCongelado(Long consumoCongelado);
	/**
	 * @return the consumoAclaracion
	 */
	public Long getConsumoAclaracion();
	/**
	 * @param consumoAclaracion the consumoAclaracion to set
	 */
	public void setConsumoAclaracion(Long consumoAclaracion);
	/**
	 * @return the consumoFinal
	 */
	public Long getConsumoFinal() ;
	/**
	 * @param consumoFinal the consumoFinal to set
	 */
	public void setConsumoFinal(Long consumoFinal) ;
	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado();
	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado);
	/**
	 * @return the consumoDisputado
	 */
	public Long getConsumoDisputado();
	/**
	 * @param consumoDisputado the consumoDisputado to set
	 */
	public void setConsumoDisputado(Long consumoDisputado);
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo();
	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo) ;

	
	
}