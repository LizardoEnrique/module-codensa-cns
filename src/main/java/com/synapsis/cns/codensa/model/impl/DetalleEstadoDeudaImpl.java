package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.DetalleEstadoDeuda;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DetalleEstadoDeudaImpl extends SynergiaBusinessObjectImpl 
	implements DetalleEstadoDeuda {

	private Long nroCuenta;
	private String acreditacionBeneficiarioServElectrico;
	private String manzanaInmobiliaria;
	private String tipoCliente;
	private String chip;
	private String contadorGradualServElectrico;
	private String estadoCobranzaServElectrico;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getAcreditacionBeneficiarioServElectrico() {
		return acreditacionBeneficiarioServElectrico;
	}

	public void setAcreditacionBeneficiarioServElectrico(
			String acreditacionBeneficiarioServElectrico) {
		this.acreditacionBeneficiarioServElectrico = acreditacionBeneficiarioServElectrico;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public String getContadorGradualServElectrico() {
		return contadorGradualServElectrico;
	}

	public void setContadorGradualServElectrico(String contadorGradualServElectrico) {
		this.contadorGradualServElectrico = contadorGradualServElectrico;
	}

	public String getEstadoCobranzaServElectrico() {
		return estadoCobranzaServElectrico;
	}

	public void setEstadoCobranzaServElectrico(String estadoCobranzaServElectrico) {
		this.estadoCobranzaServElectrico = estadoCobranzaServElectrico;
	}

	public String getManzanaInmobiliaria() {
		return manzanaInmobiliaria;
	}

	public void setManzanaInmobiliaria(String manzanaInmobiliaria) {
		this.manzanaInmobiliaria = manzanaInmobiliaria;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	
	
}
