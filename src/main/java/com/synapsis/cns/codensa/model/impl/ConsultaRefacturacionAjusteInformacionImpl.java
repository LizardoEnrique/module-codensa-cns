/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjusteInformacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 * 
 * CU 016
 *
 */
public class ConsultaRefacturacionAjusteInformacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionAjusteInformacion{

	private Long idCuenta;
	private Long nroCuenta;
	private Long idAjuste;
	private Long nroAjuste;
	private String movimientos;
	private String observaciones;
	
	public Long getIdAjuste() {
		return idAjuste;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public String getMovimientos() {
		return movimientos;
	}
	public Long getNroAjuste() {
		return nroAjuste;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setIdAjuste(Long idAjuste) {
		this.idAjuste = idAjuste;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setMovimientos(String movimientos) {
		this.movimientos = movimientos;
	}
	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
}
