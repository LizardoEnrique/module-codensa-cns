/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleLecturaConsumoAuditoria;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 */
public class DetalleLecturaConsumoAuditoriaImpl extends SynergiaBusinessObjectImpl implements DetalleLecturaConsumoAuditoria{

	private Long idCuenta;
	private Long nroCuenta;
	private Long idServicio;
	private Long nroServicio;
	private Date fechaHoraAnalisis;
	private Double consumoActivaFPOriginal;
	private Double consumoActivaFPModificado;
	private Double consumoActivaHPOriginal;
	private Double consumoActivaHPModificado;
	private Double consumoActivaXPOriginal;
	private Double consumoActivaXPModificado;
	private Double consumoReactivaFPOriginal;
	private Double consumoReactivaFPModificado;
	private Double consumoReactivaHPOriginal;
	private Double consumoReactivaHPModificado;
	private Double consumoReactivaXPOriginal;
	private Double consumoReactivaXPModificado;
	
	
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public Double getConsumoActivaFPModificado() {
		return consumoActivaFPModificado;
	}
	public void setConsumoActivaFPModificado(Double consumoActivaFPModificado) {
		this.consumoActivaFPModificado = consumoActivaFPModificado;
	}
	public Double getConsumoActivaFPOriginal() {
		return consumoActivaFPOriginal;
	}
	public void setConsumoActivaFPOriginal(Double consumoActivaFPOriginal) {
		this.consumoActivaFPOriginal = consumoActivaFPOriginal;
	}
	public Double getConsumoActivaHPModificado() {
		return consumoActivaHPModificado;
	}
	public void setConsumoActivaHPModificado(Double consumoActivaHPModificado) {
		this.consumoActivaHPModificado = consumoActivaHPModificado;
	}
	public Double getConsumoActivaHPOriginal() {
		return consumoActivaHPOriginal;
	}
	public void setConsumoActivaHPOriginal(Double consumoActivaHPOriginal) {
		this.consumoActivaHPOriginal = consumoActivaHPOriginal;
	}
	public Double getConsumoActivaXPModificado() {
		return consumoActivaXPModificado;
	}
	public void setConsumoActivaXPModificado(Double consumoActivaXPModificado) {
		this.consumoActivaXPModificado = consumoActivaXPModificado;
	}
	public Double getConsumoActivaXPOriginal() {
		return consumoActivaXPOriginal;
	}
	public void setConsumoActivaXPOriginal(Double consumoActivaXPOriginal) {
		this.consumoActivaXPOriginal = consumoActivaXPOriginal;
	}
	public Double getConsumoReactivaFPModificado() {
		return consumoReactivaFPModificado;
	}
	public void setConsumoReactivaFPModificado(Double consumoReactivaFPModificado) {
		this.consumoReactivaFPModificado = consumoReactivaFPModificado;
	}
	public Double getConsumoReactivaFPOriginal() {
		return consumoReactivaFPOriginal;
	}
	public void setConsumoReactivaFPOriginal(Double consumoReactivaFPOriginal) {
		this.consumoReactivaFPOriginal = consumoReactivaFPOriginal;
	}
	public Double getConsumoReactivaHPModificado() {
		return consumoReactivaHPModificado;
	}
	public void setConsumoReactivaHPModificado(Double consumoReactivaHPModificado) {
		this.consumoReactivaHPModificado = consumoReactivaHPModificado;
	}
	public Double getConsumoReactivaHPOriginal() {
		return consumoReactivaHPOriginal;
	}
	public void setConsumoReactivaHPOriginal(Double consumoReactivaHPOriginal) {
		this.consumoReactivaHPOriginal = consumoReactivaHPOriginal;
	}
	public Double getConsumoReactivaXPModificado() {
		return consumoReactivaXPModificado;
	}
	public void setConsumoReactivaXPModificado(Double consumoReactivaXPModificado) {
		this.consumoReactivaXPModificado = consumoReactivaXPModificado;
	}
	public Double getConsumoReactivaXPOriginal() {
		return consumoReactivaXPOriginal;
	}
	public void setConsumoReactivaXPOriginal(Double consumoReactivaXPOriginal) {
		this.consumoReactivaXPOriginal = consumoReactivaXPOriginal;
	}
	public Date getFechaHoraAnalisis() {
		return fechaHoraAnalisis;
	}
	public void setFechaHoraAnalisis(Date fechaHoraAnalisis) {
		this.fechaHoraAnalisis = fechaHoraAnalisis;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	
	
}
