package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author m3.MarioRoss - 14/11/2006
 * refactor dBraccio	- 29/11/2006
 */
public interface EventosMedidor extends SynergiaBusinessObject {

	public String getAnioFabricacion();

	public void setAnioFabricacion(String anioFabricacion);

	public String getCondicionInstalacion();

	public void setCondicionInstalacion(String condicionInstalacion);

	public String getEstadoMedidor();

	public void setEstadoMedidor(String estadoMedidor);

	public String getFactorLiquidacion();

	public void setFactorLiquidacion(String factorLiquidacion);

	public Long getIdMedidor();

	public void setIdMedidor(Long idMedidor);

	public String getMarcaMedidor();

	public void setMarcaMedidor(String marcaMedidor);

	public String getModeloMedidor();

	public void setModeloMedidor(String modeloMedidor);

	public String getNivelTension();

	public void setNivelTension(String nivelTension);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNumeroMedidor();

	public void setNumeroMedidor(String numeroMedidor);

	public String getTipoMedida();

	public void setTipoMedida(String tipoMedida);

	public String getTipoMedidor();

	public void setTipoMedidor(String tipoMedidor);	

}
