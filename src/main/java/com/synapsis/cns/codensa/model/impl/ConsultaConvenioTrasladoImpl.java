package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaConvenioTraslado;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaConvenioTrasladoImpl extends SynergiaBusinessObjectImpl implements ConsultaConvenioTraslado {
	
	private String linea;
	private Long nroCuenta;
	private Long nroConvenio;
	private String tipoConvenioAnterior;
	private Long nroCuentaAnterior;
	private Date fechaTraslado;
	private Double saldoTraslado;
	private String usuarioTraslado;
	private String nombreUsuarioTraslado;
	private String observaciones;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#getFechaTraslado()
	 */
	public Date getFechaTraslado() {
		return fechaTraslado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#setFechaTraslado(java.util.Date)
	 */
	public void setFechaTraslado(Date fechaTraslado) {
		this.fechaTraslado = fechaTraslado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#getNombreUsuarioTraslado()
	 */
	public String getNombreUsuarioTraslado() {
		return nombreUsuarioTraslado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#setNombreUsuarioTraslado(java.lang.String)
	 */
	public void setNombreUsuarioTraslado(String nombreUsuarioTraslado) {
		this.nombreUsuarioTraslado = nombreUsuarioTraslado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#getObservaciones()
	 */
	public String getObservaciones() {
		return observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#setObservaciones(java.lang.String)
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#getTipoConvenioAnterior()
	 */
	public String getTipoConvenioAnterior() {
		return tipoConvenioAnterior;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#setTipoConvenioAnterior(java.lang.String)
	 */
	public void setTipoConvenioAnterior(String tipoConvenioAnterior) {
		this.tipoConvenioAnterior = tipoConvenioAnterior;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#getUsuarioTraslado()
	 */
	public String getUsuarioTraslado() {
		return usuarioTraslado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioTraslado#setUsuarioTraslado(java.lang.String)
	 */
	public void setUsuarioTraslado(String usuarioTraslado) {
		this.usuarioTraslado = usuarioTraslado;
	}
	public Long getNroCuentaAnterior() {
		return nroCuentaAnterior;
	}
	public void setNroCuentaAnterior(Long nroCuentaAnterior) {
		this.nroCuentaAnterior = nroCuentaAnterior;
	}
	public Double getSaldoTraslado() {
		return saldoTraslado;
	}
	public void setSaldoTraslado(Double saldoTraslado) {
		this.saldoTraslado = saldoTraslado;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
}
