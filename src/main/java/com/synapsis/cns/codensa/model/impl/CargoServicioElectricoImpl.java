package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.CargoGeneral;

/**
 * Representa los Cargos por Servicio El�ctrico (Energ�a) que se muestran en el Duplicado Legal de Factura.
 * 
 * @author egrande
 *
 */
public class CargoServicioElectricoImpl extends CargoFacturaImpl implements CargoGeneral {

}
