package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaDirUrbana extends SynergiaBusinessObject {

	/**
	 * @return the indicadorLocalizacion
	 */
	public String getIndicadorLocalizacion();

	/**
	 * @return the letraCruce
	 */
	public String getLetraCruce();

	/**
	 * @return the letraVia
	 */
	public String getLetraVia();

	/**
	 * @return the nombreUnidadPredial
	 */
	public String getNombreUnidadPredial();

	/**
	 * @return the nombreVia
	 */
	public String getNombreVia();

	/**
	 * @return the numeroCruce
	 */
	public String getNumeroCruce();

	/**
	 * @return the numeroPlaca
	 */
	public String getNumeroPlaca();

	/**
	 * @return the numeroUnidadPredial
	 */
	public String getNumeroUnidadPredial();

	/**
	 * @return the numeroVia
	 */
	public String getNumeroVia();

	/**
	 * @param indicadorLocalizacion the indicadorLocalizacion to set
	 */
	public void setIndicadorLocalizacion(String indicadorLocalizacion);

	/**
	 * @param letraCruce the letraCruce to set
	 */
	public void setLetraCruce(String letraCruce);

	/**
	 * @param letraVia the letraVia to set
	 */
	public void setLetraVia(String letraVia);

	/**
	 * @param nombreUnidadPredial the nombreUnidadPredial to set
	 */
	public void setNombreUnidadPredial(String nombreUnidadPredial);

	/**
	 * @param nombreVia the nombreVia to set
	 */
	public void setNombreVia(String nombreVia);

	/**
	 * @param numeroCruce the numeroCruce to set
	 */
	public void setNumeroCruce(String numeroCruce);

	/**
	 * @param numeroPlaca the numeroPlaca to set
	 */
	public void setNumeroPlaca(String numeroPlaca);

	/**
	 * @param numeroUnidadPredial the numeroUnidadPredial to set
	 */
	public void setNumeroUnidadPredial(String numeroUnidadPredial);

	/**
	 * @param numeroVia the numeroVia to set
	 */
	public void setNumeroVia(String numeroVia);

	/**
	 * @return the claseVia
	 */
	public String getClaseVia();
	/**
	 * @param claseVia the claseVia to set
	 */
	public void setClaseVia(String claseVia);
	/**
	 * @return the puntoCardinalVia
	 */
	public String getPuntoCardinalVia();
	/**
	 * @param puntoCardinalVia the puntoCardinalVia to set
	 */
	public void setPuntoCardinalVia(String puntoCardinalVia);
	/**
	 * @return the puntoCardinalCruce
	 */
	public String getPuntoCardinalCruce();
	/**
	 * @param puntoCardinalCruce the puntoCardinalCruce to set
	 */
	public void setPuntoCardinalCruce(String puntoCardinalCruce);
	/**
	 * @return the tipoUnidadPredial
	 */
	public String getTipoUnidadPredial();
	/**
	 * @param tipoUnidadPredial the tipoUnidadPredial to set
	 */
	public void setTipoUnidadPredial(String tipoUnidadPredial);
	/**
	 * @return the numeroTipoUnidadPredial
	 */
	public String getNumeroTipoUnidadPredial();
	/**
	 * @param numeroTipoUnidadPredial the numeroTipoUnidadPredial to set
	 */
	public void setNumeroTipoUnidadPredial(String numeroTipoUnidadPredial);
	/**
	 * @return the modalidadUnidadPredial
	 */
	public String getModalidadUnidadPredial();
	/**
	 * @param modalidadUnidadPredial the modalidadUnidadPredial to set
	 */
	public void setModalidadUnidadPredial(String modalidadUnidadPredial);
	/**
	 * @return the numeroModalidadUnidadPredial
	 */
	public String getNumeroModalidadUnidadPredial();
	/**
	 * @param numeroModalidadUnidadPredial the numeroModalidadUnidadPredial to set
	 */
	public void setNumeroModalidadUnidadPredial(String numeroModalidadUnidadPredial);	
}