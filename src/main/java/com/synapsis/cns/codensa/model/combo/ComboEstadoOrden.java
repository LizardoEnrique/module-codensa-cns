/**
 * $Id: ComboEstadoOrden.java,v 1.2 2007/02/06 22:30:58 ar29261698 Exp $
 */

package com.synapsis.cns.codensa.model.combo;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ComboEstadoOrden extends SynergiaBusinessObject{

	public String getCodigo();
	
	public void setCodigo(String codigo);
	
	public String getCodigoOrden();
	
	public void setCodigoOrden(String codigoOrden);
	
	public String getDescripcion();
	
	public void setDescripcion(String descripcion);
	
}
