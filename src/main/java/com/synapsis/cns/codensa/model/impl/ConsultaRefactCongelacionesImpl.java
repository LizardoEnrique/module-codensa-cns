/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefactCongelaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaRefactCongelacionesImpl extends SynergiaBusinessObjectImpl implements ConsultaRefactCongelaciones {

	private Date fecAprobacion;         
	private String tipoOperacion;       
	private String valor;               
	private String valorTotalAntes;     
	private String valorTotalDespues;
	private Long nroCuenta;
	private String nroDocumento;
	private Long nroOrden;
	
	
	
	public Long getNroOrden() {
		return nroOrden;
	}
	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}
	/**
	 * @return the fecAprobacion
	 */
	public Date getFecAprobacion() {
		return fecAprobacion;
	}
	/**
	 * @param fecAprobacion the fecAprobacion to set
	 */
	public void setFecAprobacion(Date fecAprobacion) {
		this.fecAprobacion = fecAprobacion;
	}
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the valor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	 * @return the valorTotalAntes
	 */
	public String getValorTotalAntes() {
		return valorTotalAntes;
	}
	/**
	 * @param valorTotalAntes the valorTotalAntes to set
	 */
	public void setValorTotalAntes(String valorTotalAntes) {
		this.valorTotalAntes = valorTotalAntes;
	}
	/**
	 * @return the valorTotalDespues
	 */
	public String getValorTotalDespues() {
		return valorTotalDespues;
	}
	/**
	 * @param valorTotalDespues the valorTotalDespues to set
	 */
	public void setValorTotalDespues(String valorTotalDespues) {
		this.valorTotalDespues = valorTotalDespues;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroDocumento
	 */
	public String getNroDocumento() {
		return nroDocumento;
	}
	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}   

}
