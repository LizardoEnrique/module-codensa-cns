package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.cns.codensa.model.ItemClasificados;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Item de Producto Clasificados para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ItemClasificadosImpl extends ItemECOImpl implements ItemClasificados {
	private static final long serialVersionUID = 1L;
	

}