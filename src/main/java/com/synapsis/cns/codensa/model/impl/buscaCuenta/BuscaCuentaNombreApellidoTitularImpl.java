package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaNombreApellidoTitular;

public class BuscaCuentaNombreApellidoTitularImpl extends BuscaCuentaImpl implements BuscaCuentaNombreApellidoTitular {
	
	private String nombre;
	private String apellido;
	private String apellidoMaterno;
	
	/**
	 * @return the apellidoMaterno
	 */
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	/**
	 * @param apellidoMaterno the apellidoMaterno to set
	 */
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoTitular#getApellido()
	 */
	public String getApellido() {
		return apellido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoTitular#getNombre()
	 */
	public String getNombre() {
		return nombre;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoTitular#setApellido(java.lang.String)
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoTitular#setNombre(java.lang.String)
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}