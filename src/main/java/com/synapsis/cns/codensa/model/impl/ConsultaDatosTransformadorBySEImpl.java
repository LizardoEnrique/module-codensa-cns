package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDatosTransformadorBySE;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaDatosTransformadorBySEImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDatosTransformadorBySE {
	
	private Long servicio;
	private String transformador;
	private String circuito;
	private String propiedadTra;
	private String conexion;
	private String tension;
	private Date fechaDesAsociacion;
	private Date fechaAsociacion;
	private Date fechaProceso;
	
	public String getTransformador() {
		return transformador;
	}

	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}

	public String getCircuito() {
		return circuito;
	}

	public void setCircuito(String circuito) {
		this.circuito = circuito;
	}

	public String getPropiedadTra() {
		return propiedadTra;
	}

	public void setPropiedadTra(String propiedadTra) {
		this.propiedadTra = propiedadTra;
	}

	public String getConexion() {
		return conexion;
	}

	public void setConexion(String conexion) {
		this.conexion = conexion;
	}

	public String getTension() {
		return tension;
	}

	public void setTension(String tension) {
		this.tension = tension;
	}

	public Date getFechaDesAsociacion() {
		return fechaDesAsociacion;
	}

	public void setFechaDesAsociacion(Date fechaDesAsociacion) {
		this.fechaDesAsociacion = fechaDesAsociacion;
	}

	public Date getFechaAsociacion() {
		return fechaAsociacion;
	}

	public void setFechaAsociacion(Date fechaAsociacion) {
		this.fechaAsociacion = fechaAsociacion;
	}

	public Long getServicio() {
		return servicio;
	}

	public void setServicio(Long servicio) {
		this.servicio = servicio;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	
}
