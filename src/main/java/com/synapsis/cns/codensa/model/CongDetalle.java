/**
 * $Id: CongDetalle.java,v 1.5 2008/08/26 19:27:35 ar18817018 Exp $
 */
package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongDetalle extends SynergiaBusinessObject {

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion();

	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion);
	/**
	 * @return the nroOrden
	 */
	public String getNroOrden();

	/**
	 * @param nroOrden the nroOrden to set
	 */
	public void setNroOrden(String nroOrden);

	/**
	 * @return the clasifCongelacion
	 */
	public String getClasifCongelacion() ;

	/**
	 * @param clasifCongelacion the clasifCongelacion to set
	 */
	public void setClasifCongelacion(String clasifCongelacion);

	/**
	 * @return the clasifDescongelacion
	 */
	public String getClasifDescongelacion();

	/**
	 * @param clasifDescongelacion the clasifDescongelacion to set
	 */
	public void setClasifDescongelacion(String clasifDescongelacion);

	/**
	 * @return the areaSolicitanteInformacion
	 */
	public String getAreaSolicitanteInformacion() ;

	/**
	 * @param areaSolicitanteInformacion the areaSolicitanteInformacion to set
	 */
	public void setAreaSolicitanteInformacion(String areaSolicitanteInformacion) ;

	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado() ;

	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado);
	/**
	 * @return the fechaPagoCongelacion
	 */
	public Date getFechaPagoCongelacion();

	/**
	 * @param fechaPagoCongelacion the fechaPagoCongelacion to set
	 */
	public void setFechaPagoCongelacion(Date fechaPagoCongelacion);

	/**
	 * @return the fechaPagoDescongelacion
	 */
	public Date getFechaPagoDescongelacion();

	/**
	 * @param fechaPagoDescongelacion the fechaPagoDescongelacion to set
	 */
	public void setFechaPagoDescongelacion(Date fechaPagoDescongelacion);
	/**
	 * @return the tiempoCongelacionTransitoria
	 */
	public String getTipoCongelacion();

	/**
	 * @param tiempoCongelacionTransitoria the tiempoCongelacionTransitoria to set
	 */
	public void setTipoCongelacion(String tipoCongelacion);

	/**
	 * @return the tiempoCongelacion
	 */
	public String getTiempoCongelacion();
	/**
	 * @param tiempoCongelacion the tiempoCongelacion to set
	 */
	public void setTiempoCongelacion(String tiempoCongelacion);

	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo();

	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo);

	
	

}