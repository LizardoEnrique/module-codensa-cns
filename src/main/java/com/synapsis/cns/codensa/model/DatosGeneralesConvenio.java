package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface DatosGeneralesConvenio extends SynergiaBusinessObject{

	public Long getCuotasAtrasadas();
	public void setCuotasAtrasadas(Long cuotasAtrasadas);
	public Long getNroConvenio();
	public void setNroConvenio(Long nroConvenio);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	public String getTipoConvenio();
	public void setTipoConvenio(String tipoConvenio);	
	public Integer getCuotasFacturadas();
	public void setCuotasFacturadas(Integer cuotasFacturadas);
	public Integer getCuotasOtorgadas();
	public void setCuotasOtorgadas(Integer cuotasOtorgadas);
	public String getDeudaActual();
	public void setDeudaActual(String deudaActual);
	public String getDeudaInicial();
	public void setDeudaInicial(String deudaInicial);
	public String getEstadoActual();
	public void setEstadoActual(String estadoActual);
	public Date getFechaCreacion();
	public void setFechaCreacion(Date fechaCreacion);
	public Date getFechaFinalizacion();
	public void setFechaFinalizacion(Date fechaFinalizacion);
	public Date getFechaSiniestro();
	public void setFechaSiniestro(Date fechaSiniestro);
	public String getMontoCondonado();
	public void setMontoCondonado(String montoCondonado);
	public String getMotivo();
	public void setMotivo(String motivo);
	public String getOpcion();
	public void setOpcion(String opcion);
	public String getPeriodoGracia();
	public void setPeriodoGracia(String periodoGracia);
	public String getSaldo();
	public void setSaldo(String saldo);
	public Float getTasaInteresActual();
	public void setTasaInteresActual(Float tasaInteresActual);
	public Float getTasaInteresAplicada();
	public void setTasaInteresAplicada(Float tasaInteresAplicada);
	public String getValorCuotaInicial();
	public void setValorCuotaInicial(String valorCuotaInicial);
	public String getValorCuotaMensual();
	public void setValorCuotaMensual(String valorCuotaMensual);
	public Long getIdConvenio();
	public void setIdConvenio(Long idConvenio);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
	public Long getCuotasPendientes();
	public void setCuotasPendientes(Long cuotasPendientes);
	
}