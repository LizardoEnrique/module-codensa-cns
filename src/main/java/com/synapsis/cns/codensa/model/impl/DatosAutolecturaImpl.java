/**
 * $Id: DatosAutolecturaImpl.java,v 1.3 2007/06/28 16:47:14 syalri Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosAutolectura;

/**
 * Caso de Uso CNS020
 * 
 * @author Paola Attadio
 */
public class DatosAutolecturaImpl extends SynergiaBusinessObjectImpl implements DatosAutolectura {
	
	private Long nroCuenta;
	private String marcaMedidor;
	private String motivo;
	private String nombreSuministrador;
	private String nombreUsuario;
	private String numeroDocumento;
	private String numeroMedidor;
	private String observaciones;
	private String tipoDocumento;
	private Date ultimaActualizacion;
	private String usuario;
	private String acreditacion;
	private Long lecturaActivaFP;
	private Long lecturaActivaHP;
	private Long lecturaActivaXP;
	private Long lecturaReactivaFP;
	private Long lecturaReactivaHP;
	private Long lecturaReactivaXP;

	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setNroCuenta()
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getMarcaMedidor()
	 */
	public String getMarcaMedidor() {
		return this.marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setMarcaMedidor()
	 */
	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getMotivo()
	 */
	public String getMotivo() {
		return this.motivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setMotivo()
	 */
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getNombreSuministrador()
	 */
	public String getNombreSuministrador() {
		return this.nombreSuministrador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setNombreSuministrador()
	 */
	public void setNombreSuministrador(String nombreSuministrador) {
		this.nombreSuministrador = nombreSuministrador;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getNombreUsuario()
	 */
	public String getNombreUsuario() {
		return this.nombreUsuario;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setNombreUsuario()
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getNumeroDocumento()
	 */
	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setNumeroDocumento()
	 */
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getNumeroMedidor()
	 */
	public String getNumeroMedidor() {
		return this.numeroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setNumeroMedidor()
	 */
	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getObservaciones()
	 */
	public String getObservaciones() {
		return this.observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setObservaciones()
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getTipoDocumento()
	 */
	public String getTipoDocumento() {
		return this.tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setTipoDocumento()
	 */
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getUltimaActualizacion()
	 */
	public Date getUltimaActualizacion() {
		return this.ultimaActualizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setUltimaActualizacion()
	 */
	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getUsuario()
	 */
	public String getUsuario() {
		return this.usuario;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setUsuario()
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#getAcreditacion()
	 */
	public String getAcreditacion() {
		return acreditacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.DatosAutolectura#setAcreditacion()
	 */
	public void setAcreditacion(String acreditacion) {
		this.acreditacion = acreditacion;
	}
	/**
	 * @return the lecturaActivaFP
	 */
	public Long getLecturaActivaFP() {
		return lecturaActivaFP;
	}
	/**
	 * @param lecturaActivaFP the lecturaActivaFP to set
	 */
	public void setLecturaActivaFP(Long lecturaActivaFP) {
		this.lecturaActivaFP = lecturaActivaFP;
	}
	/**
	 * @return the lecturaActivaHP
	 */
	public Long getLecturaActivaHP() {
		return lecturaActivaHP;
	}
	/**
	 * @param lecturaActivaHP the lecturaActivaHP to set
	 */
	public void setLecturaActivaHP(Long lecturaActivaHP) {
		this.lecturaActivaHP = lecturaActivaHP;
	}
	/**
	 * @return the lecturaActivaXP
	 */
	public Long getLecturaActivaXP() {
		return lecturaActivaXP;
	}
	/**
	 * @param lecturaActivaXP the lecturaActivaXP to set
	 */
	public void setLecturaActivaXP(Long lecturaActivaXP) {
		this.lecturaActivaXP = lecturaActivaXP;
	}
	/**
	 * @return the lecturaReactivaFP
	 */
	public Long getLecturaReactivaFP() {
		return lecturaReactivaFP;
	}
	/**
	 * @param lecturaReactivaFP the lecturaReactivaFP to set
	 */
	public void setLecturaReactivaFP(Long lecturaReactivaFP) {
		this.lecturaReactivaFP = lecturaReactivaFP;
	}
	/**
	 * @return the lecturaReactivaHP
	 */
	public Long getLecturaReactivaHP() {
		return lecturaReactivaHP;
	}
	/**
	 * @param lecturaReactivaHP the lecturaReactivaHP to set
	 */
	public void setLecturaReactivaHP(Long lecturaReactivaHP) {
		this.lecturaReactivaHP = lecturaReactivaHP;
	}
	/**
	 * @return the lecturaReactivaXP
	 */
	public Long getLecturaReactivaXP() {
		return lecturaReactivaXP;
	}
	/**
	 * @param lecturaReactivaXP the lecturaReactivaXP to set
	 */
	public void setLecturaReactivaXP(Long lecturaReactivaXP) {
		this.lecturaReactivaXP = lecturaReactivaXP;
	}
}