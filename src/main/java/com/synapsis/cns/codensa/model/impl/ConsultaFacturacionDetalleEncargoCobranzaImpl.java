/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaFacturacionDetalleEncargoCobranza;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionDetalleEncargoCobranzaImpl extends
SynergiaBusinessObjectImpl implements ConsultaFacturacionDetalleEncargoCobranza {

	private Long id;
	private Long nroCuenta;
	private Date periodoPrimeraFact;
	private Date fechaCreacion;
	private Long nroServicio;
	private String tipoProducto;
	private String plan;
	private String socioNegocio;
	private Double tasaInteres;
	private String valorCuota;
	private String cuotaInteres;
	private Long nroCuotas;
	private Long cantVecesFac;
	private Integer cuotasPendientes;
	private String nombreTitlServ;
	private String nroIdentificacion;
	private Long itemsFacturados;
	private String saldoCapital;

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the periodoPrimeraFact
	 */
	public Date getPeriodoPrimeraFact() {
		return periodoPrimeraFact;
	}
	/**
	 * @param periodoPrimeraFact the periodoPrimeraFact to set
	 */
	public void setPeriodoPrimeraFact(Date periodoPrimeraFact) {
		this.periodoPrimeraFact = periodoPrimeraFact;
	}
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() {
		return tipoProducto;
	}
	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	/**
	 * @return the plan
	 */
	public String getPlan() {
		return plan;
	}
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan) {
		this.plan = plan;
	}
	/**
	 * @return the socioNegocio
	 */
	public String getSocioNegocio() {
		return socioNegocio;
	}
	/**
	 * @param socioNegocio the socioNegocio to set
	 */
	public void setSocioNegocio(String socioNegocio) {
		this.socioNegocio = socioNegocio;
	}
	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres() {
		return tasaInteres;
	}
	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres) {
		this.tasaInteres = tasaInteres;
	}
	/**
	 * @return the valorCuota
	 */
	public String getValorCuota() {
		return valorCuota;
	}
	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}
	/**
	 * @return the cuotaInteres
	 */
	public String getCuotaInteres() {
		return cuotaInteres;
	}
	/**
	 * @param cuotaInteres the cuotaInteres to set
	 */
	public void setCuotaInteres(String cuotaInteres) {
		this.cuotaInteres = cuotaInteres;
	}
	/**
	 * @return the nroCuotas
	 */
	public Long getNroCuotas() {
		return nroCuotas;
	}
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	/**
	 * @return the cantVecesFac
	 */
	public Long getCantVecesFac() {
		return cantVecesFac;
	}
	/**
	 * @param cantVecesFac the cantVecesFac to set
	 */
	public void setCantVecesFac(Long cantVecesFac) {
		this.cantVecesFac = cantVecesFac;
	}
	/**
	 * @return the cuotasPendientes
	 */
	public Integer getCuotasPendientes() {
		return cuotasPendientes;
	}
	/**
	 * @param cuotasPendientes the cuotasPendientes to set
	 */
	public void setCuotasPendientes(Integer cuotasPendientes) {
		this.cuotasPendientes = cuotasPendientes;
	}
	/**
	 * @return the nombreTitlServ
	 */
	public String getNombreTitlServ() {
		return nombreTitlServ;
	}
	/**
	 * @param nombreTitlServ the nombreTitlServ to set
	 */
	public void setNombreTitlServ(String nombreTitlServ) {
		this.nombreTitlServ = nombreTitlServ;
	}
	/**
	 * @return the nroIdentificacion
	 */
	public String getNroIdentificacion() {
		return nroIdentificacion;
	}
	/**
	 * @param nroIdentificacion the nroIdentificacion to set
	 */
	public void setNroIdentificacion(String nroIdentificacion) {
		this.nroIdentificacion = nroIdentificacion;
	}
	/**
	 * @return the itemsFacturados
	 */
	public Long getItemsFacturados() {
		return itemsFacturados;
	}
	/**
	 * @param itemsFacturados the itemsFacturados to set
	 */
	public void setItemsFacturados(Long itemsFacturados) {
		this.itemsFacturados = itemsFacturados;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the saldoCapital
	 */
	public String getSaldoCapital() {
		return saldoCapital;
	}
	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(String saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	
	
}