package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaResumenDatosCastigoCartera extends SynergiaBusinessObject {

	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	public Date getFechaGeneracionDeuda();

	public void setFechaGeneracionDeuda(Date fechaGeneracionDeuda);

	public String getUsuarioNombreIngresoCastigoDeuda();

	public void setUsuarioNombreIngresoCastigoDeuda(String usuarioNombreIngresoCastigoDeuda);

	public String getUsuarioNombreIngresoDeuda();

	public void setUsuarioNombreIngresoDeuda(String usuarioNombreIngresoDeuda);

	public String getValorTotalCastigado();

	public void setValorTotalCastigado(String valorTotalCastigado);
	
}