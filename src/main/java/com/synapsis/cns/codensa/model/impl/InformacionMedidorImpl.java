package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.InformacionMedidor;

public class InformacionMedidorImpl extends SynergiaBusinessObjectImpl 
	implements InformacionMedidor {
    // Fields    
	
    private static final long serialVersionUID = 1L;
	
    private Long nroCuenta;
	private String marcaMedidor;
	private String modeloMedidor;
	private String numeroMedidor;
    private Date fechaRetiro;
    private Long numeroInspeccion;
    private Date fechaEvaluacion;

    private Long idMedidor;

	public Date getFechaEvaluacion() {
		return fechaEvaluacion;
	}

	public void setFechaEvaluacion(Date fechaEvaluacion) {
		this.fechaEvaluacion = fechaEvaluacion;
	}

	public Date getFechaRetiro() {
		return fechaRetiro;
	}

	public void setFechaRetiro(Date fechaRetiro) {
		this.fechaRetiro = fechaRetiro;
	}

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getMarcaMedidor() {
		return marcaMedidor;
	}

	public void setMarcaMedidor(String marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}

	public String getModeloMedidor() {
		return modeloMedidor;
	}

	public void setModeloMedidor(String modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getNumeroInspeccion() {
		return numeroInspeccion;
	}

	public void setNumeroInspeccion(Long numeroInspeccion) {
		this.numeroInspeccion = numeroInspeccion;
	}

	public String getNumeroMedidor() {
		return numeroMedidor;
	}

	public void setNumeroMedidor(String numeroMedidor) {
		this.numeroMedidor = numeroMedidor;
	}
    

   
}
