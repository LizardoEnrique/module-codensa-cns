package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaCargosManuales extends SynergiaBusinessObject {
	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getIdItemDoc();

	public void setIdItemDoc(Long idItemDoc);

	public String getCodigoCargo();

	public void setCodigoCargo(String codigoCargo);

	public String getDescripcionCargo();

	public void setDescripcionCargo(String descripcionCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public String getTipoServicioCargo();

	public void setTipoServicioCargo(String tipoServicioCargo);

	public Long getNroServicioCargo();

	public void setNroServicioCargo(Long nroServicioCargo);

	public Date getFechaIngresoCargo();

	public void setFechaIngresoCargo(Date fechaIngresoCargo);

	public Date getFechaDesarrolloActividadTerreno();

	public void setFechaDesarrolloActividadTerreno(Date fechaDesarrolloActividadTerreno);

	public String getUsuarioIngresoCargo();

	public void setUsuarioIngresoCargo(String usuarioIngresoCargo);

	public String getNombreIngresoCargo();

	public void setNombreIngresoCargo(String nombreIngresoCargo);

	public Long getNroDocumentoIngresoCargo();

	public void setNroDocumentoIngresoCargo(Long nroDocumentoIngresoCargo);

	public String getEstadoFactura();

	public void setEstadoFactura(String estadoFactura);

	public String getObservaciones();

	public void setObservaciones(String observaciones);
	
	public String getOrigenCargo();

	public void setOrigenCargo(String origenCargo);


}