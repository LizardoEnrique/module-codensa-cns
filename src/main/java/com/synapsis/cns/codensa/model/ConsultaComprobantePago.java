package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaComprobantePago extends SynergiaBusinessObject {

	/**
	 * @return the fechaHoraEmicion
	 */
	public Date getFechaHoraEmision();

	/**
	 * @param fechaHoraEmicion the fechaHoraEmicion to set
	 */
	public void setFechaHoraEmision(Date fechaHoraEmision);

	/**
	 * @return the nroComprobante
	 */
	public Long getNroComprobante();

	/**
	 * @param nroComprobante the nroComprobante to set
	 */
	public void setNroComprobante(Long nroComprobante);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the tipoComprobante
	 */
	public String getTipoComprobante();

	/**
	 * @param tipoComprobante the tipoComprobante to set
	 */
	public void setTipoComprobante(String tipoComprobante);

	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio();

	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio);

	/**
	 * @return the valorComprobante
	 */
	public Long getValorComprobante();

	/**
	 * @param valorComprobante the valorComprobante to set
	 */
	public void setValorComprobante(Long valorComprobante);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
}