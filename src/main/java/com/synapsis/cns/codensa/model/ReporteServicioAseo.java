package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Interfaz del reporte de duplicado factura (CNS-003)
 * 
 * @author Lizardo Mamani - Ayesa - 27-09-2017
 */
public interface ReporteServicioAseo extends Comparable, SynergiaBusinessObject{

	public String getMes_uno();
	public void setMes_uno(String mes_uno);
	public String getMes_dos();
	public void setMes_dos(String mes_dos);
	public String getMes_tres();
	public void setMes_tres(String mes_tres);
	public String getMes_cuatro();
	public void setMes_cuatro(String mes_cuatro);
	public String getMes_cinco();
	public void setMes_cinco(String mes_cinco);
	public String getMes_seis();
	public void setMes_seis(String mes_seis);
	public String getNombre();
	public void setNombre(String nombre);
	public String getDireccion() ;
	public void setDireccion(String direccion);
	public String getPuntoatencion();
	public void setPuntoatencion(String puntoatencion);
	public String getNit();
	public void setNit(String nit) ;
	public String getDireccionweb();
	public void setDireccionweb(String direccionweb);
	public String getBarrido();
	public void setBarrido(String barrido);
	public String getRecoleccion();
	public void setRecoleccion(String recoleccion);
	public String getDensidad() ;
	public void setDensidad(String densidad);
	public String getVolumen();
	public void setVolumen(String volumen);
	public String getParticipacion();
	public void setParticipacion(String participacion);
}
