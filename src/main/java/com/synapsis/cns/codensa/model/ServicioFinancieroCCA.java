package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Interfaz del producto financiero CCA, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ServicioFinancieroCCA extends ServicioFinanciero{
	public String getTitular();
	public void setTitular(String titular);
	public Long getNroServicio();
	public BigDecimal getPagoMinimo();
	public BigDecimal getSaldoEnMora();
	public BigDecimal getInteresPorMora();
	public BigDecimal getPagoTotal();
	public Double getTasaInteres();
	public Long getCupoTotal();
	public Long getCupoDisponible();
	public Set getItemsCCA();
	public void setItemsCCA(Set itemsCCA);
	public void setNroServicio(Long nroServicio);
	public String getTipoProducto();
	public void setTipoProducto(String tipoProducto);
}
