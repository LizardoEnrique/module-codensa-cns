package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaDeuda extends SynergiaBusinessObject {

	/**
	 * @return the deudaTotalActual
	 */
	public Double getDeudaTotalActual();

	/**
	 * @param deudaTotalActual
	 *            the deudaTotalActual to set
	 */
	public void setDeudaTotalActual(Double deudaTotalActual);

	/**
	 * @return the saldoNoSujetoIntereses
	 */
	public Double getSaldoNoSujetoIntereses();

	/**
	 * @param saldoNoSujetoIntereses
	 *            the saldoNoSujetoIntereses to set
	 */
	public void setSaldoNoSujetoIntereses(Double saldoNoSujetoIntereses);

	/**
	 * @return the saldoTotalSujetoIntereses
	 */
	public Double getSaldoTotalSujetoIntereses();

	/**
	 * @param saldoTotalSujetoIntereses
	 *            the saldoTotalSujetoIntereses to set
	 */
	public void setSaldoTotalSujetoIntereses(Double saldoTotalSujetoIntereses);

	/**
	 * @return the valorTotalIntereses
	 */
	public Double getValorTotalIntereses();

	/**
	 * @param valorTotalIntereses
	 *            the valorTotalIntereses to set
	 */
	public void setValorTotalIntereses(Double valorTotalIntereses);

	/**
	 * @return the valorTotalOtrosNegocios
	 */
	public Double getValorTotalOtrosNegocios();

	/**
	 * @param valorTotalOtrosNegocios
	 *            the valorTotalOtrosNegocios to set
	 */
	public void setValorTotalOtrosNegocios(Double valorTotalOtrosNegocios);

	/**
	 * @return the valorTotalSanciones
	 */
	public Double getValorTotalSanciones();

	/**
	 * @param valorTotalSanciones
	 *            the valorTotalSanciones to set
	 */
	public void setValorTotalSanciones(Double valorTotalSanciones);

}