package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteEnvioSSPD extends SynergiaBusinessObject {

	/**
	 * @return the fechaEntrega
	 */
	public Date getFechaEntrega();

	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(Date fechaEntrega);

	/**
	 * @return the fechaEnvio
	 */
	public Date getFechaEnvio();

	/**
	 * @param fechaEnvio the fechaEnvio to set
	 */
	public void setFechaEnvio(Date fechaEnvio);

	/**
	 * @return the numeroRadicacion
	 */
	public Long getNumeroRadicacion();

	/**
	 * @param numeroRadicacion the numeroRadicacion to set
	 */
	public void setNumeroRadicacion(Long numeroRadicacion);

	/**
	 * @return the numeroRadicacionConcede
	 */
	public Long getNumeroRadicacionConcede();

	/**
	 * @param numeroRadicacionConcede the numeroRadicacionConcede to set
	 */
	public void setNumeroRadicacionConcede(Long numeroRadicacionConcede);

	/**
	 * @return the numeroRadicacionDestino
	 */
	public Long getNumeroRadicacionDestino();

	/**
	 * @param numeroRadicacionDestino the numeroRadicacionDestino to set
	 */
	public void setNumeroRadicacionDestino(Long numeroRadicacionDestino);

	/**
	 * @return the tipoEntrega
	 */
	public String getTipoEntrega();

	/**
	 * @param tipoEntrega the tipoEntrega to set
	 */
	public void setTipoEntrega(String tipoEntrega);

	/**
	 * @return the tipoRadicacion
	 */
	public String getTipoRadicacion();

	/**
	 * @param tipoRadicacion the tipoRadicacion to set
	 */
	public void setTipoRadicacion(String tipoRadicacion);

	/**
	 * @return the valorReclamacion
	 */
	public Double getValorReclamacion();

	/**
	 * @param valorReclamacion the valorReclamacion to set
	 */
	public void setValorReclamacion(Double valorReclamacion);

	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente();
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente);
}