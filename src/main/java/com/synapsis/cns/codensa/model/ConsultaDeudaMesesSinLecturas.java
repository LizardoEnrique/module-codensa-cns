package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDeudaMesesSinLecturas extends SynergiaBusinessObject {

	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta();

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta);

	/**
	 * @return the idServicio
	 */
	public Long getIdServicio();

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio);

	/**
	 * @return the mesesSinLectura
	 */
	public Long getMesesSinLectura();

	/**
	 * @param mesesSinLectura the mesesSinLectura to set
	 */
	public void setMesesSinLectura(Long mesesSinLectura);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

}