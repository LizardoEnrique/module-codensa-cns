package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.4 $ $Date: 2010/11/05 16:55:00 $
 * 
 */
public interface ConsultaDeudaCodensaHogar extends SynergiaBusinessObject {

	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda();

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda);

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza();

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza);

	/**
	 * @return the deuda
	 */
	public String getDeuda();

	/**
	 * @param deuda the deuda to set
	 */
	public void setDeuda(String deuda);

	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
	
	public String getValorDeuda();
	
	public void setValorDeuda(String valorDeuda);

	
}