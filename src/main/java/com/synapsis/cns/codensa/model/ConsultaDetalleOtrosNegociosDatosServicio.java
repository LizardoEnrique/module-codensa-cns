package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDetalleOtrosNegociosDatosServicio extends SynergiaBusinessObject {
	/**
	 * @return the fechaActivProd
	 */
	public Date getFechaActivProd();

	/**
	 * @param fechaActivProd the fechaActivProd to set
	 */
	public void setFechaActivProd(Date fechaActivProd);

	/**
	 * @return the fechaDesactProd
	 */
	public Date getFechaDesactProd();

	/**
	 * @param fechaDesactProd the fechaDesactProd to set
	 */
	public void setFechaDesactProd(Date fechaDesactProd);

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio();

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio);

	/**
	 * @return the motivoFinalizacion
	 */
	public String getMotivoFinalizacion();

	/**
	 * @param motivoFinalizacion the motivoFinalizacion to set
	 */
	public void setMotivoFinalizacion(String motivoFinalizacion);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroIdentTitular
	 */
	public String getNroIdentTitular();

	/**
	 * @param nroIdentTitular the nroIdentTitular to set
	 */
	public void setNroIdentTitular(String nroIdentTitular);
	
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the plan
	 */
	public String getPlan();

	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);

	/**
	 * @return the producto
	 */
	public String getProducto();

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto);

	/**
	 * @return the tipoIdentTitular
	 */
	public String getTipoIdentTitular();

	/**
	 * @param tipoIdentTitular the tipoIdentTitular to set
	 */
	public void setTipoIdentTitular(String tipoIdentTitular);

	/**
	 * @return the valorOrigSrv
	 */
	public Double getValorOrigSrv();

	/**
	 * @param valorOrigSrv the valorOrigSrv to set
	 */
	public void setValorOrigSrv(Double valorOrigSrv);

}