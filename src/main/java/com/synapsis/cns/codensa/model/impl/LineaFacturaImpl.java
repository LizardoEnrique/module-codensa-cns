package com.synapsis.cns.codensa.model.impl;

import java.sql.Clob;
import java.util.Date;
import java.util.Map;

import com.synapsis.cns.codensa.model.LineaFactura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhack
 */
public class LineaFacturaImpl extends SynergiaBusinessObjectImpl 
	implements LineaFactura {

	//Mapeados con la BD	
	private Long  		nroCuenta;
	private Date 		fechaFactura;
	private Clob 		datosClob;
	private String 		datos;
	private Clob 		datosAnexoClob;
	private String 		datosAnexo;
	// Sin mapear, se los pone el service
	private Map			formato;
	private Object		reporte;

	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setDatos(String datos) {
		this.datos = datos;
	}
	public void setDatosAnexo(String datosAnexo) {
		this.datosAnexo = datosAnexo;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public String getDatos() {
		try{
			if (datos == null){
				datos = datosClob.getSubString(1, (int) datosClob.length());
			}
			return datos;
		}catch(Exception ex){
			return "";			
		}
	}
	
	public String getDatosAnexo() {
		try{
			if (datosAnexo == null){
				datosAnexo = datosAnexoClob.getSubString(1, (int) datosAnexoClob.length());
			}
			return datosAnexo;
		}catch(Exception ex){
			return "";			
		}
	}

	public Map getFormato() {
		return formato;
	}
	public void setFormato(Map formato) {
		this.formato = formato;
	}
	public Object getReporte() {
		return reporte;
	}
	public void setReporte(Object reporte) {
		this.reporte = reporte;
	}
	public Clob getDatosClob() {
		return datosClob;
	}
	public void setDatosClob(Clob datosClob) {
		this.datosClob = datosClob;
	}
	public Clob getDatosAnexoClob() {
		return datosAnexoClob;
	}
	public void setDatosAnexoClob(Clob datosAnexoClob) {
		this.datosAnexoClob = datosAnexoClob;
	}
	
	

}
