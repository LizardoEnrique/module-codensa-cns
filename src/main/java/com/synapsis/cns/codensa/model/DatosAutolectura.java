/**
 * $Id: DatosAutolectura.java,v 1.3 2007/06/28 16:47:14 syalri Exp $
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * Caso de Uso CNS020
 * 
 * @author Paola Attadio
 */
public interface DatosAutolectura extends SynergiaBusinessObject {
	/**
	 * 
	 * @return
	 */
	public Long getNroCuenta();
	/**
	 * 
	 * @param nroCuenta
	 */
	public void setNroCuenta(Long nroCuenta);

	
	/**
	 * @return the marcaMedidor
	 */
	public String getMarcaMedidor();
	/**
	 * @param marcaMedidor the marcaMedidor to set
	 */
	public void setMarcaMedidor(String marcaMedidor);
	/**
	 * @return the motivo
	 */
	public String getMotivo();
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo);
	/**
	 * @return the nombreSuministrador
	 */
	public String getNombreSuministrador();
	/**
	 * @param nombreSuministrador the nombreSuministrador to set
	 */
	public void setNombreSuministrador(String nombreSuministrador);
	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario();
	/**
	 * @param nombreUduario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario);
	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento();
	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento);
	/**
	 * @return the numeroMedidor
	 */
	public String getNumeroMedidor();
	/**
	 * @param numeroMedidor the numeroMedidor to set
	 */
	public void setNumeroMedidor(String numeroMedidor);
	/**
	 * @return the observaciones
	 */
	public String getObservaciones();
	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones);
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);
	/**
	 * @return the ultimaActualizacion
	 */
	public Date getUltimaActualizacion();
	/**
	 * @param ultimaActualizacion the ultimaActualizacion to set
	 */
	public void setUltimaActualizacion(Date ultimaActualizacion);
	/**
	 * @return the usuario
	 */
	public String getUsuario();
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario);
    /**
     * 
     * @return
     */
	public String getAcreditacion();
	/**
	 * 
	 * @param acreditacion
	 */
	public void setAcreditacion(String acreditacion);
	/**
	 * @return the lecturaActivaFP
	 */
	public Long getLecturaActivaFP();
	/**
	 * @param lecturaActivaFP the lecturaActivaFP to set
	 */
	public void setLecturaActivaFP(Long lecturaActivaFP);
	/**
	 * @return the lecturaActivaHP
	 */
	public Long getLecturaActivaHP();
	/**
	 * @param lecturaActivaHP the lecturaActivaHP to set
	 */
	public void setLecturaActivaHP(Long lecturaActivaHP);
	/**
	 * @return the lecturaActivaXP
	 */
	public Long getLecturaActivaXP();
	/**
	 * @param lecturaActivaXP the lecturaActivaXP to set
	 */
	public void setLecturaActivaXP(Long lecturaActivaXP);
	/**
	 * @return the lecturaReactivaFP
	 */
	public Long getLecturaReactivaFP();
	/**
	 * @param lecturaReactivaFP the lecturaReactivaFP to set
	 */
	public void setLecturaReactivaFP(Long lecturaReactivaFP);
	/**
	 * @return the lecturaReactivaHP
	 */
	public Long getLecturaReactivaHP();
	/**
	 * @param lecturaReactivaHP the lecturaReactivaHP to set
	 */
	public void setLecturaReactivaHP(Long lecturaReactivaHP);
	/**
	 * @return the lecturaReactivaXP
	 */
	public Long getLecturaReactivaXP();
	/**
	 * @param lecturaReactivaXP the lecturaReactivaXP to set
	 */
	public void setLecturaReactivaXP(Long lecturaReactivaXP);
}