package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

import com.synapsis.cns.codensa.model.DeudaServicio;

public class DeudaServicioImpl extends SynergiaBusinessObjectImpl 
	implements DeudaServicio {


    private Long nroCuenta;
	private Long nroServicio;
    private String tipoServicio;
    private String saldoServicio;
    private String estado;
    
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroServicio() {
		return nroServicio;
	}
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public String getSaldoServicio() {
		return saldoServicio;
	}
	public void setSaldoServicio(String saldoServicio) {
		this.saldoServicio = saldoServicio;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}


}
