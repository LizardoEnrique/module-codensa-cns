package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.DeudaOtrosConvenios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaOtrosConveniosImpl extends SynergiaBusinessObjectImpl
		implements DeudaOtrosConvenios {

	private Long nroCuenta;

	private String indOtrosConvenios;

	private Double saldoOtrosConvenios;
	
	private String saldoMora;
	private Long antiguedad;
	private String deudaECO;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getIndOtrosConvenios() {
		return indOtrosConvenios;
	}

	public void setIndOtrosConvenios(String indOtrosConvenios) {
		this.indOtrosConvenios = indOtrosConvenios;
	}

	public Double getSaldoOtrosConvenios() {
		return saldoOtrosConvenios;
	}

	public void setSaldoOtrosConvenios(Double saldoOtrosConvenios) {
		this.saldoOtrosConvenios = saldoOtrosConvenios;
	}

	public Long getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(Long antiguedad) {
		this.antiguedad = antiguedad;
	}

	public String getDeudaECO() {
		return deudaECO;
	}

	public void setDeudaECO(String deudaECO) {
		this.deudaECO = deudaECO;
	}

	public String getSaldoMora() {
		return saldoMora;
	}

	public void setSaldoMora(String saldoMora) {
		this.saldoMora = saldoMora;
	}
	
	
}