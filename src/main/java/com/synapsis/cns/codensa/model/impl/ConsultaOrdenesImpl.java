package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaOrdenes;

public class ConsultaOrdenesImpl extends SynergiaBusinessObjectImpl 
	implements ConsultaOrdenes {
    // Fields    
	
    private Long nroCuenta;
	private Long numeroServicio;
	private String tipoServicio;
	private String tipoOrden;
	private String codigoOrden;	
    private String numeroOrden;
    private String estadoOrden;
    private Date fechaGeneracion;
    private Date fechaFinalizacion;
    private Long diasHabilesDif;
    private Date fechaVencimientoLegal;
    private Date fechaVencimientoInterna;
    private String observaciones;
    private String usuarioGenerador;
    private String nombreUsuarioGenerador;
    private String usuarioFinalizador;
    private String nombreUsuarioFinalizador;
    private String areaResponsable;
    private String codArea;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}    

    public Long getNumeroServicio() {
    	return numeroServicio;
    }

    public void setNumeroServicio(Long numeroServicio) {
    	this.numeroServicio = numeroServicio;
    }

    public String getTipoServicio() {
    	return tipoServicio;
    }

    public void setTipoServicio(String tipoServicio) {
    	this.tipoServicio = tipoServicio;
    }    
    
	public String getAreaResponsable() {
		return areaResponsable;
	}

	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}

	public String getEstadoOrden() {
		return estadoOrden;
	}

	public void setEstadoOrden(String estadoOrden) {
		this.estadoOrden = estadoOrden;
	}

	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}

	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaVencimientoInterna() {
		return fechaVencimientoInterna;
	}

	public void setFechaVencimientoInterna(Date fechaVencimientoInterna) {
		this.fechaVencimientoInterna = fechaVencimientoInterna;
	}

	public Date getFechaVencimientoLegal() {
		return fechaVencimientoLegal;
	}

	public void setFechaVencimientoLegal(Date fechaVencimientoLegal) {
		this.fechaVencimientoLegal = fechaVencimientoLegal;
	}

	public String getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTipoOrden() {
		return tipoOrden;
	}

	public void setTipoOrden(String tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

	public String getUsuarioFinalizador() {
		return usuarioFinalizador;
	}

	public void setUsuarioFinalizador(String usuarioFinalizador) {
		this.usuarioFinalizador = usuarioFinalizador;
	}
	
	public String getNombreUsuarioFinalizador() {
		return nombreUsuarioFinalizador;
	}

	public void setNombreUsuarioFinalizador(String nombreUsuarioFinalizador) {
		this.nombreUsuarioFinalizador = nombreUsuarioFinalizador;
	}
	
	public String getUsuarioGenerador() {
		return usuarioGenerador;
	}

	public void setUsuarioGenerador(String usuarioGenerador) {
		this.usuarioGenerador = usuarioGenerador;
	}

	public String getNombreUsuarioGenerador() {
		return nombreUsuarioGenerador;
	}

	public void setNombreUsuarioGenerador(String nombreUsuarioGenerador) {
		this.nombreUsuarioGenerador = nombreUsuarioGenerador;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public Long getDiasHabilesDif() {
		return diasHabilesDif;
	}

	public void setDiasHabilesDif(Long diasHabilesDif) {
		this.diasHabilesDif = diasHabilesDif;
	}

	/**
	 * @return the codigoOrden
	 */
	public String getCodigoOrden() {
		return this.codigoOrden;
	}

	/**
	 * @param codigoOrden the codigoOrden to set
	 */
	public void setCodigoOrden(String codigoOrden) {
		this.codigoOrden = codigoOrden;
	}
	
   
}
