package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongDetalleSaldos extends SynergiaBusinessObject {

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroSaldo the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the saldoTotal
	 */
	public Double getSaldoTotal();
	/**
	 * @param saldoTotal the saldoTotal to set
	 */
	public void setSaldoTotal(Double saldoTotal) ;
	/**
	 * @return the valorEnergiaKvar
	 */
	public Double getValorEnergiaKvar();

	/**
	 * @param valorEnergiaKvar the valorEnergiaKvar to set
	 */
	public void setValorEnergiaKvar(Double valorEnergiaKvar);

	/**
	 * @return the energiaSd
	 */
	public Double getEnergiaSd();

	/**
	 * @param energiaSd the energiaSd to set
	 */
	public void setEnergiaSd(Double energiaSd);

	/**
	 * @return the saldoOtrosNeg
	 */
	public Double getSaldoOtrosNeg();
	/**
	 * @param saldoOtrosNeg the saldoOtrosNeg to set
	 */
	public void setSaldoOtrosNeg(Double saldoOtrosNeg);
	/**
	 * @return the saldoEnergDespuesCong
	 */
	public Double getSaldoEnergDespuesCong();

	/**
	 * @param saldoEnergDespuesCong the saldoEnergDespuesCong to set
	 */
	public void setSaldoEnergDespuesCong(Double saldoEnergDespuesCong);

	/**
	 * @return the valorEnergiaKwh
	 */
	public Double getValorEnergiaKwh();

	/**
	 * @param valorEnergiaKwh the valorEnergiaKwh to set
	 */
	public void setValorEnergiaKwh(Double valorEnergiaKwh);

	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado();
	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado);
	
	/**
	 * @return the saldoInicialCuentaDesc
	 */
	public Double getSaldoInicialCuentaDesc();

	/**
	 * @param saldoInicialCuentaDesc the saldoInicialCuentaDesc to set
	 */
	public void setSaldoInicialCuentaDesc(Double saldoInicialCuentaDesc);

	/**
	 * @return the saldoFinalCuentaDesc
	 */
	public Double getSaldoFinalCuentaDesc() ;

	/**
	 * @param saldoFinalCuentaDesc the saldoFinalCuentaDesc to set
	 */
	public void setSaldoFinalCuentaDesc(Double saldoFinalCuentaDesc);

	/**
	 * @return the valorDescongelado
	 */
	public Double getValorDescongelado() ;

	/**
	 * @param valorDescongelado the valorDescongelado to set
	 */
	public void setValorDescongelado(Double valorDescongelado);

	/**
	 * @return the saldoInicialServDesc
	 */
	public Double getSaldoInicialServDesc() ;

	/**
	 * @param saldoInicialServDesc the saldoInicialServDesc to set
	 */
	public void setSaldoInicialServDesc(Double saldoInicialServDesc) ;

	/**
	 * @return the saldoFinalServDesc
	 */
	public Double getSaldoFinalServDesc() ;

	/**
	 * @param saldoFinalServDesc the saldoFinalServDesc to set
	 */
	public void setSaldoFinalServDesc(Double saldoFinalServDesc);
	
	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo();
	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo);

	
}