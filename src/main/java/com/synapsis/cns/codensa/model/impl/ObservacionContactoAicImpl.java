package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ObservacionContactoAic;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author aPaulerena
 */
public class ObservacionContactoAicImpl extends SynergiaBusinessObjectImpl 
		implements ObservacionContactoAic {

	private String observacion;
	private Long id;
	private Date fechaObservacion;
	
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getObservacion() {
		return this.observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	/**
	 * @return the fechaObservacion
	 */
	public Date getFechaObservacion() {
		return fechaObservacion;
	}
	/**
	 * @param fechaObservacion the fechaObservacion to set
	 */
	public void setFechaObservacion(Date fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}
}
