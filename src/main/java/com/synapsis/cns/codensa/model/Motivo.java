package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface Motivo extends SynergiaBusinessObject {

	public String getCodigo();
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) ;
	/**
	 * @return the descripcion
	 */
	public String getDescripcion();
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion);

}
