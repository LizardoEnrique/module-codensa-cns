package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaCuentaDatosBasicos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class ConsultaCuentaDatosBasicosImpl extends SynergiaBusinessObjectImpl implements ConsultaCuentaDatosBasicos {

	private static final long serialVersionUID = 1L;

	private Long nroCuenta;
	private Long nroDocumento;
	private Date periodoFacturacion;
	private Date fechaLectura;
	private String codigoAnomalia;
	private String tarifa;
	private String tipoCliente;
	private String nivelTension;
	private String direccionRepartoEspecial;
	private String periodoLectura;
	private String tipoLectura;
	private Long consumoMinimoSubsistencia;

	private String estratoSocioeconomico;
	private String indicadorConsumoKW;
	private String tipoLiquidacion = "Mensual";
	private String tipoEsquemaFacturacion = "Tradicional";

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getCodigoAnomalia() {
		return codigoAnomalia;
	}

	public void setCodigoAnomalia(String codigoAnomalia) {
		this.codigoAnomalia = codigoAnomalia;
	}

	public Long getConsumoMinimoSubsistencia() {
		return consumoMinimoSubsistencia;
	}

	public void setConsumoMinimoSubsistencia(Long consumoMinimoSubsistencia) {
		this.consumoMinimoSubsistencia = consumoMinimoSubsistencia;
	}

	public String getDireccionRepartoEspecial() {
		return direccionRepartoEspecial;
	}

	public void setDireccionRepartoEspecial(String direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public String getEstratoSocioeconomico() {
		return estratoSocioeconomico;
	}

	public void setEstratoSocioeconomico(String estratoSocioeconomico) {
		this.estratoSocioeconomico = estratoSocioeconomico;
	}

	public String getIndicadorConsumoKW() {
		return indicadorConsumoKW;
	}

	public void setIndicadorConsumoKW(String indicadorConsumoKW) {
		this.indicadorConsumoKW = indicadorConsumoKW;
	}

	public String getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(String nivelTension) {
		this.nivelTension = nivelTension;
	}

	public Long getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Date getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	public void setPeriodoFacturacion(Date periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public String getTipoLectura() {
		return tipoLectura;
	}

	public void setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
	}

	public void setPeriodoLectura(String periodoLectura) {
		this.periodoLectura = periodoLectura;

	}

	public String getPeriodoLectura() {
		return this.periodoLectura;
	}

	public Date getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	public String getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(String tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public String getTipoEsquemaFacturacion() {
		return tipoEsquemaFacturacion;
	}

	public void setTipoEsquemaFacturacion(String tipoEsquemaFacturacion) {
		this.tipoEsquemaFacturacion = tipoEsquemaFacturacion;
	}

}