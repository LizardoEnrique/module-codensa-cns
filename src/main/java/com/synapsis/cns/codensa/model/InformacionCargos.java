package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface InformacionCargos extends SynergiaBusinessObject {

	public String getCodigoCargo();

	public String getDescripcionCargo();

	public Date getFechaFactura();

	public Date getFechaPago();

	public String getNroConvenioEncargo();

	public Long getNroDocumento();

	public Long getNroServicio();

	public String getOrigenCargo();

	public String getTipoServicio();

	public Long getValorCargo();

	public Long getValorPago();

	public void setCodigoCargo(String codigoCargo);

	public void setDescripcionCargo(String descripcionCargo);

	public void setFechaFactura(Date fechaFactura);

	public void setFechaPago(Date fechaPago);

	public void setNroConvenioEncargo(String nroConvenioEncargo);

	public void setNroDocumento(Long nroDocumento);

	public void setNroServicio(Long nroServicio);

	public void setOrigenCargo(String origenCargo);

	public void setTipoServicio(String tipoServicio);

	public void setValorCargo(Long valorCargo);

	public void setValorPago(Long valorPago);
	
	public Long getIdPago();
	
	public void setIdPago(Long idPago);	
	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();
	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
	
	/**
	 * @return the nroDocTitularSF
	 */
	public String getNroDocTitularSF();
	/**
	 * @param nroDocTitularSF the nroDocTitularSF to set
	 */
	public void setNroDocTitularSF(String nroDocTitularSF);
}