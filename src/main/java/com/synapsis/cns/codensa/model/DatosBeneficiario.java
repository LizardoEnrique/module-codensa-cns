package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosBeneficiario extends SynergiaBusinessObject {

	/**
	 * @return the dirPredio
	 */
	public abstract String getDirPredio();

	/**
	 * @param dirPredio the dirPredio to set
	 */
	public abstract void setDirPredio(String dirPredio);

	/**
	 * @return the distrito
	 */
	public abstract String getDistrito();

	/**
	 * @param distrito the distrito to set
	 */
	public abstract void setDistrito(String distrito);

	/**
	 * @return the documentoB
	 */
	public abstract String getDocumentoB();

	/**
	 * @param documentoB the documentoB to set
	 */
	public abstract void setDocumentoB(String documentoB);

	/**
	 * @return the email
	 */
	public abstract String getEmail();

	/**
	 * @param email the email to set
	 */
	public abstract void setEmail(String email);

	/**
	 * @return the nombreB
	 */
	public abstract String getNombreB();

	/**
	 * @param nombreB the nombreB to set
	 */
	public abstract void setNombreB(String nombreB);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the rutaLec
	 */
	public abstract String getRutaLec();

	/**
	 * @param rutaLec the rutaLec to set
	 */
	public abstract void setRutaLec(String rutaLec);

	/**
	 * @return the telefonoB
	 */
	public abstract String getTelefonoB();

	/**
	 * @param telefonoB the telefonoB to set
	 */
	public abstract void setTelefonoB(String telefonoB);

	/**
	 * @return the urbaniza
	 */
	public abstract String getUrbaniza();

	/**
	 * @param urbaniza the urbaniza to set
	 */
	public abstract void setUrbaniza(String urbaniza);
	/**
	 * @return the idPersona
	 */
	public Long getIdPersona();
	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(Long idPersona);

	
	public String getMunicipio();

	public void setMunicipio(String municipio);

	public Long getIdBeneficiario() ;

	/**
	 * @param id_beneficiario
	 *            the id_beneficiario to set
	 */
	public void setIdBeneficiario(Long id_beneficiario) ;
	
	/**
	 * @return the correlativoConjunto
	 */
	public String getCorrelativoConjunto();

	/**
	 * @param correlativoConjunto the correlativoConjunto to set
	 */
	public void setCorrelativoConjunto(String correlativoConjunto);
	
	/**
	 * @return the tieneCorrelativoConjunto
	 */
	public boolean isTieneCorrelativoConjunto();

}