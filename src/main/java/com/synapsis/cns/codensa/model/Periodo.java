package com.synapsis.cns.codensa.model;

import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface Periodo extends SynergiaBusinessObject {
	
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the a�o
	 */
	public String getAnio();

	/**
	 * @param a�o the a�o to set
	 */
	public void setAnio(String anio);

	public CalendarDate getFechaProceso();
	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(CalendarDate fechaProceso);

	/**
	 * @return the idAgrupCiclo
	 */
	public Long getIdAgrupCiclo();

	/**
	 * @param idAgrupCiclo the idAgrupCiclo to set
	 */
	public void setIdAgrupCiclo(Long idAgrupCiclo);

	/**
	 * @return the idConfigFact
	 */
	public Long getIdConfigFact();

	/**
	 * @param idConfigFact the idConfigFact to set
	 */
	public void setIdConfigFact(Long idConfigFact);

	/**
	 * @return the idPeriodo
	 */
	public Long getIdPeriodo();

	/**
	 * @param idPeriodo the idPeriodo to set
	 */
	public void setIdPeriodo(Long idPeriodo);

	/**
	 * @return the mes
	 */
	public String getMes();

	/**
	 * @param mes the mes to set
	 */
	public void setMes(String mes);
	
	public String getPeriodoFacturacion();

	public void setPeriodoFacturacion(String periodoFacturacion);
	
	/**
	 * @return the cli
	 */
	public Long getCli();
	/**
	 * @param cli the cli to set
	 */
	public void setCli(Long cli);

}
