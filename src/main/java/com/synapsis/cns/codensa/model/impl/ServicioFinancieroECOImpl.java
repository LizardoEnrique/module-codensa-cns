package com.synapsis.cns.codensa.model.impl;

import java.util.Set;

import com.synapsis.cns.codensa.model.ServicioFinancieroECO;

/**
 * Producto Encargos de Cobranza para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ServicioFinancieroECOImpl extends ServicioFinancieroImpl implements ServicioFinancieroECO {
	private static final long serialVersionUID = 1L;
	
	// Items
	Set itemsECO;

	public Set getItemsECO() {
		return itemsECO;
	}

	public void setItemsECO(Set itemsECO) {
		this.itemsECO = itemsECO;
	}

}