package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DetalleCargos extends SynergiaBusinessObject {

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo();

	/**
	 * @param codigoCargo the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo);

	/**
	 * @return the correlativoFacturacion
	 */
	public String getCorrelativoFacturacion();

	/**
	 * @param correlativoFacturacion the correlativoFacturacion to set
	 */
	public void setCorrelativoFacturacion(String correlativoFacturacion);

	/**
	 * @return the descripcionCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripcionCargo the descripcionCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the fechaFacturacion
	 */
	public Date getFechaFacturacion();

	/**
	 * @param fechaFacturacion the fechaFacturacion to set
	 */
	public void setFechaFacturacion(Date fechaFacturacion);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroDocumentoAssoc
	 */
	public Long getNroDocumentoAssoc();

	/**
	 * @param nroDocumentoAssoc the nroDocumentoAssoc to set
	 */
	public void setNroDocumentoAssoc(Long nroDocumentoAssoc);

	/**
	 * @return the periodoFacturacion
	 */
	public String getPeriodoFacturacion();

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(String periodoFacturacion);

	/**
	 * @return the valorCargo
	 */
	public Long getValorCargo();

	/**
	 * @param valorCargo the valorCargo to set
	 */
	public void setValorCargo(Long valorCargo);

	public String getTipoDocumento();
	
	public void setTipoDocumento(String tipoDocumento);
}