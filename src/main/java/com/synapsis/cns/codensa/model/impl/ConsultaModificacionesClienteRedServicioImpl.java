package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaModificacionesClienteRedServicio;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaModificacionesClienteRedServicioImpl extends SynergiaBusinessObjectImpl implements ConsultaModificacionesClienteRedServicio {


	private Long id;
	private Empresa empresa;
    private Long nroSrvElectrico;
    private String tipoSrvElectrico;
    private String estadoSrvElectrico;
    private String mercado;
    private String rutaFact;
    private String dirSuministro;
    private Double tarifa;
    
	private String periodoDesde;
	private String periodoHasta;

    
    
	public String getPeriodoDesde() {
		return periodoDesde;
	}
	public void setPeriodoDesde(String periodoDesde) {
		this.periodoDesde = periodoDesde;
	}
	public String getPeriodoHasta() {
		return periodoHasta;
	}
	public void setPeriodoHasta(String periodoHasta) {
		this.periodoHasta = periodoHasta;
	}
	public String getDirSuministro() {
		return dirSuministro;
	}
	public void setDirSuministro(String dirSuministro) {
		this.dirSuministro = dirSuministro;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getEstadoSrvElectrico() {
		return estadoSrvElectrico;
	}
	public void setEstadoSrvElectrico(String estadoSrvElectrico) {
		this.estadoSrvElectrico = estadoSrvElectrico;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMercado() {
		return mercado;
	}
	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	public Long getNroSrvElectrico() {
		return nroSrvElectrico;
	}
	public void setNroSrvElectrico(Long nroSrvElectrico) {
		this.nroSrvElectrico = nroSrvElectrico;
	}
	public String getRutaFact() {
		return rutaFact;
	}
	public void setRutaFact(String rutaFact) {
		this.rutaFact = rutaFact;
	}
	public Double getTarifa() {
		return tarifa;
	}
	public void setTarifa(Double tarifa) {
		this.tarifa = tarifa;
	}
	public String getTipoSrvElectrico() {
		return tipoSrvElectrico;
	}
	public void setTipoSrvElectrico(String tipoSrvElectrico) {
		this.tipoSrvElectrico = tipoSrvElectrico;
	}


}
