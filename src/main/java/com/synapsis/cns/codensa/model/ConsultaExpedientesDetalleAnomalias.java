package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedientesDetalleAnomalias extends SynergiaBusinessObject {

	/**
	 * @return the codigoAnomalia
	 */
	public String getCodigoAnomalia();

	/**
	 * @param codigoAnomalia the codigoAnomalia to set
	 */
	public void setCodigoAnomalia(String codigoAnomalia);

	/**
	 * @return the descripcionAnomalia
	 */
	public String getDescripcionAnomalia();

	/**
	 * @param descripcionAnomalia the descripcionAnomalia to set
	 */
	public void setDescripcionAnomalia(String descripcionAnomalia);
	
	public Long getNumeroOrdenInspeccion();
	
	public void setNumeroOrdenInspeccion(Long numeroOrdenInspeccion);

	public Long getCantidad();
	public void setCantidad(Long cantidad);

	
}