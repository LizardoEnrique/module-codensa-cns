package com.synapsis.cns.codensa.model.constants;



/**
 */
public class AnexoFacturaConstants {
	
	/**
	 * Keys de todos los campos mapeados en la LINEA del anexo.
	 * DEBEN COINCIDIR CON EL CAMPO "NOMBRE" de la tabla CNS_CAMPO_FACTURA, 
	 * en cuya tabla, se encuentra la ubicaci�n actual del campo dentro de la 
	 * l�nea (posici�n inicial, posici�n final ... etc) 
	 */

	public static String TIPO_DOC_ANEXO = "ANEXO";

	// CAMPOS FIJOS
	public static String FIELD_numero_cuenta = "numero_cuenta";
	public static String FIELD_dv_numero_cuenta = "dv_numero_cuenta";
	public static String FIELD_periodo_facturado = "periodo_facturado";
	public static String FIELD_fecha_expedicion = "fecha_expedicion";
	public static String FIELD_direccion_reparto = "direccion_reparto";	

	// Ruta
	public static String FIELD_manzana_reparto = "manzana_reparto";
	public static String FIELD_sucursal_reparto = "sucursal_reparto";
	public static String FIELD_zona_reparto = "zona_reparto";
	public static String FIELD_ciclo_reparto = "ciclo_reparto";
	public static String FIELD_grupo_reparto = "grupo_reparto"; 
	public static String FIELD_correlativo_reparto = "correlativo_reparto";
	
	public static String FIELD_localizacion_reparto = "localizacion_reparto";
	public static String FIELD_cod_depto_reparto = "cod_depto_reparto";
	public static String FIELD_desc_depto_reparto = "desc_depto_reparto";
	public static String FIELD_cod_munic_reparto = "cod_munic_reparto";
	public static String FIELD_desc_munic_reparto = "desc_munic_reparto";
	public static String FIELD_cod_localidad_reparto = "cod_localidad_reparto";
	public static String FIELD_desc_localidad_reparto = "desc_localidad_reparto";
	public static String FIELD_cod_upz_reparto = "cod_upz_reparto";
	public static String FIELD_desc_upz_reparto = "desc_upz_reparto";
	public static String FIELD_cod_barrio_reparto = "cod_barrio_reparto";
	public static String FIELD_desc_barrio_reparto = "desc_barrio_reparto";
	
	public static String FIELD_tipo_dir_reparto = "tipo_dir_reparto";
	public static String FIELD_mensaje_anexo_factura= "mensaje_anexo_factura";
	
	public static String FIELD_cant_serv_financ= "cant_serv_financ";
	public static String FIELD_cant_eco= "cant_eco";
	
	// CAMPOS VARIABLES (SERVICIOS FINANCIEROS - CCA)
	public static int    SERV_FINANC_CCA_SIZE = 1024;
	public static String FIELD_nro_servicio_financiero = "nro_servicio_financiero";
	public static String FIELD_nombre_titular = "nombre_titular";
	public static String FIELD_nro_tarjeta = "nro_tarjeta";
	public static String FIELD_linea_negocio = "linea_negocio";
	public static String FIELD_tipo_producto = "tipo_producto";
	public static String FIELD_nombre_plan = "nombre_plan";
	public static String FIELD_precio_producto = "precio_producto";
	public static String FIELD_valor_cuota_mensual = "valor_cuota_mensual";
	public static String FIELD_cuota_manejo = "cuota_manejo";
	public static String FIELD_interes_cuota_manejo = "interes_cuota_manejo";
	public static String FIELD_saldo_serv_financiero = "saldo_serv_financiero";
	public static String FIELD_capital_facturado = "capital_facturado";
	public static String FIELD_intereses_facturados = "intereses_facturados";
	public static String FIELD_tasa_actual_reg_financ = "tasa_actual_reg_financ";
	public static String FIELD_tasa_int_inicial = "tasa_int_inicial";
	public static String FIELD_establecimiento_compra = "establecimiento_compra";
	public static String FIELD_fecha_creacion_convenio = "fecha_creacion_convenio";
	public static String FIELD_fecha_compra = "fecha_compra";
	public static String FIELD_cupo_aprobado = "cupo_aprobado";
	public static String FIELD_cupo_disponible = "cupo_disponible";
	
	public static String FIELD_nro_total_cuotas = "nro_total_cuotas";
	public static String FIELD_nro_cuotas_facturadas = "nro_cuotas_facturadas";
	public static String FIELD_nro_cuotas_pendientes = "nro_cuotas_pendientes";
	public static String FIELD_tasa_interes_mora = "tasa_interes_mora";
	public static String FIELD_estado_convenio = "estado_convenio";
	public static String FIELD_tasa_efectiva_anual = "tasa_efectiva_anual";
	public static String FIELD_tasa_efectiva_mensual = "tasa_efectiva_mensual";
	public static String FIELD_periodo_compra = "periodo_compra";
	public static String FIELD_periodo_creacion_convenio = "periodo_creacion_convenio";
	public static String FIELD_periodos_impagos = "periodos_impagos";
	
	// Totales facturados
//	public static String FIELD_total_todos_servicios = "total_todos_servicios";
//	public static String FIELD_total_servicios = "total_servicios";
//	public static String FIELD_total_cedulas = "total_cedulas";
//	public static String FIELD_total_int_mora_cedula = "total_int_mora_cedula";
//	public static String FIELD_total_linea_negocio= "total_linea_negocio";
//	public static String FIELD_total_tipo_producto= "total_tipo_producto";
//	public static String FIELD_total_linea_por_establecimiento= "total_linea_por_establecimiento";
//	public static String FIELD_total_producto_por_establecimiento= "total_producto_por_establecimiento";
//	public static String FIELD_total_dia_almacen_cuota = "total_dia_almacen_cuota";
//	
//	// Saldos anteriores
//	public static String FIELD_saldo_capital_mora_todos_servicios = "saldo_capital_mora_todos_servicios";
//	public static String FIELD_saldo_interes_mora_todos_servicios = "saldo_interes_mora_todos_servicios";	
//	public static String FIELD_saldo_capital_mora_por_servicio = "saldo_capital_mora_por_servicio";
//	public static String FIELD_saldo_interes_mora_por_servicio = "saldo_interes_mora_por_servicio";
//	public static String FIELD_saldo_capital_mora_por_cedula = "saldo_capital_mora_por_cedula";
//	public static String FIELD_saldo_interes_mora_por_cedula = "saldo_interes_mora_por_cedula";

	
	// CAMPOS VARIABLES (ENCARGOS DE COBRANZA) //
	public static int    ECO_SIZE = 875;
	public static String FIELD_nro_eco = "nro_eco";
	public static String FIELD_id_eco = "id_eco";
	public static String FIELD_titular_eco = "titular_eco";
	public static String FIELD_linea_negocio_eco = "linea_negocio_eco";
	public static String FIELD_socio_negocio_eco = "socio_negocio_eco";
	public static String FIELD_tipo_producto_eco = "tipo_producto_eco";
	public static String FIELD_plan_eco = "plan_eco";
	public static String FIELD_valor_cuota_eco = "valor_cuota_eco";	
	public static String FIELD_perioricidad_cuota_eco = "perioricidad_cuota_eco";
	public static String FIELD_fecha_cargue_eco = "fecha_cargue_eco";
	
	public static String FIELD_nro_total_cuotas_eco = "nro_total_cuotas_eco";
	public static String FIELD_cuotas_facturadas_eco = "cuotas_facturadas_eco";
	public static String FIELD_nro_periodos_impagos_eco = "nro_periodos_impagos_eco";
	public static String FIELD_fecha_compra_eco = "fecha_compra_eco";
	public static String FIELD_cuotas_faltantes_eco = "cuotas_faltantes_eco";

	// Totales facturados
//	public static String FIELD_total_todos_servicios_eco = "total_todos_servicios_eco";
//	public static String FIELD_total_servicios_eco = "total_servicios_eco";
//	public static String FIELD_total_cedulas_eco = "total_cedulas_eco";
//	public static String FIELD_total_int_mora_cedula_eco = "total_int_mora_cedula_eco";
//	public static String FIELD_total_linea_negocio_eco = "total_linea_negocio_eco";
//	public static String FIELD_total_tipo_producto_eco= "total_tipo_producto_eco";
//	public static String FIELD_total_linea_por_socio_eco = "total_linea_por_socio_eco";
//	public static String FIELD_total_producto_por_socio_eco = "total_producto_por_socio_eco";
//	public static String FIELD_total_dia_almacen_cuota_eco = "total_dia_almacen_cuota_eco";
//	 
//	// Saldos anteriores
//	public static String FIELD_saldo_capital_mora_todos_servicios_eco = "saldo_capital_mora_todos_servicios_eco";
//	public static String FIELD_saldo_interes_mora_todos_servicios_eco = "saldo_interes_mora_todos_servicios_eco";	
}
