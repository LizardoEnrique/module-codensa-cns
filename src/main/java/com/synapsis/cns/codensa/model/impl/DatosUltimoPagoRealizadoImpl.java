package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.cns.codensa.model.DatosUltimoPagoRealizado;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosUltimoPagoRealizadoImpl extends SynergiaBusinessObjectImpl implements DatosUltimoPagoRealizado {
	private static final long serialVersionUID = 1L;
	private Long nroCuenta;  
	private Date fechaPago;    
	private String codCajero;
	private String tipoCargo;    
	private String montoTotal;
	private String oficina;
	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#getCodCajero()
	 */
	public String getCodCajero() {
		return codCajero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#setCodCajero(java.lang.String)
	 */
	public void setCodCajero(String codCajero) {
		this.codCajero = codCajero;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#getFechaPago()
	 */
	public Date getFechaPago() {
		return fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#setFechaPago(java.util.Date)
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#getMontoTotal()
	 */
	public String getMontoTotal() {
		return montoTotal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#setMontoTotal(java.lang.Double)
	 */
	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#getTipoCargo()
	 */
	public String getTipoCargo() {
		return tipoCargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimoPagoRealizado#setTipoCargo(java.lang.String)
	 */
	public void setTipoCargo(String tipoCargo) {
		this.tipoCargo = tipoCargo;
	}
	/**
	 * @return the oficina
	 */
	public String getOficina() {
		return oficina;
	}
	/**
	 * @param oficina the oficina to set
	 */
	public void setOficina(String oficina) {
		this.oficina = oficina;
	}  
}
