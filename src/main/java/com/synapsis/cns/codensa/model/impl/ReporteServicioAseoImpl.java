package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ReporteServicioAseo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ReporteServicioAseoImpl extends SynergiaBusinessObjectImpl implements ReporteServicioAseo {

	private String mes_uno;
	private String mes_dos;
	private String mes_tres;
	private String mes_cuatro;
	private String mes_cinco;
	private String mes_seis;
	private String nombre;
	private String direccion;
	private String puntoatencion;
	private String nit;
	private String direccionweb;
	private String barrido;
	private String recoleccion;
	private String densidad;
	private String volumen;
	private String participacion;
	
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getPuntoatencion() {
		return puntoatencion;
	}


	public void setPuntoatencion(String puntoatencion) {
		this.puntoatencion = puntoatencion;
	}


	public String getNit() {
		return nit;
	}


	public void setNit(String nit) {
		this.nit = nit;
	}


	public String getDireccionweb() {
		return direccionweb;
	}


	public void setDireccionweb(String direccionweb) {
		this.direccionweb = direccionweb;
	}


	public String getBarrido() {
		return barrido;
	}


	public void setBarrido(String barrido) {
		this.barrido = barrido;
	}


	public String getRecoleccion() {
		return recoleccion;
	}


	public void setRecoleccion(String recoleccion) {
		this.recoleccion = recoleccion;
	}


	public String getDensidad() {
		return densidad;
	}


	public void setDensidad(String densidad) {
		this.densidad = densidad;
	}


	public String getVolumen() {
		return volumen;
	}


	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}


	public String getParticipacion() {
		return participacion;
	}


	public void setParticipacion(String participacion) {
		this.participacion = participacion;
	}
	
	public String getMes_uno() {
		return mes_uno;
	}


	public void setMes_uno(String mes_uno) {
		this.mes_uno = mes_uno;
	}


	public String getMes_dos() {
		return mes_dos;
	}


	public void setMes_dos(String mes_dos) {
		this.mes_dos = mes_dos;
	}


	public String getMes_tres() {
		return mes_tres;
	}


	public void setMes_tres(String mes_tres) {
		this.mes_tres = mes_tres;
	}


	public String getMes_cuatro() {
		return mes_cuatro;
	}


	public void setMes_cuatro(String mes_cuatro) {
		this.mes_cuatro = mes_cuatro;
	}


	public String getMes_cinco() {
		return mes_cinco;
	}


	public void setMes_cinco(String mes_cinco) {
		this.mes_cinco = mes_cinco;
	}


	public String getMes_seis() {
		return mes_seis;
	}


	public void setMes_seis(String mes_seis) {
		this.mes_seis = mes_seis;
	}


	public int compareTo(Object o) {
		return this.getId().compareTo(((ReporteServicioAseo) o).getId());
	}


	
	
}
