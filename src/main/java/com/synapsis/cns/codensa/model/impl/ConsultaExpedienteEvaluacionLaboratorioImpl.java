package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteEvaluacionLaboratorio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaExpedienteEvaluacionLaboratorioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteEvaluacionLaboratorio {
	private Date fechaRealizacion; 	
	private String nombrePresencio;	
	private String indicadorEnergia;	
	private Long nroExpediente;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#getAnomaliasEncontradas()
	 */
	public Date getFechaRealizacion() {
		return fechaRealizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#setFechaRealizacion(java.util.Date)
	 */
	public void setFechaRealizacion(Date fechaRealizacion) {
		this.fechaRealizacion = fechaRealizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#getIndicadorEnergia()
	 */
	public String getIndicadorEnergia() {
		return indicadorEnergia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#setIndicadorEnergia(java.lang.String)
	 */
	public void setIndicadorEnergia(String indicadorEnergia) {
		this.indicadorEnergia = indicadorEnergia;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#getNombrePresencio()
	 */
	public String getNombrePresencio() {
		return nombrePresencio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEvaluacionLaboratorio#setNombrePresencio(java.lang.String)
	 */
	public void setNombrePresencio(String nombrePresencio) {
		this.nombrePresencio = nombrePresencio;
	}
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	} 

}
