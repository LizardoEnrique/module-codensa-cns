package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface CongElim extends SynergiaBusinessObject {

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion();

	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion);
	/**
	 * @return the state
	 */
	public String getState();
	/**
	 * @param state the state to set
	 */
	public void setState(String state);
	/**
	 * @return the motivo
	 */
	public String getMotivo();
	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(String motivo);

	/**
	 * @return the fechaEliminacion
	 */
	public Date getFechaEliminacion();

	/**
	 * @param fechaEliminacion the fechaEliminacion to set
	 */
	public void setFechaEliminacion(Date fechaEliminacion);
	
	
	public String getUsrEliminadorCong();
	
	
	public void setUsrEliminadorCong(String usrEliminadorCong);
	

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroSaldo the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
}