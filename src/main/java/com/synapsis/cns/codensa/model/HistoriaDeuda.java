package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface HistoriaDeuda extends SynergiaBusinessObject {

	public Long getAntDeudaSrvCodensa();
	public void setAntDeudaSrvCodensa(Long antDeudaSrvCodensa);
	public Long getAntDeudaSrvConvenio();
	public void setAntDeudaSrvConvenio(Long antDeudaSrvConvenio);
	public Long getAntDeudaSrvElectrico();
	public void setAntDeudaSrvElectrico(Long antDeudaSrvElectrico);
	public String getEstadoCobranzaSrvCodensa();
	public void setEstadoCobranzaSrvCodensa(String estadoCobranzaSrvCodensa);
	public String getEstadoCobranzaSrvConvenio();
	public void setEstadoCobranzaSrvConvenio(String estadoCobranzaSrvConvenio);
	public String getEstadoCobranzaSrvElectrico();
	public void setEstadoCobranzaSrvElectrico(String estadoCobranzaSrvElectrico);
	public Date getFechaMovimiento();
	public void setFechaMovimiento(Date fechaMovimiento);
	public Long getMovimiento();
	public void setMovimiento(Long movimiento);
	public Long getNroDocumento();
	public void setNroDocumento(Long nroDocumento);
	public String getTipoMovimiento();
	public void setTipoMovimiento(String tipoMovimiento);
	public Long getValorMovimiento();
	public void setValorMovimiento(Long valorMovimiento);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
}