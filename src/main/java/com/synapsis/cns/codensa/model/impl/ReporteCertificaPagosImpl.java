package com.synapsis.cns.codensa.model.impl;


/**
 * @author m3.MarioRoss - 12/12/2006
 *
 */
public class ReporteCertificaPagosImpl extends ReporteDatosGeneralesConDetalleImpl {
	private Double total;

	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
}
