package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.DatosConvenio;

public class DatosConvenioImpl extends SynergiaBusinessObjectImpl 
	implements DatosConvenio {
	
	private Date fechaPago;	
	private Long nroConvenioEncargo;	
	private Long valorPagado;	
	private Long valorPagar;	
	private Long valorCuotas;	
	private Long intereses;	
	private Long interesesMora;	
	private Long iva;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getFechaPago()
	 */
	public Date getFechaPago() {
		return fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getIntereses()
	 */
	public Long getIntereses() {
		return intereses;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getInteresesMora()
	 */
	public Long getInteresesMora() {
		return interesesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getIva()
	 */
	public Long getIva() {
		return iva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getNroConvenioEncargo()
	 */
	public Long getNroConvenioEncargo() {
		return nroConvenioEncargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getValorCuotas()
	 */
	public Long getValorCuotas() {
		return valorCuotas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getValorPagado()
	 */
	public Long getValorPagado() {
		return valorPagado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#getValorPagar()
	 */
	public Long getValorPagar() {
		return valorPagar;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setFechaPago(java.lang.String)
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setIntereses(java.lang.Long)
	 */
	public void setIntereses(Long intereses) {
		this.intereses = intereses;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setInteresesMora(java.lang.Long)
	 */
	public void setInteresesMora(Long interesesMora) {
		this.interesesMora = interesesMora;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setIva(java.lang.Long)
	 */
	public void setIva(Long iva) {
		this.iva = iva;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setNroConvenioEncargo(java.lang.Long)
	 */
	public void setNroConvenioEncargo(Long nroConvenioEncargo) {
		this.nroConvenioEncargo = nroConvenioEncargo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setValorCuotas(java.lang.Long)
	 */
	public void setValorCuotas(Long valorCuotas) {
		this.valorCuotas = valorCuotas;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setValorPagado(java.lang.Long)
	 */
	public void setValorPagado(Long valorPagado) {
		this.valorPagado = valorPagado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaHistoricoPagosDatosConvenio#setValorPagar(java.lang.Long)
	 */
	public void setValorPagar(Long valorPagar) {
		this.valorPagar = valorPagar;
	}
}