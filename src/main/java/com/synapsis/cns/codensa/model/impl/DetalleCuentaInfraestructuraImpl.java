package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.DetalleCuentaInfraestructura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ccamba
 * 
 */
public class DetalleCuentaInfraestructuraImpl extends SynergiaBusinessObjectImpl implements
		DetalleCuentaInfraestructura {

	private Long nroCuenta;
	private Long nroServicio;
	private String titularCuenta;
	private String rutaReparto;
	private String manzanaReparto;
	private String nroContrato;
	private Long idClienteTelem;
	private String aplicaDescuento;
	private String aplicaPeriodoGracia;
	private String contratoVigente;
	private Date fechaInstalacion;

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public Long getNroServicio() {
		return this.nroServicio;
	}

	public String getTitularCuenta() {
		return this.titularCuenta;
	}

	public String getRutaReparto() {
		return this.rutaReparto;
	}

	public String getManzanaReparto() {
		return this.manzanaReparto;
	}

	public String getNroContrato() {
		return this.nroContrato;
	}

	public Long getIdClienteTelem() {
		return this.idClienteTelem;
	}

	public String getAplicaDescuento() {
		return this.aplicaDescuento;
	}

	public String getAplicaPeriodoGracia() {
		return this.aplicaPeriodoGracia;
	}

	public String getContratoVigente() {
		return this.contratoVigente;
	}

	public Date getFechaInstalacion() {
		return this.fechaInstalacion;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}

	public void setRutaReparto(String rutaReparto) {
		this.rutaReparto = rutaReparto;
	}

	public void setManzanaReparto(String manzanaReparto) {
		this.manzanaReparto = manzanaReparto;
	}

	public void setNroContrato(String nroContrato) {
		this.nroContrato = nroContrato;
	}

	public void setIdClienteTelem(Long idClienteTelem) {
		this.idClienteTelem = idClienteTelem;
	}

	public void setAplicaDescuento(String aplicaDescuento) {
		this.aplicaDescuento = aplicaDescuento;
	}

	public void setAplicaPeriodoGracia(String aplicaPeriodoGracia) {
		this.aplicaPeriodoGracia = aplicaPeriodoGracia;
	}

	public void setContratoVigente(String contratoVigente) {
		this.contratoVigente = contratoVigente;
	}

	public void setFechaInstalacion(Date fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}

}
