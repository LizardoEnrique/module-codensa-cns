/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 * 
 * CU 006
 *
 */
public interface DetalleLecturaConsumoAuditoria extends SynergiaBusinessObject {

	public Long getIdCuenta() ;
	public Long getIdServicio();
	public Long getNroCuenta() ;
	public Long getNroServicio();
	public Double getConsumoActivaFPModificado() ;
	public Double getConsumoActivaFPOriginal() ;
	public Double getConsumoActivaHPModificado();
	public Double getConsumoActivaHPOriginal() ;
	public Double getConsumoActivaXPModificado() ;
	public Double getConsumoActivaXPOriginal() ;
	public Double getConsumoReactivaFPModificado();
	public Double getConsumoReactivaFPOriginal() ;
	public Double getConsumoReactivaHPModificado();
	public Double getConsumoReactivaHPOriginal() ;
	public Double getConsumoReactivaXPModificado();
	public Double getConsumoReactivaXPOriginal() ;
	public Date getFechaHoraAnalisis() ;
	
}
