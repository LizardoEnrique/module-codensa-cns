/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaRefacturacionAjusteInformacion extends
		SynergiaBusinessObject {

	public Long getIdAjuste() ;
	public Long getIdCuenta() ;
	public String getMovimientos();
	public Long getNroAjuste() ;
	public Long getNroCuenta() ;
	public String getObservaciones();
}
