package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.InformacionOperacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class InformacionOperacionImpl extends SynergiaBusinessObjectImpl 
	implements InformacionOperacion {
	private static final long serialVersionUID = 1L;
	
    private Long nroCuenta;
	private String tipoOperacion;
    private String contratista;
    private Date fechaGeneracion;
    private String usuarioCreador;
    private String nombreUsuarioCreador;
    private String estadoOperacion;
    private String motivoGeneracion;
    private Date fechaCumplimiento;
    private String resultadoOperacion;
    private String situacionEncontrada;
    private String subtipoOperacion;
    private String observaciones;
    
    //estos no es�n pedidos en el CU
    private Long idCuenta;
    private Long idServicio;
    private Long nroServicio;
    
    private Long nroOrden;
    

    public Long getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(Long nroOrden) {
		this.nroOrden = nroOrden;
	}

	//getter and setter
	public String getContratista() {
		return contratista;
	}
	
	public void setContratista(String contratista) {
		this.contratista = contratista;
	}
	
	public Date getFechaCumplimiento() {
		return fechaCumplimiento;
	}
	
	public void setFechaCumplimiento(Date fechaCumplimiento) {
		this.fechaCumplimiento = fechaCumplimiento;
	}
	
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	
	public String getNombreUsuarioCreador() {
		return nombreUsuarioCreador;
	}
	
	public void setNombreUsuarioCreador(String nombreUsuarioCreador) {
		this.nombreUsuarioCreador = nombreUsuarioCreador;
	}
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getResultadoOperacion() {
		return resultadoOperacion;
	}
	
	public void setResultadoOperacion(String resultadoOperacion) {
		this.resultadoOperacion = resultadoOperacion;
	}
	
	public String getSituacionEncontrada() {
		return situacionEncontrada;
	}
	
	public void setSituacionEncontrada(String situacionEncontrada) {
		this.situacionEncontrada = situacionEncontrada;
	}
	
	public String getSubtipoOperacion() {
		return subtipoOperacion;
	}
	
	public void setSubtipoOperacion(String subtipoOperacion) {
		this.subtipoOperacion = subtipoOperacion;
	}
	
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	public String getUsuarioCreador() {
		return usuarioCreador;
	}
	
	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public String getEstadoOperacion() {
		return estadoOperacion;
	}

	public void setEstadoOperacion(String estadoOperacion) {
		this.estadoOperacion = estadoOperacion;
	}

	public String getMotivoGeneracion() {
		return motivoGeneracion;
	}

	public void setMotivoGeneracion(String motivoGeneracion) {
		this.motivoGeneracion = motivoGeneracion;
	}

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}      
   
}
