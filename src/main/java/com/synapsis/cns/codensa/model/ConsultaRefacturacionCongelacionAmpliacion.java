/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 * CU 016
 */
public interface ConsultaRefacturacionCongelacionAmpliacion extends
		SynergiaBusinessObject {

	public Date getFechaAmpliacion() ;
	public Long getIdCuenta() ;
	public Long getIdOperacion();
	public Long getNroCuenta() ;
	public Long getNroOperacion();
	public String getObservacionAmpliacion();
	public String getSoporteAmpliacion() ;
}
