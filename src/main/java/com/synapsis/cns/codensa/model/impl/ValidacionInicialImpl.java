/**
 * $Id: ValidacionInicialImpl.java,v 1.1 2008/02/26 12:14:41 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ValidacionInicial;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ValidacionInicialImpl extends SynergiaBusinessObjectImpl 
	implements ValidacionInicial {

	
	private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;
	private String servElectEmpresarial;
	private String otroComercializador;
	private String ctaCorporativa;
	private String ctaOficial;
	private String denunciaContratorLeyArrendamiento;
	private String ctaProcesoFacturacion;
	private String ctaCodigoRetencion;
	private String ctadeudaCongelacion;
	private String ctaDeudaCastigada;
	private String indicatorClienteVip;
	
	public String getCtaCodigoRetencion() {
		return ctaCodigoRetencion;
	}
	public void setCtaCodigoRetencion(String ctaCodigoRetencion) {
		this.ctaCodigoRetencion = ctaCodigoRetencion;
	}
	public String getCtaCorporativa() {
		return ctaCorporativa;
	}
	public void setCtaCorporativa(String ctaCorporativa) {
		this.ctaCorporativa = ctaCorporativa;
	}
	public String getCtaDeudaCastigada() {
		return ctaDeudaCastigada;
	}
	public void setCtaDeudaCastigada(String ctaDeudaCastigada) {
		this.ctaDeudaCastigada = ctaDeudaCastigada;
	}
	public String getCtadeudaCongelacion() {
		return ctadeudaCongelacion;
	}
	public void setCtadeudaCongelacion(String ctadeudaCongelacion) {
		this.ctadeudaCongelacion = ctadeudaCongelacion;
	}
	public String getCtaOficial() {
		return ctaOficial;
	}
	public void setCtaOficial(String ctaOficial) {
		this.ctaOficial = ctaOficial;
	}
	public String getCtaProcesoFacturacion() {
		return ctaProcesoFacturacion;
	}
	public void setCtaProcesoFacturacion(String ctaProcesoFacturacion) {
		this.ctaProcesoFacturacion = ctaProcesoFacturacion;
	}
	public String getDenunciaContratorLeyArrendamiento() {
		return denunciaContratorLeyArrendamiento;
	}
	public void setDenunciaContratorLeyArrendamiento(
			String denunciaContratorLeyArrendamiento) {
		this.denunciaContratorLeyArrendamiento = denunciaContratorLeyArrendamiento;
	}
	public String getIndicatorClienteVip() {
		return indicatorClienteVip;
	}
	public void setIndicatorClienteVip(String indicatorClienteVip) {
		this.indicatorClienteVip = indicatorClienteVip;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getOtroComercializador() {
		return otroComercializador;
	}
	public void setOtroComercializador(String otroComercializador) {
		this.otroComercializador = otroComercializador;
	}
	public String getServElectEmpresarial() {
		return servElectEmpresarial;
	}
	public void setServElectEmpresarial(String servElectEmpresarial) {
		this.servElectEmpresarial = servElectEmpresarial;
	}
}
