package com.synapsis.cns.codensa.model;

import java.util.Date;
import java.util.Set;

/**
 * Interfaz del producto financiero Seguros Codensa, para el reporte de duplicado de anexo de factura (Servicios Financieros)
 *    
 * @author jhack
 */
public interface ProductoSeguroCodensa extends ServicioFinancieroECO{
	public Set getItemsSegurosCodensa();
	public void setItemsSegurosCodensa(Set itemsSegurosCodensa);
}
