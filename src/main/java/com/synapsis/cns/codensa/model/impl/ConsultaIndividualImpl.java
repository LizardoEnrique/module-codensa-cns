package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaIndividual;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * 
 * @author lcanas
 * 
 */
public class ConsultaIndividualImpl extends SynergiaBusinessObjectImpl implements ConsultaIndividual {

	private Long id;
	private Empresa empresa;
	private Long nroSrvElectrico;
	private Long nroCuenta;
	private String nombreCliente;
	private String sucursal;
	private String ciclo;
	private String estadoSrvElectrico;
	private Long nroTransformador;
	private String accion;
	private String tipoModificacion;
	private String rolRealizoModificacion;
	private Date fechaModificacion;
	private String ipTerminal;
	private String transDistribucion;
	

	
	//_______________________________________________________ SETTERS && GETTERS __________________________________
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getCiclo() {
		return ciclo;
	}
	public void setCiclo(String ciclo) {
		this.ciclo = ciclo;
	}
	public String getEstadoSrvElectrico() {
		return estadoSrvElectrico;
	}
	public void setEstadoSrvElectrico(String estadoSrvElectrico) {
		this.estadoSrvElectrico = estadoSrvElectrico;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getIpTerminal() {
		return ipTerminal;
	}
	public void setIpTerminal(String ipTerminal) {
		this.ipTerminal = ipTerminal;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Long getNroSrvElectrico() {
		return nroSrvElectrico;
	}
	public void setNroSrvElectrico(Long nroSrvElectrico) {
		this.nroSrvElectrico = nroSrvElectrico;
	}
	public Long getNroTransformador() {
		return nroTransformador;
	}
	public void setNroTransformador(Long nroTransformador) {
		this.nroTransformador = nroTransformador;
	}
	public String getRolRealizoModificacion() {
		return rolRealizoModificacion;
	}
	public void setRolRealizoModificacion(String rolRealizoModificacion) {
		this.rolRealizoModificacion = rolRealizoModificacion;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getTipoModificacion() {
		return tipoModificacion;
	}
	public void setTipoModificacion(String tipoModificacion) {
		this.tipoModificacion = tipoModificacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getTransDistribucion() {
		return transDistribucion;
	}
	public void setTransDistribucion(String transDistribucion) {
		this.transDistribucion = transDistribucion;
	}
	

}
