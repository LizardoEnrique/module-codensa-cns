package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.CargoFacturaDuplicadoLegal;

/**
 * 
 * @author egrande
 */
public class CargoFacturaDuplicadoLegalImpl extends AbstractCargoFactura implements CargoFacturaDuplicadoLegal {

	private String valor;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
