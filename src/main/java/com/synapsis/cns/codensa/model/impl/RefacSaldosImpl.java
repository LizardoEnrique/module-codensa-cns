package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.RefacSaldos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacSaldosImpl extends SynergiaBusinessObjectImpl implements RefacSaldos {

	private Long nroCuenta;
	private String nroAjuste;
	private Double valorEnergiaAjuste;
	private Double valorOtrosNegAjuste;
	private Double saldoTotalAntesAjuste;
	private Double saldoTotalDespuesAjuste;
	private Double saldoEnergiaAntesAjuste;
	private Double saldoEnergiaDespuesAjuste;
	private Double saldoOtrosNegAntesAjuste;
	private Double saldoOtrosNegDespuesAjuste;
	
	private Double saldoInicialServicioElectrico;
	private Double saldoInicialServicioConvenios;
	private Double saldoInicialServicioECO;
	private Double saldoInicialServicioFinanciero;
	private Double saldoInicialServicioVenta;
	private Double saldoInicialServicioCuotaManejo;
	private Double saldoFinalServicioElectrico;
	private Double saldoFinalServicioConvenios;
	private Double saldoFinalServicioECO;
	private Double saldoFinalServicioFinanciero;
	private Double saldoFinalServicioVenta;
	private Double saldoFinalCuotaManejo;
	private Double saldoAjusteServicioElectrico;
	private Double saldoAjusteServicioConvenios;
	private Double saldoAjusteServicioECO;
	private Double saldoAjusteServicioFinanciero;
	private Double saldoAjusteServicioVentas;
	private Double saldoAjusteServicioCuotaManejo;
	

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	public Double getValorEnergiaAjuste() {
		return valorEnergiaAjuste;
	}

	public void setValorEnergiaAjuste(Double valorEnergiaAjuste) {
		this.valorEnergiaAjuste = valorEnergiaAjuste;
	}

	public Double getValorOtrosNegAjuste() {
		return valorOtrosNegAjuste;
	}

	public void setValorOtrosNegAjuste(Double valorOtrosNegAjuste) {
		this.valorOtrosNegAjuste = valorOtrosNegAjuste;
	}

	public Double getSaldoTotalAntesAjuste() {
		return saldoTotalAntesAjuste;
	}

	public void setSaldoTotalAntesAjuste(Double saldoTotalAntesAjuste) {
		this.saldoTotalAntesAjuste = saldoTotalAntesAjuste;
	}

	public Double getSaldoTotalDespuesAjuste() {
		return saldoTotalDespuesAjuste;
	}

	public void setSaldoTotalDespuesAjuste(Double saldoTotalDespuesAjuste) {
		this.saldoTotalDespuesAjuste = saldoTotalDespuesAjuste;
	}

	public Double getSaldoEnergiaAntesAjuste() {
		return saldoEnergiaAntesAjuste;
	}

	public void setSaldoEnergiaAntesAjuste(Double saldoEnergiaAntesAjuste) {
		this.saldoEnergiaAntesAjuste = saldoEnergiaAntesAjuste;
	}

	public Double getSaldoEnergiaDespuesAjuste() {
		return saldoEnergiaDespuesAjuste;
	}

	public void setSaldoEnergiaDespuesAjuste(Double saldoEnergiaDespuesAjuste) {
		this.saldoEnergiaDespuesAjuste = saldoEnergiaDespuesAjuste;
	}

	public Double getSaldoOtrosNegAntesAjuste() {
		return saldoOtrosNegAntesAjuste;
	}

	public void setSaldoOtrosNegAntesAjuste(Double saldoOtrosNegAntesAjuste) {
		this.saldoOtrosNegAntesAjuste = saldoOtrosNegAntesAjuste;
	}

	public Double getSaldoOtrosNegDespuesAjuste() {
		return saldoOtrosNegDespuesAjuste;
	}

	public void setSaldoOtrosNegDespuesAjuste(Double saldoOtrosNegDespuesAjuste) {
		this.saldoOtrosNegDespuesAjuste = saldoOtrosNegDespuesAjuste;	
	}

	public Double getSaldoAjusteServicioConvenios() {
		return saldoAjusteServicioConvenios;
	}

	public void setSaldoAjusteServicioConvenios(Double saldoAjusteServicioConvenios) {
		this.saldoAjusteServicioConvenios = saldoAjusteServicioConvenios;
	}

	public Double getSaldoAjusteServicioCuotaManejo() {
		return saldoAjusteServicioCuotaManejo;
	}

	public void setSaldoAjusteServicioCuotaManejo(
			Double saldoAjusteServicioCuotaManejo) {
		this.saldoAjusteServicioCuotaManejo = saldoAjusteServicioCuotaManejo;
	}

	public Double getSaldoAjusteServicioECO() {
		return saldoAjusteServicioECO;
	}

	public void setSaldoAjusteServicioECO(Double saldoAjusteServicioECO) {
		this.saldoAjusteServicioECO = saldoAjusteServicioECO;
	}

	public Double getSaldoAjusteServicioElectrico() {
		return saldoAjusteServicioElectrico;
	}

	public void setSaldoAjusteServicioElectrico(Double saldoAjusteServicioElectrico) {
		this.saldoAjusteServicioElectrico = saldoAjusteServicioElectrico;
	}

	public Double getSaldoAjusteServicioFinanciero() {
		return saldoAjusteServicioFinanciero;
	}

	public void setSaldoAjusteServicioFinanciero(
			Double saldoAjusteServicioFinanciero) {
		this.saldoAjusteServicioFinanciero = saldoAjusteServicioFinanciero;
	}

	public Double getSaldoAjusteServicioVentas() {
		return saldoAjusteServicioVentas;
	}

	public void setSaldoAjusteServicioVentas(Double saldoAjusteServicioVentas) {
		this.saldoAjusteServicioVentas = saldoAjusteServicioVentas;
	}

	public Double getSaldoFinalCuotaManejo() {
		return saldoFinalCuotaManejo;
	}

	public void setSaldoFinalCuotaManejo(Double saldoFinalCuotaManejo) {
		this.saldoFinalCuotaManejo = saldoFinalCuotaManejo;
	}

	public Double getSaldoFinalServicioConvenios() {
		return saldoFinalServicioConvenios;
	}

	public void setSaldoFinalServicioConvenios(Double saldoFinalServicioConvenios) {
		this.saldoFinalServicioConvenios = saldoFinalServicioConvenios;
	}

	public Double getSaldoFinalServicioECO() {
		return saldoFinalServicioECO;
	}

	public void setSaldoFinalServicioECO(Double saldoFinalServicioECO) {
		this.saldoFinalServicioECO = saldoFinalServicioECO;
	}

	public Double getSaldoFinalServicioElectrico() {
		return saldoFinalServicioElectrico;
	}

	public void setSaldoFinalServicioElectrico(Double saldoFinalServicioElectrico) {
		this.saldoFinalServicioElectrico = saldoFinalServicioElectrico;
	}

	public Double getSaldoFinalServicioFinanciero() {
		return saldoFinalServicioFinanciero;
	}

	public void setSaldoFinalServicioFinanciero(Double saldoFinalServicioFinanciero) {
		this.saldoFinalServicioFinanciero = saldoFinalServicioFinanciero;
	}

	public Double getSaldoFinalServicioVenta() {
		return saldoFinalServicioVenta;
	}

	public void setSaldoFinalServicioVenta(Double saldoFinalServicioVenta) {
		this.saldoFinalServicioVenta = saldoFinalServicioVenta;
	}

	public Double getSaldoInicialServicioConvenios() {
		return saldoInicialServicioConvenios;
	}

	public void setSaldoInicialServicioConvenios(
			Double saldoInicialServicioConvenios) {
		this.saldoInicialServicioConvenios = saldoInicialServicioConvenios;
	}

	public Double getSaldoInicialServicioCuotaManejo() {
		return saldoInicialServicioCuotaManejo;
	}

	public void setSaldoInicialServicioCuotaManejo(
			Double saldoInicialServicioCuotaManejo) {
		this.saldoInicialServicioCuotaManejo = saldoInicialServicioCuotaManejo;
	}

	public Double getSaldoInicialServicioECO() {
		return saldoInicialServicioECO;
	}

	public void setSaldoInicialServicioECO(Double saldoInicialServicioECO) {
		this.saldoInicialServicioECO = saldoInicialServicioECO;
	}

	public Double getSaldoInicialServicioElectrico() {
		return saldoInicialServicioElectrico;
	}

	public void setSaldoInicialServicioElectrico(
			Double saldoInicialServicioElectrico) {
		this.saldoInicialServicioElectrico = saldoInicialServicioElectrico;
	}

	public Double getSaldoInicialServicioFinanciero() {
		return saldoInicialServicioFinanciero;
	}

	public void setSaldoInicialServicioFinanciero(
			Double saldoInicialServicioFinanciero) {
		this.saldoInicialServicioFinanciero = saldoInicialServicioFinanciero;
	}

	public Double getSaldoInicialServicioVenta() {
		return saldoInicialServicioVenta;
	}

	public void setSaldoInicialServicioVenta(Double saldoInicialServicioVenta) {
		this.saldoInicialServicioVenta = saldoInicialServicioVenta;
	}
	
	
	
}