package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaUltimasLect;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 * 
 */
public class ConsultaUltimasLectImpl extends SynergiaBusinessObjectImpl implements ConsultaUltimasLect {
	private Long nroCuenta;
	private String lectura;
	private Date fechaLectura;
	private Double lectEne_fp;
	private Double energiaFp;

	public Double getEnergiaFp() {
		return energiaFp;
	}

	public void setEnergiaFp(Double energiaFp) {
		this.energiaFp = energiaFp;
	}

	public Date getFechaLectura() {
		return fechaLectura;
	}

	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	public Double getLectEne_fp() {
		return lectEne_fp;
	}

	public void setLectEne_fp(Double lectEne_fp) {
		this.lectEne_fp = lectEne_fp;
	}

	public String getLectura() {
		return lectura;
	}

	public void setLectura(String lectura) {
		this.lectura = lectura;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

}
