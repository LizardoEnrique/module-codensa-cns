/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDatosTransformador;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaDatosTransformadorImpl extends SynergiaBusinessObjectImpl implements ConsultaDatosTransformador {
	
	private static final long serialVersionUID = 1L;

	private String transformador;
	private String estado;
	private String circuito;
	private String propiedad;
	private String conexion;
	private String tension;
	private String red;
	private String alumbrado;
	private String periodoFact;
	private Double cantidadServicios;
	private Date fechaProceso;
	
	
	public String getAlumbrado() {
		return alumbrado;
	}
	public void setAlumbrado(String alumbrado) {
		this.alumbrado = alumbrado;
	}
	public Double getCantidadServicios() {
		return cantidadServicios;
	}
	public void setCantidadServicios(Double cantidadServicios) {
		this.cantidadServicios = cantidadServicios;
	}
	public String getPeriodoFact() {
		return periodoFact;
	}
	public void setPeriodoFact(String periodoFact) {
		this.periodoFact = periodoFact;
	}
	public String getCircuito() {
		return circuito;
	}
	public void setCircuito(String circuito) {
		this.circuito = circuito;
	}
	public String getConexion() {
		return conexion;
	}
	public void setConexion(String conexion) {
		this.conexion = conexion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPropiedad() {
		return propiedad;
	}
	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}
	public String getRed() {
		return red;
	}
	public void setRed(String red) {
		this.red = red;
	}
	public String getTension() {
		return tension;
	}
	public void setTension(String tension) {
		this.tension = tension;
	}
	public String getTransformador() {
		return transformador;
	}
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
}