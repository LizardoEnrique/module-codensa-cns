package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaDatosTransformadorBySE extends SynergiaBusinessObject {
	public String getCircuito();

	public void setCircuito(String circuito);

	public String getConexion();

	public void setConexion(String conexion);

	public String getPropiedadTra();

	public void setPropiedadTra(String propiedadTra);

	public String getTension();

	public void setTension(String tension);

	public String getTransformador();

	public void setTransformador(String transformador);

	public Date getFechaAsociacion();

	public void setFechaAsociacion(Date fechaAsociacion);

	public Date getFechaDesAsociacion();

	public void setFechaDesAsociacion(Date fechaDesAsociacion);
	
	public Long getServicio();
	
	public void setServicio(Long servicio);
	
	public Date getFechaProceso();

	public void setFechaProceso(Date fechaProceso);
		
}
