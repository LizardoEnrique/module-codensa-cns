/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDeudaInformacionRefacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class ConsultaDeudaInformacionRefacturacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDeudaInformacionRefacturacion {

	private static final long serialVersionUID = 1L;
	
	private Long idCuenta;
	private String idDocumentoOriginal;
	private Long nroCuenta;
	private Long idServicio;
	private Long nroServicio;
	private String nroUltimoRefacturacion;
	private Date fechaEmicionUltimaFactura;
	private String valorRefacturacion;
	
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getFechaEmicionUltimaFactura()
	 */
	public Date getFechaEmicionUltimaFactura() {
		return fechaEmicionUltimaFactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setFechaEmicionUltimaFactura(java.util.Date)
	 */
	public void setFechaEmicionUltimaFactura(Date fechaEmicionUltimaFactura) {
		this.fechaEmicionUltimaFactura = fechaEmicionUltimaFactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getIdCuenta()
	 */
	public Long getIdCuenta() {
		return idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setIdCuenta(java.lang.Long)
	 */
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getIdServicio()
	 */
	public Long getIdServicio() {
		return idServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setIdServicio(java.lang.Long)
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getNroUltimoRefacturacion()
	 */
	public String getNroUltimoRefacturacion() {
		return nroUltimoRefacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setNroUltimoRefacturacion(java.lang.Long)
	 */
	public void setNroUltimoRefacturacion(String nroUltimoRefacturacion) {
		this.nroUltimoRefacturacion = nroUltimoRefacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#getValorRefacturacion()
	 */
	public String getValorRefacturacion() {
		return valorRefacturacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaDeudaInformacionRefacturacion#setValorRefacturacion(java.lang.Long)
	 */
	public void setValorRefacturacion(String valorRefacturacion) {
		this.valorRefacturacion = valorRefacturacion;
	}
	public String getIdDocumentoOriginal() {
		return idDocumentoOriginal;
	}
	public void setIdDocumentoOriginal(String idDocumentoOriginal) {
		this.idDocumentoOriginal = idDocumentoOriginal;
	}
}