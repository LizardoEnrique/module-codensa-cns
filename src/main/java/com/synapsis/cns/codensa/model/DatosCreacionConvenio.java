package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface DatosCreacionConvenio extends SynergiaBusinessObject{

	public String getCalidadSolicitante();
	public void setCalidadSolicitante(String calidadSolicitante);
	public Date getFechaAprobacion();
	public void setFechaAprobacion(Date fechaAprobacion);
	public Date getFechaCaducacion();
	public void setFechaCaducacion(Date fechaCaducacion);
	public Long getIdConvenio();
	public void setIdConvenio(Long idConvenio);
	public String getMotivoEstadoConvenio();
	public void setMotivoEstadoConvenio(String motivoEstadoConvenio);
	public String getNombreAprobador();
	public void setNombreAprobador(String nombreAprobador);
	public String getNombreCaduco();
	public void setNombreCaduco(String nombreCaduco);
	public String getNombreCreador();
	public void setNombreCreador(String nombreCreador);
	public String getNombreSolicitante();
	public void setNombreSolicitante(String nombreSolicitante);
	public Long getNroConvenio();
	public void setNroConvenio(Long nroConvenio);
	public String getNumeroAprobacion();
	public void setNumeroAprobacion(String numeroAprobacion);
	public String getUsuarioAprobador();
	public void setUsuarioAprobador(String usuarioAprobador);
	public String getUsuarioCaduco();
	public void setUsuarioCaduco(String usuarioCaduco);
	public String getUsuarioCreador();
	public void setUsuarioCreador(String usuarioCreador);
	public Double getValorCondonado();
	public void setValorCondonado(Double valorCondonado);
	public String getNumeroIdentificacionSolicitante();
	public void setNumeroIdentificacionSolicitante(String numeroIdentificacionSolicitante);
	public String getTipoIdentificacionSolicitante();
	public void setTipoIdentificacionSolicitante(String tipoIdentificacionSolicitante);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
}