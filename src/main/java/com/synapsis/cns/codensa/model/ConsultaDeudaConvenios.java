package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaDeudaConvenios extends SynergiaBusinessObject {

	/**
	 * @return the antiguedadDeuda
	 */
	public Long getAntiguedadDeuda();

	/**
	 * @param antiguedadDeuda
	 *            the antiguedadDeuda to set
	 */
	public void setAntiguedadDeuda(Long antiguedadDeuda);

	/**
	 * @return the deuda
	 */
	public Double getDeuda();

	/**
	 * @param deuda
	 *            the deuda to set
	 */
	public void setDeuda(Double deuda);

	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza();

	/**
	 * @param estadoCobranza
	 *            the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza);

	/**
	 * @return the indicadorDeudaPendiente
	 */
	public String getIndicadorDeudaPendiente();

	/**
	 * @param indicadorDeudaPendiente
	 *            the indicadorDeudaPendiente to set
	 */
	public void setIndicadorDeudaPendiente(String indicadorDeudaPendiente);

	/**
	 * @return the saldosEnergia
	 */
	public Long getSaldosEnergia();

	/**
	 * @param saldosEnergia
	 *            the saldosEnergia to set
	 */
	public void setSaldosEnergia(Long saldosEnergia);

	/**
	 * @return the totalCuotasFacturadas
	 */
	public Long getTotalCuotasFacturadas();

	/**
	 * @param totalCuotasFacturadas
	 *            the totalCuotasFacturadas to set
	 */
	public void setTotalCuotasFacturadas(Long totalCuotasFacturadas);

	/**
	 * @return the totalCuotasOtorgadas
	 */
	public Long getTotalCuotasOtorgadas();

	/**
	 * @param totalCuotasOtorgadas
	 *            the totalCuotasOtorgadas to set
	 */
	public void setTotalCuotasOtorgadas(Long totalCuotasOtorgadas);

	/**
	 * @return the valorDeudaCondonada
	 */
	public Double getValorDeudaCondonada();

	/**
	 * @param valorDeudaCondonada
	 *            the valorDeudaCondonada to set
	 */
	public void setValorDeudaCondonada(Double valorDeudaCondonada);

}