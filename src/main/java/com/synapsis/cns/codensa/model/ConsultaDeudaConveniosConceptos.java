package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.1 $ $Date: 2007/02/28 21:47:47 $
 * 
 */
public interface ConsultaDeudaConveniosConceptos extends SynergiaBusinessObject {

	/**
	 * @return the indicador
	 */
	public String getIndicador();

	/**
	 * @param indicador
	 *            the indicador to set
	 */
	public void setIndicador(String indicador);

	/**
	 * @return the saldo
	 */
	public Double getSaldo();

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(Double saldo);

}