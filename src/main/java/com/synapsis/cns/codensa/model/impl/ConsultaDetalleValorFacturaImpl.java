/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleValorFactura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 * 
 */
public class ConsultaDetalleValorFacturaImpl extends SynergiaBusinessObjectImpl
		implements ConsultaDetalleValorFactura {

	private String codigoCargo;
	private String nombreCargo;
	private Double valorCargo;
	private String unidad;
	private Long numeroUnidades;
	private String usuarioGenerador;
	private String nombreGenerador;
	private Date fechaCreacion;
	private Long numeroDocumento;
	private Long idCuenta;

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	/**
	 * @return the codigoCargo
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}

	/**
	 * @param codigoCargo
	 *            the codigoCargo to set
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the nombreCargo
	 */
	public String getNombreCargo() {
		return nombreCargo;
	}

	/**
	 * @param nombreCargo
	 *            the nombreCargo to set
	 */
	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}

	/**
	 * @return the nombreGenerador
	 */
	public String getNombreGenerador() {
		return nombreGenerador;
	}

	/**
	 * @param nombreGenerador
	 *            the nombreGenerador to set
	 */
	public void setNombreGenerador(String nombreGenerador) {
		this.nombreGenerador = nombreGenerador;
	}

	/**
	 * @return the numeroDocumento
	 */
	public Long getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento
	 *            the numeroDocumento to set
	 */
	public void setNumeroDocumento(Long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the numeroUnidades
	 */
	public Long getNumeroUnidades() {
		return numeroUnidades;
	}

	/**
	 * @param numeroUnidades
	 *            the numeroUnidades to set
	 */
	public void setNumeroUnidades(Long numeroUnidades) {
		this.numeroUnidades = numeroUnidades;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad
	 *            the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return the usuarioGenerador
	 */
	public String getUsuarioGenerador() {
		return usuarioGenerador;
	}

	/**
	 * @param usuarioGenerador
	 *            the usuarioGenerador to set
	 */
	public void setUsuarioGenerador(String usuarioGenerador) {
		this.usuarioGenerador = usuarioGenerador;
	}

	/**
	 * @return the valorCargo
	 */
	public Double getValorCargo() {
		return valorCargo;
	}

	/**
	 * @param valorCargo
	 *            the valorCargo to set
	 */
	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

}
