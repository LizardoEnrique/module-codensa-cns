package com.synapsis.cns.codensa.model.impl;
import java.util.Date;

import com.synapsis.cns.codensa.model.DatosUltimaRefacturacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DatosUltimaRefacturacionImpl extends SynergiaBusinessObjectImpl implements DatosUltimaRefacturacion   {
	private static final long serialVersionUID = 1L;
	
	private Long nroCuenta;  
	private Long nroServicio;  
	private Date fechaRefactura;
	private String aprobado;
	private String totalDocumento;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#getAprobado()
	 */
	public String getAprobado() {
		return aprobado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#setAprobado(java.lang.String)
	 */
	public void setAprobado(String aprobado) {
		this.aprobado = aprobado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#getFechaRefactura()
	 */
	public Date getFechaRefactura() {
		return fechaRefactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#setFechaRefactura(java.util.Date)
	 */
	public void setFechaRefactura(Date fechaRefactura) {
		this.fechaRefactura = fechaRefactura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#getNroServicio()
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#setNroServicio(java.lang.Long)
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#getTotalDocumento()
	 */
	public String getTotalDocumento() {
		return totalDocumento;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.edelnor.model.impl.DatosUltimaRefacturacion#setTotalDocumento(java.lang.Double)
	 */
	public void setTotalDocumento(String totalDocumento) {
		this.totalDocumento = totalDocumento;
	}  
}
