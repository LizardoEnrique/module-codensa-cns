package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturaDetalleValoresConsumo extends SynergiaBusinessObject{

	public Date getFechaFacturacion();

	public void setFechaFacturacion(Date fechaFacturacion);

	public Date getFechaLectura();

	public void setFechaLectura(Date fechaLectura);

	public String getPeriodo();

	public void setPeriodo(String periodo);

	public String getValorConsumoActiva();

	public void setValorConsumoActiva(String valorConsumoActiva);

	public String getValorConsumoReactiva();

	public void setValorConsumoReactiva(String valorConsumoReactiva);

	public String getValorContribucion();

	public void setValorContribucion(String valorContribucion);

	public String getValorSubsidio();

	public void setValorSubsidio(String valorSubsidio);
	
	public void setNroCuenta(Long nroCuenta);
	
	public Long getNroCuenta();

	public Double getAlumbradoPublico();

	public void setAlumbradoPublico(Double alumbradoPublico);

	public Double getCargoFijo();

	public void setCargoFijo(Double cargoFijo);
	
	public Double getDemandaHp();
	
	public void setDemandaHp(Double demandaHp);
	
	public Double getDemandaNormal();
	
	public void setDemandaNormal(Double demandaNormal);
	
	public Double getEnergiaActivaHp();
	
	public void setEnergiaActivaHp(Double energiaActivaHp);
	
	public Double getMantMedidor();
	
	public void setMantMedidor(Double mantMedidor);
	
	public Double getPotenciaGeneradaFp();
	
	public void setPotenciaGeneradaFp(Double potenciaGeneradaFp);
	
	public Double getPotenciaGeneradaHp();
	
	public void setPotenciaGeneradaHp(Double potenciaGeneradaHp);
	
	public String getCaracteristica082();
	
	public void setCaracteristica082(String caracteristica082);
	
	public Date getVigenciaTarifa();
	
	public void setVigenciaTarifa(Date vigenciaTarifa);
}