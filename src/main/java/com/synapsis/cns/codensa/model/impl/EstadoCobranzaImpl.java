package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.EstadoCobranza;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class EstadoCobranzaImpl extends SynergiaBusinessObjectImpl 
	implements EstadoCobranza {
    // Fields
	
	private Long antiguedad;
	private String caracterContacto;
	private String deuda;
	private String estadoAnterior;
	private String estadoCobranza;
	private Date fechaDemanda;
	private Date fechaHonorarioJuridico;
	private Date fechaHonorarioPrejuridico;
	private Date fechaModificacion;
	private String firmaAbogado;
	private String honorarioJuridico;
	private String honorarioPrejuridico;
	private String nombre;
	private String nombreContacto;
	private String nombreEmpresaProcEsp;
	private Long nroCuenta;
	private Long nroServicio;
	private String observacion;
	private String restriccionConvenio;
	private String tipoServicio;
	private String ubicacionJuridicional;
	private String usuario;
	private String etapaCobranza;
	private Long numeroCartera;
	
	/**
	 * @return the antiguedad
	 */
	public Long getAntiguedad() {
		return this.antiguedad;
	}
	/**
	 * @param antiguedad the antiguedad to set
	 */
	public void setAntiguedad(Long antiguedad) {
		this.antiguedad = antiguedad;
	}
	/**
	 * @return the caracterContacto
	 */
	public String getCaracterContacto() {
		return this.caracterContacto;
	}
	/**
	 * @param caracterContacto the caracterContacto to set
	 */
	public void setCaracterContacto(String caracterContacto) {
		this.caracterContacto = caracterContacto;
	}
	/**
	 * @return the deuda
	 */
	public String getDeuda() {
		return this.deuda;
	}
	/**
	 * @param deuda the deuda to set
	 */
	public void setDeuda(String deuda) {
		this.deuda = deuda;
	}
	/**
	 * @return the estadoAnterior
	 */
	public String getEstadoAnterior() {
		return this.estadoAnterior;
	}
	/**
	 * @param estadoAnterior the estadoAnterior to set
	 */
	public void setEstadoAnterior(String estadoAnterior) {
		this.estadoAnterior = estadoAnterior;
	}
	/**
	 * @return the estadoCobranza
	 */
	public String getEstadoCobranza() {
		return this.estadoCobranza;
	}
	/**
	 * @param estadoCobranza the estadoCobranza to set
	 */
	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}
	/**
	 * @return the fechaDemanda
	 */
	public Date getFechaDemanda() {
		return this.fechaDemanda;
	}
	/**
	 * @param fechaDemanda the fechaDemanda to set
	 */
	public void setFechaDemanda(Date fechaDemanda) {
		this.fechaDemanda = fechaDemanda;
	}
	/**
	 * @return the fechaHonorarioJuridico
	 */
	public Date getFechaHonorarioJuridico() {
		return this.fechaHonorarioJuridico;
	}
	/**
	 * @param fechaHonorarioJuridico the fechaHonorarioJuridico to set
	 */
	public void setFechaHonorarioJuridico(Date fechaHonorarioJuridico) {
		this.fechaHonorarioJuridico = fechaHonorarioJuridico;
	}
	/**
	 * @return the fechaHonorarioPrejuridico
	 */
	public Date getFechaHonorarioPrejuridico() {
		return this.fechaHonorarioPrejuridico;
	}
	/**
	 * @param fechaHonorarioPrejuridico the fechaHonorarioPrejuridico to set
	 */
	public void setFechaHonorarioPrejuridico(Date fechaHonorarioPrejuridico) {
		this.fechaHonorarioPrejuridico = fechaHonorarioPrejuridico;
	}
	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	/**
	 * @return the firmaAbogado
	 */
	public String getFirmaAbogado() {
		return this.firmaAbogado;
	}
	/**
	 * @param firmaAbogado the firmaAbogado to set
	 */
	public void setFirmaAbogado(String firmaAbogado) {
		this.firmaAbogado = firmaAbogado;
	}
	/**
	 * @return the honorarioJuridico
	 */
	public String getHonorarioJuridico() {
		return this.honorarioJuridico;
	}
	/**
	 * @param honorarioJuridico the honorarioJuridico to set
	 */
	public void setHonorarioJuridico(String honorarioJuridico) {
		this.honorarioJuridico = honorarioJuridico;
	}
	/**
	 * @return the honorarioPrejuridico
	 */
	public String getHonorarioPrejuridico() {
		return this.honorarioPrejuridico;
	}
	/**
	 * @param honorarioPrejuridico the honorarioPrejuridico to set
	 */
	public void setHonorarioPrejuridico(String honorarioPrejuridico) {
		this.honorarioPrejuridico = honorarioPrejuridico;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return this.nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the nombreContacto
	 */
	public String getNombreContacto() {
		return this.nombreContacto;
	}
	/**
	 * @param nombreContacto the nombreContacto to set
	 */
	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	/**
	 * @return the nombreEmpresaProcEsp
	 */
	public String getNombreEmpresaProcEsp() {
		return this.nombreEmpresaProcEsp;
	}
	/**
	 * @param nombreEmpresaProcEsp the nombreEmpresaProcEsp to set
	 */
	public void setNombreEmpresaProcEsp(String nombreEmpresaProcEsp) {
		this.nombreEmpresaProcEsp = nombreEmpresaProcEsp;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return this.nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return this.observacion;
	}
	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	/**
	 * @return the restriccionConvenio
	 */
	public String getRestriccionConvenio() {
		return this.restriccionConvenio;
	}
	/**
	 * @param restriccionConvenio the restriccionConvenio to set
	 */
	public void setRestriccionConvenio(String restriccionConvenio) {
		this.restriccionConvenio = restriccionConvenio;
	}
	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return this.tipoServicio;
	}
	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	/**
	 * @return the ubicacionJuridicional
	 */
	public String getUbicacionJuridicional() {
		return this.ubicacionJuridicional;
	}
	/**
	 * @param ubicacionJuridicional the ubicacionJuridicional to set
	 */
	public void setUbicacionJuridicional(String ubicacionJuridicional) {
		this.ubicacionJuridicional = ubicacionJuridicional;
	}
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return this.usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEtapaCobranza() {
		return etapaCobranza;
	}
	public void setEtapaCobranza(String etapaCobranza) {
		this.etapaCobranza = etapaCobranza;
	}
	public Long getNumeroCartera() {
		return numeroCartera;
	}
	public void setNumeroCartera(Long numeroCartera) {
		this.numeroCartera = numeroCartera;
	}

	
   
}
