package com.synapsis.cns.codensa.model.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import com.synapsis.cns.codensa.model.CargoFactura;
import com.synapsis.cns.codensa.model.FacturaTotales;
import com.synapsis.cns.codensa.model.InformacionConsumos;
import com.synapsis.cns.codensa.model.InformacionPago;
import com.synapsis.cns.codensa.model.ReporteFactura;

/**
 * Cabecera del reporte de Impresion Duplicado Facturas (CNS003)
 * 
 * @author adambrosio
 */
public class ReporteFacturaImpl extends AbstractReporteFactura implements ReporteFactura {
	private FacturaTotales totalesFactura;
	private InformacionConsumos infoCons;
	private Date fechaExpedicion;
	private Date fechaProceso;
	private Date periodoFacturadoInicio;
	private Date periodoFacturadoFin;
	private Date fechaVencimientoPagoOportuno;
	private Date fechaVencimientoAvisoSuspension;
	private Long totalAPagar;

	public ReporteFacturaImpl() {
		this.setConsumos(new TreeSet());
		this.setConsumosUltimos6Meses(new TreeSet());
        this.setConsumosServicioAseo(new TreeSet());
		this.setCargos(new TreeSet());
		this.setOtrosCargos(new TreeSet());
		this.setCargosDescuento(new TreeSet());
		this.setCargosOtrosServPort(new TreeSet());
		this.setCargosServicioElectrico(new TreeSet());
	
	}

	public InformacionConsumos getInfoCons() {
		if (this.infoCons == null) {
			// this.infoCons = new InformacionConsumosImpl();
			this.infoCons = new InformacionConsumosImpl(new HashMap());
			this.infoCons.setAnomaliaLecturaPeriodoActual("ASDASFG");
			this.infoCons.setMesTarifaParaLiquidacion("Agosto");
			this.infoCons.setConsumoPromedioUltimosMeses(new Double(203.5));
			this.infoCons.setCostoUnitario(new Double(2.5));
			this.infoCons.setEnergiaConsumida(new Double(50.5));
			this.infoCons.setEnergiaFacturada(new Double(50.5));
		}
		return this.infoCons;
	}

	public void setInfoCons(InformacionConsumos infoCons) {
		this.infoCons = infoCons;
	}

	/*
	 * DATOS de InformacionConsumo No est� bueno hacerlo as� pero no encontre otra forma
	 */

	public String getAnomaliaLecturaPeriodoActual() {
		return this.getInfoCons().getAnomaliaLecturaPeriodoActual();
	}

	public Double getComercializacion() {
		return this.getInfoCons().getComercializacion();
	}

	public Double getCostoFijo() {
		return this.getInfoCons().getCostoFijo();
	}

	public Double getOpcionTarifaria() {
		return this.getInfoCons().getOpcionTarifaria();
	}

	public Double getConsumoPromedioUltimosMeses() {
		return this.getInfoCons().getConsumoPromedioUltimosMeses();
	}

	public Double getCostoUnitario() {
		return this.getInfoCons().getCostoUnitario();
	}

	public Double getDiferenciaEntreLecturaActualYAnterior() {
		return this.getInfoCons().getDiferenciaEntreLecturaActualYAnterior();
	}

	public Double getDistribucion() {
		return this.getInfoCons().getDistribucion();
	}

	public Double getEnergiaConsumida() {
		return this.getInfoCons().getEnergiaConsumida();
	}

	public Double getEnergiaFacturada() {
		return this.getInfoCons().getEnergiaFacturada();
	}

	public Double getFactorLiquidacion() {
		return this.getInfoCons().getFactorLiquidacion();
	}

	public Double getGeneracion() {
		return this.getInfoCons().getGeneracion();
	}

	public Double getLecturaActual() {
		return this.getInfoCons().getLecturaActual();
	}

	public Double getLecturaAnterior() {
		return this.getInfoCons().getLecturaAnterior();
	}

	public Integer getMaximoPermitidoHoras() {
		return this.getInfoCons().getMaximoPermitidoHoras();
	}

	public Integer getMaximoPermitidoInterrupciones() {
		return this.getInfoCons().getMaximoPermitidoInterrupciones();
	}

	public String getMesTarifaParaLiquidacion() {
		return this.getInfoCons().getMesTarifaParaLiquidacion();
	}

	public Integer getNumeroHorasInterrupcionesAcumuladas() {
		return this.getInfoCons().getNumeroHorasInterrupcionesAcumuladas();
	}

	public Integer getNumeroInterrupcionesAcumuladas() {
		return this.getInfoCons().getNumeroInterrupcionesAcumuladas();
	}

	public Double getOtros() {
		return this.getInfoCons().getOtros();
	}

	public Double getPerdidas() {
		return this.getInfoCons().getPerdidas();
	}

	public Long getPeriodoInterrupciones() {
		return this.getInfoCons().getPeriodoInterrupciones();
	}

	public String getTarifaParaFacturacionActual() {
		return this.getInfoCons().getTarifaParaFacturacionActual();
	}

	public String getTipoLecturaPeriodoActual() {
		return this.getInfoCons().getTipoLecturaPeriodoActual();
	}

	public Double getTotalConsumoKwh() {
		return this.getInfoCons().getTotalConsumoKwh();
	}

	public Double getTransmision() {
		return this.getInfoCons().getTransmision();
	}

	public Double getTrimestreInterrupciones() {
		return this.getInfoCons().getTrimestreInterrupciones();
	}

	public Double getValorPromedioKwh() {
		return this.getInfoCons().getValorPromedioKwh();
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaVencimientoAvisoSuspension() {
		return fechaVencimientoAvisoSuspension;
	}

	public void setFechaVencimientoAvisoSuspension(Date fechaVencimientoAvisoSuspension) {
		this.fechaVencimientoAvisoSuspension = fechaVencimientoAvisoSuspension;
	}

	public Date getFechaVencimientoPagoOportuno() {
		return fechaVencimientoPagoOportuno;
	}

	public void setFechaVencimientoPagoOportuno(Date fechaVencimientoPagoOportuno) {
		this.fechaVencimientoPagoOportuno = fechaVencimientoPagoOportuno;
	}

	public Date getPeriodoFacturadoFin() {
		return periodoFacturadoFin;
	}

	public void setPeriodoFacturadoFin(Date periodoFacturadoFin) {
		this.periodoFacturadoFin = periodoFacturadoFin;
	}

	public Date getPeriodoFacturadoInicio() {
		return periodoFacturadoInicio;
	}

	public void setPeriodoFacturadoInicio(Date periodoFacturadoInicio) {
		this.periodoFacturadoInicio = periodoFacturadoInicio;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Double getTotalFactura() {
		return this.getTotalesFactura().getTotalFactura();
	}

	public Long getTotalAPagar() {
		return totalAPagar;
	}

	public void setTotalAPagar(Long totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

	public void cargarLaManija() {
		for (Iterator iterator = this.getConsumos().iterator(); iterator.hasNext();) {
			InformacionConsumos informacionConsumos = (InformacionConsumos) iterator.next();
			String tarifa = informacionConsumos.getTarifaParaFacturacionActual();
			if (tarifa.equals("EAHP")) {
				this.setTarifaParaFacturacionActual_EAHP(informacionConsumos.getTarifaParaFacturacionActual());
				this.setLecturaActual_EAHP(informacionConsumos.getLecturaActual());
				this.setLecturaAnterior_EAHP(informacionConsumos.getLecturaAnterior());
				this.setDiferenciaEntreLecturaActualYAnterior_EAHP(informacionConsumos
					.getDiferenciaEntreLecturaActualYAnterior());
				this.setFactorLiquidacion_EAHP(informacionConsumos.getFactorLiquidacion());
				this.setTotalConsumoKwh_EAHP(informacionConsumos.getTotalConsumoKwh());
			}
			if (tarifa.equals("EAFP")) {
				this.setTarifaParaFacturacionActual_EAFP(informacionConsumos.getTarifaParaFacturacionActual());
				this.setLecturaActual_EAFP(informacionConsumos.getLecturaActual());
				this.setLecturaAnterior_EAFP(informacionConsumos.getLecturaAnterior());
				this.setDiferenciaEntreLecturaActualYAnterior_EAFP(informacionConsumos
					.getDiferenciaEntreLecturaActualYAnterior());
				this.setFactorLiquidacion_EAFP(informacionConsumos.getFactorLiquidacion());
				this.setTotalConsumoKwh_EAFP(informacionConsumos.getTotalConsumoKwh());
				// this.getInfoCons().setAnomaliaLecturaPeriodoActual(informacionConsumos.getAnomaliaLecturaPeriodoActual());
				// this.getInfoCons().setTarifaParaFacturacionActual(informacionConsumos.getTarifaParaFacturacionActual());
				// this.getInfoCons().setGeneracion(informacionConsumos.getGeneracion());
				// this.getInfoCons().setTransmision(informacionConsumos.getTransmision());
				// this.getInfoCons().setDistribucion(informacionConsumos.getDistribucion());
				// this.getInfoCons().setPerdidas(informacionConsumos.getPerdidas());
				// this.getInfoCons().setPeriodoInterrupciones(informacionConsumos.getPeriodoInterrupciones());
				// this.getInfoCons().setOtros(informacionConsumos.getOtros());
				// this.getInfoCons().setCostoUnitario(informacionConsumos.getCostoUnitario());
				// this.getInfoCons().setEnergiaConsumida(informacionConsumos.getEnergiaConsumida());
				// this.getInfoCons().setEnergiaFacturada(informacionConsumos.getEnergiaFacturada());
				// this.getInfoCons().setValorPromedioKwh(informacionConsumos.getValorPromedioKwh());
				// this.getInfoCons().setMesTarifaParaLiquidacion(informacionConsumos.getMesTarifaParaLiquidacion());
				// this.getInfoCons().setTipoLecturaPeriodoActual(informacionConsumos.getTipoLecturaPeriodoActual());
			}
			if (tarifa.equals("ERHP")) {
				this.setTarifaParaFacturacionActual_ERHP(informacionConsumos.getTarifaParaFacturacionActual());
				this.setLecturaActual_ERHP(informacionConsumos.getLecturaActual());
				this.setLecturaAnterior_ERHP(informacionConsumos.getLecturaAnterior());
				this.setDiferenciaEntreLecturaActualYAnterior_ERHP(informacionConsumos
					.getDiferenciaEntreLecturaActualYAnterior());
				this.setFactorLiquidacion_ERHP(informacionConsumos.getFactorLiquidacion());
				this.setTotalConsumoKwh_ERHP(informacionConsumos.getTotalConsumoKwh());

			}
			if (tarifa.equals("ERFP")) {
				this.setTarifaParaFacturacionActual_ERFP(informacionConsumos.getTarifaParaFacturacionActual());
				this.setLecturaActual_ERFP(informacionConsumos.getLecturaActual());
				this.setLecturaAnterior_ERFP(informacionConsumos.getLecturaAnterior());
				this.setDiferenciaEntreLecturaActualYAnterior_ERFP(informacionConsumos
					.getDiferenciaEntreLecturaActualYAnterior());
				this.setFactorLiquidacion_ERFP(informacionConsumos.getFactorLiquidacion());
				this.setTotalConsumoKwh_ERFP(informacionConsumos.getTotalConsumoKwh());

			}

			this.getInfoCons().setTipoLecturaPeriodoActual(informacionConsumos.getTipoLecturaPeriodoActual());
			this.getInfoCons().setAnomaliaLecturaPeriodoActual(informacionConsumos.getAnomaliaLecturaPeriodoActual());
			this.getInfoCons().setEnergiaConsumida(informacionConsumos.getEnergiaConsumida());
			this.getInfoCons().setEnergiaFacturada(informacionConsumos.getEnergiaFacturada());
			this.getInfoCons().setMesTarifaParaLiquidacion(informacionConsumos.getMesTarifaParaLiquidacion());
			this.getInfoCons().setConsumoPromedioUltimosMeses(informacionConsumos.getConsumoPromedioUltimosMeses());
			this.getInfoCons().setCostoUnitario(informacionConsumos.getCostoUnitario());
			this.getInfoCons().setGeneracion(informacionConsumos.getGeneracion());
			this.getInfoCons().setTransmision(informacionConsumos.getTransmision());
			this.getInfoCons().setPerdidas(informacionConsumos.getPerdidas());
			this.getInfoCons().setDistribucion(informacionConsumos.getDistribucion());
			this.getInfoCons().setOtros(informacionConsumos.getOtros());
			this.getInfoCons().setComercializacion(informacionConsumos.getComercializacion());
			this.getInfoCons().setCostoFijo(informacionConsumos.getCostoFijo());
			this.getInfoCons().setOpcionTarifaria(informacionConsumos.getOpcionTarifaria());
			this.getInfoCons().setValorPromedioKwh(informacionConsumos.getValorPromedioKwh());
			this.getInfoCons().setTrimestreInterrupciones(informacionConsumos.getTrimestreInterrupciones());
			this.getInfoCons().setNumeroHorasInterrupcionesAcumuladas(
				informacionConsumos.getNumeroHorasInterrupcionesAcumuladas());
			this.getInfoCons().setNumeroInterrupcionesAcumuladas(
				informacionConsumos.getNumeroInterrupcionesAcumuladas());
			this.getInfoCons().setMaximoPermitidoHoras(informacionConsumos.getMaximoPermitidoHoras());
			this.getInfoCons().setMaximoPermitidoInterrupciones(informacionConsumos.getMaximoPermitidoInterrupciones());
		}
	}

	private FacturaTotales obtenerTotalesFactura() {
		FacturaTotales totalesFactura;
		if (this.getTotalesFacturaSet() != null && this.getTotalesFacturaSet().size() > 0) {
			totalesFactura = (FacturaTotales) this.getTotalesFacturaSet().iterator().next();
		}
		else {
			// Dejo como estaba esto, si era nulo manda fruta
			totalesFactura = new FacturaTotalesImpl();
			totalesFactura.setNroFactura(this.getNroFactura());
			totalesFactura.setSubtTotCargosEne(new Double(10.5));
			totalesFactura.setSubtCargosCons(calcularMontosCargos());
			totalesFactura.setSubtCargosDesc(new Double(10.5));
			totalesFactura.setSubtCargosSrvPort(new Double(10.5));
			totalesFactura.setSubtOtrosCargos(new Double(10.5));
			totalesFactura.setTotalFactura(new Double(100.5));
		}
		return totalesFactura;
	}

	private Long calcularMontosCargos() {
		Long total = new Long(0);
		for (Iterator iterator = this.getCargos().iterator(); iterator.hasNext();) {
			CargoFactura cargo = (CargoFactura) iterator.next();
			total = new Long(cargo.getValor().longValue() + total.longValue());
		}
		return total;
	}

	public FacturaTotales getTotalesFactura() {
		if (this.totalesFactura == null) {
			this.totalesFactura = this.obtenerTotalesFactura();
		}
		return totalesFactura;
	}

	public void setTotalesFactura(FacturaTotales totalesFactura) {
		this.totalesFactura = totalesFactura;
	}

	/*
	 * Subtotales Factura
	 */

	public Long getSubtotalValorConsumo() {
		return this.getTotalesFactura().getSubtCargosCons();
	}

	public Double getSubtotalValorCargosDescuento() {
		return this.getTotalesFactura().getSubtCargosDesc();
	}

	public Double getSubtotalPortafolio() {
		return this.getTotalesFactura().getSubtCargosSrvPort();
	}

	public Double getSubtotalValorOtrosCargos() {
		return this.getTotalesFactura().getSubtOtrosCargos();
	}

	public Double getSubtotalConceptoEnergia() {
		return this.getTotalesFactura().getSubtTotCargosEne();
	}
}
