package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaConvenioDetalle extends SynergiaBusinessObject {

	/**
	 * @return the antiguedadCargo
	 */
	public String getAntiguedadCargo();

	/**
	 * @param antiguedadCargo the antiguedadCargo to set
	 */
	public void setAntiguedadCargo(String antiguedadCargo);

	/**
	 * @return the codCargo
	 */
	public String getCodCargo();

	/**
	 * @param codCargo the codCargo to set
	 */
	public void setCodCargo(String codCargo);

	/**
	 * @return the descripCargo
	 */
	public String getDescripcionCargo();

	/**
	 * @param descripCargo the descripCargo to set
	 */
	public void setDescripcionCargo(String descripcionCargo);

	/**
	 * @return the fechaDocumento
	 */
	public Date getFechaDocumento();

	/**
	 * @param fechaDocumento the fechaDocumento to set
	 */
	public void setFechaDocumento(Date fechaDocumento);

	/**
	 * @return the fechaFactura
	 */
	public Date getFechaFactura();

	/**
	 * @param fechaFactura the fechaFactura to set
	 */
	public void setFechaFactura(Date fechaFactura);

	/**
	 * @return the periodoFacturacionCargo
	 */
	public Date getPeriodoFacturacionCargo();

	/**
	 * @param periodoFacturacionCargo the periodoFacturacionCargo to set
	 */
	public void setPeriodoFacturacionCargo(Date periodoFacturacionCargo);

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);

	public Long getNumDocumento();
	public void setNumDocumento(Long numDocumento);
	public Double getValorCargo();
	public void setValorCargo(Double valorCargo);
	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
	
}