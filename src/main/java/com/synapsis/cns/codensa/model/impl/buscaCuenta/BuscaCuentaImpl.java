package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuenta;

/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public class BuscaCuentaImpl extends SynergiaBusinessObjectImpl 
implements BuscaCuenta {
	
	private Long nroCuenta;	
	private String nombreTitular;	
	private String direccionTitular;
	private String localizacion;	
	private String nombreBeneficiarioSrvElectrico;
	private String apellidoBeneficiarioSrvElectrico;
	private String estado;	
	private String municipio;	
	private String manzana;	
	private String rutaLectura; 	
	private Long cantidadSrvAsoc;
	private String correlativoConjunto;
	
	
	public String getApellidoBeneficiarioSrvElectrico() {
		return apellidoBeneficiarioSrvElectrico;
	}
	public void setApellidoBeneficiarioSrvElectrico(
			String apellidoBeneficiarioSrvElectrico) {
		this.apellidoBeneficiarioSrvElectrico = apellidoBeneficiarioSrvElectrico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getCantidadSrvAsoc()
	 */
	public Long getCantidadSrvAsoc() {
		return cantidadSrvAsoc;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getDireccionTitular()
	 */
	public String getDireccionTitular() {
		return direccionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getEstado()
	 */
	public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getLocalizacion()
	 */
	public String getLocalizacion() {
		return localizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getManzana()
	 */
	public String getManzana() {
		return manzana;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getMunicipio()
	 */
	public String getMunicipio() {
		return municipio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getNombreBeneficiarioSrvElectrico()
	 */
	public String getNombreBeneficiarioSrvElectrico() {
		return nombreBeneficiarioSrvElectrico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getNombreTitular()
	 */
	public String getNombreTitular() {
		return nombreTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#getRutaLectura()
	 */
	public String getRutaLectura() {
		return rutaLectura;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setCantidadSrvAsoc(java.lang.Long)
	 */
	public void setCantidadSrvAsoc(Long cantidadSrvAsoc) {
		this.cantidadSrvAsoc = cantidadSrvAsoc;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setDireccionTitular(java.lang.String)
	 */
	public void setDireccionTitular(String direccionTitular) {
		this.direccionTitular = direccionTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setLocalizacion(java.lang.String)
	 */
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setManzana(java.lang.String)
	 */
	public void setManzana(String manzana) {
		this.manzana = manzana;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setMunicipio(java.lang.String)
	 */
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setNombreBeneficiarioSrvElectrico(java.lang.String)
	 */
	public void setNombreBeneficiarioSrvElectrico(
			String nombreBeneficiarioSrvElectrico) {
		this.nombreBeneficiarioSrvElectrico = nombreBeneficiarioSrvElectrico;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setNombreTitular(java.lang.String)
	 */
	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.BuscaCuenta#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;		
	}	
	
	/**
	 * @return the correlativoConjunto
	 */
	public String getCorrelativoConjunto() {
		return correlativoConjunto;
	}

	/**
	 * @param correlativoConjunto the correlativoConjunto to set
	 */
	public void setCorrelativoConjunto(String correlativoConjunto) {
		this.correlativoConjunto = correlativoConjunto;
	}	
}