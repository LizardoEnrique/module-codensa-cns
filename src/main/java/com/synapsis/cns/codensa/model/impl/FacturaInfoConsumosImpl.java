package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.FacturaInfoConsumos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhv
 */

public class FacturaInfoConsumosImpl extends SynergiaBusinessObjectImpl
		implements FacturaInfoConsumos {

	private Long nroFactura;

	private String tarifa;

	private Double lectActual;

	private Double lectAnterior;

	private Double difLecturas;

	private Double factorLiq;

	private Double totalConsKwh;

	private Double energiaCons;

	private Double energiaFac;

	private Double valorPromKwh;

	private String mesTarifa;

	private Double hrsIntrPermMax;

	private Double hrsIntrAcum;

	private Double nroIntrPermMax;

	private Double nroIntrAcum;

	private Double consPromUlt6;

	private Double generacion;

	private Double transmision;

	private Double perdidas;

	private Double distribucion;

	private Double otros;

	private Double comercializacion;

	private Double costoUnitario;

	private String tipoLectActual;

	private String anomaliaLectActual;

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getTarifa() {
		return tarifa;
	}

	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}

	public Double getLectActual() {
		return lectActual;
	}

	public void setLectActual(Double lectActual) {
		this.lectActual = lectActual;
	}

	public Double getLectAnterior() {
		return lectAnterior;
	}

	public void setLectAnterior(Double lectAnterior) {
		this.lectAnterior = lectAnterior;
	}

	public Double getDifLecturas() {
		return difLecturas;
	}

	public void setDifLecturas(Double difLecturas) {
		this.difLecturas = difLecturas;
	}

	public Double getFactorLiq() {
		return factorLiq;
	}

	public void setFactorLiq(Double factorLiq) {
		this.factorLiq = factorLiq;
	}

	public Double getTotalConsKwh() {
		return totalConsKwh;
	}

	public void setTotalConsKwh(Double totalConsKwh) {
		this.totalConsKwh = totalConsKwh;
	}

	public Double getEnergiaCons() {
		return energiaCons;
	}

	public void setEnergiaCons(Double energiaCons) {
		this.energiaCons = energiaCons;
	}

	public Double getEnergiaFac() {
		return energiaFac;
	}

	public void setEnergiaFac(Double energiaFac) {
		this.energiaFac = energiaFac;
	}

	public Double getValorPromKwh() {
		return valorPromKwh;
	}

	public void setValorPromKwh(Double valorPromKwh) {
		this.valorPromKwh = valorPromKwh;
	}

	public String getMesTarifa() {
		return mesTarifa;
	}

	public void setMesTarifa(String mesTarifa) {
		this.mesTarifa = mesTarifa;
	}

	public Double getHrsIntrPermMax() {
		return hrsIntrPermMax;
	}

	public void setHrsIntrPermMax(Double hrsIntrPermMax) {
		this.hrsIntrPermMax = hrsIntrPermMax;
	}

	public Double getHrsIntrAcum() {
		return hrsIntrAcum;
	}

	public void setHrsIntrAcum(Double hrsIntrAcum) {
		this.hrsIntrAcum = hrsIntrAcum;
	}

	public Double getNroIntrPermMax() {
		return nroIntrPermMax;
	}

	public void setNroIntrPermMax(Double nroIntrPermMax) {
		this.nroIntrPermMax = nroIntrPermMax;
	}

	public Double getNroIntrAcum() {
		return nroIntrAcum;
	}

	public void setNroIntrAcum(Double nroIntrAcum) {
		this.nroIntrAcum = nroIntrAcum;
	}

	public Double getConsPromUlt6() {
		return consPromUlt6;
	}

	public void setConsPromUlt6(Double consPromUlt6) {
		this.consPromUlt6 = consPromUlt6;
	}

	public Double getGeneracion() {
		return generacion;
	}

	public void setGeneracion(Double generacion) {
		this.generacion = generacion;
	}

	public Double getTransmision() {
		return transmision;
	}

	public void setTransmision(Double transmision) {
		this.transmision = transmision;
	}

	public Double getPerdidas() {
		return perdidas;
	}

	public void setPerdidas(Double perdidas) {
		this.perdidas = perdidas;
	}

	public Double getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Double distribucion) {
		this.distribucion = distribucion;
	}

	public Double getOtros() {
		return otros;
	}

	public void setOtros(Double otros) {
		this.otros = otros;
	}

	public Double getComercializacion() {
		return comercializacion;
	}

	public void setComercializacion(Double comercializacion) {
		this.comercializacion = comercializacion;
	}

	public Double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public String getTipoLectActual() {
		return tipoLectActual;
	}

	public void setTipoLectActual(String tipoLectActual) {
		this.tipoLectActual = tipoLectActual;
	}

	public String getAnomaliaLectActual() {
		return anomaliaLectActual;
	}

	public void setAnomaliaLectActual(String anomaliaLectActual) {
		this.anomaliaLectActual = anomaliaLectActual;
	}
}