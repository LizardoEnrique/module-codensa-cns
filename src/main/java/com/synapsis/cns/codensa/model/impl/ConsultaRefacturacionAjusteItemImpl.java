/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjusteItem;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionAjusteItemImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionAjusteItem{
	
	private Long idCuenta;
	private Long nroCuenta;
	private Long idAjuste;
	private Long nroAjuste;
	private Long codigoItem;
	private String descripcionItem;
	private Double valorAjusteItem;
	private Double valorAntesAjusteItem;
	private Double valorDespuesAjusteImpl;
	private Date fechaAjusteItem;
	private Double consumoAjusteKW;
	private Double consumoAntesAjusteKW;
	private Double consumoDespuesAjusteKW;
	
	public Long getCodigoItem() {
		return codigoItem;
	}
	public Double getConsumoAjusteKW() {
		return consumoAjusteKW;
	}
	public Double getConsumoAntesAjusteKW() {
		return consumoAntesAjusteKW;
	}
	public Double getConsumoDespuesAjusteKW() {
		return consumoDespuesAjusteKW;
	}
	public String getDescripcionItem() {
		return descripcionItem;
	}
	public Date getFechaAjusteItem() {
		return fechaAjusteItem;
	}
	public Long getIdAjuste() {
		return idAjuste;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getNroAjuste() {
		return nroAjuste;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Double getValorAjusteItem() {
		return valorAjusteItem;
	}
	public Double getValorAntesAjusteItem() {
		return valorAntesAjusteItem;
	}
	public Double getValorDespuesAjusteImpl() {
		return valorDespuesAjusteImpl;
	}
	public void setCodigoItem(Long codigoItem) {
		this.codigoItem = codigoItem;
	}
	public void setConsumoAjusteKW(Double consumoAjusteKW) {
		this.consumoAjusteKW = consumoAjusteKW;
	}
	public void setConsumoAntesAjusteKW(Double consumoAntesAjusteKW) {
		this.consumoAntesAjusteKW = consumoAntesAjusteKW;
	}
	public void setConsumoDespuesAjusteKW(Double consumoDespuesAjusteKW) {
		this.consumoDespuesAjusteKW = consumoDespuesAjusteKW;
	}
	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}
	public void setFechaAjusteItem(Date fechaAjusteItem) {
		this.fechaAjusteItem = fechaAjusteItem;
	}
	public void setIdAjuste(Long idAjuste) {
		this.idAjuste = idAjuste;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setValorAjusteItem(Double valorAjusteItem) {
		this.valorAjusteItem = valorAjusteItem;
	}
	public void setValorAntesAjusteItem(Double valorAntesAjusteItem) {
		this.valorAntesAjusteItem = valorAntesAjusteItem;
	}
	public void setValorDespuesAjusteImpl(Double valorDespuesAjusteImpl) {
		this.valorDespuesAjusteImpl = valorDespuesAjusteImpl;
	}
	
	
	
		
}
