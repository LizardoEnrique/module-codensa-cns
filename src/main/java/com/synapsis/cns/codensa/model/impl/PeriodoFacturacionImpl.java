/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.PeriodoFacturacion;
import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class PeriodoFacturacionImpl extends SynergiaBusinessObjectImpl implements PeriodoFacturacion {
	
	private static final long serialVersionUID = 1L;
	
	private CalendarDate fechaProceso;
	private String periodoFacturacion;

	
	public CalendarDate getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(CalendarDate fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	
}