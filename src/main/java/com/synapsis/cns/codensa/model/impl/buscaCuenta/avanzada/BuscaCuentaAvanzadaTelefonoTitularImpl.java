package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoTitular;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

public class BuscaCuentaAvanzadaTelefonoTitularImpl extends BuscaCuentaImpl implements BuscaCuentaAvanzadaTelefonoTitular {
	private String telefonoTitular;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoTitular#getTelefonoTitular()
	 */
	public String getTelefonoTitular() {
		return telefonoTitular;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaTelefonoTitular#setTelefonoTitular(java.lang.String)
	 */
	public void setTelefonoTitular(String telefonoTitular) {
		this.telefonoTitular = telefonoTitular;
	}
	
	
}
