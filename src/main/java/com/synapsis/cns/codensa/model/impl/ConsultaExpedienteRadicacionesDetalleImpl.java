/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteRadicacionesDetalle;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteRadicacionesDetalleImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteRadicacionesDetalle {
	private Date fechaRadicacion; 	
	private Date radicacion;	
	private String tipoRadicacion; 	
	private Long numeroExpediente;
	
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#getFechaRadicacion()
	 */
	public Date getFechaRadicacion() {
		return fechaRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#setFechaRadicacion(java.util.Date)
	 */
	public void setFechaRadicacion(Date fechaRadicacion) {
		this.fechaRadicacion = fechaRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#getNumeroRadicacion()
	 */
	public Long getNumeroRadicacion() {
		return this.getId();
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#setNumeroRadicacion(java.lang.Long)
	 */
	public void setNumeroRadicacion(Long numeroRadicacion) {
		this.setId(numeroRadicacion);
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#getRadicacion()
	 */
	public Date getRadicacion() {
		return radicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#setRadicacion(java.lang.String)
	 */
	public void setRadicacion(Date radicacion) {
		this.radicacion = radicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#getTipoRadicacion()
	 */
	public String getTipoRadicacion() {
		return tipoRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteRadicacionesDetalle#setTipoRadicacion(java.lang.String)
	 */
	public void setTipoRadicacion(String tipoRadicacion) {
		this.tipoRadicacion = tipoRadicacion;
	}

}
