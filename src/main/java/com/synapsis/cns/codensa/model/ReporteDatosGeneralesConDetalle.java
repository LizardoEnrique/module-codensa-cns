package com.synapsis.cns.codensa.model;

import java.util.Collection;

/**
 * @author m3.MarioRoss - 21/12/2006
 *
 */
public interface ReporteDatosGeneralesConDetalle extends ReporteDatosGenerales {
	public Collection getDetalles();

	public void setDetalles(Collection detalles);

}