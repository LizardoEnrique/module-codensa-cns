package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.CargoFacturaComun;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * 
 * @author adambrosio
 */
public abstract class AbstractCargoFactura extends SynergiaBusinessObjectImpl implements CargoFacturaComun {

	private Long nroFactura;
	private String codigo;
	private String concepto;
	private Long indicadorCargo;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public Long getIndicadorCargo() {
		return indicadorCargo;
	}

	public void setIndicadorCargo(Long indicadorCargo) {
		this.indicadorCargo = indicadorCargo;

	}

}
