package com.synapsis.cns.codensa.model;

public interface ConsultaRepresLegales {

	/**
	 * @return the direccion
	 */
	public String getDireccion();

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion);

	/**
	 * @return the documento
	 */
	public String getDocumento();

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(String documento);

	/**
	 * @return the email
	 */
	public String getEmail();

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the representante
	 */
	public String getRepresentante();

	/**
	 * @param representante the representante to set
	 */
	public void setRepresentante(String representante);

	/**
	 * @return the telefono
	 */
	public String getTelefono();

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono);

}