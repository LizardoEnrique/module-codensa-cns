package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsumoSUltimos6Meses;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author adambrosio
 */
public class ConsumosUltimos6MesesImpl extends SynergiaBusinessObjectImpl implements ConsumoSUltimos6Meses {
	private Long nroFactura;
	private String mes;
	private Double Cons_KWH;
	
	public Double getCons_KWH() {
		return Cons_KWH;
	}
	public void setCons_KWH(Double cons_KWH) {
		Cons_KWH = cons_KWH;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public Long getNroFactura() {
		return nroFactura;
	}
	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}
	
	public int compareTo(Object o) {
		return this.getId().compareTo(((ConsumoSUltimos6Meses) o).getId());
	}
}
