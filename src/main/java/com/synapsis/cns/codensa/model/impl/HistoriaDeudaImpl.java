package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.HistoriaDeuda;
/**
 * 
 * @author ar30557486
 *
 */
public class HistoriaDeudaImpl extends SynergiaBusinessObjectImpl 
	implements HistoriaDeuda {
	
    private Long nroCuenta;
	private Long movimiento;
	private Date fechaMovimiento;
	private String tipoMovimiento;
	private Long nroDocumento;
	private Long valorMovimiento;
	private String estadoCobranzaSrvElectrico;
	private String estadoCobranzaSrvCodensa;
	private String estadoCobranzaSrvConvenio;
	private Long antDeudaSrvElectrico;
	private Long antDeudaSrvCodensa;
	private Long antDeudaSrvConvenio;
	
	public Long getAntDeudaSrvCodensa() {
		return antDeudaSrvCodensa;
	}
	public void setAntDeudaSrvCodensa(Long antDeudaSrvCodensa) {
		this.antDeudaSrvCodensa = antDeudaSrvCodensa;
	}
	public Long getAntDeudaSrvConvenio() {
		return antDeudaSrvConvenio;
	}
	public void setAntDeudaSrvConvenio(Long antDeudaSrvConvenio) {
		this.antDeudaSrvConvenio = antDeudaSrvConvenio;
	}
	public Long getAntDeudaSrvElectrico() {
		return antDeudaSrvElectrico;
	}
	public void setAntDeudaSrvElectrico(Long antDeudaSrvElectrico) {
		this.antDeudaSrvElectrico = antDeudaSrvElectrico;
	}
	public String getEstadoCobranzaSrvCodensa() {
		return estadoCobranzaSrvCodensa;
	}
	public void setEstadoCobranzaSrvCodensa(String estadoCobranzaSrvCodensa) {
		this.estadoCobranzaSrvCodensa = estadoCobranzaSrvCodensa;
	}
	public String getEstadoCobranzaSrvConvenio() {
		return estadoCobranzaSrvConvenio;
	}
	public void setEstadoCobranzaSrvConvenio(String estadoCobranzaSrvConvenio) {
		this.estadoCobranzaSrvConvenio = estadoCobranzaSrvConvenio;
	}
	public String getEstadoCobranzaSrvElectrico() {
		return estadoCobranzaSrvElectrico;
	}
	public void setEstadoCobranzaSrvElectrico(String estadoCobranzaSrvElectrico) {
		this.estadoCobranzaSrvElectrico = estadoCobranzaSrvElectrico;
	}
	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}
	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}
	public Long getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(Long movimiento) {
		this.movimiento = movimiento;
	}
	public Long getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public Long getValorMovimiento() {
		return valorMovimiento;
	}
	public void setValorMovimiento(Long valorMovimiento) {
		this.valorMovimiento = valorMovimiento;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
			
	
}