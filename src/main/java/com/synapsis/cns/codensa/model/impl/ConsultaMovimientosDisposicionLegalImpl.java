package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaMovimientosDisposicionLegal;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaMovimientosDisposicionLegalImpl extends SynergiaBusinessObjectImpl implements ConsultaMovimientosDisposicionLegal {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	//private Empresa empresa;
	private Long idMovRefa;
	private Long nroAjuste;
	private String descripcion;
	

	public ConsultaMovimientosDisposicionLegalImpl() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	//public Empresa getEmpresa() {
	//	return empresa;
	//}
	//public void setEmpresa(Empresa empresa) {
	//	this.empresa = empresa;
//	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdMovRefa() {
		return idMovRefa;
	}

	public void setIdMovRefa(Long idMovRefa) {
		this.idMovRefa = idMovRefa;
	}


	public Long getNroAjuste() {
		return nroAjuste;
	}


	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

		
}
