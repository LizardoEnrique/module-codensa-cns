package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.HistoriaEstadosConvenios;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class HistoriaEstadosConveniosImpl extends SynergiaBusinessObjectImpl implements HistoriaEstadosConvenios  {

    private static final long serialVersionUID = 1L;
 
    private String estado;
    private String usuarioRealizoOperacion;
    private Date fechaCambioEstado;
    private Long idServicio;

    /* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#getEstado()
	 */
    public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#getFechaCambioEstado()
	 */
	public Date getFechaCambioEstado() {
		return fechaCambioEstado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#setFechaCambioEstado(java.util.Date)
	 */
	public void setFechaCambioEstado(Date fechaCambioEstado) {
		this.fechaCambioEstado = fechaCambioEstado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#getUsuarioRealizoOperacion()
	 */
	public String getUsuarioRealizoOperacion() {
		return usuarioRealizoOperacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.HistoriaEstadosConvenios#setUsuarioRealizoOperacion(java.lang.String)
	 */
	public void setUsuarioRealizoOperacion(String usuarioRealizoOperacion) {
		this.usuarioRealizoOperacion = usuarioRealizoOperacion;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
}