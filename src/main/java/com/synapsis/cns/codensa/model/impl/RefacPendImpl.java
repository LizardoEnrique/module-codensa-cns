package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.RefacPend;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacPendImpl extends SynergiaBusinessObjectImpl implements
		RefacPend {

	private Long nroCuenta;

	private String nroAjuste;

	private Long nroOrdenRefa;

	private Long servAjustados;

	private Double valorTotalAjustado;

	private Double cantKwhAjust;

	private Double valorTotalPagar;

	private Date fecVencPagoAjuste;

	private Date fecHorRealizAjuste;

	private String usuarioRealizAjuste;

	private String nombreUsuarioRealizAjuste;

	private String tipoOperacion;

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	public Long getNroOrdenRefa() {
		return nroOrdenRefa;
	}

	public void setNroOrdenRefa(Long nroOrdenRefa) {
		this.nroOrdenRefa = nroOrdenRefa;
	}

	public Long getServAjustados() {
		return servAjustados;
	}

	public void setServAjustados(Long servAjustados) {
		this.servAjustados = servAjustados;
	}

	public Double getValorTotalAjustado() {
		return valorTotalAjustado;
	}

	public void setValorTotalAjustado(Double valorTotalAjustado) {
		this.valorTotalAjustado = valorTotalAjustado;
	}

	public Double getCantKwhAjust() {
		return cantKwhAjust;
	}

	public void setCantKwhAjust(Double cantKwhAjust) {
		this.cantKwhAjust = cantKwhAjust;
	}

	public Double getValorTotalPagar() {
		return valorTotalPagar;
	}

	public void setValorTotalPagar(Double valorTotalPagar) {
		this.valorTotalPagar = valorTotalPagar;
	}

	public Date getFecVencPagoAjuste() {
		return fecVencPagoAjuste;
	}

	public void setFecVencPagoAjuste(Date fecVencPagoAjuste) {
		this.fecVencPagoAjuste = fecVencPagoAjuste;
	}

	public Date getFecHorRealizAjuste() {
		return fecHorRealizAjuste;
	}

	public void setFecHorRealizAjuste(Date fecHorRealizAjuste) {
		this.fecHorRealizAjuste = fecHorRealizAjuste;
	}

	public String getUsuarioRealizAjuste() {
		return usuarioRealizAjuste;
	}

	public void setUsuarioRealizAjuste(String usuarioRealizAjuste) {
		this.usuarioRealizAjuste = usuarioRealizAjuste;
	}

	public String getNombreUsuarioRealizAjuste() {
		return nombreUsuarioRealizAjuste;
	}

	public void setNombreUsuarioRealizAjuste(String nombreUsuarioRealizAjuste) {
		this.nombreUsuarioRealizAjuste = nombreUsuarioRealizAjuste;
	}
}