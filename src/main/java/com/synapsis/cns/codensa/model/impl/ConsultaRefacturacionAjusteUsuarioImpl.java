/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjusteUsuario;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionAjusteUsuarioImpl extends
		SynergiaBusinessObjectImpl implements ConsultaRefacturacionAjusteUsuario{
	
	private Long idCuenta;
	private Long nroCuenta;
	private Long idAjuste;
	private Long nroAjuste;
	private String usuario;
	private String nombreUsuario;
	private String fechaIntervencion;
	private String accionEjecutada;
	private String tiempoIntervencion;
	
	public String getAccionEjecutada() {
		return accionEjecutada;
	}
	public String getFechaIntervencion() {
		return fechaIntervencion;
	}
	public Long getIdAjuste() {
		return idAjuste;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public Long getNroAjuste() {
		return nroAjuste;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public String getTiempoIntervencion() {
		return tiempoIntervencion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setAccionEjecutada(String accionEjecutada) {
		this.accionEjecutada = accionEjecutada;
	}
	public void setFechaIntervencion(String fechaIntervencion) {
		this.fechaIntervencion = fechaIntervencion;
	}
	public void setIdAjuste(Long idAjuste) {
		this.idAjuste = idAjuste;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public void setNroAjuste(Long nroAjuste) {
		this.nroAjuste = nroAjuste;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setTiempoIntervencion(String tiempoIntervencion) {
		this.tiempoIntervencion = tiempoIntervencion;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
