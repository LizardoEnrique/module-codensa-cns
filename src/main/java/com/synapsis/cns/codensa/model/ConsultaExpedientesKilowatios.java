package com.synapsis.cns.codensa.model;

public interface ConsultaExpedientesKilowatios {


	/**
	 * @return the consumoReintegrar
	 */
	public Double getConsumoReintegrar();

	/**
	 * @param consumoReintegrar the consumoReintegrar to set
	 */
	public void setConsumoReintegrar(Double consumoReintegrar);

	/**
	 * @return the consumoTotal
	 */
	public Double getConsumoTotal();

	/**
	 * @param consumoTotal the consumoTotal to set
	 */
	public void setConsumoTotal(Double consumoTotal);

	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente();

	/**
	 * @param idCnrExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente);

	/**
	 * @return the idServicio
	 */
	public Long getIdServicio();

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio);

	public Double getPromedio();
	
	public void setPromedio(Double promedio);
}