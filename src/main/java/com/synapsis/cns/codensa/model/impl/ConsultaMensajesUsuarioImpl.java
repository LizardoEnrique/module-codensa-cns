/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaMensajesUsuario;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaMensajesUsuarioImpl extends SynergiaBusinessObjectImpl implements ConsultaMensajesUsuario {
	   private String mensaje;             
	   private Long idUsuario;             
	   private Long idArea;                
	   private Long idOficina;             
	   private Date fecVig_desde;          
	   private Date fecVig_hasta;          
	   private Date fecRegistro;
	   private String userName;
	/**
	 * @return the fecRegistro
	 */
	public Date getFecRegistro() {
		return fecRegistro;
	}
	/**
	 * @param fecRegistro the fecRegistro to set
	 */
	public void setFecRegistro(Date fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	/**
	 * @return the fecVig_desde
	 */
	public Date getFecVig_desde() {
		return fecVig_desde;
	}
	/**
	 * @param fecVig_desde the fecVig_desde to set
	 */
	public void setFecVig_desde(Date fecVig_desde) {
		this.fecVig_desde = fecVig_desde;
	}
	/**
	 * @return the fecVig_hasta
	 */
	public Date getFecVig_hasta() {
		return fecVig_hasta;
	}
	/**
	 * @param fecVig_hasta the fecVig_hasta to set
	 */
	public void setFecVig_hasta(Date fecVig_hasta) {
		this.fecVig_hasta = fecVig_hasta;
	}
	/**
	 * @return the idArea
	 */
	public Long getIdArea() {
		return idArea;
	}
	/**
	 * @param idArea the idArea to set
	 */
	public void setIdArea(Long idArea) {
		this.idArea = idArea;
	}
	/**
	 * @return the idOficina
	 */
	public Long getIdOficina() {
		return idOficina;
	}
	/**
	 * @param idOficina the idOficina to set
	 */
	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}
	/**
	 * @return the idUsuario
	 */
	public Long getIdUsuario() {
		return idUsuario;
	}
	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}
	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}           

}
