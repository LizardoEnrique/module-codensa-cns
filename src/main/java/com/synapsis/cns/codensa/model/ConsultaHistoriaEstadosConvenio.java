package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaHistoriaEstadosConvenio extends SynergiaBusinessObject {

	/**
	 * @return the fechaactivacionServicio
	 */
	public Date getFechaActivacionServicio();

	/**
	 * @param fechaactivacionServicio the fechaactivacionServicio to set
	 */
	public void setFechaActivacionServicio(Date fechaActivacionServicio);

	/**
	 * @return the lineaNegocioPerteneciente
	 */
	public String getLineaNegocioPerteneciente();

	/**
	 * @param lineaNegocioPerteneciente the lineaNegocioPerteneciente to set
	 */
	public void setLineaNegocioPerteneciente(String lineaNegocioPerteneciente);

	/**
	 * @return the nroIdentificacionSolicitantecredito
	 */
	public Long getNroIdentificacionSolicitantecredito();

	/**
	 * @param nroIdentificacionSolicitantecredito the nroIdentificacionSolicitantecredito to set
	 */
	public void setNroIdentificacionSolicitantecredito(
			Long nroIdentificacionSolicitantecredito);

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);

	/**
	 * @return the saldoServicio
	 */
	public Long getSaldoServicio();

	/**
	 * @param saldoServicio the saldoServicio to set
	 */
	public void setSaldoServicio(Long saldoServicio);

	/**
	 * @return the valorOriginalServicio
	 */
	public Long getValorOriginalServicio();

	/**
	 * @param valorOriginalServicio the valorOriginalServicio to set
	 */
	public void setValorOriginalServicio(Long valorOriginalServicio);

}