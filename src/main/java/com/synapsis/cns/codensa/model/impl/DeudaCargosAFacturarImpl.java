package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.DeudaCargosAFacturar;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class DeudaCargosAFacturarImpl extends SynergiaBusinessObjectImpl
		implements DeudaCargosAFacturar {	

	private Long nroCuenta;

	private Long idCobroAdic;

	private String codigoCargo;

	private String descripcionCargo;

	private Double valorCargo;

	private String tipoServicioCargo;

	private Long nroServicioCargo;

	private String usuarioIngresoCargo;

	private String nombreIngresoCargo;

	private String observaciones;
	
	private Date fechaIngresoCargo;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getIdCobroAdic() {
		return idCobroAdic;
	}

	public void setIdCobroAdic(Long idCobroAdic) {
		this.idCobroAdic = idCobroAdic;
	}

	public String getCodigoCargo() {
		return codigoCargo;
	}

	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	public String getDescripcionCargo() {
		return descripcionCargo;
	}

	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}

	public Double getValorCargo() {
		return valorCargo;
	}

	public void setValorCargo(Double valorCargo) {
		this.valorCargo = valorCargo;
	}

	public String getTipoServicioCargo() {
		return tipoServicioCargo;
	}

	public void setTipoServicioCargo(String tipoServicioCargo) {
		this.tipoServicioCargo = tipoServicioCargo;
	}

	public Long getNroServicioCargo() {
		return nroServicioCargo;
	}

	public void setNroServicioCargo(Long nroServicioCargo) {
		this.nroServicioCargo = nroServicioCargo;
	}

	public String getUsuarioIngresoCargo() {
		return usuarioIngresoCargo;
	}

	public void setUsuarioIngresoCargo(String usuarioIngresoCargo) {
		this.usuarioIngresoCargo = usuarioIngresoCargo;
	}

	public String getNombreIngresoCargo() {
		return nombreIngresoCargo;
	}

	public void setNombreIngresoCargo(String nombreIngresoCargo) {
		this.nombreIngresoCargo = nombreIngresoCargo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String	observaciones) {
		this.observaciones = observaciones;
	}
	
	public Date getFechaIngresoCargo() {
		return this.fechaIngresoCargo;
	}

	public void setFechaIngresoCargo(Date fechaIngresoCargo) {
		this.fechaIngresoCargo = fechaIngresoCargo;
	}

}