package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;

import com.synapsis.cns.codensa.model.DetalleInventarioAcumuladoInfraestructura;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ccamba
 * 
 */
public class DetalleInventarioAcumuladoInfraestructuraImpl extends SynergiaBusinessObjectImpl implements
		DetalleInventarioAcumuladoInfraestructura {

	private Long nroCuenta;
	private Long nroServicio;
	private Long idClienteTelem;
	private Long periodo;
	private String zona;
	private String producto;
	private String subProducto;
	private Integer grupo;
	private BigDecimal cantidad;
	private BigDecimal acumuladoApoyos;
	private BigDecimal acumuladoValores;
	private BigDecimal descuentoEstructura;
	private BigDecimal valorDescEstructura;
	private BigDecimal acumuladoValoresDescuento;
	private BigDecimal tarifaAplicar;
	private String estadoFacturacion;

	public Long getNroCuenta() {
		return this.nroCuenta;
	}

	public Long getNroServicio() {
		return this.nroServicio;
	}

	public Long getIdClienteTelem() {
		return this.idClienteTelem;
	}

	public Long getPeriodo() {
		return this.periodo;
	}

	public String getZona() {
		return this.zona;
	}

	public String getProducto() {
		return this.producto;
	}

	public String getSubProducto() {
		return this.subProducto;
	}

	public Integer getGrupo() {
		return this.grupo;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public BigDecimal getAcumuladoApoyos() {
		return this.acumuladoApoyos;
	}

	public BigDecimal getAcumuladoValores() {
		return this.acumuladoValores;
	}

	public BigDecimal getDescuentoEstructura() {
		return this.descuentoEstructura;
	} 

	public BigDecimal getValorDescEstructura() {
		return this.valorDescEstructura;
	}

	public BigDecimal getAcumuladoValoresDescuento() {
		return this.acumuladoValoresDescuento;
	}

	public BigDecimal getTarifaAplicar() {
		return this.tarifaAplicar;
	}

	public String getEstadoFacturacion() {
		return this.estadoFacturacion;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	public void setIdClienteTelem(Long idClienteTelem) {
		this.idClienteTelem = idClienteTelem;
	}

	public void setPeriodo(Long periodo) {
		this.periodo = periodo;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public void setSubProducto(String subProducto) {
		this.subProducto = subProducto;
	}

	public void setGrupo(Integer grupo) {
		this.grupo = grupo;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public void setAcumuladoApoyos(BigDecimal acumuladoApoyos) {
		this.acumuladoApoyos = acumuladoApoyos;
	}

	public void setAcumuladoValores(BigDecimal acumuladoValores) {
		this.acumuladoValores = acumuladoValores;
	}

	public void setDescuentoEstructura(BigDecimal descuentoEstructura) {
		this.descuentoEstructura = descuentoEstructura;
	}

	public void setValorDescEstructura(BigDecimal valorDescEstructura) {
		this.valorDescEstructura = valorDescEstructura;
	}

	public void setAcumuladoValoresDescuento(BigDecimal acumuladoValoresDescuento) {
		this.acumuladoValoresDescuento = acumuladoValoresDescuento;
	}

	public void setTarifaAplicar(BigDecimal tarifaAplicar) {
		this.tarifaAplicar = tarifaAplicar;
	}

	public void setEstadoFacturacion(String estadoFacturacion) {
		this.estadoFacturacion = estadoFacturacion;
	}

}
