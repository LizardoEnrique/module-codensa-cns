package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaItemsDisposicionLegal extends SynergiaBusinessObject {

	public Long getId();
	public void setId(Long id);

	public Empresa getEmpresa();
	public void setEmpresa(Empresa empresa);

	public String getDescripcion();
	public void setDescripcion(String descripcion);

	public Long getIdCargo();
	public void setIdCargo(Long idCargo);


	
}
