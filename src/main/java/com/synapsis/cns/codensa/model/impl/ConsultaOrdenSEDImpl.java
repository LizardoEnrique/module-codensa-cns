package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaOrdenSED;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaOrdenSEDImpl extends SynergiaBusinessObjectImpl implements ConsultaOrdenSED{

	private Date fechaAprobacion;
	private Long numeroSaldo;
	private String estado;
	private Double valor;
	private Double valorAntesCongelacion;
	private Double valorDespuesCongelacion;
	private Double valorAntesDescongelacion;
	private Double valorDespuesDescongelacion;
	private Long nroCuenta;
	
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public Date getFechaAprobacion() {
		return fechaAprobacion;
	}
	public void setFechaAprobacion(Date fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}
	
	public Long getNumeroSaldo() {
		return numeroSaldo;
	}
	public void setNumeroSaldo(Long numeroSaldo) {
		this.numeroSaldo = numeroSaldo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getValorAntesCongelacion() {
		return valorAntesCongelacion;
	}
	public void setValorAntesCongelacion(Double valorAntesCongelacion) {
		this.valorAntesCongelacion = valorAntesCongelacion;
	}
	public Double getValorDespuesCongelacion() {
		return valorDespuesCongelacion;
	}
	public void setValorDespuesCongelacion(Double valorDespuesCongelacion) {
		this.valorDespuesCongelacion = valorDespuesCongelacion;
	}
	public Double getValorAntesDescongelacion() {
		return valorAntesDescongelacion;
	}
	public void setValorAntesDescongelacion(Double valorAntesDescongelacion) {
		this.valorAntesDescongelacion = valorAntesDescongelacion;
	}
	public Double getValorDespuesDescongelacion() {
		return valorDespuesDescongelacion;
	}
	public void setValorDespuesDescongelacion(Double valorDespuesDescongelacion) {
		this.valorDespuesDescongelacion = valorDespuesDescongelacion;
	}
	
}
