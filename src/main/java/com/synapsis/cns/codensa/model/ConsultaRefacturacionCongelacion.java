/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 016
 */
public interface ConsultaRefacturacionCongelacion extends
		SynergiaBusinessObject {

	public String getAnalistaRealizador() ;
	public String getAreaSolicitante() ;
	public String getCicloDeLaCuenta() ;
	public String getClasificacion() ;
	public String getCondicionFinalDeCuenta() ;
	public Date getFechaAprobacion() ;
	public Date getFechaCiclo() ;
	public Date getFechaEliminacion();
	public Date getFechaRealizacion();
	public Date getFechaVencimiento();
	public Date getHoraRealizacion() ;
	public Long getIdCuenta() ;
	public Double getKwCongeladosODescongelados();
	public Long getNroCuenta() ;
	public Long getNroOperacion();
	public Long getNroOrden() ;
	public Double getTiempoCongelacionTransitoria();
	public Double getTiempoTranscurrido() ;
	public String getTipoOperacion() ;
	public String getUsuarioEliminador();
	public String getUsuarioQueDebeAprobar();
	public String getUsuarioRealizador() ;
	public String getUsuariosAprobadores();
	public Double getValorDeOperacionSaldoDisputa();
	public Long getIdOperacion();
}
