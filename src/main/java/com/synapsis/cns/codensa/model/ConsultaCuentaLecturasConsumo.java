package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaLecturasConsumo extends SynergiaBusinessObject {

	/**
	 * @return the consumoEAFP
	 */
	public Double getConsumoEAFP();

	/**
	 * @param consumoEAFP the consumoEAFP to set
	 */
	public void setConsumoEAFP(Double consumoEAFP);

	/**
	 * @return the consumoEAXP
	 */
	public Double getConsumoEAXP();

	/**
	 * @param consumoEAXP the consumoEAXP to set
	 */
	public void setConsumoEAXP(Double consumoEAXP);

	/**
	 * @return the consumoERHP
	 */
	public Double getConsumoERHP();

	/**
	 * @param consumoERHP the consumoERHP to set
	 */
	public void setConsumoERHP(Double consumoERHP);

	/**
	 * @return the consumoERXP
	 */
	public Double getConsumoERXP();

	/**
	 * @param consumoERXP the consumoERXP to set
	 */
	public void setConsumoERXP(Double consumoERXP);

	/**
	 * @return the demandaFP
	 */
	public Double getDemandaFP();

	/**
	 * @param demandaFP the demandaFP to set
	 */
	public void setDemandaFP(Double demandaFP);

	/**
	 * @return the demandaHP
	 */
	public Double getDemandaHP();

	/**
	 * @param demandaHP the demandaHP to set
	 */
	public void setDemandaHP(Double demandaHP);

	/**
	 * @return the demandaXP
	 */
	public Double getDemandaXP();

	/**
	 * @param demandaXP the demandaXP to set
	 */
	public void setDemandaXP(Double demandaXP);

	/**
	 * @return the fechaProceso
	 */
	public Date getFechaProceso();

	/**
	 * @param fechaProceso the fechaProceso to set
	 */
	public void setFechaProceso(Date fechaProceso);

	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta();

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta);

	/**
	 * @return the idDetalleCuenta
	 */
	public Long getIdDetalleCuenta();

	/**
	 * @param idDetalleCuenta the idDetalleCuenta to set
	 */
	public void setIdDetalleCuenta(Long idDetalleCuenta);
	
	/**
	 * @return the lecturaActualEAFP
	 */
	public Long getLecturaActualEAFP();

	/**
	 * @param lecturaActualEAFP the lecturaActualEAFP to set
	 */
	public void setLecturaActualEAFP(Long lecturaActualEAFP);

	/**
	 * @return the lecturaActualEAXP
	 */
	public Long getLecturaActualEAXP();

	/**
	 * @param lecturaActualEAXP the lecturaActualEAXP to set
	 */
	public void setLecturaActualEAXP(Long lecturaActualEAXP);

	/**
	 * @return the lecturaActualERHP
	 */
	public Long getLecturaActualERHP();

	/**
	 * @param lecturaActualERHP the lecturaActualERHP to set
	 */
	public void setLecturaActualERHP(Long lecturaActualERHP);

	/**
	 * @return the lecturaActualERXP
	 */
	public Long getLecturaActualERXP();

	/**
	 * @param lecturaActualERXP the lecturaActualERXP to set
	 */
	public void setLecturaActualERXP(Long lecturaActualERXP);

	/**
	 * @return the lecturaAnteriorEAFP
	 */
	public Long getLecturaAnteriorEAFP();

	/**
	 * @param lecturaAnteriorEAFP the lecturaAnteriorEAFP to set
	 */
	public void setLecturaAnteriorEAFP(Long lecturaAnteriorEAFP);

	/**
	 * @return the lecturaAnteriorEAXP
	 */
	public Long getLecturaAnteriorEAXP();

	/**
	 * @param lecturaAnteriorEAXP the lecturaAnteriorEAXP to set
	 */
	public void setLecturaAnteriorEAXP(Long lecturaAnteriorEAXP);

	/**
	 * @return the lecturaAnteriorERHP
	 */
	public Long getLecturaAnteriorERHP();

	/**
	 * @param lecturaAnteriorERHP the lecturaAnteriorERHP to set
	 */
	public void setLecturaAnteriorERHP(Long lecturaAnteriorERHP);

	/**
	 * @return the lecturaAnteriorERXP
	 */
	public Long getLecturaAnteriorERXP();

	/**
	 * @param lecturaAnteriorERXP the lecturaAnteriorERXP to set
	 */
	public void setLecturaAnteriorERXP(Long lecturaAnteriorERXP);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	
	public Double getConsumoEAHP();
	public void setConsumoEAHP(Double consumoEAHP);
	public Double getConsumoERFP();
	public void setConsumoERFP(Double consumoERFP);
	
	public Long getLecAnteriorERHP();
	
	public void setLecAnteriorERHP(Long lecAnteriorERHP);

	public Long getLecAcERHP();
	
	public void setLecAcERHP(Long lecAcERHP);

	public Long getLecAntERXP();
	
	public void setLecAntERXP(Long lecAntERXP);

	public Long getLecAcERXP();
	
	public void setLecAcERXP(Long lecAcERXP);

}