package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaServiciosFinancierosOCH extends SynergiaBusinessObject {
	
  public String getTipoServicioFinanciero();
  public void setTipoServicioFinanciero(String tipoServicioFinanciero);
  public Long getNroServicioFinanciero();
  public void setNroServicioFinanciero(Long nroServicioFinanciero);
  public String getNombreTitular();
  public void setNombreTitular (String nombreTitular);
  public String getNroIdentificacionSocio();
  public void setNroIdentificacionSocio(String nroIdentificacionSocio);
  public String getEstado();
  public void setEstado(String estado);
  public Long getNroServicio();
  public void setNroServicio(Long nroServicio);
  public String getLineaNegocio();
  public void setLineaNegocio(String lineaNegocio);
  public String getTipoProducto();
  public void setTipoProducto(String tipoProducto);
  public String getMarcaProducto();
  public void setMarcaProducto(String marcaProducto);
  public Date getFechaCompraProducto();
  public void setFechaCompraProducto(Date fechaCompraProducto);
  public Date getFechaActivacionServicio();
  public void setFechaActivacionServicio(Date fechaActivacionServicio);
  public String getNombreSocio();
  public void setNombreSocio(String nombreSocio);
  public String getSucursalSocio();
  public void setSucursalSocio(String sucursalSocio);
  public String getSolicitanteCredito();
  public void setSolicitanteCredito(String solicitanteCredito);
  public String getNroIdSolicitante();
  public void setNroIdSolicitante(String nroIdSolicitante);
  public String getResultadoEstadoCredito();
  public void setResultadoEstadoCredito(String resultadoEstadoCredito);
  public Long getMontoAprobado();
  public void setMontoAprobado(Long montoAprobado);
  public Long getMontoDisponible();
  public void setMontoDisponible(Long montoDisponible);
  public Date getFechaSolicitudCredito();
  public void setFechaSolicitudCredito(Date fechaSolicitudCredito);
  public String getCausalNegacion();
  public void setCausalNegacion(String causalNegacion);

}
