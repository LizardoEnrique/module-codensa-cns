package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCortesProgramadosService extends SynergiaBusinessObject {

	public String getCodCircuito();
	public void setCodCircuito(String codCircuito);
	public String getCodTransformador();
	public void setCodTransformador(String codTransformador);
	public String getComercializador(); 
	public void setComercializador(String comercializador);
	public String getContacto(); 
	public void setContacto(String contacto); 
	public String getCorrFacturacion(); 
	public void setCorrFacturacion(String corrFacturacion); 
	public String getDireccion(); 
	public void setDireccion(String direccion); 
	public String getDirRepartoEspecial(); 
	public void setDirRepartoEspecial(String dirRepartoEspecial); 
	public Date getFechaFinPrevista(); 
	public void setFechaFinPrevista(Date fechaFinPrevista); 
	public Date getFechaInicioPrevista(); 
	public void setFechaInicioPrevista(Date fechaInicioPrevista); 
	public Date getFechaProceso(); 
	public void setFechaProceso(Date fechaProceso); 
	public String getGestorNegocio(); 
	public void setGestorNegocio(String gestorNegocio); 
	public String getGrupoRuta(); 
	public void setGrupoRuta(String grupoRuta); 
	public String getIsVip(); 
	public void setIsVip(String isVip);
	public String getLocalizacion();
	public void setLocalizacion(String localizacion);
	public String getMarcaMedidor();
	public void setMarcaMedidor(String marcaMedidor);
	public String getMunicipio(); 
	public void setMunicipio(String municipio); 
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta); 
	public Long getNroCuentaPadre();
	public void setNroCuentaPadre(Long nroCuentaPadre); 
	public String getNroMedidor(); 
	public void setNroMedidor(String nroMedidor);
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio); 
	public String getRegional();
	public void setRegional(String regional); 
	public String getTelContacto(); 
	public void setTelContacto(String telContacto); 
	public String getTipoClienteEmpresarial(); 
	public void setTipoClienteEmpresarial(String tipoClienteEmpresarial); 
	public String getTitularCuenta(); 
	public void setTitularCuenta(String titularCuenta); 
	public String getManzana();
	public void setManzana(String manzana);
	public String getCicloFacturacion();
	public void setCicloFacturacion(String cicloFacturacion);
	
	public Date getFechaProcesoTrunc();
	public void setFechaProcesoTrunc(Date fechaProcesoTrunc);	
}
