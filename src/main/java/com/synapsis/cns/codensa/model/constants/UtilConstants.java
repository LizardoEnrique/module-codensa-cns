/**
 * 
 */
package com.synapsis.cns.codensa.model.constants;

/**
 * @author syalri
 *
 *  DIEGO BRACCIO -----------------------
 * la vamosa utilizar para definir variables que son utilizadas.
 *  ------------------------
 */
public class UtilConstants {
	
	//estas tres constantes son utilizadas para filtrar ConsultaConvenioObservacion
	public static Long Consulta_Convenio_Observacion_Tipo_1 = new Long("1");
	public static Long Consulta_Convenio_Observacion_Tipo_2 = new Long("2");
	public static Long Consulta_Convenio_Observacion_Tipo_3 = new Long("3");
	
	
	
}
