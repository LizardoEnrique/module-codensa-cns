package com.synapsis.cns.codensa.model;



import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author m3.MarioRoss - 14/11/2006
 * 	refactor dBraccio 29/11/2006
 */
public interface SellosMedidor extends SynergiaBusinessObject {

	public String getColorSello();
	public void setColorSello(String colorSello);
	public String getEstadoSello();
	public void setEstadoSello(String estadoSello);
	public Long getIdComponente() ;
	public void setIdComponente(Long idComponente);
	public String getMaterialSello() ;
	public void setMaterialSello(String materialSello) ;
	public Long getNroSello() ;
	public void setNroSello(Long nroSello) ;
	public Date getFechaDescarga();
	public void setFechaDescarga(Date fechaDescarga);	
	
}