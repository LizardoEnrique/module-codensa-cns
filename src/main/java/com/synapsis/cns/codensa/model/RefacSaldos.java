package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacSaldos extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public Double getValorEnergiaAjuste();

	public void setValorEnergiaAjuste(Double valorEnergiaAjuste);

	public Double getValorOtrosNegAjuste();

	public void setValorOtrosNegAjuste(Double valorOtrosNegAjuste);

	public Double getSaldoTotalAntesAjuste();

	public void setSaldoTotalAntesAjuste(Double saldoTotalAntesAjuste);

	public Double getSaldoTotalDespuesAjuste();

	public void setSaldoTotalDespuesAjuste(Double saldoTotalDespuesAjuste);

	public Double getSaldoEnergiaAntesAjuste();

	public void setSaldoEnergiaAntesAjuste(Double saldoEnergiaAntesAjuste);

	public Double getSaldoEnergiaDespuesAjuste();

	public void setSaldoEnergiaDespuesAjuste(Double saldoEnergiaDespuesAjuste);

	public Double getSaldoOtrosNegAntesAjuste();

	public void setSaldoOtrosNegAntesAjuste(Double saldoOtrosNegAntesAjuste);

	public Double getSaldoOtrosNegDespuesAjuste();

	public void setSaldoOtrosNegDespuesAjuste(Double saldoOtrosNegDespuesAjuste);
	
	public Double getSaldoAjusteServicioConvenios();

	public void setSaldoAjusteServicioConvenios(Double saldoAjusteServicioConvenios);

	public Double getSaldoAjusteServicioCuotaManejo();

	public void setSaldoAjusteServicioCuotaManejo(
			Double saldoAjusteServicioCuotaManejo) ;

	public Double getSaldoAjusteServicioECO();

	public void setSaldoAjusteServicioECO(Double saldoAjusteServicioECO);

	public Double getSaldoAjusteServicioElectrico();

	public void setSaldoAjusteServicioElectrico(Double saldoAjusteServicioElectrico);

	public Double getSaldoAjusteServicioFinanciero();

	public void setSaldoAjusteServicioFinanciero(
			Double saldoAjusteServicioFinanciero);

	public Double getSaldoAjusteServicioVentas();

	public void setSaldoAjusteServicioVentas(Double saldoAjusteServicioVentas);

	public Double getSaldoFinalCuotaManejo();

	public void setSaldoFinalCuotaManejo(Double saldoFinalCuotaManejo);

	public Double getSaldoFinalServicioConvenios();

	public void setSaldoFinalServicioConvenios(Double saldoFinalServicioConvenios);

	public Double getSaldoFinalServicioECO();

	public void setSaldoFinalServicioECO(Double saldoFinalServicioECO);
	
	public Double getSaldoFinalServicioElectrico();

	public void setSaldoFinalServicioElectrico(Double saldoFinalServicioElectrico);

	public Double getSaldoFinalServicioFinanciero();

	public void setSaldoFinalServicioFinanciero(Double saldoFinalServicioFinanciero);
	
	public Double getSaldoFinalServicioVenta();

	public void setSaldoFinalServicioVenta(Double saldoFinalServicioVenta);

	public Double getSaldoInicialServicioConvenios();

	public void setSaldoInicialServicioConvenios(
			Double saldoInicialServicioConvenios);

	public Double getSaldoInicialServicioCuotaManejo();

	public void setSaldoInicialServicioCuotaManejo(
			Double saldoInicialServicioCuotaManejo);

	public Double getSaldoInicialServicioECO();

	public void setSaldoInicialServicioECO(Double saldoInicialServicioECO);

	public Double getSaldoInicialServicioElectrico();

	public void setSaldoInicialServicioElectrico(
			Double saldoInicialServicioElectrico);

	public Double getSaldoInicialServicioFinanciero();
	public void setSaldoInicialServicioFinanciero(
			Double saldoInicialServicioFinanciero);

	public Double getSaldoInicialServicioVenta();

	public void setSaldoInicialServicioVenta(Double saldoInicialServicioVenta);
	
	
	
	
	

}