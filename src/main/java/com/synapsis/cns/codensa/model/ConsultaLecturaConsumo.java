/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 006
 */
public interface ConsultaLecturaConsumo extends SynergiaBusinessObject {

	public Long getIdMedidor();
	public void setIdMedidor(Long idMedidor);
	public Double getConsumoActivaServicioDirecto();
	public String getConsumoDemandaFP();
	public Double getConsumoFacturadoActivaFP();
	public String getDescripcionAnomaliaLecturaFacturacion();
	public Date getFechaLecturaActual();
	public Date getFechaLecturaAnterior();
	public Long getIdCuenta();
	public Long getIdServicio();
	public Double getLecturaFacturaActivaFP();
	public Double getLecturaTerrenoActivaFP();
	public Long getNroCuenta();
	public String getPeriodoFacturacion();
	public String getUbicacionMedidor();
	public Long getNroServicio();
	public void setNroServicio(Long nroServicio);
	public void setConsumoActivaServicioDirecto(Double consumoActivaServicioDirecto);
	public void setConsumoDemandaFP(String consumoDemandaFP);
	public void setConsumoFacturadoActivaFP(Double consumoFacturadoActivaFP);
	public void setDescripcionAnomaliaLecturaFacturacion(String descripcionAnomaliaLecturaFacturacion);
	public void setFechaLecturaActual(Date fechaLecturaActual);
	public void setFechaLecturaAnterior(Date fechaLecturaAnterior);
	public void setIdCuenta(Long idCuenta);
	public void setIdServicio(Long idServicio);
	public void setLecturaFacturaActivaFP(Double lecturaFacturaActivaFP);
	public void setLecturaTerrenoActivaFP(Double lecturaTerrenoActivaFP);
	public void setNroCuenta(Long nroCuenta);
	public void setPeriodoFacturacion(String periodoFacturacion);
	public void setUbicacionMedidor(String ubicacionMedidor);
	public String getCodigoAnomaliaLecturaFacturacion();
	public String getCodigoAnomaliaLecturaTerreno();
	public void setCodigoAnomaliaLecturaFacturacion(String codigoAnomaliaLecturaFacturacion);
	public void setCodigoAnomaliaLecturaTerreno(String codigoAnomaliaLecturaTerreno);
	public Double getConsumoDemandaHP();
	public void setConsumoDemandaHP(Double consumoDemandaHP);
	public Double getConsumoDemandaXP();
	public void setConsumoDemandaXP(Double consumoDemandaXP);
	public String getTipoLectura();
	public void setTipoLectura(String tipoLectura);
	public String getEvento();
	public void setEvento(String evento);	
	public Double getConsumoFacturadoActivaHP();
	public void setConsumoFacturadoActivaHP(Double consumoFacturadoActivaHP);
	public Double getConsumoFacturadoActivaXP();
	public void setConsumoFacturadoActivaXP(Double consumoFacturadoActivaXP);
	public Double getLecturaFacturaActivaHP();
	public void setLecturaFacturaActivaHP(Double lecturaFacturaActivaHP);
	public Double getLecturaFacturaActivaXP();
	public void setLecturaFacturaActivaXP(Double lecturaFacturaActivaXP);
	public Double getLecturaTerrenoActivaHP();
	public void setLecturaTerrenoActivaHP(Double lecturaTerrenoActivaHP);
	public Double getLecturaTerrenoActivaXP();
	public void setLecturaTerrenoActivaXP(Double lecturaTerrenoActivaXP);
}
