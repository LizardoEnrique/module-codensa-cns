package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaAccionesModificacionesVinculoClienteRed;
import com.synapsis.cns.codensa.model.ConsultaDatosVinculoClienteRedByParams;
import com.synapsis.cns.codensa.model.ConsultaServiciosElectricosByNroTransformador;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaAccionesModificacionesVinculoClienteRedImpl extends SynergiaBusinessObjectImpl implements ConsultaAccionesModificacionesVinculoClienteRed {


	private String tipoAccion;
	private String formaActual;
	private String usuarioSolic;
	private String fecCambio;
	private String ipCambio;
	private String transformador;
	
	public String getFecCambio() {
		return fecCambio;
	}
	public void setFecCambio(String fecCambio) {
		this.fecCambio = fecCambio;
	}
	public String getFormaActual() {
		return formaActual;
	}
	public void setFormaActual(String formaActual) {
		this.formaActual = formaActual;
	}
	public String getIpCambio() {
		return ipCambio;
	}
	public void setIpCambio(String ipCambio) {
		this.ipCambio = ipCambio;
	}
	public String getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public String getTransformador() {
		return transformador;
	}
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}
	public String getUsuarioSolic() {
		return usuarioSolic;
	}
	public void setUsuarioSolic(String usuarioSolic) {
		this.usuarioSolic = usuarioSolic;
	}
	
	
	
}
