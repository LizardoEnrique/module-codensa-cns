package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaConvenioDocumentosSoporte;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaConvenioDocumentosSoporteImpl extends SynergiaBusinessObjectImpl implements  ConsultaConvenioDocumentosSoporte {
	
	private String documentosSoporte;
	private Long nroConvenio;
	
	private Long nroCuenta;
	

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDocumentosSoporte#getDocumentosSoporte()
	 */
	public String getDocumentosSoporte() {
		return documentosSoporte;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaConvenioDocumentosSoporte#setDocumentosSoporte(java.lang.String)
	 */
	public void setDocumentosSoporte(String documentosSoporte) {
		this.documentosSoporte = documentosSoporte;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	
}
