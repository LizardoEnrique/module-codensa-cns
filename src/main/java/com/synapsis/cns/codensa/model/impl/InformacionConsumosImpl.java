package com.synapsis.cns.codensa.model.impl;

import java.util.HashMap;

import com.synapsis.cns.codensa.model.InformacionConsumos;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author adambrosio
 */
public class InformacionConsumosImpl extends SynergiaBusinessObjectImpl implements InformacionConsumos {

	private Long nroFactura;

	// INFORMACIÓN DEL CONSUMO
	private String tipoLecturaPeriodoActual;
	private Double consumoPromedioUltimosMeses;
	private String anomaliaLecturaPeriodoActual;

	private String mesTarifaParaLiquidacion;
	private Double valorPromedioKwh;
	private String tarifaParaFacturacionActual;
	private Double lecturaActual;
	private Double lecturaAnterior;
	private Double diferenciaEntreLecturaActualYAnterior;
	private Double factorLiquidacion;
	private Double energiaConsumida;
	private Double energiaFacturada;
	private Double totalConsumoKwh;

	// CALIDAD DEL SERVICIO
	private Integer maximoPermitidoInterrupciones;
	private Integer maximoPermitidoHoras;
	private Integer numeroInterrupcionesAcumuladas;
	private Integer numeroHorasInterrupcionesAcumuladas;
	private Long periodoInterrupciones;
	private Double trimestreInterrupciones;

	// INFORMACION DE INTERES
	private Double generacion;
	private Double transmision;
	private Double perdidas;
	private Double distribucion;
	private Double otros;
	private Double comercializacion;
	private Double costoUnitario;
	private Double costoFijo;
	private Double opcionTarifaria;

	private Long idFactura;

	public InformacionConsumosImpl() {
	}

	public InformacionConsumosImpl(HashMap values) {

	}

	public String getAnomaliaLecturaPeriodoActual() {
		return anomaliaLecturaPeriodoActual;
	}

	public void setAnomaliaLecturaPeriodoActual(String anomaliaLecturaPeriodoActual) {
		this.anomaliaLecturaPeriodoActual = anomaliaLecturaPeriodoActual;
	}

	public Double getComercializacion() {
		return comercializacion;
	}

	public void setComercializacion(Double comercializacion) {
		this.comercializacion = comercializacion;
	}

	public Double getConsumoPromedioUltimosMeses() {
		return consumoPromedioUltimosMeses;
	}

	public void setConsumoPromedioUltimosMeses(Double consumoPromedioUltimosMeses) {
		this.consumoPromedioUltimosMeses = consumoPromedioUltimosMeses;
	}

	public Double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public Double getDiferenciaEntreLecturaActualYAnterior() {
		return diferenciaEntreLecturaActualYAnterior;
	}

	public void setDiferenciaEntreLecturaActualYAnterior(Double diferenciaEntreLecturaActualYAnterior) {
		this.diferenciaEntreLecturaActualYAnterior = diferenciaEntreLecturaActualYAnterior;
	}

	public Double getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Double distribucion) {
		this.distribucion = distribucion;
	}

	public Double getEnergiaConsumida() {
		return energiaConsumida;
	}

	public void setEnergiaConsumida(Double energiaConsumida) {
		this.energiaConsumida = energiaConsumida;
	}

	public Double getEnergiaFacturada() {
		return energiaFacturada;
	}

	public void setEnergiaFacturada(Double energiaFacturada) {
		this.energiaFacturada = energiaFacturada;
	}

	public Double getFactorLiquidacion() {
		return factorLiquidacion;
	}

	public void setFactorLiquidacion(Double factorLiquidacion) {
		this.factorLiquidacion = factorLiquidacion;
	}

	public Double getGeneracion() {
		return generacion;
	}

	public void setGeneracion(Double generacion) {
		this.generacion = generacion;
	}

	public Double getLecturaActual() {
		return lecturaActual;
	}

	public void setLecturaActual(Double lecturaActual) {
		this.lecturaActual = lecturaActual;
	}

	public Double getLecturaAnterior() {
		return lecturaAnterior;
	}

	public void setLecturaAnterior(Double lecturaAnterior) {
		this.lecturaAnterior = lecturaAnterior;
	}

	public Integer getMaximoPermitidoHoras() {
		return maximoPermitidoHoras;
	}

	public void setMaximoPermitidoHoras(Integer maximoPermitidoHoras) {
		this.maximoPermitidoHoras = maximoPermitidoHoras;
	}

	public Integer getMaximoPermitidoInterrupciones() {
		return maximoPermitidoInterrupciones;
	}

	public void setMaximoPermitidoInterrupciones(Integer maximoPermitidoInterrupciones) {
		this.maximoPermitidoInterrupciones = maximoPermitidoInterrupciones;
	}

	public String getMesTarifaParaLiquidacion() {
		return mesTarifaParaLiquidacion;
	}

	public void setMesTarifaParaLiquidacion(String mesTarifaParaLiquidacion) {
		this.mesTarifaParaLiquidacion = mesTarifaParaLiquidacion;
	}

	public Long getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}

	public Integer getNumeroHorasInterrupcionesAcumuladas() {
		return numeroHorasInterrupcionesAcumuladas;
	}

	public void setNumeroHorasInterrupcionesAcumuladas(Integer numeroHorasInterrupcionesAcumuladas) {
		this.numeroHorasInterrupcionesAcumuladas = numeroHorasInterrupcionesAcumuladas;
	}

	public Integer getNumeroInterrupcionesAcumuladas() {
		return numeroInterrupcionesAcumuladas;
	}

	public void setNumeroInterrupcionesAcumuladas(Integer numeroInterrupcionesAcumuladas) {
		this.numeroInterrupcionesAcumuladas = numeroInterrupcionesAcumuladas;
	}

	public Double getOtros() {
		return otros;
	}

	public void setOtros(Double otros) {
		this.otros = otros;
	}

	public Double getPerdidas() {
		return perdidas;
	}

	public void setPerdidas(Double perdidas) {
		this.perdidas = perdidas;
	}

	public Long getPeriodoInterrupciones() {
		return periodoInterrupciones;
	}

	public void setPeriodoInterrupciones(Long periodoInterrupciones) {
		this.periodoInterrupciones = periodoInterrupciones;
	}

	public String getTarifaParaFacturacionActual() {
		return tarifaParaFacturacionActual;
	}

	public void setTarifaParaFacturacionActual(String tarifaParaFacturacionActual) {
		this.tarifaParaFacturacionActual = tarifaParaFacturacionActual;
	}

	public String getTipoLecturaPeriodoActual() {
		return tipoLecturaPeriodoActual;
	}

	public void setTipoLecturaPeriodoActual(String tipoLecturaPeriodoActual) {
		this.tipoLecturaPeriodoActual = tipoLecturaPeriodoActual;
	}

	public Double getTotalConsumoKwh() {
		return totalConsumoKwh;
	}

	public void setTotalConsumoKwh(Double totalConsumoKwh) {
		this.totalConsumoKwh = totalConsumoKwh;
	}

	public Double getTransmision() {
		return transmision;
	}

	public void setTransmision(Double transmision) {
		this.transmision = transmision;
	}

	public Double getTrimestreInterrupciones() {
		return trimestreInterrupciones;
	}

	public void setTrimestreInterrupciones(Double trimestreInterrupciones) {
		this.trimestreInterrupciones = trimestreInterrupciones;
	}

	public Double getValorPromedioKwh() {
		return valorPromedioKwh;
	}

	public void setValorPromedioKwh(Double valorPromedioKwh) {
		this.valorPromedioKwh = valorPromedioKwh;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public Double getCostoFijo() {
		return costoFijo;
	}

	public void setCostoFijo(Double costoFijo) {
		this.costoFijo = costoFijo;
	}

	public Double getOpcionTarifaria() {
		return opcionTarifaria;
	}

	public void setOpcionTarifaria(Double opcionTarifaria) {
		this.opcionTarifaria = opcionTarifaria;
	}

}
