package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedientesNotificaciones extends SynergiaBusinessObject {

	/**
	 * @return the calidadActua
	 */
	public String getCalidadActua();

	/**
	 * @param calidadActua the calidadActua to set
	 */
	public void setCalidadActua(String calidadActua);

	/**
	 * @return the fechaDesfijacionEdicto
	 */
	public Date getFechaDesfijacionEdicto();

	/**
	 * @param fechaDesfijacionEdicto the fechaDesfijacionEdicto to set
	 */
	public void setFechaDesfijacionEdicto(Date fechaDesfijacionEdicto);

	/**
	 * @return the fechaFijacionEdicto
	 */
	public Date getFechaFijacionEdicto();

	/**
	 * @param fechaFijacionEdicto the fechaFijacionEdicto to set
	 */
	public void setFechaFijacionEdicto(Date fechaFijacionEdicto);

	/**
	 * @return the fechaNotificacion
	 */
	public Date getFechaNotificacion();

	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(Date fechaNotificacion);

	/**
	 * @return the nombreNotifico
	 */
	public String getNombreNotifico();

	/**
	 * @param nombreNotifico the nombreNotifico to set
	 */
	public void setNombreNotifico(String nombreNotifico);

	/**
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento();

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(String numeroDocumento);

	/**
	 * @return the tipoDocumento
	 */
	public String getTipoDocumento();

	/**
	 * @param tipoDocumento the tipoDocumento to set
	 */
	public void setTipoDocumento(String tipoDocumento);

	/**
	 * @return the tipoNotificacion
	 */
	public String getTipoNotificacion();

	/**
	 * @param tipoNotificacion the tipoNotificacion to set
	 */
	public void setTipoNotificacion(String tipoNotificacion);
	
	public Long getNumeroExpediente();
	
	public void setNumeroExpediente(Long numeroExpediente);

}