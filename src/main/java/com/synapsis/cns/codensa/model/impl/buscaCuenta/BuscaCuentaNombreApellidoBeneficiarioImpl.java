package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaNombreApellidoBeneficiario;

public class BuscaCuentaNombreApellidoBeneficiarioImpl extends BuscaCuentaImpl implements BuscaCuentaNombreApellidoBeneficiario {
	
	private String nombre;
	private String apellido;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoBeneficiario#getApellido()
	 */
	public String getApellido() {
		return apellido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoBeneficiario#getNombre()
	 */
	public String getNombre() {
		return nombre;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoBeneficiario#setApellido(java.lang.String)
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaNombreApellidoBeneficiario#setNombre(java.lang.String)
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}