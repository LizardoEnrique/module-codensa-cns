package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface DatosConvenio extends SynergiaBusinessObject{

	/**
	 * @return the fechaPago
	 */
	public Date getFechaPago();

	/**
	 * @return the intereses
	 */
	public Long getIntereses();

	/**
	 * @return the interesesMora
	 */
	public Long getInteresesMora();

	/**
	 * @return the iva
	 */
	public Long getIva();

	/**
	 * @return the nroConvenioEncargo
	 */
	public Long getNroConvenioEncargo();

	/**
	 * @return the valorCuotas
	 */
	public Long getValorCuotas();

	/**
	 * @return the valorPagado
	 */
	public Long getValorPagado();

	/**
	 * @return the valorPagar
	 */
	public Long getValorPagar();

	/**
	 * @param fechaPago the fechaPago to set
	 */
	public void setFechaPago(Date fechaPago);

	/**
	 * @param intereses the intereses to set
	 */
	public void setIntereses(Long intereses);

	/**
	 * @param interesesMora the interesesMora to set
	 */
	public void setInteresesMora(Long interesesMora);

	/**
	 * @param iva the iva to set
	 */
	public void setIva(Long iva);

	/**
	 * @param nroConvenioEncargo the nroConvenioEncargo to set
	 */
	public void setNroConvenioEncargo(Long nroConvenioEncargo);

	/**
	 * @param valorCuotas the valorCuotas to set
	 */
	public void setValorCuotas(Long valorCuotas);

	/**
	 * @param valorPagado the valorPagado to set
	 */
	public void setValorPagado(Long valorPagado);

	/**
	 * @param valorPagar the valorPagar to set
	 */
	public void setValorPagar(Long valorPagar);
	
}