package com.synapsis.cns.codensa.model.impl;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaConvenioImpl extends SynergiaBusinessObjectImpl 
	implements ConsultaConvenio {
	
//	private Long idConvenio;
	private Long nroCuenta;
	private Long nroConvenio;
	private String tipoConvenio;
	private String estado;	
	private Date fechaCreacion;	
	private Date fechaFinalizacion;	
	private Date fechaTerminacion;		
	private String opcion;	
	private Long deudaFinanciada;	
	private Long valorCuotaInicial;
	private Long idCargo;
	private Long idCuota;
	private String nroCuotas;
	private String tipoServicioFinanciado;
	private Long numeroServicio;
	private Long idCondonacion;
	private BigDecimal saldoVencido;
	private BigDecimal saldoEnConvenio;
	private Integer nroCuotasFacturadas;
	private Integer nroCuotasFaltantes;
	private Integer nroCuotasVencidas;
	
	public Integer getNroCuotasFacturadas() {
		return nroCuotasFacturadas;
	}
	public void setNroCuotasFacturadas(Integer nroCuotasFacturadas) {
		this.nroCuotasFacturadas = nroCuotasFacturadas;
	}
	public Integer getNroCuotasFaltantes() {
		return nroCuotasFaltantes;
	}
	public void setNroCuotasFaltantes(Integer nroCuotasFaltantes) {
		this.nroCuotasFaltantes = nroCuotasFaltantes;
	}
	public Integer getNroCuotasVencidas() {
		return nroCuotasVencidas;
	}
	public void setNroCuotasVencidas(Integer nroCuotasVencidas) {
		this.nroCuotasVencidas = nroCuotasVencidas;
	}
	public Long getDeudaFinanciada() {
		return deudaFinanciada;
	}
	public void setDeudaFinanciada(Long deudaFinanciada) {
		this.deudaFinanciada = deudaFinanciada;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}
	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}
	public Long getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}
	public Long getIdCondonacion() {
		return idCondonacion;
	}
	public void setIdCondonacion(Long idCondonacion) {
		this.idCondonacion = idCondonacion;
	}
	public Long getIdCuota() {
		return idCuota;
	}
	public void setIdCuota(Long idCuota) {
		this.idCuota = idCuota;
	}
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getNroCuotas() {
		return nroCuotas;
	}
	public void setNroCuotas(String nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	public Long getNumeroServicio() {
		return numeroServicio;
	}
	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public String getTipoConvenio() {
		return tipoConvenio;
	}
	public void setTipoConvenio(String tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}
	public String getTipoServicioFinanciado() {
		return tipoServicioFinanciado;
	}
	public void setTipoServicioFinanciado(String tipoServicioFinanciado) {
		this.tipoServicioFinanciado = tipoServicioFinanciado;
	}
	public Long getValorCuotaInicial() {
		return valorCuotaInicial;
	}
	public void setValorCuotaInicial(Long valorCuotaInicial) {
		this.valorCuotaInicial = valorCuotaInicial;
	}
	public BigDecimal getSaldoVencido() {
		return saldoVencido;
	}
	public void setSaldoVencido(BigDecimal saldoVencido) {
		this.saldoVencido = saldoVencido;
	}
	public BigDecimal getSaldoEnConvenio() {
		return saldoEnConvenio;
	}
	public void setSaldoEnConvenio(BigDecimal saldoEnConvenio) {
		this.saldoEnConvenio = saldoEnConvenio;
	}

}