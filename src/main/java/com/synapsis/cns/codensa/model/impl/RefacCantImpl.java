package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import com.synapsis.cns.codensa.model.RefacCant;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacCantImpl extends SynergiaBusinessObjectImpl implements RefacCant {

	private Long nroCuenta;

	private Long cantidadRefac;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public Long getCantidadRefac() {
		return cantidadRefac;
	}

	public void setCantidadRefac(Long cantidadRefac) {
		this.cantidadRefac = cantidadRefac;
	}
}