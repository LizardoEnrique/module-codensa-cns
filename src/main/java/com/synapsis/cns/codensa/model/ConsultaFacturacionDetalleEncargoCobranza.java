package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaFacturacionDetalleEncargoCobranza extends SynergiaBusinessObject {

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);
	/**
	 * @return the periodoPrimeraFact
	 */
	public Date getPeriodoPrimeraFact();
	/**
	 * @param periodoPrimeraFact the periodoPrimeraFact to set
	 */
	public void setPeriodoPrimeraFact(Date periodoPrimeraFact) ;
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() ;
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) ;
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() ;
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio);
	/**
	 * @return the tipoProducto
	 */
	public String getTipoProducto() ;
	/**
	 * @param tipoProducto the tipoProducto to set
	 */
	public void setTipoProducto(String tipoProducto);
	/**
	 * @return the plan
	 */
	public String getPlan();
	/**
	 * @param plan the plan to set
	 */
	public void setPlan(String plan);
	/**
	 * @return the socioNegocio
	 */
	public String getSocioNegocio() ;
	/**
	 * @param socioNegocio the socioNegocio to set
	 */
	public void setSocioNegocio(String socioNegocio);
	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres() ;
	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres);
	/**
	 * @return the valorCuota
	 */
	public String getValorCuota();
	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(String valorCuota);
	/**
	 * @return the cuotaInteres
	 */
	public String getCuotaInteres();
	/**
	 * @param cuotaInteres the cuotaInteres to set
	 */
	public void setCuotaInteres(String cuotaInteres);
	/**
	 * @return the nroCuotas
	 */
	public Long getNroCuotas() ;
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(Long nroCuotas);
	/**
	 * @return the cantVecesFac
	 */
	public Long getCantVecesFac();
	/**
	 * @param cantVecesFac the cantVecesFac to set
	 */
	public void setCantVecesFac(Long cantVecesFac);
	/**
	 * @return the cuotasPendientes
	 */
	public Integer getCuotasPendientes() ;
	/**
	 * @param cuotasPendientes the cuotasPendientes to set
	 */
	public void setCuotasPendientes(Integer cuotasPendientes) ;
	/**
	 * @return the nombreTitlServ
	 */
	public String getNombreTitlServ() ;
	/**
	 * @param nombreTitlServ the nombreTitlServ to set
	 */
	public void setNombreTitlServ(String nombreTitlServ);
	/**
	 * @return the nroIdentificacion
	 */
	public String getNroIdentificacion() ;
	/**
	 * @param nroIdentificacion the nroIdentificacion to set
	 */
	public void setNroIdentificacion(String nroIdentificacion) ;
	/**
	 * @return the itemsFacturados
	 */
	public Long getItemsFacturados();
	/**
	 * @param itemsFacturados the itemsFacturados to set
	 */
	public void setItemsFacturados(Long itemsFacturados);
	/**
	 * @return the id
	 */
	public Long getId();
	/**
	 * @param id the id to set
	 */
	public void setId(Long id);
	
	/**
	 * @return the saldoCapital
	 */
	public String getSaldoCapital();
	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(String saldoCapital);
	
}