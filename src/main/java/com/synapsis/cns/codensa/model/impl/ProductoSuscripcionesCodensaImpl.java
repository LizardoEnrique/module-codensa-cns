package com.synapsis.cns.codensa.model.impl;

import java.util.Set;

import com.synapsis.cns.codensa.model.ProductoSuscripcionesCodensa;

/**
 * Producto Suscripciones Codensa para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ProductoSuscripcionesCodensaImpl extends ServicioFinancieroECOImpl implements ProductoSuscripcionesCodensa {
	private static final long serialVersionUID = 1L;
	
	// Items
	Set itemsSuscripcionesCodensa;

	public Set getItemsSuscripcionesCodensa() {
		return itemsSuscripcionesCodensa;
	}

	public void setItemsSuscripcionesCodensa(Set itemsSuscripcionesCodensa) {
		this.itemsSuscripcionesCodensa = itemsSuscripcionesCodensa;
	}


}