/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaMensajes;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaMensajesImpl extends SynergiaBusinessObjectImpl implements ConsultaMensajes {
	private Long nroCuenta;
	private String mensaje1;
	private String mensaje2;
	/**
	 * @return the mensaje1
	 */
	public String getMensaje1() {
		return mensaje1;
	}
	/**
	 * @param mensaje1 the mensaje1 to set
	 */
	public void setMensaje1(String mensaje1) {
		this.mensaje1 = mensaje1;
	}
	/**
	 * @return the mensaje2
	 */
	public String getMensaje2() {
		return mensaje2;
	}
	/**
	 * @param mensaje2 the mensaje2 to set
	 */
	public void setMensaje2(String mensaje2) {
		this.mensaje2 = mensaje2;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

}
