package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ServicioFinanciero;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * Servicio Financiero para mostrar en el Reporte de Impresion de Anexo de Factura (CNS003)
 * 
 * @author jhack
 */
public class ServicioFinancieroImpl extends SynergiaBusinessObjectImpl implements ServicioFinanciero {
	private static final long serialVersionUID = 1L;

	private String 	nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}