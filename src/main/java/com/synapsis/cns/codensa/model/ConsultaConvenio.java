package com.synapsis.cns.codensa.model;

import java.math.BigDecimal;
import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


public interface ConsultaConvenio extends SynergiaBusinessObject{

	public Long getDeudaFinanciada();
	public void setDeudaFinanciada(Long deudaFinanciada);
	public String getEstado();
	public void setEstado(String estado);
	public Date getFechaCreacion();
	public void setFechaCreacion(Date fechaCreacion);
	public Date getFechaFinalizacion();
	public void setFechaFinalizacion(Date fechaFinalizacion);
	public Date getFechaTerminacion();
	public void setFechaTerminacion(Date fechaTerminacion);
	public Long getIdCargo();
	public void setIdCargo(Long idCargo);
	public Long getIdCondonacion();
	public void setIdCondonacion(Long idCondonacion);
	public Long getIdCuota();
	public void setIdCuota(Long idCuota);
	public String getNroCuotas();
	public void setNroCuotas(String nroCuotas);
	public Long getNumeroServicio();
	public void setNumeroServicio(Long numeroServicio);
	public String getOpcion();
	public void setOpcion(String opcion);
	public String getTipoConvenio();
	public void setTipoConvenio(String tipoConvenio);
	public String getTipoServicioFinanciado();
	public void setTipoServicioFinanciado(String tipoServicioFinanciado);
	public Long getValorCuotaInicial();
	public void setValorCuotaInicial(Long valorCuotaInicial);
	public Long getNroConvenio();
	public void setNroConvenio(Long nroConvenio);
	public Long getNroCuenta();
	public void setNroCuenta(Long nroCuenta);
	
	public BigDecimal getSaldoVencido();
	public void setSaldoVencido(BigDecimal saldoVencido);
	public BigDecimal getSaldoEnConvenio();
	public void setSaldoEnConvenio(BigDecimal saldoEnConvenio);
	
	public Integer getNroCuotasFacturadas();
	public void setNroCuotasFacturadas(Integer nroCuotasFacturadas);
	public Integer getNroCuotasFaltantes();
	public void setNroCuotasFaltantes(Integer nroCuotasFaltantes);
	public Integer getNroCuotasVencidas();
	public void setNroCuotasVencidas(Integer nroCuotasVencidas);
		
	
}