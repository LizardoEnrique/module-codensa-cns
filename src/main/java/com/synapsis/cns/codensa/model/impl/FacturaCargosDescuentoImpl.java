package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.FacturaCargosDescuento;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author jhv
 */

public class FacturaCargosDescuentoImpl extends SynergiaBusinessObjectImpl
		implements FacturaCargosDescuento {

	private String nroFactura;

	private String codigo;

	private String concepto;

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
}