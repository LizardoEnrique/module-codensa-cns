/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaLecturaConsumo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 * 
 * CU 006
 *
 */
public class ConsultaLecturaConsumoImpl extends SynergiaBusinessObjectImpl implements ConsultaLecturaConsumo{

	private Long nroCuenta;
	private Long idCuenta;
	private Long idServicio;
	private Long nroServicio;
	private Long idMedidor;
	private Date fechaLecturaAnterior;
	private Date fechaLecturaActual;
	private String periodoFacturacion;
	private String tipoLectura;
	private String evento;
	private String codigoAnomaliaLecturaTerreno;
	private String codigoAnomaliaLecturaFacturacion;	
	private String descripcionAnomaliaLecturaFacturacion;
	private Double lecturaTerrenoActivaFP;
	private Double lecturaFacturaActivaFP;
	private Double consumoFacturadoActivaFP;

	private Double lecturaTerrenoActivaHP;
	private Double lecturaFacturaActivaHP;
	private Double consumoFacturadoActivaHP;
	private Double lecturaTerrenoActivaXP;
	private Double lecturaFacturaActivaXP;
	private Double consumoFacturadoActivaXP;
	
	private Double consumoActivaServicioDirecto;
	private String ubicacionMedidor;	
	private String consumoDemandaFP;
	private Double consumoDemandaHP;
	private Double consumoDemandaXP;	
	
	public Long getIdMedidor() {
		return idMedidor;
	}
	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}
	public Double getConsumoActivaServicioDirecto() {
		return consumoActivaServicioDirecto;
	}
	public String getConsumoDemandaFP() {
		return consumoDemandaFP;
	}
	public Double getConsumoFacturadoActivaFP() {
		return consumoFacturadoActivaFP;
	}
	public String getDescripcionAnomaliaLecturaFacturacion() {
		return descripcionAnomaliaLecturaFacturacion;
	}
	public Date getFechaLecturaActual() {
		return fechaLecturaActual;
	}
	public Date getFechaLecturaAnterior() {
		return fechaLecturaAnterior;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdServicio() {
		return idServicio;
	}
	public Double getLecturaFacturaActivaFP() {
		return lecturaFacturaActivaFP;
	}
	public Double getLecturaTerrenoActivaFP() {
		return lecturaTerrenoActivaFP;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}
	public String getUbicacionMedidor() {
		return ubicacionMedidor;
	}
	
	public Long getNroServicio() {
		return nroServicio;
	}
	
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	public void setConsumoActivaServicioDirecto(Double consumoActivaServicioDirecto) {
		this.consumoActivaServicioDirecto = consumoActivaServicioDirecto;
	}
	public void setConsumoDemandaFP(String consumoDemandaFP) {
		this.consumoDemandaFP = consumoDemandaFP;
	}
	public void setConsumoFacturadoActivaFP(Double consumoFacturadoActivaFP) {
		this.consumoFacturadoActivaFP = consumoFacturadoActivaFP;
	}
	public void setDescripcionAnomaliaLecturaFacturacion(
			String descripcionAnomaliaLecturaFacturacion) {
		this.descripcionAnomaliaLecturaFacturacion = descripcionAnomaliaLecturaFacturacion;
	}
	public void setFechaLecturaActual(Date fechaLecturaActual) {
		this.fechaLecturaActual = fechaLecturaActual;
	}
	public void setFechaLecturaAnterior(Date fechaLecturaAnterior) {
		this.fechaLecturaAnterior = fechaLecturaAnterior;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
	public void setLecturaFacturaActivaFP(Double lecturaFacturaActivaFP) {
		this.lecturaFacturaActivaFP = lecturaFacturaActivaFP;
	}
	public void setLecturaTerrenoActivaFP(Double lecturaTerrenoActivaFP) {
		this.lecturaTerrenoActivaFP = lecturaTerrenoActivaFP;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}
	public void setUbicacionMedidor(String ubicacionMedidor) {
		this.ubicacionMedidor = ubicacionMedidor;
	}
	public String getCodigoAnomaliaLecturaFacturacion() {
		return codigoAnomaliaLecturaFacturacion;
	}
	public String getCodigoAnomaliaLecturaTerreno() {
		return codigoAnomaliaLecturaTerreno;
	}
	public void setCodigoAnomaliaLecturaFacturacion(
			String codigoAnomaliaLecturaFacturacion) {
		this.codigoAnomaliaLecturaFacturacion = codigoAnomaliaLecturaFacturacion;
	}
	public void setCodigoAnomaliaLecturaTerreno(String codigoAnomaliaLecturaTerreno) {
		this.codigoAnomaliaLecturaTerreno = codigoAnomaliaLecturaTerreno;
	}
	public Double getConsumoDemandaHP() {
		return consumoDemandaHP;
	}
	public void setConsumoDemandaHP(Double consumoDemandaHP) {
		this.consumoDemandaHP = consumoDemandaHP;
	}
	public Double getConsumoDemandaXP() {
		return consumoDemandaXP;
	}
	public void setConsumoDemandaXP(Double consumoDemandaXP) {
		this.consumoDemandaXP = consumoDemandaXP;
	}
	public String getTipoLectura() {
		return tipoLectura;
	}
	public void setTipoLectura(String tipoLectura) {
		this.tipoLectura = tipoLectura;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public Double getConsumoFacturadoActivaHP() {
		return consumoFacturadoActivaHP;
	}
	public void setConsumoFacturadoActivaHP(Double consumoFacturadoActivaHP) {
		this.consumoFacturadoActivaHP = consumoFacturadoActivaHP;
	}
	public Double getConsumoFacturadoActivaXP() {
		return consumoFacturadoActivaXP;
	}
	public void setConsumoFacturadoActivaXP(Double consumoFacturadoActivaXP) {
		this.consumoFacturadoActivaXP = consumoFacturadoActivaXP;
	}
	public Double getLecturaFacturaActivaHP() {
		return lecturaFacturaActivaHP;
	}
	public void setLecturaFacturaActivaHP(Double lecturaFacturaActivaHP) {
		this.lecturaFacturaActivaHP = lecturaFacturaActivaHP;
	}
	public Double getLecturaFacturaActivaXP() {
		return lecturaFacturaActivaXP;
	}
	public void setLecturaFacturaActivaXP(Double lecturaFacturaActivaXP) {
		this.lecturaFacturaActivaXP = lecturaFacturaActivaXP;
	}
	public Double getLecturaTerrenoActivaHP() {
		return lecturaTerrenoActivaHP;
	}
	public void setLecturaTerrenoActivaHP(Double lecturaTerrenoActivaHP) {
		this.lecturaTerrenoActivaHP = lecturaTerrenoActivaHP;
	}
	public Double getLecturaTerrenoActivaXP() {
		return lecturaTerrenoActivaXP;
	}
	public void setLecturaTerrenoActivaXP(Double lecturaTerrenoActivaXP) {
		this.lecturaTerrenoActivaXP = lecturaTerrenoActivaXP;
	}
}
