package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface DetalleCCA {

	public Long getAntiguedadAervicio();

	public void setAntiguedadAervicio(Long antiguedadAervicio);

	public Long getCuotaCapitalMora();

	public void setCuotaCapitalMora(Long cuotaCapitalMora);

	public Long getCuotasFacturadas();

	public void setCuotasFacturadas(Long cuotasFacturadas);

	public Long getCuotasFaltantes();

	public void setCuotasFaltantes(Long cuotasFaltantes);

	public Long getCuotasImpagas();

	public void setCuotasImpagas(Long cuotasImpagas);

	public Long getCuotasPactadas();

	public void setCuotasPactadas(Long cuotasPactadas);

	public Long getDeudaActual();

	public void setDeudaActual(Long deudaActual);

	public Long getDiasMora();

	public void setDiasMora(Long diasMora);

	public Date getFechaCompra();

	public void setFechaCompra(Date fechaCompra);

	public Long getFechaEstudioCredito();

	public void setFechaEstudioCredito(Long fechaEstudioCredito);

	public String getMarca();

	public void setMarca(String marca);

	public String getPeriodoPrimeraCuota();

	public void setPeriodoPrimeraCuota(String periodoPrimeraCuota);

	public String getPuntoVenta();

	public void setPuntoVenta(String puntoVenta);

	public Long getSaldoCapital();

	public void setSaldoCapital(Long saldoCapital);

	public Long getSaldoIntereses();

	public void setSaldoIntereses(Long saldoIntereses);

	public String getSocioNegocio();

	public void setSocioNegocio(String socioNegocio);

	public Double getTasaInteresActual();

	public void setTasaInteresActual(Double tasaInteresActual);

	public Double getTasaInteresPactada();

	public void setTasaInteresPactada(Double tasaInteresPactada);

	public String getTipoProducto();

	public void setTipoProducto(String tipoProducto);

	public Long getValorCompra();

	public void setValorCompra(Long valorCompra);

	public Long getValorCuotaCapital();

	public void setValorCuotaCapital(Long valorCuotaCapital);

	public Long getValorCuotaCapitalNoAfecto();

	public void setValorCuotaCapitalNoAfecto(Long valorCuotaCapitalNoAfecto);

	public Long getValorCuotaIntereses();

	public void setValorCuotaIntereses(Long valorCuotaIntereses);

	public Long getValorCuotaMensual();

	public void setValorCuotaMensual(Long valorCuotaMensual);

	public Long getValorCuotasInteresMora();

	public void setValorCuotasInteresMora(Long valorCuotasInteresMora);

	public Long getValorInteresesMoraCapital();

	public void setValorInteresesMoraCapital(Long valorInteresesMoraCapital);

	
	public Long getCicloFacturacion();

	public void setCicloFacturacion(Long cicloFacturacion);

	public String getNombreTitular();

	public void setNombreTitular(String nombreTitular);
	
	
}