package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DatosUltimaRefacturacion extends SynergiaBusinessObject {

	/**
	 * @return the aprobado
	 */
	public abstract String getAprobado();

	/**
	 * @param aprobado the aprobado to set
	 */
	public abstract void setAprobado(String aprobado);

	/**
	 * @return the fechaRefactura
	 */
	public abstract Date getFechaRefactura();

	/**
	 * @param fechaRefactura the fechaRefactura to set
	 */
	public abstract void setFechaRefactura(Date fechaRefactura);

	/**
	 * @return the nroCuenta
	 */
	public abstract Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public abstract void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroServicio
	 */
	public abstract Long getNroServicio();

	/**
	 * @param nroServicio the nroServicio to set
	 */
	public abstract void setNroServicio(Long nroServicio);

	/**
	 * @return the totalDocumento
	 */
	public abstract String getTotalDocumento();

	/**
	 * @param totalDocumento the totalDocumento to set
	 */
	public abstract void setTotalDocumento(String totalDocumento);

}