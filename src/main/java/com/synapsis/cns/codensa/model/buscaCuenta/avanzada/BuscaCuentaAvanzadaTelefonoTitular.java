package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaTelefonoTitular extends SynergiaBusinessObject {

	/**
	 * @return the telefonoTitular
	 */
	public String getTelefonoTitular();

	/**
	 * @param telefonoTitular the telefonoTitular to set
	 */
	public void setTelefonoTitular(String telefonoTitular);

}