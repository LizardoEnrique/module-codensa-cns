package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.CongDetalle;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class CongDetalleImpl extends SynergiaBusinessObjectImpl implements
		CongDetalle {

	private Long nroOperacion;

	private String nroOrden;
	
	private Long nroSaldo;

	private String clasifCongelacion;

	private String clasifDescongelacion;

	private String areaSolicitanteInformacion;

	private Long valorDisputado;

	private Date fechaPagoCongelacion;

	private Date fechaPagoDescongelacion;

	private String tipoCongelacion;

	private String tiempoCongelacion;
	

	/**
	 * @return the nroOperacion
	 */
	public Long getNroOperacion() {
		return nroOperacion;
	}

	/**
	 * @param nroOperacion the nroOperacion to set
	 */
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}

	/**
	 * @return the nroOrden
	 */
	public String getNroOrden() {
		return nroOrden;
	}

	/**
	 * @param nroOrden the nroOrden to set
	 */
	public void setNroOrden(String nroOrden) {
		this.nroOrden = nroOrden;
	}

	/**
	 * @return the clasifCongelacion
	 */
	public String getClasifCongelacion() {
		return clasifCongelacion;
	}

	/**
	 * @param clasifCongelacion the clasifCongelacion to set
	 */
	public void setClasifCongelacion(String clasifCongelacion) {
		this.clasifCongelacion = clasifCongelacion;
	}

	/**
	 * @return the clasifDescongelacion
	 */
	public String getClasifDescongelacion() {
		return clasifDescongelacion;
	}

	/**
	 * @param clasifDescongelacion the clasifDescongelacion to set
	 */
	public void setClasifDescongelacion(String clasifDescongelacion) {
		this.clasifDescongelacion = clasifDescongelacion;
	}

	/**
	 * @return the areaSolicitanteInformacion
	 */
	public String getAreaSolicitanteInformacion() {
		return areaSolicitanteInformacion;
	}

	/**
	 * @param areaSolicitanteInformacion the areaSolicitanteInformacion to set
	 */
	public void setAreaSolicitanteInformacion(String areaSolicitanteInformacion) {
		this.areaSolicitanteInformacion = areaSolicitanteInformacion;
	}

	/**
	 * @return the valorDisputado
	 */
	public Long getValorDisputado() {
		return valorDisputado;
	}

	/**
	 * @param valorDisputado the valorDisputado to set
	 */
	public void setValorDisputado(Long valorDisputado) {
		this.valorDisputado = valorDisputado;
	}

	/**
	 * @return the fechaPagoCongelacion
	 */
	public Date getFechaPagoCongelacion() {
		return fechaPagoCongelacion;
	}

	/**
	 * @param fechaPagoCongelacion the fechaPagoCongelacion to set
	 */
	public void setFechaPagoCongelacion(Date fechaPagoCongelacion) {
		this.fechaPagoCongelacion = fechaPagoCongelacion;
	}

	/**
	 * @return the fechaPagoDescongelacion
	 */
	public Date getFechaPagoDescongelacion() {
		return fechaPagoDescongelacion;
	}

	/**
	 * @param fechaPagoDescongelacion the fechaPagoDescongelacion to set
	 */
	public void setFechaPagoDescongelacion(Date fechaPagoDescongelacion) {
		this.fechaPagoDescongelacion = fechaPagoDescongelacion;
	}

	/**
	 * @return the tiempoCongelacionTransitoria
	 */
	public String getTipoCongelacion() {
		return tipoCongelacion;
	}

	/**
	 * @param tiempoCongelacionTransitoria the tiempoCongelacionTransitoria to set
	 */
	public void setTipoCongelacion(String tipoCongelacion) {
		this.tipoCongelacion = tipoCongelacion;
	}

	/**
	 * @return the tiempoCongelacionIndefinida
	 */
	public String getTiempoCongelacion() {
		return tiempoCongelacion;
	}

	/**
	 * @param tiempoCongelacionIndefinida the tiempoCongelacionIndefinida to set
	 */
	public void setTiempoCongelacion(String tiempoCongelacion) {
		this.tiempoCongelacion = tiempoCongelacion;
	}

	/**
	 * @return the nroSaldo
	 */
	public Long getNroSaldo() {
		return nroSaldo;
	}

	/**
	 * @param nroSaldo the nroSaldo to set
	 */
	public void setNroSaldo(Long nroSaldo) {
		this.nroSaldo = nroSaldo;
	}

}