package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaDatosServElect extends SynergiaBusinessObject {
	
	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Double getAntiguedadDeuda();

	public void setAntiguedadDeuda(Double antiguedadDeuda);

	public Double getUltimoValorConsumoKw();

	public void setUltimoValorConsumoKw(Double ultimoValorConsumoKw);

	public String getUltimoValorConsumoPesos();

	public void setUltimoValorConsumoPesos(String ultimoValorConsumoPesos);

	public Date getFechaFacturaUltimoConsumo();

	public void setFechaFacturaUltimoConsumo(Date fechaFacturaUltimoConsumo);

	public Double getActivoFp();

	public void setActivoFp(Double activoFp);

	public Double getActivoHp();

	public void setActivoHp(Double activoHp);

	public Double getReactivoFp();

	public void setReactivoFp(Double reactivoFp);

	public Double getReactivoHp();

	public void setReactivoHp(Double reactivoHp);

	public String getEstadoCobranza();

	public void setEstadoCobranza(String estadoCobranza);

	public Double getActivoXP();

	public void setActivoXP(Double activoXP);

	public Double getReactivoXP();

	public void setReactivoXP(Double reactivoXP);
}