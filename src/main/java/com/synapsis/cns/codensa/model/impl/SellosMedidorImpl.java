package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.SellosMedidor;

/**
 * 
 * @author dBraccio
 *29/11/2006
 */
public class SellosMedidorImpl extends SynergiaBusinessObjectImpl 
	implements SellosMedidor {
    // Fields    
	
    private Long idComponente;
    private Long nroSello;
    private String colorSello;
    private String materialSello;
    private String estadoSello;
    private Date fechaDescarga;
    
    //getter and setter
	public String getColorSello() {
		return colorSello;
	}
	public void setColorSello(String colorSello) {
		this.colorSello = colorSello;
	}
	public String getEstadoSello() {
		return estadoSello;
	}
	public void setEstadoSello(String estadoSello) {
		this.estadoSello = estadoSello;
	}
	public Long getIdComponente() {
		return idComponente;
	}
	public void setIdComponente(Long idComponente) {
		this.idComponente = idComponente;
	}
	public String getMaterialSello() {
		return materialSello;
	}
	public void setMaterialSello(String materialSello) {
		this.materialSello = materialSello;
	}
	public Long getNroSello() {
		return nroSello;
	}
	public void setNroSello(Long nroSello) {
		this.nroSello = nroSello;
	}
	public Date getFechaDescarga() {
		return fechaDescarga;
	}
	public void setFechaDescarga(Date fechaDescarga) {
		this.fechaDescarga = fechaDescarga;
	}
   
}
