package com.synapsis.cns.codensa.model.buscaCuenta;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaDirRural extends SynergiaBusinessObject {

	/**
	 * @return the direccion
	 */
	public String getDireccion();

	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion);

}