package com.synapsis.cns.codensa.model.buscaCuenta.avanzada;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface BuscaCuentaAvanzadaTipoNroOrden extends SynergiaBusinessObject {

	/**
	 * @return the nroOrden
	 */
	public String getNroOrden();

	/**
	 * @return the tipoOrden
	 */
	public String getTipoOrden();

	/**
	 * @param nroOrden the nroOrden to set
	 */
	public void setNroOrden(String nroOrden);

	/**
	 * @param tipoOrden the tipoOrden to set
	 */
	public void setTipoOrden(String tipoOrden);

}