package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaTotales extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);
	
	public String getDeudaTotalActual();

	public void setDeudaTotalActual(String deudaTotalActual);
		
	public String getSaldoTotalSujetoIntereses();

	public void setSaldoTotalSujetoIntereses(String saldoTotalSujetoIntereses);

	public String getSaldoNoSujetoIntereses();

	public void setSaldoNoSujetoIntereses(String saldoNoSujetoIntereses);

	public String getValorTotalIntereses();

	public void setValorTotalIntereses(String valorTotalIntereses);

	public String getValorTotalOtrosNegocios();

	public void setValorTotalOtrosNegocios(String valorTotalOtrosNegocios);

	public String getSaldoConvenios();

	public void setSaldoConvenios(String saldoConvenios);

	public String getSaldoCodensaHogar();

	public void setSaldoCodensaHogar(String saldoCodensaHogar);
	
	public String getValorTotalSanciones();
	
	public void setValorTotalSanciones(String valorTotalSanciones);
	
	public String getSaldoEncargoCobranza();
	
	public void setSaldoEncargoCobranza(String saldoEncargoCobranza);

}