/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaDetalleCongelaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaDetalleCongelacionesImpl extends
		SynergiaBusinessObjectImpl implements ConsultaDetalleCongelaciones {

	   private Long nroCuenta;                
	   private String codItemsCongDes;        
	   private String descripItemCongDes;     
	   private String codItemAclaracion;      
	   private String descItemAclaracion;     
	   private Double valorItemsCongDes;      
	   private Double valorAclaracion;        
	   private Long energiaConge;             
	   private Long energiaAclaracion;        
	   private Date perFacCong;               
	   private Date perFacAclaracion;         
	   private Double saldoAntes;             
	   private Long saldoDespues;             
	   private Long saldoEnergia;             
	   private Long saldoOtrosNegociosantes;  
	   private Long saldoOtrosNegociosdespues;
	   private Long conusmoCongdesc;          
	   private Long valorEnergiaCongdesc;     
	   private Long valorOtrosCongdes;        
	   private Long saldoEnergiaDespuesCong;  
	   private Long nroDocumento;
	   private Long numeroSaldo;
	   private Double valorFinal;
	   private Double valorInicial;
	   private Long consumoFinal; 
	   
	   public Long getConsumoInicial(){ 
		   long consfinal = this.getConsumoFinal()!=null?this.getConsumoFinal().longValue():0 ;
		   long consdisputado = this.getEnergiaConge()!=null?this.getEnergiaConge().longValue():0;
		   return new Long(consfinal+consdisputado );
	   }
	   
	public Long getConsumoFinal() {
		return consumoFinal;
	}
	public void setConsumoFinal(Long consumoFinal) {
		this.consumoFinal = consumoFinal;
	}
	public Double getValorInicial() {
		return valorInicial;
	}
	public void setValorInicial(Double valorInicial) {
		this.valorInicial = valorInicial;
	}
	public Double getValorFinal() {
		return valorFinal;
	}
	public void setValorFinal(Double valorFinal) {
		this.valorFinal = valorFinal;
	}
	public Long getNumeroSaldo() {
		return numeroSaldo;
	}
	public void setNumeroSaldo(Long numeroSaldo) {
		this.numeroSaldo = numeroSaldo;
	}
	/**
	 * @return the codItemAclaracion
	 */
	public String getCodItemAclaracion() {
		return codItemAclaracion;
	}
	/**
	 * @param codItemAclaracion the codItemAclaracion to set
	 */
	public void setCodItemAclaracion(String codItemAclaracion) {
		this.codItemAclaracion = codItemAclaracion;
	}
	/**
	 * @return the codItemsCongDes
	 */
	public String getCodItemsCongDes() {
		return codItemsCongDes;
	}
	/**
	 * @param codItemsCongDes the codItemsCongDes to set
	 */
	public void setCodItemsCongDes(String codItemsCongDes) {
		this.codItemsCongDes = codItemsCongDes;
	}
	/**
	 * @return the conusmoCongdesc
	 */
	public Long getConusmoCongdesc() {
		return conusmoCongdesc;
	}
	/**
	 * @param conusmoCongdesc the conusmoCongdesc to set
	 */
	public void setConusmoCongdesc(Long conusmoCongdesc) {
		this.conusmoCongdesc = conusmoCongdesc;
	}
	/**
	 * @return the descItemAclaracion
	 */
	public String getDescItemAclaracion() {
		return descItemAclaracion;
	}
	/**
	 * @param descItemAclaracion the descItemAclaracion to set
	 */
	public void setDescItemAclaracion(String descItemAclaracion) {
		this.descItemAclaracion = descItemAclaracion;
	}
	/**
	 * @return the descripItemCongDes
	 */
	public String getDescripItemCongDes() {
		return descripItemCongDes;
	}
	/**
	 * @param descripItemCongDes the descripItemCongDes to set
	 */
	public void setDescripItemCongDes(String descripItemCongDes) {
		this.descripItemCongDes = descripItemCongDes;
	}
	/**
	 * @return the energiaAclaracion
	 */
	public Long getEnergiaAclaracion() {
		return energiaAclaracion;
	}
	/**
	 * @param energiaAclaracion the energiaAclaracion to set
	 */
	public void setEnergiaAclaracion(Long energiaAclaracion) {
		this.energiaAclaracion = energiaAclaracion;
	}
	/**
	 * @return the energiaConge
	 */
	public Long getEnergiaConge() {
		return energiaConge;
	}
	/**
	 * @param energiaConge the energiaConge to set
	 */
	public void setEnergiaConge(Long energiaConge) {
		this.energiaConge = energiaConge;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento() {
		return nroDocumento;
	}
	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	/**
	 * @return the perFacAclaracion
	 */
	public Date getPerFacAclaracion() {
		return perFacAclaracion;
	}
	/**
	 * @param perFacAclaracion the perFacAclaracion to set
	 */
	public void setPerFacAclaracion(Date perFacAclaracion) {
		this.perFacAclaracion = perFacAclaracion;
	}
	/**
	 * @return the perFacCong
	 */
	public Date getPerFacCong() {
		return perFacCong;
	}
	/**
	 * @param perFacCong the perFacCong to set
	 */
	public void setPerFacCong(Date perFacCong) {
		this.perFacCong = perFacCong;
	}
	/**
	 * @return the saldoAntes
	 */
	public Double getSaldoAntes() {
		return saldoAntes;
	}
	/**
	 * @param saldoAntes the saldoAntes to set
	 */
	public void setSaldoAntes(Double saldoAntes) {
		this.saldoAntes = saldoAntes;
	}
	/**
	 * @return the saldoDespues
	 */
	public Long getSaldoDespues() {
		return saldoDespues;
	}
	/**
	 * @param saldoDespues the saldoDespues to set
	 */
	public void setSaldoDespues(Long saldoDespues) {
		this.saldoDespues = saldoDespues;
	}
	/**
	 * @return the saldoEnergia
	 */
	public Long getSaldoEnergia() {
		return saldoEnergia;
	}
	/**
	 * @param saldoEnergia the saldoEnergia to set
	 */
	public void setSaldoEnergia(Long saldoEnergia) {
		this.saldoEnergia = saldoEnergia;
	}
	/**
	 * @return the saldoEnergiaDespuesCong
	 */
	public Long getSaldoEnergiaDespuesCong() {
		return saldoEnergiaDespuesCong;
	}
	/**
	 * @param saldoEnergiaDespuesCong the saldoEnergiaDespuesCong to set
	 */
	public void setSaldoEnergiaDespuesCong(Long saldoEnergiaDespuesCong) {
		this.saldoEnergiaDespuesCong = saldoEnergiaDespuesCong;
	}
	/**
	 * @return the saldoOtrosNegociosantes
	 */
	public Long getSaldoOtrosNegociosantes() {
		return saldoOtrosNegociosantes;
	}
	/**
	 * @param saldoOtrosNegociosantes the saldoOtrosNegociosantes to set
	 */
	public void setSaldoOtrosNegociosantes(Long saldoOtrosNegociosantes) {
		this.saldoOtrosNegociosantes = saldoOtrosNegociosantes;
	}
	/**
	 * @return the saldoOtrosNegociosdespues
	 */
	public Long getSaldoOtrosNegociosdespues() {
		return saldoOtrosNegociosdespues;
	}
	/**
	 * @param saldoOtrosNegociosdespues the saldoOtrosNegociosdespues to set
	 */
	public void setSaldoOtrosNegociosdespues(Long saldoOtrosNegociosdespues) {
		this.saldoOtrosNegociosdespues = saldoOtrosNegociosdespues;
	}
	/**
	 * @return the valorAclaracion
	 */
	public Double getValorAclaracion() {
		return valorAclaracion;
	}
	/**
	 * @param valorAclaracion the valorAclaracion to set
	 */
	public void setValorAclaracion(Double valorAclaracion) {
		this.valorAclaracion = valorAclaracion;
	}
	/**
	 * @return the valorEnergiaCongdesc
	 */
	public Long getValorEnergiaCongdesc() {
		return valorEnergiaCongdesc;
	}
	/**
	 * @param valorEnergiaCongdesc the valorEnergiaCongdesc to set
	 */
	public void setValorEnergiaCongdesc(Long valorEnergiaCongdesc) {
		this.valorEnergiaCongdesc = valorEnergiaCongdesc;
	}
	/**
	 * @return the valorItemsCongDes
	 */
	public Double getValorItemsCongDes() {
		return valorItemsCongDes;
	}
	/**
	 * @param valorItemsCongDes the valorItemsCongDes to set
	 */
	public void setValorItemsCongDes(Double valorItemsCongDes) {
		this.valorItemsCongDes = valorItemsCongDes;
	}
	/**
	 * @return the valorOtrosCongdes
	 */
	public Long getValorOtrosCongdes() {
		return valorOtrosCongdes;
	}
	/**
	 * @param valorOtrosCongdes the valorOtrosCongdes to set
	 */
	public void setValorOtrosCongdes(Long valorOtrosCongdes) {
		this.valorOtrosCongdes = valorOtrosCongdes;
	}
	public Double getValorDisputado() {
		double congelado =  this.getValorItemsCongDes()!=null?this.getValorItemsCongDes().doubleValue():0 ;
		double aclaracion = this.getValorAclaracion()!=null?this.getValorAclaracion().doubleValue():0;
		return new Double(congelado+aclaracion );
	}             
	
}
