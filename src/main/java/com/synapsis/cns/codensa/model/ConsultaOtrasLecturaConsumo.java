/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaOtrasLecturaConsumo extends SynergiaBusinessObject{

	public Long getIdMedidor();
	public void setIdMedidor(Long idMedidor);
	public Double getConsumoFacturadoReactivaHP();
	public Double getConsumoFacturadoReactivaXP();
	public Double getConsumoRealReactivaHP();
	public Double getConsumoRealReactivaXP();
	public String getDescripcionAnomaliaLecturaFacturacion();
	public String getEstadoMedidor();
	public Date getFechaLecturaActual();
	public Date getFechaLecturaAnterior();
	public Long getIdCuenta();
	public Long getIdServicio();
	public Double getLecturaFacturaReactivaHP();
	public Double getLecturaFacturaReactivaXP();
	public Double getLecturaTerrenoReactivaHP();
	public Double getLecturaTerrenoReactivaXP();
	public Long getNroCuenta();
	public String getPeriodoFacturacion();
	public String getUbicacionMedidor();
	public Long getNroServicio();
	public String getCodigoAnomaliaLecturaFacturacion();
	public String getCodigoAnomaliaLecturaTerreno();
	public void setCodigoAnomaliaLecturaFacturacion(
			String codigoAnomaliaLecturaFacturacion);
	public void setCodigoAnomaliaLecturaTerreno(String codigoAnomaliaLecturaTerreno);
	public void setNroServicio(Long nroServicio);
	public void setConsumoFacturadoReactivaHP(Double consumoFacturadoReactivaHP);
	public void setConsumoFacturadoReactivaXP(Double consumoFacturadoReactivaXP);
	public void setConsumoRealReactivaHP(Double consumoRealReactivaHP);
	public void setConsumoRealReactivaXP(Double consumoRealReactivaXP);
	public void setDescripcionAnomaliaLecturaFacturacion(
			String descripcionAnomaliaLecturaFacturacion);
	public void setEstadoMedidor(String estadoMedidor);
	public void setFechaLecturaActual(Date fechaLecturaActual);
	public void setFechaLecturaAnterior(Date fechaLecturaAnterior);
	public void setIdCuenta(Long idCuenta);
	public void setIdServicio(Long idServicio);
	public void setLecturaFacturaReactivaHP(Double lecturaFacturaReactivaHP);
	public void setLecturaFacturaReactivaXP(Double lecturaFacturaReactivaXP);
	public void setLecturaTerrenoReactivaHP(Double lecturaTerrenoReactivaHP);
	public void setLecturaTerrenoReactivaXP(Double lecturaTerrenoReactivaXP);
	public void setNroCuenta(Long nroCuenta);
	public void setPeriodoFacturacion(String periodoFacturacion);
	public void setUbicacionMedidor(String ubicacionMedidor);
	public String getEvento();
	public void setEvento(String evento);
	public String getTipoLectura();
	public void setTipoLectura(String tipoLectura);
	public Double getConsumoFacturadoReactivaFP();
	public void setConsumoFacturadoReactivaFP(Double consumoFacturadoReactivaFP);
	public Double getConsumoRealReactivaFP();
	public void setConsumoRealReactivaFP(Double consumoRealReactivaFP);
	public Double getLecturaFacturaReactivaFP();
	public void setLecturaFacturaReactivaFP(Double lecturaFacturaReactivaFP);
	public Double getLecturaTerrenoReactivaFP();
	public void setLecturaTerrenoReactivaFP(Double lecturaTerrenoReactivaFP);
	
}
