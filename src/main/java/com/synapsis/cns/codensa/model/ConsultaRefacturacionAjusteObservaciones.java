package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaRefacturacionAjusteObservaciones extends
		SynergiaBusinessObject {
	public Long getIdCuenta();

	public void setIdCuenta(Long idCuenta);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public String getObservacionCom();

	public void setObservacionCom(String observacionCom);

	public String getObservacionTec();

	public void setObservacionTec(String observacionTec);

	public String getObservacionEli();

	public void setObservacionEli(String observacionEli);
}
