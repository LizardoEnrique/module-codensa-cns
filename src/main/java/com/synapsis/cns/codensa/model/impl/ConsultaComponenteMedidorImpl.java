/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaComponenteMedidor;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author AR18799631
 *
 */
public class ConsultaComponenteMedidorImpl extends SynergiaBusinessObjectImpl implements ConsultaComponenteMedidor {
	private Long nroCuenta;
	private String tipoServicio;
	private Long nroServicio;
	private Date fechaInstalacion;
	private String nroComponente;
	private String marca;
	private String fase;
	private Double factor;
	/**
	 * @return the factor
	 */
	public Double getFactor() {
		return factor;
	}
	/**
	 * @param factor the factor to set
	 */
	public void setFactor(Double factor) {
		this.factor = factor;
	}
	/**
	 * @return the fase
	 */
	public String getFase() {
		return fase;
	}
	/**
	 * @param fase the fase to set
	 */
	public void setFase(String fase) {
		this.fase = fase;
	}
	/**
	 * @return the fechaInstalacion
	 */
	public Date getFechaInstalacion() {
		return fechaInstalacion;
	}
	/**
	 * @param fechaInstalacion the fechaInstalacion to set
	 */
	public void setFechaInstalacion(Date fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the nroComponente
	 */
	public String getNroComponente() {
		return nroComponente;
	}
	/**
	 * @param nroComponente the nroComponente to set
	 */
	public void setNroComponente(String nroComponente) {
		this.nroComponente = nroComponente;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}
	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

}
