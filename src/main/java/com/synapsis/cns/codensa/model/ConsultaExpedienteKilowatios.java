package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaExpedienteKilowatios extends SynergiaBusinessObject {

	/**
	 * @return the promedio
	 */
	public abstract Double getPromedio();

	/**
	 * @param promedio the promedio to set
	 */
	public abstract void setPromedio(Double promedio);

	/**
	 * @return the reintegrar
	 */
	public abstract String getReintegrar();

	/**
	 * @param reintegrar the reintegrar to set
	 */
	public abstract void setReintegrar(String reintegrar);

	/**
	 * @return the total
	 */
	public abstract Double getTotal();

	/**
	 * @param total the total to set
	 */
	public abstract void setTotal(Double total);

}