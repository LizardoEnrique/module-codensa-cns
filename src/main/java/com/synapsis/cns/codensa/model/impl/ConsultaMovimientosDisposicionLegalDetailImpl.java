package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaMovimientosDisposicionLegalDetail;
import com.synapsis.synergia.core.model.Empresa;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaMovimientosDisposicionLegalDetailImpl extends SynergiaBusinessObjectImpl implements ConsultaMovimientosDisposicionLegalDetail {

	private Long id;
	private Empresa empresa;
	private String movimiento;
	private Long idMovRefa;
	private Long codigoItem;
	private String descItem;
	private Date fecPeriodo;
	private Double valorBeforeAjuste;
	private Double valorAfterAjuste;
	private Double valorAjuste;
	private Double consumoKW;
	
	public ConsultaMovimientosDisposicionLegalDetailImpl() {
		
	}
	
	public Long getCodigoItem() {
		return codigoItem;
	}
	public void setCodigoItem(Long codigoItem) {
		this.codigoItem = codigoItem;
	}
	public Double getConsumoKW() {
		return consumoKW;
	}
	public void setConsumoKW(Double consumoKW) {
		this.consumoKW = consumoKW;
	}
	public String getDescItem() {
		return descItem;
	}
	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Date getFecPeriodo() {
		return fecPeriodo;
	}
	public void setFecPeriodo(Date fecPeriodo) {
		this.fecPeriodo = fecPeriodo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Double getValorAfterAjuste() {
		return valorAfterAjuste;
	}
	public void setValorAfterAjuste(Double valorAfterAjuste) {
		this.valorAfterAjuste = valorAfterAjuste;
	}
	public Double getValorAjuste() {
		return valorAjuste;
	}
	public void setValorAjuste(Double valorAjuste) {
		this.valorAjuste = valorAjuste;
	}
	public Double getValorBeforeAjuste() {
		return valorBeforeAjuste;
	}
	public void setValorBeforeAjuste(Double valorBeforeAjuste) {
		this.valorBeforeAjuste = valorBeforeAjuste;
	}

	public Long getIdMovRefa() {
		return idMovRefa;
	}

	public void setIdMovRefa(Long idMovRefa) {
		this.idMovRefa = idMovRefa;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}
	
}
