package com.synapsis.cns.codensa.model.combo;

public interface ConsultaTipoIdentificacion {

	/**
	 * @return the codTipDocPersona
	 */
	public String getCodTipDocPersona();

	/**
	 * @param codTipDocPersona the codTipDocPersona to set
	 */
	public void setCodTipDocPersona(String codTipDocPersona);

	/**
	 * @return the desTipDocPersona
	 */
	public String getDesTipDocPersona();

	/**
	 * @param desTipDocPersona the desTipDocPersona to set
	 */
	public void setDesTipDocPersona(String desTipDocPersona);

}