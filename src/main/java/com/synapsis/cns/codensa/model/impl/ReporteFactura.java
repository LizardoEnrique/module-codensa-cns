package com.synapsis.cns.codensa.model.impl;


import java.util.Collection;
import java.util.Date;


/**
 * Reporte de Reimpresion de Facturas
 * @deprecated
 */
public class ReporteFactura {
	// Cup�n cliente
	// DESCRIPCION DEL CAMPO
	private Date fechaExpedicion;
	private String nit;
	private String direccionOficinaPrincipal;
	private Long numeroCuenta;
	private Long numeroServicio;
	private Date periodoFacturadoInicio;
	private Date periodoFacturadoFin;
	
	// CUENTA O CLIENTE
	private String nombreCliente;
	private Collection tiposServicioCliente;
	private String telefono;
	private String direccion;
	private String barrio;
	private String municipioCliente;
	
	// REPARTO ESPECIAL
	private String direccionRepartoEspecial;
	
	// INFORMACI�N T�CNICA
	private String tipoServicioInfoTecnica;
	private String rutaLectura;
	private String numeroMedidor1;
	private String numeroMedidor2;
	private String numeroCircuito;
	private String estratoSocioeconomico;
	private String cargaContratada;
	private Double nivelTension;
	
	// EVOLUCI�N  DEL CONSUMO (�ltimos 6 meses)
	private Collection ultimosPeriodosFacturados;
	private Collection consumoKwhPorPeriodo;
	private String periodoFacturadoActual;
	private Double consumoPeriodoActual;
	private String tipoLecturaPeridoActual;
	private Double consumoPromedioUltimosMeses;
	private String anomaliaLecturaPeriodoActual;
	
	// INFORMACI�N DEL CONSUMO
	private String mesTarifaParaLiquidacion;
	private Double valorPromedioKwh;
	private String tarifaParaFacturacionActual;
	private Double lecturaActual;
	private Double lecturaAnterior;
	private Double diferenciaEntreLecturaActualYAnterior;
	private Double factorLiquidacion;
	private Double energiaConsumida;
	private Double energiaFacturada;
	private Double totalConsumoKwh;
	
	// INFORMACI�N AL CLIENTE
	private String telefonosCliente;
	
	// DETALLES DE LA CUENTA
	// Colecciones de tipo DetalleCuenta
	private Collection cargos;
	private Collection otrosCargos;
	private Collection cargosDescuento;
	private Collection cargosPortafolio;

	private Long subtotalValorConsumo;
	private Double subtotalValorOtrosCargos;
	private Double subtotalValorCargosDescuento;
	private Double subtotalConceptoEnergia;
	private Double subtotalPortafolio;

	private Date fechaVencimientoPagoOportuno;
	private Date fechaVencimientoAvisoSuspension;
	
	private Double totalAPagar;
	
	// CALIDAD DEL SERVICIO
	private Integer maximoPermitidoInterrupciones;
	private Integer maximoPermitidoHoras;
	private Integer numeroInterrupciones;	
	private Integer numeroHoras;	
	private Double trimestre;
	
	// INFORMACI�N DE INTERES
	private Double generacion;
	private Double transmision;
	private Double perdidas;
	private Double distribucion;
	private Double otros;
	private Double comercializacion;
	private Double costoUnitario;
	
	private String codigoDeBarras;

	public String getAnomaliaLecturaPeriodoActual() {
		return anomaliaLecturaPeriodoActual;
	}

	public void setAnomaliaLecturaPeriodoActual(String anomaliaLecturaPeriodoActual) {
		this.anomaliaLecturaPeriodoActual = anomaliaLecturaPeriodoActual;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCargaContratada() {
		return cargaContratada;
	}

	public void setCargaContratada(String cargaContratada) {
		this.cargaContratada = cargaContratada;
	}

	public Collection getCargos() {
		return cargos;
	}

	public void setCargos(Collection cargos) {
		this.cargos = cargos;
	}

	public Collection getCargosDescuento() {
		return cargosDescuento;
	}

	public void setCargosDescuento(Collection cargosDescuento) {
		this.cargosDescuento = cargosDescuento;
	}

	public Collection getCargosPortafolio() {
		return cargosPortafolio;
	}

	public void setCargosPortafolio(Collection cargosPortafolio) {
		this.cargosPortafolio = cargosPortafolio;
	}

	public String getCodigoDeBarras() {
		return codigoDeBarras;
	}

	public void setCodigoDeBarras(String codigoDeBarras) {
		this.codigoDeBarras = codigoDeBarras;
	}

	public Double getComercializacion() {
		return comercializacion;
	}

	public void setComercializacion(Double comercializacion) {
		this.comercializacion = comercializacion;
	}

	public Collection getConsumoKwhPorPeriodo() {
		return consumoKwhPorPeriodo;
	}

	public void setConsumoKwhPorPeriodo(Collection consumoKwhPorPeriodo) {
		this.consumoKwhPorPeriodo = consumoKwhPorPeriodo;
	}

	public Double getConsumoPeriodoActual() {
		return consumoPeriodoActual;
	}

	public void setConsumoPeriodoActual(Double consumoPeriodoActual) {
		this.consumoPeriodoActual = consumoPeriodoActual;
	}

	public Double getConsumoPromedioUltimosMeses() {
		return consumoPromedioUltimosMeses;
	}

	public void setConsumoPromedioUltimosMeses(Double consumoPromedioUltimosMeses) {
		this.consumoPromedioUltimosMeses = consumoPromedioUltimosMeses;
	}

	public Double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(Double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public Double getDiferenciaEntreLecturaActualYAnterior() {
		return diferenciaEntreLecturaActualYAnterior;
	}

	public void setDiferenciaEntreLecturaActualYAnterior(
			Double diferenciaEntreLecturaActualYAnterior) {
		this.diferenciaEntreLecturaActualYAnterior = diferenciaEntreLecturaActualYAnterior;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDireccionOficinaPrincipal() {
		return direccionOficinaPrincipal;
	}

	public void setDireccionOficinaPrincipal(String direccionOficinaPrincipal) {
		this.direccionOficinaPrincipal = direccionOficinaPrincipal;
	}

	public String getDireccionRepartoEspecial() {
		return direccionRepartoEspecial;
	}

	public void setDireccionRepartoEspecial(String direccionRepartoEspecial) {
		this.direccionRepartoEspecial = direccionRepartoEspecial;
	}

	public Double getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Double distribucion) {
		this.distribucion = distribucion;
	}

	public Double getEnergiaConsumida() {
		return energiaConsumida;
	}

	public void setEnergiaConsumida(Double energiaConsumida) {
		this.energiaConsumida = energiaConsumida;
	}

	public Double getEnergiaFacturada() {
		return energiaFacturada;
	}

	public void setEnergiaFacturada(Double energiaFacturada) {
		this.energiaFacturada = energiaFacturada;
	}

	public String getEstratoSocioeconomico() {
		return estratoSocioeconomico;
	}

	public void setEstratoSocioeconomico(String estratoSocioeconomico) {
		this.estratoSocioeconomico = estratoSocioeconomico;
	}

	public Double getFactorLiquidacion() {
		return factorLiquidacion;
	}

	public void setFactorLiquidacion(Double factorLiquidacion) {
		this.factorLiquidacion = factorLiquidacion;
	}

	public Date getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(Date fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public Date getFechaVencimientoAvisoSuspension() {
		return fechaVencimientoAvisoSuspension;
	}

	public void setFechaVencimientoAvisoSuspension(
			Date fechaVencimientoAvisoSuspension) {
		this.fechaVencimientoAvisoSuspension = fechaVencimientoAvisoSuspension;
	}

	public Date getFechaVencimientoPagoOportuno() {
		return fechaVencimientoPagoOportuno;
	}

	public void setFechaVencimientoPagoOportuno(Date fechaVencimientoPagoOportuno) {
		this.fechaVencimientoPagoOportuno = fechaVencimientoPagoOportuno;
	}

	public Double getGeneracion() {
		return generacion;
	}

	public void setGeneracion(Double generacion) {
		this.generacion = generacion;
	}

	public Double getLecturaActual() {
		return lecturaActual;
	}

	public void setLecturaActual(Double lecturaActual) {
		this.lecturaActual = lecturaActual;
	}

	public Double getLecturaAnterior() {
		return lecturaAnterior;
	}

	public void setLecturaAnterior(Double lecturaAnterior) {
		this.lecturaAnterior = lecturaAnterior;
	}

	public Integer getMaximoPermitidoHoras() {
		return maximoPermitidoHoras;
	}

	public void setMaximoPermitidoHoras(Integer maximoPermitidoHoras) {
		this.maximoPermitidoHoras = maximoPermitidoHoras;
	}

	public Integer getMaximoPermitidoInterrupciones() {
		return maximoPermitidoInterrupciones;
	}

	public void setMaximoPermitidoInterrupciones(Integer maximoPermitidoInterrupciones) {
		this.maximoPermitidoInterrupciones = maximoPermitidoInterrupciones;
	}

	public String getMesTarifaParaLiquidacion() {
		return mesTarifaParaLiquidacion;
	}

	public void setMesTarifaParaLiquidacion(String mesTarifaParaLiquidacion) {
		this.mesTarifaParaLiquidacion = mesTarifaParaLiquidacion;
	}

	public String getMunicipioCliente() {
		return municipioCliente;
	}

	public void setMunicipioCliente(String municipioCliente) {
		this.municipioCliente = municipioCliente;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public Double getNivelTension() {
		return nivelTension;
	}

	public void setNivelTension(Double nivelTension) {
		this.nivelTension = nivelTension;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNumeroCircuito() {
		return numeroCircuito;
	}

	public void setNumeroCircuito(String numeroCircuito) {
		this.numeroCircuito = numeroCircuito;
	}

	public Long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(Long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public Integer getNumeroHoras() {
		return numeroHoras;
	}

	public void setNumeroHoras(Integer numeroHoras) {
		this.numeroHoras = numeroHoras;
	}

	public Integer getNumeroInterrupciones() {
		return numeroInterrupciones;
	}

	public void setNumeroInterrupciones(Integer numeroInterrupciones) {
		this.numeroInterrupciones = numeroInterrupciones;
	}

	public String getNumeroMedidor1() {
		return numeroMedidor1;
	}

	public void setNumeroMedidor1(String numeroMedidor1) {
		this.numeroMedidor1 = numeroMedidor1;
	}

	public String getNumeroMedidor2() {
		return numeroMedidor2;
	}

	public void setNumeroMedidor2(String numeroMedidor2) {
		this.numeroMedidor2 = numeroMedidor2;
	}

	public Long getNumeroServicio() {
		return numeroServicio;
	}

	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}

	public Double getOtros() {
		return otros;
	}

	public void setOtros(Double otros) {
		this.otros = otros;
	}

	public Collection getOtrosCargos() {
		return otrosCargos;
	}

	public void setOtrosCargos(Collection otrosCargos) {
		this.otrosCargos = otrosCargos;
	}

	public Double getPerdidas() {
		return perdidas;
	}

	public void setPerdidas(Double perdidas) {
		this.perdidas = perdidas;
	}

	public String getPeriodoFacturadoActual() {
		return periodoFacturadoActual;
	}

	public void setPeriodoFacturadoActual(String periodoFacturadoActual) {
		this.periodoFacturadoActual = periodoFacturadoActual;
	}

	public Date getPeriodoFacturadoFin() {
		return periodoFacturadoFin;
	}

	public void setPeriodoFacturadoFin(Date periodoFacturadoFin) {
		this.periodoFacturadoFin = periodoFacturadoFin;
	}

	public Date getPeriodoFacturadoInicio() {
		return periodoFacturadoInicio;
	}

	public void setPeriodoFacturadoInicio(Date periodoFacturadoInicio) {
		this.periodoFacturadoInicio = periodoFacturadoInicio;
	}

	public String getRutaLectura() {
		return rutaLectura;
	}

	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;
	}

	public Double getSubtotalConceptoEnergia() {
		return subtotalConceptoEnergia;
	}

	public void setSubtotalConceptoEnergia(Double subtotalConceptoEnergia) {
		this.subtotalConceptoEnergia = subtotalConceptoEnergia;
	}

	public Double getSubtotalPortafolio() {
		return subtotalPortafolio;
	}

	public void setSubtotalPortafolio(Double subtotalPortafolio) {
		this.subtotalPortafolio = subtotalPortafolio;
	}

	public Double getSubtotalValorCargosDescuento() {
		return subtotalValorCargosDescuento;
	}

	public void setSubtotalValorCargosDescuento(Double subtotalValorCargosDescuento) {
		this.subtotalValorCargosDescuento = subtotalValorCargosDescuento;
	}

	public Long getSubtotalValorConsumo() {
		return subtotalValorConsumo;
	}

	public void setSubtotalValorConsumo(Long subtotalValorConsumo) {
		this.subtotalValorConsumo = subtotalValorConsumo;
	}

	public Double getSubtotalValorOtrosCargos() {
		return subtotalValorOtrosCargos;
	}

	public void setSubtotalValorOtrosCargos(Double subtotalValorOtrosCargos) {
		this.subtotalValorOtrosCargos = subtotalValorOtrosCargos;
	}

	public String getTarifaParaFacturacionActual() {
		return tarifaParaFacturacionActual;
	}

	public void setTarifaParaFacturacionActual(String tarifaParaFacturacionActual) {
		this.tarifaParaFacturacionActual = tarifaParaFacturacionActual;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefonosCliente() {
		return telefonosCliente;
	}

	public void setTelefonosCliente(String telefonosCliente) {
		this.telefonosCliente = telefonosCliente;
	}

	public String getTipoLecturaPeridoActual() {
		return tipoLecturaPeridoActual;
	}

	public void setTipoLecturaPeridoActual(String tipoLecturaPeridoActual) {
		this.tipoLecturaPeridoActual = tipoLecturaPeridoActual;
	}

	public String getTipoServicioInfoTecnica() {
		return tipoServicioInfoTecnica;
	}

	public void setTipoServicioInfoTecnica(String tipoServicioInfoTecnica) {
		this.tipoServicioInfoTecnica = tipoServicioInfoTecnica;
	}

	public Collection getTiposServicioCliente() {
		return tiposServicioCliente;
	}

	public void setTiposServicioCliente(Collection tiposServicioCliente) {
		this.tiposServicioCliente = tiposServicioCliente;
	}

	public Double getTotalAPagar() {
		return totalAPagar;
	}

	public void setTotalAPagar(Double totalAPagar) {
		this.totalAPagar = totalAPagar;
	}

	public Double getTotalConsumoKwh() {
		return totalConsumoKwh;
	}

	public void setTotalConsumoKwh(Double totalConsumoKwh) {
		this.totalConsumoKwh = totalConsumoKwh;
	}

	public Double getTransmision() {
		return transmision;
	}

	public void setTransmision(Double transmision) {
		this.transmision = transmision;
	}

	public Double getTrimestre() {
		return trimestre;
	}

	public void setTrimestre(Double trimestre) {
		this.trimestre = trimestre;
	}

	public Collection getUltimosPeriodosFacturados() {
		return ultimosPeriodosFacturados;
	}

	public void setUltimosPeriodosFacturados(Collection ultimosPeriodosFacturados) {
		this.ultimosPeriodosFacturados = ultimosPeriodosFacturados;
	}

	public Double getValorPromedioKwh() {
		return valorPromedioKwh;
	}

	public void setValorPromedioKwh(Double valorPromedioKwh) {
		this.valorPromedioKwh = valorPromedioKwh;
	}
}
