package com.synapsis.cns.codensa.model.impl;

public interface ConsultaExpediente {

	/**
	 * @return the buzonExpediente
	 */
	public String getBuzonExpediente();

	/**
	 * @param buzonExpediente the buzonExpediente to set
	 */
	public void setBuzonExpediente(String buzonExpediente);

	/**
	 * @return the diasPermanencia
	 */
	public Double getDiasPermanencia();

	/**
	 * @param diasPermanencia the diasPermanencia to set
	 */
	public void setDiasPermanencia(Double diasPermanencia);

	/**
	 * @return the estadoExpediente
	 */
	public String getEstadoExpediente();

	/**
	 * @param estadoExpediente the estadoExpediente to set
	 */
	public void setEstadoExpediente(String estadoExpediente);

	/**
	 * @return the estadoLiquidacion
	 */
	public String getEstadoLiquidacion();

	/**
	 * @param estadoLiquidacion the estadoLiquidacion to set
	 */
	public void setEstadoLiquidacion(String estadoLiquidacion);

	/**
	 * @return the infoSancion
	 */
	public String getInfoSancion();

	/**
	 * @param infoSancion the infoSancion to set
	 */
	public void setInfoSancion(String infoSancion);

	/**
	 * @return the metodoLiquidacion
	 */
	public String getMetodoLiquidacion();

	/**
	 * @param metodoLiquidacion the metodoLiquidacion to set
	 */
	public void setMetodoLiquidacion(String metodoLiquidacion);

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta();

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta);

	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente();

	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente);

	/**
	 * @return the obsLiquidacion
	 */
	public String getObsLiquidacion();

	/**
	 * @param obsLiquidacion the obsLiquidacion to set
	 */
	public void setObsLiquidacion(String obsLiquidacion);

	/**
	 * @return the tipoExpediente
	 */
	public String getTipoExpediente();

	/**
	 * @param tipoExpediente the tipoExpediente to set
	 */
	public void setTipoExpediente(String tipoExpediente);

	/**
	 * @return the totalLiquidado
	 */
	public Double getTotalLiquidado();

	/**
	 * @param totalLiquidado the totalLiquidado to set
	 */
	public void setTotalLiquidado(Double totalLiquidado);

}