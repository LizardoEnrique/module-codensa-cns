package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface RefacUsuMovimientos extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNroAjuste();

	public void setNroAjuste(String nroAjuste);

	public String getObservaciones();

	public void setObservaciones(String observaciones);

	public String getMovimientos();

	public void setMovimientos(String movimientos);
	
	public Long getValorAjustado();

	public void setValorAjustado(Long valorAjustado);
	

}