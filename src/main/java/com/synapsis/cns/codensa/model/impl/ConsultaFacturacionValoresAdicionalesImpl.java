/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaFacturacionValoresAdicionales;

import java.util.Date;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionValoresAdicionalesImpl extends
	SynergiaBusinessObjectImpl implements ConsultaFacturacionValoresAdicionales {

	private String periodo;
	private Date fechaFacturacion;
	private Date fechaLectura;
	private String codigoCargo;
	private String descripcionCargo;
	private String valor;
	private Long nroCuenta;

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getDescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setDescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getFechaFacturacion()
	 */
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setFechaFacturacion(java.util.Date)
	 */
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getFechaLectura()
	 */
	public Date getFechaLectura() {
		return fechaLectura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setFechaLectura(java.util.Date)
	 */
	public void setFechaLectura(Date fechaLectura) {
		this.fechaLectura = fechaLectura;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getPeriodo()
	 */
	public String getPeriodo() {
		return periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setPeriodo(java.util.Date)
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#getValor()
	 */
	public String getValor() {
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaFacturacionValoresAdicionales#setValor(java.lang.Double)
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
}