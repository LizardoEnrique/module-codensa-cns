package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionAjusteObservaciones;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ConsultaRefacturacionAjusteObservacionesImpl extends
		SynergiaBusinessObjectImpl implements
		ConsultaRefacturacionAjusteObservaciones {
	private Long idCuenta;

	private Long nroCuenta;

	private String nroAjuste;

	private String observacionCom;

	private String observacionTec;

	private String observacionEli;

	public Long getIdCuenta() {
		return idCuenta;
	}

	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	public String getObservacionCom() {
		return observacionCom;
	}

	public void setObservacionCom(String observacionCom) {
		this.observacionCom = observacionCom;
	}

	public String getObservacionTec() {
		return observacionTec;
	}

	public void setObservacionTec(String observacionTec) {
		this.observacionTec = observacionTec;
	}

	public String getObservacionEli() {
		return observacionEli;
	}

	public void setObservacionEli(String observacionEli) {
		this.observacionEli = observacionEli;
	}

}