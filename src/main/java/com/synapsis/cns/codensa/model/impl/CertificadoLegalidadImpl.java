/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.CertificadoLegalidad;

/**
 * 
 * 
 * @author Paola Attadio
 * @version $Id: CertificadoLegalidadImpl.java,v 1.3 2007/02/06 22:30:57 ar29261698 Exp $
 */
public class CertificadoLegalidadImpl extends SynergiaBusinessObjectImpl 
		implements CertificadoLegalidad {

	
	private Long nroCuenta;
	private Long nroServicioElectrico;
	private String idSolicitante;
	private Date fechaSolicitudGarantia;
	private Date fechaGeneracionDocEmitido;
	private Long nroDocEmitido;
	private Date fechaImpresionDocEmitido;
	private String nroOrdenGarantia;

	/**
	 * @return the fechaGeneracionDocEmitido
	 */
	public Date getFechaGeneracionDocEmitido() {
		return this.fechaGeneracionDocEmitido;
	}
	/**
	 * @param fechaGeneracionDocEmitido the fechaGeneracionDocEmitido to set
	 */
	public void setFechaGeneracionDocEmitido(Date fechaGeneracionDocEmitido) {
		this.fechaGeneracionDocEmitido = fechaGeneracionDocEmitido;
	}
	/**
	 * @return the fechaImpresionDocEmitido
	 */
	public Date getFechaImpresionDocEmitido() {
		return this.fechaImpresionDocEmitido;
	}
	/**
	 * @param fechaImpresionDocEmitido the fechaImpresionDocEmitido to set
	 */
	public void setFechaImpresionDocEmitido(Date fechaImpresionDocEmitido) {
		this.fechaImpresionDocEmitido = fechaImpresionDocEmitido;
	}
	/**
	 * @return the fechaSolicitudGarantia
	 */
	public Date getFechaSolicitudGarantia() {
		return this.fechaSolicitudGarantia;
	}
	/**
	 * @param fechaSolicitudGarantia the fechaSolicitudGarantia to set
	 */
	public void setFechaSolicitudGarantia(Date fechaSolicitudGarantia) {
		this.fechaSolicitudGarantia = fechaSolicitudGarantia;
	}
	/**
	 * @return the getIdSolicitante
	 */
	public String getIdSolicitante() {
		return this.idSolicitante;
	}
	/**
	 * @param getIdSolicitante the getIdSolicitante to set
	 */
	public void setIdSolicitante(String idSolicitante) {
		this.idSolicitante = idSolicitante;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return this.nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the nroDocEmitido
	 */
	public Long getNroDocEmitido() {
		return this.nroDocEmitido;
	}
	/**
	 * @param nroDocEmitido the nroDocEmitido to set
	 */
	public void setNroDocEmitido(Long nroDocEmitido) {
		this.nroDocEmitido = nroDocEmitido;
	}
	/**
	 * @return the nroOrdenGarantia
	 */
	public String getNroOrdenGarantia() {
		return this.nroOrdenGarantia;
	}
	/**
	 * @param nroOrdenGarantia the nroOrdenGarantia to set
	 */
	public void setNroOrdenGarantia(String nroOrdenGarantia) {
		this.nroOrdenGarantia = nroOrdenGarantia;
	}
	/**
	 * @return the nroServicioElectrico
	 */
	public Long getNroServicioElectrico() {
		return this.nroServicioElectrico;
	}
	/**
	 * @param nroServicioElectrico the nroServicioElectrico to set
	 */
	public void setNroServicioElectrico(Long nroServicioElectrico) {
		this.nroServicioElectrico = nroServicioElectrico;
	}

}
