package com.synapsis.cns.codensa.model;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface DeudaCargosNoSujetoIntereses extends SynergiaBusinessObject {

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public Long getIdItemDoc();

	public void setIdItemDoc(Long idItemDoc);

	public String getCodCargo();

	public void setCodCargo(String codCargo);

	public String getDescCargo();

	public void setDescCargo(String descCargo);

	public Double getValorCargo();

	public void setValorCargo(Double valorCargo);

	public Double getSaldoCargo();

	public void setSaldoCargo(Double saldoCargo);

	public String getCorrelativoFacturacion();

	public void setCorrelativoFacturacion(String correlativoFacturacion);

	public Date getFecFactura();

	public void setFecFactura(Date fecFactura);

	public Date getPeriodoFacturacion();

	public void setPeriodoFacturacion(Date periodoFacturacion);

	public Long getNroDocumento();

	public void setNroDocumento(Long nroDocumento);

}