package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ConsultaRefacturacionCongelacionSaldo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 * 
 * CU 016
 *
 */
public class ConsultaRefacturacionCongelacionSaldoImpl extends
	SynergiaBusinessObjectImpl implements ConsultaRefacturacionCongelacionSaldo {

	private Long idCuenta;
	private Long nroCuenta;
	private Long idOperacion;
	private Long nroOperacion;

	private Double saldoTotalAntes;
	private Double saldoEnergiaAntes;
	private Double saldoOtrosNegociosAntes;
	private Double saldoEnergiaDespues;
	private Double saldoOtrosNegociosDespues;
	private Double saldoTotalDespues;
	private Double valorEnergia;
	private Double valorOtrosNegocios;
	
	public Long getIdCuenta() {
		return idCuenta;
	}
	public Long getIdOperacion() {
		return idOperacion;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOperacion() {
		return nroOperacion;
	}
	public Double getSaldoEnergiaAntes() {
		return saldoEnergiaAntes;
	}
	public Double getSaldoEnergiaDespues() {
		return saldoEnergiaDespues;
	}
	public Double getSaldoOtrosNegociosAntes() {
		return saldoOtrosNegociosAntes;
	}
	public Double getSaldoOtrosNegociosDespues() {
		return saldoOtrosNegociosDespues;
	}
	public Double getSaldoTotalAntes() {
		return saldoTotalAntes;
	}
	public Double getSaldoTotalDespues() {
		return saldoTotalDespues;
	}
	public Double getValorEnergia() {
		return valorEnergia;
	}
	public Double getValorOtrosNegocios() {
		return valorOtrosNegocios;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdOperacion(Long idOperacion) {
		this.idOperacion = idOperacion;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public void setSaldoEnergiaAntes(Double saldoEnergiaAntes) {
		this.saldoEnergiaAntes = saldoEnergiaAntes;
	}
	public void setSaldoEnergiaDespues(Double saldoEnergiaDespues) {
		this.saldoEnergiaDespues = saldoEnergiaDespues;
	}
	public void setSaldoOtrosNegociosAntes(Double saldoOtrosNegociosAntes) {
		this.saldoOtrosNegociosAntes = saldoOtrosNegociosAntes;
	}
	public void setSaldoOtrosNegociosDespues(Double saldoOtrosNegociosDespues) {
		this.saldoOtrosNegociosDespues = saldoOtrosNegociosDespues;
	}
	public void setSaldoTotalAntes(Double saldoTotalAntes) {
		this.saldoTotalAntes = saldoTotalAntes;
	}
	public void setSaldoTotalDespues(Double saldoTotalDespues) {
		this.saldoTotalDespues = saldoTotalDespues;
	}
	public void setValorEnergia(Double valorEnergia) {
		this.valorEnergia = valorEnergia;
	}
	public void setValorOtrosNegocios(Double valorOtrosNegocios) {
		this.valorOtrosNegocios = valorOtrosNegocios;
	}
	
	
	
}
