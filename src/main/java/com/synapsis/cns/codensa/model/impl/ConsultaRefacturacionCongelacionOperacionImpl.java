/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author dbraccio
 *
 *CU 016
 */
public class ConsultaRefacturacionCongelacionOperacionImpl extends
		SynergiaBusinessObjectImpl {

	private Long idCuenta;
	private Long nroCuenta;
	private Long idOperacion;
	private Long nroOperacion;
	private String codigoItem;
	private String descripcionItem;
	private Date fechaFacturacion;
	private Double valorItem;
	private Double consumoKW;
	private Double energiaKW;
	private Double valorItemAclaracion;
	private Double energiaKWAclaracion;
	private Double valorItemAntesYDespues;
	private String identificacionItem;
	private String observaciones;
	
	public String getCodigoItem() {
		return codigoItem;
	}
	public Double getConsumoKW() {
		return consumoKW;
	}
	public String getDescripcionItem() {
		return descripcionItem;
	}
	public Double getEnergiaKW() {
		return energiaKW;
	}
	public Double getEnergiaKWAclaracion() {
		return energiaKWAclaracion;
	}
	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}
	public Long getIdCuenta() {
		return idCuenta;
	}
	public String getIdentificacionItem() {
		return identificacionItem;
	}
	public Long getIdOperacion() {
		return idOperacion;
	}
	public Long getNroCuenta() {
		return nroCuenta;
	}
	public Long getNroOperacion() {
		return nroOperacion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public Double getValorItem() {
		return valorItem;
	}
	public Double getValorItemAclaracion() {
		return valorItemAclaracion;
	}
	public Double getValorItemAntesYDespues() {
		return valorItemAntesYDespues;
	}
	public void setCodigoItem(String codigoItem) {
		this.codigoItem = codigoItem;
	}
	public void setConsumoKW(Double consumoKW) {
		this.consumoKW = consumoKW;
	}
	public void setDescripcionItem(String descripcionItem) {
		this.descripcionItem = descripcionItem;
	}
	public void setEnergiaKW(Double energiaKW) {
		this.energiaKW = energiaKW;
	}
	public void setEnergiaKWAclaracion(Double energiaKWAclaracion) {
		this.energiaKWAclaracion = energiaKWAclaracion;
	}
	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}
	public void setIdCuenta(Long idCuenta) {
		this.idCuenta = idCuenta;
	}
	public void setIdentificacionItem(String identificacionItem) {
		this.identificacionItem = identificacionItem;
	}
	public void setIdOperacion(Long idOperacion) {
		this.idOperacion = idOperacion;
	}
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public void setNroOperacion(Long nroOperacion) {
		this.nroOperacion = nroOperacion;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public void setValorItem(Double valorItem) {
		this.valorItem = valorItem;
	}
	public void setValorItemAclaracion(Double valorItemAclaracion) {
		this.valorItemAclaracion = valorItemAclaracion;
	}
	public void setValorItemAntesYDespues(Double valorItemAntesYDespues) {
		this.valorItemAntesYDespues = valorItemAntesYDespues;
	}
	
	
	
	

}
