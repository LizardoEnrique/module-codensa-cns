/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteAgendaEjecutivo;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteAgendaEjecutivoImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteAgendaEjecutivo {
	private Long numeroExpediente;
	private Date fechaAgendaOtorgada;	
	private String lugarCitacion;	
	private Date fechaHoraCita;	
	private String nombreEjecutivo;	
	private String resultadoCita;	
	private String observacionesEjecutivo;
	public Long getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(Long numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getFechaAgendaOtorgada()
	 */
	public Date getFechaAgendaOtorgada() {
		return fechaAgendaOtorgada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setFechaAgendaOtorgada(java.util.Date)
	 */
	public void setFechaAgendaOtorgada(Date fechaAgendaOtorgada) {
		this.fechaAgendaOtorgada = fechaAgendaOtorgada;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getFechaHoraCita()
	 */
	public Date getFechaHoraCita() {
		return fechaHoraCita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setFechaHoraCita(java.util.Date)
	 */
	public void setFechaHoraCita(Date fechaHoraCita) {
		this.fechaHoraCita = fechaHoraCita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getLugarCitacion()
	 */
	public String getLugarCitacion() {
		return lugarCitacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setLugarCitacion(java.lang.String)
	 */
	public void setLugarCitacion(String lugarCitacion) {
		this.lugarCitacion = lugarCitacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getNombreEjecutivo()
	 */
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setNombreEjecutivo(java.lang.String)
	 */
	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getObservacionesEjecutivo()
	 */
	public String getObservacionesEjecutivo() {
		return observacionesEjecutivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setObservacionesEjecutivo(java.lang.String)
	 */
	public void setObservacionesEjecutivo(String observacionesEjecutivo) {
		this.observacionesEjecutivo = observacionesEjecutivo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#getResultadoCita()
	 */
	public String getResultadoCita() {
		return resultadoCita;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteAgendaEjecutivo#setResultadoCita(java.lang.String)
	 */
	public void setResultadoCita(String resultadoCita) {
		this.resultadoCita = resultadoCita;
	}

}
