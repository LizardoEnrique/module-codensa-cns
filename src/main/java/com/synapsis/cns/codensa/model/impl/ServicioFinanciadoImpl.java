package com.synapsis.cns.codensa.model.impl;

import com.synapsis.cns.codensa.model.ServicioFinanciado;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class ServicioFinanciadoImpl extends SynergiaBusinessObjectImpl implements ServicioFinanciado {

	private Long idConvenio;
	private String tipoServicio;
	private Long numeroServicio;
	private Long nroConvenio;
	
	public Long getNroConvenio() {
		return nroConvenio;
	}
	public void setNroConvenio(Long nroConvenio) {
		this.nroConvenio = nroConvenio;
	}
	public Long getIdConvenio() {
		return idConvenio;
	}
	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public Long getNumeroServicio() {
		return numeroServicio;
	}
	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}
	
	
	
	
}
