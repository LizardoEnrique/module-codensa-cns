package com.synapsis.cns.codensa.model;

import java.util.Date;

public interface ConsultaMensajesUsuario {

	/**
	 * @return the fecRegistro
	 */
	public Date getFecRegistro();

	/**
	 * @param fecRegistro the fecRegistro to set
	 */
	public void setFecRegistro(Date fecRegistro);

	/**
	 * @return the fecVig_desde
	 */
	public Date getFecVig_desde();

	/**
	 * @param fecVig_desde the fecVig_desde to set
	 */
	public void setFecVig_desde(Date fecVig_desde);

	/**
	 * @return the fecVig_hasta
	 */
	public Date getFecVig_hasta();

	/**
	 * @param fecVig_hasta the fecVig_hasta to set
	 */
	public void setFecVig_hasta(Date fecVig_hasta);

	/**
	 * @return the idArea
	 */
	public Long getIdArea();

	/**
	 * @param idArea the idArea to set
	 */
	public void setIdArea(Long idArea);


	/**
	 * @return the idOficina
	 */
	public Long getIdOficina();

	/**
	 * @param idOficina the idOficina to set
	 */
	public void setIdOficina(Long idOficina);

	/**
	 * @return the idUsuario
	 */
	public Long getIdUsuario();

	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(Long idUsuario);

	/**
	 * @return the mensaje
	 */
	public String getMensaje();

	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje);
	/**
	 * @return the userName
	 */
	public String getUserName();
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName);

}