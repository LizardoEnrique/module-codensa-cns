package com.synapsis.cns.codensa.model;

/**
 * @author adambrosio
 */
public interface InformacionConsumos {

	public String getAnomaliaLecturaPeriodoActual();

	public void setAnomaliaLecturaPeriodoActual(String anomaliaLecturaPeriodoActual);

	public Double getComercializacion();

	public void setComercializacion(Double comercializacion);

	public Double getConsumoPromedioUltimosMeses();

	public void setConsumoPromedioUltimosMeses(Double consumoPromedioUltimosMeses);

	public Double getCostoUnitario();

	public void setCostoUnitario(Double costoUnitario);

	public Double getDiferenciaEntreLecturaActualYAnterior();

	public void setDiferenciaEntreLecturaActualYAnterior(Double diferenciaEntreLecturaActualYAnterior);

	public Double getDistribucion();

	public void setDistribucion(Double distribucion);

	public Double getEnergiaConsumida();

	public void setEnergiaConsumida(Double energiaConsumida);

	public Double getEnergiaFacturada();

	public void setEnergiaFacturada(Double energiaFacturada);

	public Double getFactorLiquidacion();

	public void setFactorLiquidacion(Double factorLiquidacion);

	public Double getGeneracion();

	public void setGeneracion(Double generacion);

	public Double getLecturaActual();

	public void setLecturaActual(Double lecturaActual);

	public Double getLecturaAnterior();

	public void setLecturaAnterior(Double lecturaAnterior);

	public Integer getMaximoPermitidoHoras();

	public void setMaximoPermitidoHoras(Integer maximoPermitidoHoras);

	public Integer getMaximoPermitidoInterrupciones();

	public void setMaximoPermitidoInterrupciones(Integer maximoPermitidoInterrupciones);

	public String getMesTarifaParaLiquidacion();

	public void setMesTarifaParaLiquidacion(String mesTarifaParaLiquidacion);

	public Long getNroFactura();

	public void setNroFactura(Long nroFactura);

	public Integer getNumeroHorasInterrupcionesAcumuladas();

	public void setNumeroHorasInterrupcionesAcumuladas(Integer numeroHorasInterrupcionesAcumuladas);

	public Integer getNumeroInterrupcionesAcumuladas();

	public void setNumeroInterrupcionesAcumuladas(Integer numeroInterrupcionesAcumuladas);

	public Double getOtros();

	public void setOtros(Double otros);

	public Double getPerdidas();

	public void setPerdidas(Double perdidas);

	public Long getPeriodoInterrupciones();

	public void setPeriodoInterrupciones(Long periodoInterrupciones);

	public String getTarifaParaFacturacionActual();

	public void setTarifaParaFacturacionActual(String tarifaParaFacturacionActual);

	public String getTipoLecturaPeriodoActual();

	public void setTipoLecturaPeriodoActual(String tipoLecturaPeriodoActual);

	public Double getTotalConsumoKwh();

	public void setTotalConsumoKwh(Double totalConsumoKwh);

	public Double getTransmision();

	public void setTransmision(Double transmision);

	public Double getTrimestreInterrupciones();

	public void setTrimestreInterrupciones(Double trimestreInterrupciones);

	public Double getValorPromedioKwh();

	public void setValorPromedioKwh(Double valorPromedioKwh);

	public Long getIdFactura();

	public void setIdFactura(Long idFactura);

	public Double getCostoFijo();

	public void setCostoFijo(Double costoFijo);

	public Double getOpcionTarifaria();

	public void setOpcionTarifaria(Double opcionTarifaria);

}