package com.synapsis.cns.codensa.model;

import java.util.Date;

/**
 * @author jhv - 20/03/2007
 *
 */
public interface ReporteCertificaConvenio {

	public Integer getDiaExpedicion();

	public String getMesExpedicion();

	public Integer getAnioExpedicion();

	public String getBarrio();

	public void setBarrio(String barrio);

	public String getDireccion();

	public void setDireccion(String direccion);

	public String getDireccionRepartoEspecial();

	public void setDireccionRepartoEspecial(String direccionRepartoEspecial);

	public void setFechaExpedicion(Date fechaExpedicion);

	public String getLocalizacion();

	public void setLocalizacion(String localizacion);

	public String getMunicipio();

	public void setMunicipio(String municipio);

	public String getNombreSolicitante();

	public void setNombreSolicitante(String nombreSolicitante);

	public String getNombreTitularCuenta();

	public void setNombreTitularCuenta(String nombreTitularCuenta);

	public Long getNroCuenta();

	public void setNroCuenta(Long nroCuenta);

	public String getNumeroIdentificacion();

	public void setNumeroIdentificacion(String numeroIdentificacion);

	public String getTelefono();

	public void setTelefono(String telefono);

	public String getTipoIdentificacion();

	public void setTipoIdentificacion(String tipoIdentificacion);

	public String getUsuarioSistema();

	public void setUsuarioSistema(String usuarioSistema);

	public String getApellidoTitularCuenta();

	public void setApellidoTitularCuenta(String apellidoTitularCuenta);

}