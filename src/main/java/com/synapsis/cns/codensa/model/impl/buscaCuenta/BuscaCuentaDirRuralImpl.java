/**
 * 
 */
package com.synapsis.cns.codensa.model.impl.buscaCuenta;

import com.synapsis.cns.codensa.model.buscaCuenta.BuscaCuentaDirRural;

/**
 * @author Emiliano Arango (ar30557486)
 *
 */
public class BuscaCuentaDirRuralImpl extends BuscaCuentaImpl implements BuscaCuentaDirRural {
	private String direccion;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirRural#getDireccion()
	 */
	public String getDireccion() {
		return direccion;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaDirRural#setDireccion(java.lang.String)
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
}
