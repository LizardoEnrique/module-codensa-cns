/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaHistoriaEstados;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaHistoriaEstadosImpl extends SynergiaBusinessObjectImpl implements ConsultaHistoriaEstados {
	   private String estado;              
	   private String usuario;             
	   private Date fechaModif;
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the fechaModif
	 */
	public Date getFechaModif() {
		return fechaModif;
	}
	/**
	 * @param fechaModif the fechaModif to set
	 */
	public void setFechaModif(Date fechaModif) {
		this.fechaModif = fechaModif;
	}
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}          

}
