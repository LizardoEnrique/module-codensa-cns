package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public interface ConsultaCuentaDatosFacDetalle extends SynergiaBusinessObject{
	
	/**
	 * @return the idConfigFact
	 */
	public Long getIdConfigFact();
	/**
	 * @param idConfigFact the idConfigFact to set
	 */
	public void setIdConfigFact(Long idConfigFact);
	
	/**
	 * @return the indicadorConsumo
	 */
	public Long getIndicadorConsumo() ;
	/**
	 * @param indicadorConsumo the indicadorConsumo to set
	 */
	public void setIndicadorConsumo(Long indicadorConsumo);
	
	/**
	 * @return the fechaEmicion
	 */
	public Date getFechaEmicion();

	/**
	 * @param fechaEmicion the fechaEmicion to set
	 */
	public void setFechaEmicion(Date fechaEmicion);

	/**
	 * @return the fechaPrimerVto
	 */
	public Date getFechaPrimerVto();

	/**
	 * @param fechaPrimerVto the fechaPrimerVto to set
	 */
	public void setFechaPrimerVto(Date fechaPrimerVto);

	/**
	 * @return the fechaSegundoVto
	 */
	public Date getFechaSegundoVto();

	/**
	 * @param fechaSegundoVto the fechaSegundoVto to set
	 */
	public void setFechaSegundoVto(Date fechaSegundoVto);

	/**
	 * @return the idCuenta
	 */
	public Long getIdCuenta();

	/**
	 * @param idCuenta the idCuenta to set
	 */
	public void setIdCuenta(Long idCuenta);

	/**
	 * @return the idDetalleCuenta
	 */
	public Long getIdDetalleCuenta();

	/**
	 * @param idDetalleCuenta the idDetalleCuenta to set
	 */
	public void setIdDetalleCuenta(Long idDetalleCuenta);

	/**
	 * @return the periodoFacturacion
	 */
	public Date getPeriodoFacturacion();

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(Date periodoFacturacion);

	/**
	 * @return the totalPagar
	 */
	public Long getTotalPagar();

	/**
	 * @param totalPagar the totalPagar to set
	 */
	public void setTotalPagar(Long totalPagar);
	
	
	public String getCodFactura();
	
	public void setCodFactura(String codFactura);
	
	public String getMessFactura();
	
	public void setMessFactura(String messFactura);
	
	public Long getNroCuenta();
	
	public void setNroCuenta(Long nroCuenta);
}