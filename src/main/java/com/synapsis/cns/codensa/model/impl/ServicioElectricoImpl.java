/**
 * $Id: ServicioElectricoImpl.java,v 1.4 2008/07/01 20:42:21 ar29261698 Exp $
 */
package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ServicioElectrico;

public class ServicioElectricoImpl extends SynergiaBusinessObjectImpl 
	implements ServicioElectrico{
    
	private static final long serialVersionUID = 1L;
		
    private Long nroCuenta;
	private Long numeroServicio;
	private Long idServicio;
    private String direccion;
    private String municipio;
    private String barrio;
    private String idState;
    private String manzana;
    private String rutaLectura;
    private String rutaReparto;
    private String tipoCliente;
    private String codigoTarifa;
    private String descripcionTarifa;
    private String estratoSocial;
    private String contadorGradual;
    private String localizacion;
    private String estadoServicioElectrico;
    private String nombreBeneficiario;
    private String tipoDocumentoBeneficiario;
    private String numeroDocumentoBeneficiario;
    private String telefonoBeneficiario;
    private String mailBeneficiario;
    private String acreditacionBeneficiario;
    private String estadoCobranza;
    private String ejecutivoCuenta;
    private String chip;
    

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
    
	public String getAcreditacionBeneficiario() {
		return acreditacionBeneficiario;
	}
	
	public void setAcreditacionBeneficiario(String acreditacionBeneficiario) {
		this.acreditacionBeneficiario = acreditacionBeneficiario;
	}
	
	public String getBarrio() {
		return barrio;
	}
	
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
	
	public String getCodigoTarifa() {
		return codigoTarifa;
	}
	
	public void setCodigoTarifa(String codigoTarifa) {
		this.codigoTarifa = codigoTarifa;
	}
	
	public String getContadorGradual() {
		return contadorGradual;
	}
	
	public void setContadorGradual(String contadorGradual) {
		this.contadorGradual = contadorGradual;
	}
	
	public String getDescripcionTarifa() {
		return descripcionTarifa;
	}
	
	public void setDescripcionTarifa(String descripcionTarifa) {
		this.descripcionTarifa = descripcionTarifa;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getEstadoServicioElectrico() {
		return estadoServicioElectrico;
	}
	
	public void setEstadoServicioElectrico(String estadoServicioElectrico) {
		this.estadoServicioElectrico = estadoServicioElectrico;
	}
	
	public String getEstratoSocial() {
		return estratoSocial;
	}
	
	public void setEstratoSocial(String estratoSocial) {
		this.estratoSocial = estratoSocial;
	}
	
	public String getIdState() {
		return idState;
	}
	
	public void setIdState(String idState) {
		this.idState = idState;
	}
	
	public String getLocalizacion() {
		return localizacion;
	}
	
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	
	public String getMailBeneficiario() {
		return mailBeneficiario;
	}
	
	public void setMailBeneficiario(String mailBeneficiario) {
		this.mailBeneficiario = mailBeneficiario;
	}
	
	public String getManzana() {
		return manzana;
	}
	
	public void setManzana(String manzana) {
		this.manzana = manzana;
	}
	
	public String getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	
	public String getNumeroDocumentoBeneficiario() {
		return numeroDocumentoBeneficiario;
	}
	
	public void setNumeroDocumentoBeneficiario(String numeroDocumentoBeneficiario) {
		this.numeroDocumentoBeneficiario = numeroDocumentoBeneficiario;
	}
	
	public String getRutaLectura() {
		return rutaLectura;
	}
	
	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;
	}
	
	public String getRutaReparto() {
		return rutaReparto;
	}
	
	public void setRutaReparto(String rutaReparto) {
		this.rutaReparto = rutaReparto;
	}
	
	public String getTelefonoBeneficiario() {
		return telefonoBeneficiario;
	}
	
	public void setTelefonoBeneficiario(String telefonoBeneficiario) {
		this.telefonoBeneficiario = telefonoBeneficiario;
	}
	
	public String getTipoCliente() {
		return tipoCliente;
	}
	
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	
	public String getTipoDocumentoBeneficiario() {
		return tipoDocumentoBeneficiario;
	}
	
	public void setTipoDocumentoBeneficiario(String tipoDocumentoBeneficiario) {
		this.tipoDocumentoBeneficiario = tipoDocumentoBeneficiario;
	}

	public Long getNumeroServicio() {
		return numeroServicio;
	}

	public void setNumeroServicio(Long numeroServicio) {
		this.numeroServicio = numeroServicio;
	}
	
	public String getEjecutivoCuenta() {
		return ejecutivoCuenta;
	}
	
	public void setEjecutivoCuenta(String ejecutivoCuenta) {
		this.ejecutivoCuenta = ejecutivoCuenta;
	}
	
	public String getEstadoCobranza() {
		return estadoCobranza;
	}
	
	public void setEstadoCobranza(String estadoCobranza) {
		this.estadoCobranza = estadoCobranza;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	/**
	 * @return the idServicio
	 */
	public Long getIdServicio() {
		return idServicio;
	}

	/**
	 * @param idServicio the idServicio to set
	 */
	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}
}
