package com.synapsis.cns.codensa.model.impl;

/**
 * @author jhv
 */

import java.util.Date;

import com.synapsis.cns.codensa.model.RefacDetalleAjuste;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

public class RefacDetalleAjusteImpl extends SynergiaBusinessObjectImpl
		implements RefacDetalleAjuste {

	private Long nroCuenta;

	private String nroAjuste;

	private String clasificacionAjuste;

	private String areaSolicitanteAjuste;

	private String areaResponsableAjuste;

	private String submotivoAjuste;

	private Double tiempoDesdeCreacionAjuste;
	private Date fechaCreacionAjuste;

	public Long getNroCuenta() {
		return nroCuenta;
	}

	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	public String getNroAjuste() {
		return nroAjuste;
	}

	public void setNroAjuste(String nroAjuste) {
		this.nroAjuste = nroAjuste;
	}

	public String getClasificacionAjuste() {
		return clasificacionAjuste;
	}

	public void setClasificacionAjuste(String clasificacionAjuste) {
		this.clasificacionAjuste = clasificacionAjuste;
	}

	public String getAreaSolicitanteAjuste() {
		return areaSolicitanteAjuste;
	}

	public void setAreaSolicitanteAjuste(String areaSolicitanteAjuste) {
		this.areaSolicitanteAjuste = areaSolicitanteAjuste;
	}

	public String getAreaResponsableAjuste() {
		return areaResponsableAjuste;
	}

	public void setAreaResponsableAjuste(String areaResponsableAjuste) {
		this.areaResponsableAjuste = areaResponsableAjuste;
	}

	public String getSubmotivoAjuste() {
		return submotivoAjuste;
	}

	public void setSubmotivoAjuste(String submotivoAjuste) {
		this.submotivoAjuste = submotivoAjuste;
	}

	public Double getTiempoDesdeCreacionAjuste() {
		return tiempoDesdeCreacionAjuste;
	}

	public void setTiempoDesdeCreacionAjuste(Double tiempoDesdeCreacionAjuste) {
		this.tiempoDesdeCreacionAjuste = tiempoDesdeCreacionAjuste;
	}

	public Date getFechaCreacionAjuste() {
		return fechaCreacionAjuste;
	}

	public void setFechaCreacionAjuste(Date fechaCreacionAjuste) {
		this.fechaCreacionAjuste = fechaCreacionAjuste;
	}
	
	
}