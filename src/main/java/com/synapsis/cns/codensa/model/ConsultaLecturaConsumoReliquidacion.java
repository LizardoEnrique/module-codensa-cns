/**
 * 
 */
package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 */
public interface ConsultaLecturaConsumoReliquidacion extends
		SynergiaBusinessObject {

	public Long getIdCuenta() ;
	public Long getIdServicio() ;
	public Long getNroCuenta() ;
	public Long getNroServicio() ;
	public String getCorrelativoFacturacion() ;
	public Date getFechaFinalReliquidacion() ;
	public Date getFechaInicialReliquidacion() ;
	public String getPeriodosLiquidados() ;
	public Double getValorReliquidacionConsumo();
	public Double getValorReliquidacionContribucion();
	public Double getValorReliquidacionSubsidios() ;
	public Double getConsumoFacturadoActivaFP();
	public Double getConsumoFacturadoActivaHP() ;
	public Double getConsumoFacturadoActivaXP() ;
	public Double getConsumoFacturadoReactivaFP();
	public Double getConsumoFacturadoReactivaHP();
	public Double getConsumoFacturadoReactivaXP() ;
	public Double getConsumoReliquiadoActivaFP() ;
	public Double getConsumoReliquiadoActivaHP() ;
	public Double getConsumoReliquiadoActivaXP() ;
	public Double getConsumoReliquiadoReactivaFP();
	public Double getConsumoReliquiadoReactivaHP() ;
	public Double getConsumoReliquiadoReactivaXP() ;
	public Double getDiferenciaConsumoActivaFP();
	public Double getDiferenciaConsumoActivaHP() ;
	public Double getDiferenciaConsumoActivaXP() ;
	public Double getDiferenciaConsumoReactivaFP();
	public Double getDiferenciaConsumoReactivaHP() ;
	public Double getDiferenciaConsumoReactivaXP() ;
	public Long getIdMedidor();
	public String getNumeroMedidor();
}
