package com.synapsis.cns.codensa.model;

import java.util.Date;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;


/**
 * @author aPaulerena
 */
public interface ObservacionContactoAic extends SynergiaBusinessObject {
	
	public Long getId();
	public void setId(Long idContactoDist);
	
	public String getObservacion();
	public void setObservacion(String observacion);
	/**
	 * @return the fechaObservacion
	 */
	public Date getFechaObservacion();
	/**
	 * @param fechaObservacion the fechaObservacion to set
	 */
	public void setFechaObservacion(Date fechaObservacion);
}
