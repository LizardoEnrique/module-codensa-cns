/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaExpedienteEnvioRadicacion;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar18799631
 *
 */
public class ConsultaExpedienteEnvioRadicacionImpl extends
		SynergiaBusinessObjectImpl implements ConsultaExpedienteEnvioRadicacion {
	private Long numeroResolucion;	
	private Date fechaResolucion;	
	private String decision;	
	private String estado;	
	private Date fechaEmision;	
	private Double valorFinal;	
	private Long numeroRadicacion;	
	private Date fechaRadicacion;
	private Long nroExpediente;
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getDecision()
	 */
	public String getDecision() {
		return decision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setDecision(java.lang.String)
	 */
	public void setDecision(String decision) {
		this.decision = decision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getEstado()
	 */
	public String getEstado() {
		return estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setEstado(java.lang.String)
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getFechaEmision()
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setFechaEmision(java.util.Date)
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getFechaRadicacion()
	 */
	public Date getFechaRadicacion() {
		return fechaRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setFechaRadicacion(java.util.Date)
	 */
	public void setFechaRadicacion(Date fechaRadicacion) {
		this.fechaRadicacion = fechaRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getFechaResolucion()
	 */
	public Date getFechaResolucion() {
		return fechaResolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setFechaResolucion(java.util.Date)
	 */
	public void setFechaResolucion(Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getNumeroRadicacion()
	 */
	public Long getNumeroRadicacion() {
		return numeroRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setNumeroRadicacion(java.lang.String)
	 */
	public void setNumeroRadicacion(Long numeroRadicacion) {
		this.numeroRadicacion = numeroRadicacion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getNumeroResolucion()
	 */
	public Long getNumeroResolucion() {
		return numeroResolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setNumeroResolucion(java.lang.Long)
	 */
	public void setNumeroResolucion(Long numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#getValorFinal()
	 */
	public Double getValorFinal() {
		return valorFinal;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaExpedienteEnvioRadicacion#setValorFinal(java.lang.String)
	 */
	public void setValorFinal(Double valorFinal) {
		this.valorFinal = valorFinal;
	}
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}
	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	}

}
