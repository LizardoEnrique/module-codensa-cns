package com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada;

import com.synapsis.cns.codensa.model.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroPCR;
import com.synapsis.cns.codensa.model.impl.buscaCuenta.BuscaCuentaImpl;

public class BuscaCuentaAvanzadaNroPCRImpl extends BuscaCuentaImpl implements BuscaCuentaAvanzadaNroPCR {
	
	private String nroPCR;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroPCR#getNroPCR()
	 */
	public String getNroPCR() {
		return nroPCR;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.buscaCuenta.avanzada.BuscaCuentaAvanzadaNroPCR#setNroPCR(java.lang.Integer)
	 */
	public void setNroPCR(String nroPCR) {
		this.nroPCR = nroPCR;
	}
	
	
}
