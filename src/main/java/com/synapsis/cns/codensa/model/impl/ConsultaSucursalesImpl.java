package com.synapsis.cns.codensa.model.impl;

import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;
import com.synapsis.cns.codensa.model.ConsultaSucursales;
/**
 * 
 * @author Emiliano Arango (ar30557486)
 *
 */
public class ConsultaSucursalesImpl extends SynergiaBusinessObjectImpl implements ConsultaSucursales {
	
	private Long codigo;
	private String descripcion;
	
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaSucursales#getCodigo()
	 */
	public Long getCodigo() {
		return codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaSucursales#getDescripcion()
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaSucursales#setCodigo(java.lang.String)
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.ConsultaSucursales#setDescripcion(java.lang.String)
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
