/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.CargosManuales;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 *
 */
public class CargosManualesImpl extends SynergiaBusinessObjectImpl implements CargosManuales {
	private static final long serialVersionUID = 1L;

   	private String codigoCargo;
   	private String descripcionCargo;
   	private String valorCargo;
   	private String tipoServicioCargo;
   	private String nroServicioCargo;
   	private Date fechaIngresoCargo;
   	private Date fechaDesarrolloActividadTerreno;
   	private String usuarioIngresoCargo;
   	private String nombreIngresoCargo;
	private String nroDocumentoIngresoCargo;
   	private String estadoFactura;
   	private String origenCargo;
   	private String observaciones;
   	
   	private Long nroCuenta;

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getCodigoCargo()
	 */
	public String getCodigoCargo() {
		return codigoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setCodigoCargo(java.lang.String)
	 */
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getDescripcionCargo()
	 */
	public String getDescripcionCargo() {
		return descripcionCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setDescripcionCargo(java.lang.String)
	 */
	public void setDescripcionCargo(String descripcionCargo) {
		this.descripcionCargo = descripcionCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getEstadoFactura()
	 */
	public String getEstadoFactura() {
		return estadoFactura;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setEstadoFactura(java.lang.String)
	 */
	public void setEstadoFactura(String estadoFactura) {
		this.estadoFactura = estadoFactura;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getFechaDesarrolloActividadTerreno()
	 */
	public Date getFechaDesarrolloActividadTerreno() {
		return fechaDesarrolloActividadTerreno;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setFechaDesarrolloActividadTerreno(java.util.Date)
	 */
	public void setFechaDesarrolloActividadTerreno(
			Date fechaDesarrolloActividadTerreno) {
		this.fechaDesarrolloActividadTerreno = fechaDesarrolloActividadTerreno;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getFechaIngresoCargo()
	 */
	public Date getFechaIngresoCargo() {
		return fechaIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setFechaIngresoCargo(java.util.Date)
	 */
	public void setFechaIngresoCargo(Date fechaIngresoCargo) {
		this.fechaIngresoCargo = fechaIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getNombreIngresoCargo()
	 */
	public String getNombreIngresoCargo() {
		return nombreIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setNombreIngresoCargo(java.lang.String)
	 */
	public void setNombreIngresoCargo(String nombreIngresoCargo) {
		this.nombreIngresoCargo = nombreIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getNroCuenta()
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setNroCuenta(java.lang.Long)
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getNroDocumentoIngresoCargo()
	 */
	public String getNroDocumentoIngresoCargo() {
		return nroDocumentoIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setNroDocumentoIngresoCargo(java.lang.String)
	 */
	public void setNroDocumentoIngresoCargo(String nroDocumentoIngresoCargo) {
		this.nroDocumentoIngresoCargo = nroDocumentoIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getNroServicioCargo()
	 */
	public String getNroServicioCargo() {
		return nroServicioCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setNroServicioCargo(java.lang.String)
	 */
	public void setNroServicioCargo(String nroServicioCargo) {
		this.nroServicioCargo = nroServicioCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getObservaciones()
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setObservaciones(java.lang.String)
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getOrigenCargo()
	 */
	public String getOrigenCargo() {
		return origenCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setOrigenCargo(java.lang.String)
	 */
	public void setOrigenCargo(String origenCargo) {
		this.origenCargo = origenCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getTipoServicioCargo()
	 */
	public String getTipoServicioCargo() {
		return tipoServicioCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setTipoServicioCargo(java.lang.String)
	 */
	public void setTipoServicioCargo(String tipoServicioCargo) {
		this.tipoServicioCargo = tipoServicioCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getUsuarioIngresoCargo()
	 */
	public String getUsuarioIngresoCargo() {
		return usuarioIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setUsuarioIngresoCargo(java.lang.String)
	 */
	public void setUsuarioIngresoCargo(String usuarioIngresoCargo) {
		this.usuarioIngresoCargo = usuarioIngresoCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#getValorCargo()
	 */
	public String getValorCargo() {
		return valorCargo;
	}

	/* (non-Javadoc)
	 * @see com.synapsis.cns.codensa.model.impl.CarnosManuales#setValorCargo(java.lang.String)
	 */
	public void setValorCargo(String valorCargo) {
		this.valorCargo = valorCargo;
	}
}