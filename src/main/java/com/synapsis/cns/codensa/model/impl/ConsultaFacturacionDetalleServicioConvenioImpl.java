/**
 * 
 */
package com.synapsis.cns.codensa.model.impl;

import java.util.Date;

import com.synapsis.cns.codensa.model.ConsultaFacturacionDetalleServicioConvenio;
import com.synapsis.synergia.core.model.impl.SynergiaBusinessObjectImpl;

/**
 * @author ar30557486
 * 
 */
public class ConsultaFacturacionDetalleServicioConvenioImpl extends
SynergiaBusinessObjectImpl implements ConsultaFacturacionDetalleServicioConvenio {

	private Long id;
	private Long nroCuenta;
	private Date periodoPrimeraFact;
	private Date fechaCreacion;
	private Long nroServicio;
	private String tipoConvenio;
	private String opConvenio;
	private String estado;
	private Double tasaInteres;
	private String valorCuota;
	private String cuotaInteres;
	private Long nroCuotas;
	private Long cantVecesFac;
	private Integer cuotasPendientes;
	private String nombreTitlServ;
	private String nroIdentificacion;
	private Long itemsFacturados;
	private String saldoCapital;

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the periodoPrimeraFact
	 */
	public Date getPeriodoPrimeraFact() {
		return periodoPrimeraFact;
	}
	/**
	 * @param periodoPrimeraFact the periodoPrimeraFact to set
	 */
	public void setPeriodoPrimeraFact(Date periodoPrimeraFact) {
		this.periodoPrimeraFact = periodoPrimeraFact;
	}
	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the tasaInteres
	 */
	public Double getTasaInteres() {
		return tasaInteres;
	}
	/**
	 * @param tasaInteres the tasaInteres to set
	 */
	public void setTasaInteres(Double tasaInteres) {
		this.tasaInteres = tasaInteres;
	}
	/**
	 * @return the valorCuota
	 */
	public String getValorCuota() {
		return valorCuota;
	}
	/**
	 * @param valorCuota the valorCuota to set
	 */
	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}
	/**
	 * @return the cuotaInteres
	 */
	public String getCuotaInteres() {
		return cuotaInteres;
	}
	/**
	 * @param cuotaInteres the cuotaInteres to set
	 */
	public void setCuotaInteres(String cuotaInteres) {
		this.cuotaInteres = cuotaInteres;
	}
	/**
	 * @return the nroCuotas
	 */
	public Long getNroCuotas() {
		return nroCuotas;
	}
	/**
	 * @param nroCuotas the nroCuotas to set
	 */
	public void setNroCuotas(Long nroCuotas) {
		this.nroCuotas = nroCuotas;
	}
	/**
	 * @return the cantVecesFac
	 */
	public Long getCantVecesFac() {
		return cantVecesFac;
	}
	/**
	 * @param cantVecesFac the cantVecesFac to set
	 */
	public void setCantVecesFac(Long cantVecesFac) {
		this.cantVecesFac = cantVecesFac;
	}
	/**
	 * @return the cuotasPendientes
	 */
	public Integer getCuotasPendientes() {
		return cuotasPendientes;
	}
	/**
	 * @param cuotasPendientes the cuotasPendientes to set
	 */
	public void setCuotasPendientes(Integer cuotasPendientes) {
		this.cuotasPendientes = cuotasPendientes;
	}
	/**
	 * @return the nombreTitlServ
	 */
	public String getNombreTitlServ() {
		return nombreTitlServ;
	}
	/**
	 * @param nombreTitlServ the nombreTitlServ to set
	 */
	public void setNombreTitlServ(String nombreTitlServ) {
		this.nombreTitlServ = nombreTitlServ;
	}
	/**
	 * @return the nroIdentificacion
	 */
	public String getNroIdentificacion() {
		return nroIdentificacion;
	}
	/**
	 * @param nroIdentificacion the nroIdentificacion to set
	 */
	public void setNroIdentificacion(String nroIdentificacion) {
		this.nroIdentificacion = nroIdentificacion;
	}
	/**
	 * @return the itemsFacturados
	 */
	public Long getItemsFacturados() {
		return itemsFacturados;
	}
	/**
	 * @param itemsFacturados the itemsFacturados to set
	 */
	public void setItemsFacturados(Long itemsFacturados) {
		this.itemsFacturados = itemsFacturados;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the tipoConvenio
	 */
	public String getTipoConvenio() {
		return tipoConvenio;
	}
	/**
	 * @param tipoConvenio the tipoConvenio to set
	 */
	public void setTipoConvenio(String tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}
	/**
	 * @return the opConvenio
	 */
	public String getOpConvenio() {
		return opConvenio;
	}
	/**
	 * @param opConvenio the opConvenio to set
	 */
	public void setOpConvenio(String opConvenio) {
		this.opConvenio = opConvenio;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the saldoCapital
	 */
	public String getSaldoCapital() {
		return saldoCapital;
	}
	/**
	 * @param saldoCapital the saldoCapital to set
	 */
	public void setSaldoCapital(String saldoCapital) {
		this.saldoCapital = saldoCapital;
	}
}