/**
 * 
 */
package com.synapsis.cns.codensa.model;

import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * @author dbraccio
 *
 *CU 006
 */
public interface ConsultaLecturaConsumoAuditoriaHeader extends
		SynergiaBusinessObject {

	public String getCiclo() ;
	public Long getIdCuenta() ;
	public Long getIdServicio();
	public Long getNroCuenta() ;
	public Long getNroServicio();
	public String getSucursal() ;
	public String getZona() ;
	
}
