/*$Id: AICContactoService.java,v 1.2 2008/03/20 00:38:08 ar29261698 Exp $*/
package com.synapsis.cns.codensa.integration.kendari.service;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.suivant.arquitectura.core.service.Service;

/**
 * Interface de un servicio para generar contactos
 * @autor: chino
 * @date: 17/01/2007
 */
public interface AICContactoService extends Service{

    public String generarContactoCertificaBuenCliente(Long cuenta, String nombreSolicitante, String tipoDocumento, Long nroDocumento, String userName) throws BusinessException;

    public String generarContactoCertificaConvenio(Long cuenta, String nombreSolicitante, String tipoDocumento, Long nroDocumento, String userName) throws BusinessException;

}
