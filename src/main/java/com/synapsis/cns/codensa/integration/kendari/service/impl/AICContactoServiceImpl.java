/*$Id: AICContactoServiceImpl.java,v 1.5 2008/10/06 13:15:48 ar18817018 Exp $*/
package com.synapsis.cns.codensa.integration.kendari.service.impl;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.suivant.arquitectura.core.exception.SystemException;
import com.suivant.arquitectura.core.integration.Module;
import com.synapsis.cns.codensa.integration.kendari.remote.service.AICWServices;
import com.synapsis.cns.codensa.integration.kendari.service.AICContactoService;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * Implementacion de un servicio que se comunica con kendari via web services
 * para que genere un contacto Ejemplo de servicio de integracion
 * 
 * @autor: chino
 * @autor: dbraccio - Suivant
 * @date: 18/01/2007
 */
public class AICContactoServiceImpl extends SynergiaServiceImpl implements
		AICContactoService {

	// /importante no utilizar anotaciones para esta clase porque hice las
	// anotacion del servicio a mano [dbraccio]

	// bean que tiene seteado para hacer las llamadas a web services
	AICWServices aicWService;
	
	/**
	 * default constructor
	 */
	public AICContactoServiceImpl() {
		super();
	}
	/**
	 * constructor with module
	 * @param arg0
	 */
	public AICContactoServiceImpl(Module arg0) {
		super(arg0);
	}

	/**
	 * 
	 * llama al servicio del web service de KND para generar contacto
	 * @throws BusinessException 
	 * @throws BusinessException 
	 */
	public String generarContactoCertificaBuenCliente(Long cuenta, String nombreSolicitante,
			String tipoDocumento, Long nroDocumento, String userName) throws BusinessException {
		try {
			return aicWService.generarContactoCertificaBuenCliente(cuenta.longValue(), nombreSolicitante,
					tipoDocumento, nroDocumento.longValue(), userName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * realiza la llamada al web service de KND para generar contacto
	 */
	public String generarContactoCertificaConvenio(Long cuenta,
			String nombreSolicitante, String tipoDocumento, Long nroDocumento,
			String userName) throws BusinessException {
		try {
			return aicWService.generarContactoCertificaConvenio(cuenta.longValue(),
					nombreSolicitante, tipoDocumento, nroDocumento.longValue(), userName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(
					"certificaConvenio.error.generando.contacto");
		}
	}

	public AICWServices getAicWService() {
		return aicWService;
	}

	public void setAicWService(AICWServices aicWService) {
		this.aicWService = aicWService;
	}

}
