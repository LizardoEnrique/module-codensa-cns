/**
 * AICWServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.synapsis.cns.codensa.integration.kendari.remote.service;

/**
 * interface para llamar un web service que publica kendari. Esta interface es
 * la misma que usa kendari para publicar el web Service, entonces debe serusada
 * para llamarlo
 * 
 * @author dbraccio - Suivant
 * 
 */
public interface AICWServices {

	/**
	 * 
	 * @param cuenta
	 * @param nombreSolicitante
	 * @param tipoDocumento
	 * @param nroDocumento
	 * @param userName
	 * @return
	 * @throws java.rmi.RemoteException
	 */
	public java.lang.String generarContactoCertificaBuenCliente(long nroCuenta,
			java.lang.String nombreSolicitante, java.lang.String tipoDocumento,
			long numeroDocumento, java.lang.String userName)
			throws java.rmi.RemoteException;

	
	
	/**
	 * 
	 * @param cuenta
	 * @param nombreSolicitante
	 * @param tipoDocumento
	 * @param nroDocumento
	 * @param userName
	 * @return
	 * @throws java.rmi.RemoteException
	 */
	public java.lang.String generarContactoCertificaConvenio(long nroCuenta,
			java.lang.String nombreSolicitante, java.lang.String tipoDocumento,
			long numeroDocumento, java.lang.String userName)
			throws java.rmi.RemoteException;

}
