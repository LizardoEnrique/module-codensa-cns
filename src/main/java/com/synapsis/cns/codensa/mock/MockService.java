/**
 * 
 */
package com.synapsis.cns.codensa.mock;

import java.util.List;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.integration.Module;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.model.PersistentObject;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.CRUDService;
import com.suivant.arquitectura.core.service.request.impl.QueryFilterFactory;
import com.synapsis.synergia.core.service.impl.SynergiaServiceImpl;

/**
 * TODO:[dbraccio] esta una clase solamente para test hasta que las tablas de BD
 * esten realizadas
 * 
 * @author ar18799631
 * 
 */
public class MockService extends SynergiaServiceImpl implements CRUDService {

	public MockService(Module module) {
		super(module);
	}

	List mockList;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#countAll(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public int countAll(QueryFilter filter) {
		// TODO Auto-generated method stub
		return mockList.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#createNewInstance()
	 */
	public PersistentObject createNewInstance() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#findByCriteria(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public List findByCriteria(QueryFilter filter) {
		return mockList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#findByCriteriaUnique(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public BusinessObject findByCriteriaUnique(QueryFilter filter)
			throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#findById(java.lang.Long)
	 */
	public BusinessObject findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#getAll()
	 */
	public List getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#getBeanClass()
	 */
	public Class getBeanClass() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Query Filter Factory que es utilizada desde CRUDAspectConnector, para
	 * armar el filtro. antes de instanciar el servicio..
	 * 
	 * @return
	 */
	public QueryFilterFactory getQueryFilterFactory() {
		// if (queryFilterFactory == null)
		// queryFilterFactory = GenericQueryFilterFactory.getInstance();
		// return queryFilterFactory;
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#isValidNewPrimaryKey(com.suivant.arquitectura.core.queryFilter.QueryFilter)
	 */
	public Boolean isValidNewPrimaryKey(QueryFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#remove(com.suivant.arquitectura.core.model.PersistentObject)
	 */
	public void remove(PersistentObject persistentObject) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#save(com.suivant.arquitectura.core.model.PersistentObject)
	 */
	public void save(PersistentObject persistentObject) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#saveOrUpdate(com.suivant.arquitectura.core.model.PersistentObject)
	 */
	public void saveOrUpdate(PersistentObject persistentObject) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.suivant.arquitectura.core.service.CRUDService#update(com.suivant.arquitectura.core.model.PersistentObject)
	 */
	public void update(PersistentObject persistentObject) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the mockList
	 */
	public List getMockList() {
		return mockList;

	}

	/**
	 * @param mockList
	 *            the mockList to set
	 */
	public void setMockList(List mockList) {
		this.mockList = mockList;
	}

	/**
	 * @param beanClass
	 *            the beanClass to set
	 */
	public void setBeanClass(Class beanClass) {
		// do nothing - only for compatibility
	}

	public void removeCollection(List listPersistentsObject) {
		// TODO Auto-generated method stub

	}

	public void saveCollection(List listPersistentsObject) {
		// TODO Auto-generated method stub

	}

	public Boolean exists(QueryFilter q) {
		return Boolean.TRUE;
	}

	public Integer removeByCriteria(QueryFilter arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public int findByCriteriaToPageable(QueryFilter arg0) {
		return countAll(arg0);
	}

	

}
