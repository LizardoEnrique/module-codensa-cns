package com.synapsis.cns.codensa.web;

/**
 * @author ccamba
 * 
 */
public class CUOrigenCertificaBuenClienteBB extends
		GeneracionAutomaticaContactoKendariBB {

	protected String getCasoUsoOrigen() {
		return this.getParamEncryptedAndEncoded("CNS034");
	}

}
