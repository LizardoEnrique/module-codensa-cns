/**
 * 
 */
package com.synapsis.cns.codensa.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.ConsultaCuentaDatosBasicos;
import com.synapsis.cns.codensa.model.impl.ConsultaCuentaDatosFacDetalleImpl;
import com.synapsis.cns.codensa.model.impl.ConsultaCuentaLecturasConsumoImpl;
import com.synapsis.commons.services.ServiceHelper;
import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author ar30557486 (Emiliano Arango)
 * @UseCase CNS018
 * @author Paola Attadio (reactor completo para ponerlo en funcionamiento)
 */
public class DetalleCuentaBB extends ListableBB {

	private static final long serialVersionUID = 1L;
	private ConsultaCuentaDatosBasicos detallesBasicos;	
	private ConsultaCuentaLecturasConsumoImpl detalleLecturasConsumo;
	private ConsultaCuentaDatosFacDetalleImpl facDetalle;
	private Long periodoConfig;
	private HtmlSelectOneMenu selectorPeriodos;
	
	/**
	 * afalduto: este valor pasa por el dataTime converter de faces, por lo
	 * tanto nunca retorna un com.synapsis.commons.types.datetime.CalendarDate
	 * como esta mapeado en el periodo. Lo que se podria hacer es definir un
	 * conversor para este tipo de datos en el caso de hacer falta, io subi una
	 * impl muy peque�a que no se si funca ... pero ya ta configurada para
	 * hacerla andar. Nota: en el imprimirFactura.xhtml vamos a ver un conversor
	 * con esta forma <f:convertDateTime type="both" />, basicamente el type
	 * both lo que hace es generar un date que sepa manejar un timestamp (por
	 * llamarlo de alguna forma).
	 * 
	 * @uml.property name="fechaProceso"
	 */
	private Date fechaProceso;
	
	
	/**
	 * Init page 
	 * This set the current periodo and then, executes the querys
	 */
	public void init() {
		if (this.fechaProceso !=null) {
			this.callServiceFinderDatosBasicos();
			this.callServiceFinderLecturasConsumo();
			this.callServiceFinderDatosFacDetalle();
			this.callServiceFinderDatosMedidor();
			this.callServiceFinderDetalleCargos();			
			this.callServiceFinderCargosTerceros();			
		} else
			this.addMessage("No se ha seleccionado un periodo de facturaci�n");
	}
	
	private void addMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(message));
	}

	/**
	 * Traigo los datos basicos y los plancho a un field.
	 */
	private void callServiceFinderDatosBasicos() {
		
		Map parameters = new HashMap();

		// Buscamos en la BD los datos historicos de la factura ...
		parameters.put("nroCuenta", ((EqualQueryFilter) VariableResolverUtils
				.getObject("numeroCuentaQueryFilter")).getAttributeValue());
		parameters.put("periodoFacturacion", this.fechaProceso);
		try {
			this.setDetallesBasicos((ConsultaCuentaDatosBasicos) ServiceHelper.getResponse(
					SynergiaApplicationContext.getServiceManager(),
					"consultaCuentaDatosBasicosService", "findByCriteriaUnique",
					parameters));		
		} catch (Throwable e) {}
	}

	private void callServiceFinderDatosMedidor() {
		this.doMakeDataTableValues(this.makeFilter("periodoFacturacionMedidorQueryFilter", 
				this.fechaProceso)
				,"medidoresTable","consultaCuentaDatosMedidorServiceFinder");
	}

	private void callServiceFinderDetalleCargos() {
		this.doMakeDataTableValues(this.makeFilter("periodoFacturacionDetalleCargosQueryFilter", 
				this.fechaProceso)
				,"detalleCargos","consultaCuentaDetalleCargosServiceFinder");
	}

	private void callServiceFinderCargosTerceros() {
		this.doMakeDataTableValues(this.makeFilter("periodoFacturacionCargosTercerosQueryFilter",
				this.fechaProceso)
				,"detalleCargosTerceros","consultaCuentaDetalleCargosTercerosServiceFinder");
	}

	private void callServiceFinderLecturasConsumo() {
		Map parameters = new HashMap();
		// Buscamos en la BD los datos historicos de la factura ...
		parameters.put("nroCuenta", ((EqualQueryFilter) VariableResolverUtils
				.getObject("numeroCuentaQueryFilter")).getAttributeValue());
		parameters.put("fechaProceso", this.fechaProceso);
		
		try {
			this.setDetalleLecturasConsumo((ConsultaCuentaLecturasConsumoImpl) ServiceHelper.getResponse(
					SynergiaApplicationContext.getServiceManager(),
					"consultaCuentaDetalleLecturasConsumoService", "findByCriteriaUnique",
					parameters));
		} catch (Throwable e) {
			
		}	
	}	

	private void callServiceFinderDatosFacDetalle() {			
		Map parameters = new HashMap();
		// Buscamos en la BD los datos historicos de la factura ...
		parameters.put("nroCuenta", ((EqualQueryFilter) VariableResolverUtils
				.getObject("numeroCuentaQueryFilter")).getAttributeValue());
		parameters.put("periodoFacturacion", this.fechaProceso);
		
		try {
	 		this.setFacDetalle((ConsultaCuentaDatosFacDetalleImpl) ServiceHelper.getResponse(
						SynergiaApplicationContext.getServiceManager(),
						"detalleCuentaFacDetalleQuery", "findByCriteriaUnique",
						parameters));
		} catch (Throwable e) {}
	}
		
	/**
	 * 
	 * @param filter
	 * @return
	 */
	private CompositeQueryFilter makeFilter(String filter, Object value) {
		CompositeQueryFilter query = (CompositeQueryFilter) VariableResolverUtils.getObject(filter);
		EqualQueryFilter equal = (EqualQueryFilter)query.getFiltersList().get(1);
		equal.setAttributeValue(value);
		query.getFiltersList().set(1, equal);
		return query;
	}

	private void doMakeDataTableValues(QueryFilter queryFilter, String componentName, String serviceName) {
		StringBuffer sb = new StringBuffer().append("formDetalleCuenta:").append(componentName);
		
		HtmlDataTable dataTable = (HtmlDataTable)JSFUtils.getComponentFromTree(sb.toString());
		if (dataTable != null) {
			dataTable.setValue(new ScrollableList(queryFilter, serviceName));
			dataTable.setRows(10);
			dataTable.setFirst(0);
			
		}
	}

	


	/**
	 * Mueve el cursor un elemento periodo adelante o atras. Luego executa los
	 * querys para todos los campos
	 * 
	 * @param event
	 */
	public void navigateAction(ActionEvent event) {
		this.workWithNavigationEvent(event);
	}

	private void workWithNavigationEvent(ActionEvent event) {
//		this.getSelectedPeriodo(event);
//		Periodo bo;
//		try {
//			bo = (Periodo) NavigatorUtil.getNavigationObject(event,(Object) this.getPeriodo(), 
//				this.getPeriodoList());
//		} catch (RuntimeException e) {
//			bo =  this.getPeriodo();
//		}
//		this.setPeriodo(bo);
//		this.doExecuteQuerys();
	}
	
	
	
	// ---- Getters & Setters
	/**
	 * @return the detallesBasicos
	 */
	public ConsultaCuentaDatosBasicos getDetallesBasicos() {
		return this.detallesBasicos;
	}

	/**
	 * @param detallesBasicos
	 *            the detallesBasicos to set
	 */
	public void setDetallesBasicos(ConsultaCuentaDatosBasicos detallesBasicos) {
		this.detallesBasicos = detallesBasicos;
	}


	/**
	 * @return the detalleLecturasConsumo
	 */
	public ConsultaCuentaLecturasConsumoImpl getDetalleLecturasConsumo() {
		return detalleLecturasConsumo;
	}

	/**
	 * @param detalleLecturasConsumo
	 *            the detalleLecturasConsumo to set
	 */
	public void setDetalleLecturasConsumo(
			ConsultaCuentaLecturasConsumoImpl detalleLecturasConsumo) {
		this.detalleLecturasConsumo = detalleLecturasConsumo;
	}
	/**
	 * @return the facDetalle
	 */
	public ConsultaCuentaDatosFacDetalleImpl getFacDetalle() {
		return facDetalle;
	}
	/**
	 * @param facDetalle the facDetalle to set
	 */
	public void setFacDetalle(ConsultaCuentaDatosFacDetalleImpl facDetalle) {
		this.facDetalle = facDetalle;
	}
	/**
	 * @return the periodoConfig
	 */
	public Long getPeriodoConfig() {
		return periodoConfig;
	}
	/**
	 * @param periodoConfig the periodoConfig to set
	 */
	public void setPeriodoConfig(Long periodoConfig) {
		this.periodoConfig = periodoConfig;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public HtmlSelectOneMenu getSelectorPeriodos() {
		return selectorPeriodos;
	}

	public void setSelectorPeriodos(HtmlSelectOneMenu selectorPeriodos) {
		this.selectorPeriodos = selectorPeriodos;
	}

	/**
	 * Obtiene el per�odo anterior al seleccionado en el Selector, soluci�n r�pida para entrar a Prod!
	 *
	 */
	public void anteriorPeriodo() {
		SelectItem[] si = this.getSelectItems();
		// Si se presion� anterior y todav�a no se seleccion� ninguna Fecha no hago nada
		if (fechaProceso!=null) {
			int i = obtenerPosicionPeriodoSeleccionado(si);
			// Si no es la primera opci�n del selector, seteo el valoranterior
			if (i+1<si.length) {
				setFechaProceso(si, i+1);
			}
		}

		this.init();
	}

	/**
	 * Obtiene el siguiente per�odo al seleccionado en el Selector, soluci�n r�pida para entrar a Prod!
	 *
	 */
	public void siguientePeriodo() {
		SelectItem[] si = this.getSelectItems();
		// Si se presion� siguiente y todav�a no se seleccion� ninguna Fecha, selecciona la primera
		if (fechaProceso == null && si.length >= 1) {
			fechaProceso = new Date(this.getTime(si[si.length-1]));
		} else {
			int i = obtenerPosicionPeriodoSeleccionado(si);
			// Si no es la �ltima opci�n del selector, seteo el siguiente valor
			if (i - 1 >= 0) {
				setFechaProceso(si, i - 1);
			}
		}

		this.init();
	}

	private int obtenerPosicionPeriodoSeleccionado(SelectItem[] si) {
		int i = 0;
		while ((i < si.length-1) && (this.getTime(si[i]) != fechaProceso.getTime())) {
			i++;
		}
		return i;
	}

	private void setFechaProceso(SelectItem[] si, int i) {
		fechaProceso = new Date(this.getTime(si[i]));
	}
	
	private long getTime(SelectItem selectItem) {
		return ((CalendarDate) selectItem.getValue()).getTime();
	}
	
	private SelectItem[] getSelectItems() {
		// ESTO CON UN POQUITO DE TIEMPO HAY QUE CAMBIARLO, OBTENERLO POR ID!!
		UISelectItems items = (UISelectItems) selectorPeriodos.getChildren().get(1);
		return (SelectItem[]) items.getValue();
	}
}