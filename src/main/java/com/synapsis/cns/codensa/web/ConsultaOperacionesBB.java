/**
 * $Id: ConsultaOperacionesBB.java,v 1.2 2008/07/27 19:57:25 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.integration.codensa.backingBeans.helpers.DataTableFactoryRefresh;
import com.synapsis.synergia.core.integration.ListableData;

/**
 * @author ar18799631
 * @author Paola Attadio (se agrego seleccion en caso de invocación directa desde kendari)
 *
 */
public class ConsultaOperacionesBB extends ListableBB {
	
	
	public ConsultaOperacionesBB() {
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			Long nroCuentaaa = new Long(parameter.toString());
			queryFilter.setAttributeValue(nroCuentaaa);
		}		
	}	
	
	public void detalleMedidor(ActionEvent event) {
		String idOperacion = (String) JSFUtils.getParameter("nroOperacion");
		
		//obtengo el filtro del contexto y le seteo el valor
		SimpleQueryFilter queryFilter = (SimpleQueryFilter) VariableResolverUtils.getObject("detalleMedidorQueryFilter");
		queryFilter.setAttributeValue(new Long(idOperacion));	
		
		//realizo el refresco de la tabla 
		DataTableFactoryRefresh.makeDataTable("consultaOperacionesForm:detalleMedidorTable", queryFilter,
				 ((ListableData)getMultiList().getData().get("datosMedidor")).getServiceName(), 
				 ((ListableData)getMultiList().getData().get("datosMedidor")).getMetadata());

	}	
}
