/**
 * $Id: DetalleDeudaSaldoBB.java,v 1.3 2007/11/26 19:45:28 syalri Exp $
 */
package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.model.BusinessObject;
import com.synapsis.cns.codensa.model.ConsultaRefactCongelaciones;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Paola Attadio
 * @version $Revision: 1.3 $
 */
public class DetalleDeudaSaldoBB {
	
	/**
	 * Descripcion del detalle que se debe mostrar 
	 */
	private String detailName;
	private BusinessObject bo;	
	private Long nroDocumento;
	private Long nroFactura;
	private static final String OUTCOME_CONGELACION = "congelacion";
	private static final String OUTCOME_REFACTURACION = "refacturacion";
	/**
	 * 
	 * @return
	 */
	public String getDetailName() {
		return detailName;
	}

	/**
	 * 
	 * @param detailName
	 */
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}
	
	public String getOutcome() {
		ConsultaRefactCongelaciones mov = (ConsultaRefactCongelaciones)bo;
		String tipoMov =mov.getTipoOperacion();
		if(StringUtils.equalsIgnoreCase(tipoMov,"congelacion")  || 
				StringUtils.equalsIgnoreCase(tipoMov,"descongelacion"))
			return OUTCOME_CONGELACION;
		else 		
			return OUTCOME_REFACTURACION;
	}

	/**
	 * @return the bo
	 */
	public BusinessObject getBo() {
		return bo;
	}

	/**
	 * @param bo the bo to set
	 */
	public void setBo(BusinessObject bo) {
		this.bo = bo;
	}

	/**
	 * @return the nroDocumento
	 */
	public Long getNroDocumento() {
		return nroDocumento;
	}

	/**
	 * @param nroDocumento the nroDocumento to set
	 */
	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	/**
	 * @return the nroFactura
	 */
	public Long getNroFactura() {
		return nroFactura;
	}

	/**
	 * @param nroFactura the nroFactura to set
	 */
	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}
}	
