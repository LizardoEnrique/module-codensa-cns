package com.synapsis.cns.codensa.web.filters;

import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;

/**
 * Permite tener en un mismo bb, diferentes servicios
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class CustomServiceQueryFilter extends CompositeQueryFilter implements
		CustomQueryFilter {

	private String serviceName;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
