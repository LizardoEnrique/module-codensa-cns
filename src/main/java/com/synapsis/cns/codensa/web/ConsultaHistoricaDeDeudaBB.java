package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

public class ConsultaHistoricaDeDeudaBB {
	private Long numeroCuentaQueryFilter;
	
	public ConsultaHistoricaDeDeudaBB (){
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			this.numeroCuentaQueryFilter = new Long(parameter.toString());
			queryFilter.setAttributeValue(this.numeroCuentaQueryFilter);
			
		} else 
			this.numeroCuentaQueryFilter = (Long) queryFilter.getAttributeValue();
	}

	public Long getNumeroCuentaQueryFilter() {
		return numeroCuentaQueryFilter;
	}

	public void setNumeroCuentaQueryFilter(Long numeroCuentaQueryFilter) {
		this.numeroCuentaQueryFilter = numeroCuentaQueryFilter;
	}
}