package com.synapsis.cns.codensa.web;

import java.io.Serializable;

import org.apache.myfaces.custom.service.UIService;

import com.synapsis.cns.codensa.model.DetalleCuentaInfraestructura;

/**
 * @author ccamba
 * 
 */
public class DetalleInventariosInfraestructuraBB implements Serializable {
	private DetalleCuentaInfraestructura detalleCuentaInfraestructuraBO;
	private transient UIService inventariosInicialInfraestructuraService;
	private transient UIService inventariosAcumuladoInfraestructuraService;

	public UIService getInventariosInicialInfraestructuraService() {
		return this.inventariosInicialInfraestructuraService;
	}

	public void setInventariosInicialInfraestructuraService(UIService inventariosInicialInfraestructuraService) {
		this.inventariosInicialInfraestructuraService = inventariosInicialInfraestructuraService;
	}

	public UIService getInventariosAcumuladoInfraestructuraService() {
		return this.inventariosAcumuladoInfraestructuraService;
	}

	public void setInventariosAcumuladoInfraestructuraService(UIService inventariosAcumuladoInfraestructuraService) {
		this.inventariosAcumuladoInfraestructuraService = inventariosAcumuladoInfraestructuraService;
	}

	public Long getNroCuenta() {
		return this.getDetalleCuentaInfraestructuraBO().getNroCuenta();
	}

	public void setNroCuenta(Long nroCuenta) {
		this.getDetalleCuentaInfraestructuraBO().setNroCuenta(nroCuenta);
	}

	public DetalleCuentaInfraestructura getDetalleCuentaInfraestructuraBO() {
		return this.detalleCuentaInfraestructuraBO;
	}

	public void setDetalleCuentaInfraestructuraBO(DetalleCuentaInfraestructura detalleCuentaInfraestructuraBO) {
		this.detalleCuentaInfraestructuraBO = detalleCuentaInfraestructuraBO;
	}

}
