package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.3 $ $Date: 2008/08/29 20:52:22 $
 * 
 */
public class DetalleCargosFactBB extends ListableBB {

	/**
	 * Comment for <code>serviceToCall</code>
	 */
	private String serviceToCall;

	/**
	 * @return the serviceToCall
	 */
	public String getServiceToCall() {
		return this.serviceToCall;
	}

	/**
	 * @param serviceToCall
	 *            the serviceToCall to set
	 */
	public void setServiceToCall(String serviceToCall) {
		this.serviceToCall = serviceToCall;
	}

	/**
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public void buildScrolleableList() {
		this.getMultiList().getUniqueList().setQueryFilter((QueryFilter) VariableResolverUtils
				.getObject("numeroCuentaQueryFilter"));
		this.getMultiList().getUniqueList().setServiceName(this.getServiceToCall());
		super.buildScrolleableList();
		
//
//			super.setListResults(this.getService(this.getServiceToCall())
//					.findByCriteria(
//							(QueryFilter) VariableResolverUtils
//									.getObject("numeroCuentaQueryFilter")));
		
	}

	/**
	 * @param serviceFinder
	 * @return
	 */
	private FinderService getService(String serviceFinder) {
		return (FinderService) SynergiaApplicationContext
				.findService(serviceFinder);
	}

}