package com.synapsis.cns.codensa.web;

import java.util.Iterator;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;

import org.apache.myfaces.custom.datatable.ext.HtmlServiceDataTable;
import org.apache.myfaces.custom.service.UIService;

import com.synapsis.commons.faces.messages.Message;

public class ConsultaCuentaAgilBB {

	// filtros
	// fin filtros

	private UIService svcBusquedaMedidorService;
	private UIComponent component;
	private HtmlServiceDataTable tablaBusqueda;
	private String nombreTitular;
	private String rutaLectura;
	private String municipio;
	private String direccionTitular;
	private Long nroServicio;
	private String sector;
	private String zona;
	private String correlativo;
	private String sucursal;
	private String nroMedidor;
	private String urbanizacion;
	private String calle;
	private String numero;
	private String apellidoPaterno;
	private String apellidoMaterno;

	private boolean validateForm() {
		List children = component.getChildren();
		UIComponent parent = component.getParent();
		// boolean ok = false;
		Iterator iter = children.iterator();
		while (iter.hasNext()) {
			UIParameter element = (UIParameter) iter.next();
			String value = (String) this.getValue(parent, (String) element
					.getValue());
			if (value != null && !"".equals(value)) {
				return true;
			}
		}
		FacesContext
				.getCurrentInstance()
				.addMessage(
						null,
						new Message(
								Message.TYPE_FORM,
								Message.SEVERITY_ERROR,
								"",
								"Al menos un campo es requerido para la b�squeda",
								null));
		return false;
	}

	/**
	 * @param idComponent
	 * @return Value of the uiInput
	 */
	protected Object getValue(UIComponent component, String idComponent) {
		UIInput input = (UIInput) component.findComponent(idComponent);
		if (input.getSubmittedValue() == null) {
			if (input.getConverter() != null) {
				return input.getConverter().getAsString(
						FacesContext.getCurrentInstance(), input,
						input.getValue());
			}
			return input.getValue() != null ? input.getValue().toString() : "";
		}
		return input.getSubmittedValue();
	}

	public void search() {
		if (this.validateForm()) {
			this.getSvcBusquedaMedidorService().execute();
			this.getTablaBusqueda().setFirst(0);
		}
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getRutaLectura() {
		return rutaLectura;
	}

	public void setRutaLectura(String rutaLectura) {
		this.rutaLectura = rutaLectura;
	}

	public HtmlServiceDataTable getTablaBusqueda() {
		return tablaBusqueda;
	}

	public void setTablaBusqueda(HtmlServiceDataTable tablaBusqueda) {
		this.tablaBusqueda = tablaBusqueda;
	}

	public String getDireccionTitular() {
		return direccionTitular;
	}

	public void setDireccionTitular(String direccionTitular) {
		this.direccionTitular = direccionTitular;
	}

	public Long getNroServicio() {
		return nroServicio;
	}

	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}

	/**
	 * @return the correlativo
	 */
	public String getCorrelativo() {
		return correlativo;
	}

	/**
	 * @param correlativo
	 *            the correlativo to set
	 */
	public void setCorrelativo(String correlativo) {
		this.correlativo = correlativo;
	}

	/**
	 * @return the sector
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * @param sector
	 *            the sector to set
	 */
	public void setSector(String sector) {
		this.sector = sector;
	}

	/**
	 * @return the zona
	 */
	public String getZona() {
		return zona;
	}

	/**
	 * @param zona
	 *            the zona to set
	 */
	public void setZona(String zona) {
		this.zona = zona;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the nroMedidor
	 */
	public String getNroMedidor() {
		return nroMedidor;
	}

	/**
	 * @param nroMedidor
	 *            the nroMedidor to set
	 */
	public void setNroMedidor(String nroMedidor) {
		this.nroMedidor = nroMedidor;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getUrbanizacion() {
		return urbanizacion;
	}

	public void setUrbanizacion(String urbanizacion) {
		this.urbanizacion = urbanizacion;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	/**
	 * @return the component
	 */
	public UIComponent getComponent() {
		return component;
	}

	/**
	 * @param component
	 *            the component to set
	 */
	public void setComponent(UIComponent component) {
		this.component = component;
	}

	/**
	 * @return the svcBusquedaMedidorService
	 */
	public UIService getSvcBusquedaMedidorService() {
		return svcBusquedaMedidorService;
	}

	/**
	 * @param svcBusquedaMedidorService
	 *            the svcBusquedaMedidorService to set
	 */
	public void setSvcBusquedaMedidorService(UIService svcBusquedaMedidorService) {
		this.svcBusquedaMedidorService = svcBusquedaMedidorService;
	}

}
