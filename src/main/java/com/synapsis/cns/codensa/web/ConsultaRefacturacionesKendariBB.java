package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.security.util.KendaryAmeEncryptUtil;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.suivant.arquitectura.support.utf8.URLUTF8Encoder;
import com.synapsis.integration.codensa.integration.kendari.URLHelper;
import com.synapsis.integration.codensa.integration.kendari.URLParameter;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author ar29302553
 */
public abstract class ConsultaRefacturacionesKendariBB {

	private URLHelper urlHelper;
	private String useCaseUrl;

	public String getUseCaseUrl() {
		return useCaseUrl;
	}

	public void setUseCaseUrl(String useCaseUrl) {
		this.useCaseUrl = useCaseUrl;
	}

	public URLHelper getUrlHelper() {
		return urlHelper;
	}

	public void setUrlHelper(URLHelper urlHelper) {
		this.urlHelper = urlHelper;
	}

	public abstract String execKndUseCase(ActionEvent event); 	
	
	protected List makeParameters() {
		List list = new ArrayList();

		URLParameter param1 = new URLParameter("NAS_nroCuenta", this
				.getNumeroCuenta());
		URLParameter param2 = new URLParameter("NAS_userName", this
				.getUserName());
		
		list.add(param1);
		list.add(param2);
	
		return list;
	}

	/** 
	 * Override segun corresponda.
	 */
	protected List makeSpecificParameters() {
		return new ArrayList();
	}

	protected String getParamEncryptedAndEncoded(String param) {
		param = KendaryAmeEncryptUtil.encrypt(param);
		param = URLUTF8Encoder.encode(param);

		return param;
	}

	private String getNumeroCuenta() {
		EqualQueryFilter eq = ((EqualQueryFilter) VariableResolverUtils
				.getObject("numeroCuentaQueryFilter"));
		String nroCuenta = eq.getAttributeValue().toString();

		return this.getParamEncryptedAndEncoded(nroCuenta);
	}

	private String getUserName() {
		String userName = null;
		try {
			userName = SynergiaApplicationContext.getCurrentUser()
					.getUsername();
		} catch (Exception e) {
			userName = "SystemUser";
		}

		return this.getParamEncryptedAndEncoded(userName);
	}

}