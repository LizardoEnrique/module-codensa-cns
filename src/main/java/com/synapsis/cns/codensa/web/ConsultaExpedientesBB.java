package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

/**
 * @author Paola Attadio (se agrego seleccion en caso de invocación directa desde kendari)
 *
 */
public class ConsultaExpedientesBB {
	private Long nroExpediente;
	private Long nroRadicacion;

	private Long numeroCuentaQueryFilter;
	

	public ConsultaExpedientesBB (){
		
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			this.numeroCuentaQueryFilter = new Long(parameter.toString());
			queryFilter.setAttributeValue(this.numeroCuentaQueryFilter);
			
		} else 
			this.numeroCuentaQueryFilter = (Long) queryFilter.getAttributeValue();
	}


	public Long getNumeroCuentaQueryFilter() {
		return numeroCuentaQueryFilter;
	}


	public void setNumeroCuentaQueryFilter(Long numeroCuentaQueryFilter) {
		this.numeroCuentaQueryFilter = numeroCuentaQueryFilter;
	}
	
	/**
	 * @return the nroExpediente
	 */
	public Long getNroExpediente() {
		return nroExpediente;
	}

	/**
	 * @param nroExpediente the nroExpediente to set
	 */
	public void setNroExpediente(Long nroExpediente) {
		this.nroExpediente = nroExpediente;
	}

	/**
	 * @return the nroRadicacion
	 */
	public Long getNroRadicacion() {
		return nroRadicacion;
	}

	/**
	 * @param nroRadicacion the nroRadicacion to set
	 */
	public void setNroRadicacion(Long nroRadicacion) {
		this.nroRadicacion = nroRadicacion;
	}
	
}
