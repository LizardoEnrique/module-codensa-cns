package com.synapsis.cns.codensa.web;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

/**
 * @author aPaulerena (se agrego seleccion en caso de invocación directa desde kendari)
 *
 */
public class ConsultaOrdenesInspeccionBB {
	private Long numeroCuentaQueryFilter;
	
	public ConsultaOrdenesInspeccionBB (){
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			this.numeroCuentaQueryFilter = new Long(parameter.toString());
			queryFilter.setAttributeValue(this.numeroCuentaQueryFilter);
			
		} else 
			this.numeroCuentaQueryFilter = (Long) queryFilter.getAttributeValue();
	}

	public Long getNumeroCuentaQueryFilter() {
		return numeroCuentaQueryFilter;
	}

	public void setNumeroCuentaQueryFilter(Long numeroCuentaQueryFilter) {
		this.numeroCuentaQueryFilter = numeroCuentaQueryFilter;
	}
	
	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc
				.getExternalContext().getRequest();
		String nroCuenta = request.getParameter("nroCuenta");
		this.setNumeroCuentaQueryFilter(Long.valueOf(nroCuenta));				
	}

}
