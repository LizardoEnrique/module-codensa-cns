package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

/**
 * 
 * @author jhv
 * 
 */
public class ConsultaRefacturacionesAjusteBB extends ListableBB {

	private SynergiaBusinessObject businessObject;	

	/**
	 * 
	 *
	 */
	public void buscarCantidadRefacturaciones() {
		try {
			searchBusiness();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	/**
	 * 
	 * @throws ObjectNotFoundException
	 */
	private void searchBusiness() throws ObjectNotFoundException {		
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("numeroCuentaQueryFilter");		
		setBusinessObject((SynergiaBusinessObject) this.getService("refacCantServiceFinder").findByCriteriaUnique(queryFilter));		
	}

	/** 
	 * Devuleve un servicio pidiendoselo al SynergiaApplicacontext
	 */
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext
				.findService(service);
	}
	
	/**
	 * 
	 * @param businessObject
	 */
	public void setBusinessObject(SynergiaBusinessObject businessObject) {
		this.businessObject = businessObject;
	}

	/**
	 * 
	 * @return
	 */
	public SynergiaBusinessObject getBusinessObject() {
		return businessObject;
	}
}
