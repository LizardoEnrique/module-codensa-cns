package com.synapsis.cns.codensa.web;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

public class ConsultaServiciosFinancierosOCHBB {
	
	private UIService findByCriteriaService;
	private HtmlDataTable dataTable;	
	
	
	public HtmlDataTable getDataTable() {
		return dataTable;
	}



	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}



	public void search() {
		this.getFindByCriteriaService().execute();
		this.getDataTable().setFirst(0);
	}
	

	
	public UIService getFindByCriteriaService() {
		return findByCriteriaService;
	}

	public void setFindByCriteriaService(UIService findByCriteriaService) {
		this.findByCriteriaService = findByCriteriaService;
	}

	
}
