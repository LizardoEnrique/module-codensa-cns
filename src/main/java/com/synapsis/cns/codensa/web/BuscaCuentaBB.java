package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.myfaces.custom.service.UIService;
import org.apache.myfaces.custom.serviceunmarshaller.UIServiceUnmarshaller;

import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.suivant.arquitectura.presentation.utils.ComboUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.web.filters.CustomQueryFilter;
import com.synapsis.components.UIMainGroup;

/**
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 * @dBraccio: meti algunos arreglos porque lo de ajax no estaba haciendo nada,
 *            los combos anidados no estaban siendo filtrados por el id del
 *            padre. Generalice la creacion de combos y la carga de una sola vez
 *            de los combos.
 * 
 */
public class BuscaCuentaBB extends ListableBB implements BackingBeansConstants {

	private static final long serialVersionUID = 1L;
	
	private Object queryFilters;
	private Object serviceNames;
	private QueryFilter queryFilter;
	
	

	private String serviceCombo;

	private QueryFilter comboQueryFilter;

	private SelectItem[] sucursales;

	private SelectItem[] ordenes;

	private SelectItem[] departamentos;

	private SelectItem[] manzanas;

	// estos combos son anidados y deben ser refrescados via Ajax4j

	private SelectItem[] zonas;

	private SelectItem[] ciclos;

	private SelectItem[] grupos;

	private SelectItem[] municipios;

	private SelectItem[] zonasMunicipios;

	private SelectItem[] ciclosMunicipios;

	private SelectItem[] gruposMunicipios;

	private UIMainGroup group;

	// nombres de filtros de combos
	private final static String COMBO_MUNICIPIO_FILTER = "comboMunicipioQueryFilter";

	private final static String COMBO_ZONAS_SUCURSAL_FILTER = "comboZonasSucursalQueryFilter";

	private final static String COMBO_CICLOS_ZONAS_FILTER = "comboCiclosZonasQueryFilter";

	private final static String COMBO_GRUPOS_CICLOS_FILTER = "comboGruposCiclosQueryFilter";

	private final static String COMBO_DEFAULT_FILTER = " comboQueryFilter";

	// nombres de filtros
	private final static String DIRECCION_RURAL_FILTER = "busquedaCuentaDireccionRuralQueryFilter";

	private final static String DIRECCION_URBANA_FILTER = "busquedaCuentaDireccionUrbanaQueryFilter";

	// outcomes
	private final static String OUTCOME_BUSQUEDA_EXITOSA = "busquedaExitosa";

	private final static String OUTCOME_BUSQUEDA_RURAL = "busquedaRural";

	private final static String OUTCOME_AVANZADA = "avanzada";

	private final static String OUTCOME_BUSQUEDA_AVANZADA = "busquedaAvanzada";

	private final static String OUTCOME_BUSQUEDA_BASICA = "busquedaBasica";

	private final static String OUTCOME_BUSQUEDA_URBANA = "busquedaUrbana";
	
	private UIService comboNombreViasService;
	private UIServiceUnmarshaller comboServiceUnmarshelled;
	private List nombreViasList;
	
	
	public void refresh(ActionEvent e) {
		UIInput u = (UIInput)e.getComponent().getParent();
		String value = u.getValueBinding("value").getExpressionString();
		StringTokenizer a = new StringTokenizer(value,".");
		String object = a.nextToken().substring(2);
		SimpleQueryFilter filter = (SimpleQueryFilter) VariableResolverUtils.getObject(
		object, SimpleQueryFilter.class);
		filter.setAttributeValue(u.getSubmittedValue());
		if(object.equals("busquedaCuentaClaseViaQueryFilter")){
			getComboNombreViasService().execute(true);
			nombreViasList = (List)comboServiceUnmarshelled.getValue();
		}
	}
	
	

	public UIService getComboNombreViasService() {
		return comboNombreViasService;
	}

	public void setComboNombreViasService(UIService comboNombreViasService) {
		this.comboNombreViasService = comboNombreViasService;
	}

	public List getNombreViasList() {
		return nombreViasList;
	}

	public void setNombreViasList(List nombreViasList) {
		this.nombreViasList = nombreViasList;
	}


	public UIServiceUnmarshaller getComboServiceUnmarshelled() {
		return comboServiceUnmarshelled;
	}

	public void setComboServiceUnmarshelled(
			UIServiceUnmarshaller comboServiceUnmarshelled) {
		this.comboServiceUnmarshelled = comboServiceUnmarshelled;
	}

	/**
	 * entrada a la busqueda de cuentas
	 * 
	 * @return
	 */
	public String buscarCuentaAction() {
		this.makeFilter();
		return OUTCOME_BUSQUEDA_EXITOSA;
	}

	/**
	 * entrada a la busqueda por direccion rural
	 */
	public String buscarCuentaDireccionRural() {
		QueryFilter filter = (QueryFilter) VariableResolverUtils.getObject(
				DIRECCION_RURAL_FILTER, QueryFilter.class);
		setValuesToFilter(filter);
		return OUTCOME_BUSQUEDA_EXITOSA;
	}

	/**
	 * entrada a la busqueda por direccion urbana
	 */
	public String buscarCuentaActionDireccionUrbana() {
		QueryFilter filter = (QueryFilter) VariableResolverUtils.getObject(
				DIRECCION_URBANA_FILTER, QueryFilter.class);
		setValuesToFilter(filter);
		return OUTCOME_BUSQUEDA_EXITOSA;
	}

	/**
	 * En base al radio button le pega a un servicio diferente para realizar la
	 * busqueda de cuenta.
	 * 
	 */
	private void makeFilter() {
		// obtengo del grupo seleccionado el nombre, ya que el id del grupo me
		// indica a que servicio pegarle
		String filterName = group.getGroupSelected().getId();
		QueryFilter filter = (QueryFilter) VariableResolverUtils.getObject(
				filterName, QueryFilter.class);
		setValuesToFilter(filter);
	}

	private void setValuesToFilter(QueryFilter filter) {
		super.setQueryFilters(filter);
		super.setServiceNames(((CustomQueryFilter) filter).getServiceName());
		filter.setMaxResults(10);
		filter.setPositionStart(0);
		filter.setMaxResults(10);
	}

	// ******************************************************************
	// *** metodos que son invocados via ajax4j para cargar los
	// *** combos anidados
	// **********************************************************
	public void fillMunicipios(ActionEvent event) {
		String submittedValue = ((UIInput) event.getComponent().getParent())
		.getSubmittedValue().toString();
		
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_MUNICIPIO_FILTER);
		Long l = new Long(-1);
		sq.setAttributeValue(new Long (submittedValue));
		if (!sq.getAttributeValue().equals(l)) {
			this.municipios = ComboFactory.makeCombo(SRV_COMBO_MUNICIPIOS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.municipios = makeEmptySelectItem();
		}
	}

	public void fillZonas() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_ZONAS_SUCURSAL_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.zonas = ComboFactory.makeCombo(SRV_COMBO_ZONAS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.zonas = makeEmptySelectItem();
		}
		this.ciclos = makeEmptySelectItem();
		this.grupos = makeEmptySelectItem();
	}

	public void fillCiclos() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_CICLOS_ZONAS_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.ciclos = ComboFactory.makeCombo(SRV_COMBO_CICLOS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.ciclos = makeEmptySelectItem();
		}
		this.grupos = makeEmptySelectItem();
	}

	public void fillGrupos() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_GRUPOS_CICLOS_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.grupos = ComboFactory.makeCombo(SRV_COMBO_GRUPOS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.grupos = makeEmptySelectItem();
		}

	}
	
	public void fillZonasMunicipiosSmile(ActionEvent event) {
		String sucursalSeleccionada = ((UIInput) event.getComponent().getParent())
		.getSubmittedValue().toString();
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_ZONAS_SUCURSAL_FILTER);
		Long l = new Long(-1);
		sq.setAttributeValue(new Long (sucursalSeleccionada));
		if (!sq.getAttributeValue().equals(l)) {
			this.zonasMunicipios = ComboFactory.makeCombo(SRV_COMBO_ZONAS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.zonasMunicipios = makeEmptySelectItem();
		}
		this.ciclosMunicipios = makeEmptySelectItem();
		this.gruposMunicipios = makeEmptySelectItem();
	}
	

	public void fillZonasMunicipios() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_ZONAS_SUCURSAL_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.zonasMunicipios = ComboFactory.makeCombo(SRV_COMBO_ZONAS, sq,
					COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.zonasMunicipios = makeEmptySelectItem();
		}
		this.ciclosMunicipios = makeEmptySelectItem();
		this.gruposMunicipios = makeEmptySelectItem();
	}
	
	
	public void fillCiclosMunicipiosListener(ActionEvent event) {
		String zonaSeleccionada = ((UIInput) event.getComponent().getParent())
		.getSubmittedValue().toString();
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_CICLOS_ZONAS_FILTER);
		Long l = new Long(-1);
		sq.setAttributeValue(new Long(zonaSeleccionada));
		if (!sq.getAttributeValue().equals(l)) {
			this.ciclosMunicipios = ComboFactory.makeCombo(SRV_COMBO_CICLOS,
					sq, COMBO_DEFAULT_LABEL, "id", null,l);
		} else {
			this.ciclosMunicipios = makeEmptySelectItem();
		}
		this.gruposMunicipios = makeEmptySelectItem();
	}

	public void fillCiclosMunicipios() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_CICLOS_ZONAS_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.ciclosMunicipios = ComboFactory.makeCombo(SRV_COMBO_CICLOS,
					sq, COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.ciclosMunicipios = makeEmptySelectItem();
		}
		this.gruposMunicipios = makeEmptySelectItem();
	}

	public void fillGruposMunicipios() {
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_GRUPOS_CICLOS_FILTER);
		Long l = new Long(-1);
		if (!sq.getAttributeValue().equals(l)) {
			this.gruposMunicipios = ComboFactory.makeCombo(SRV_COMBO_GRUPOS,
					sq, COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.gruposMunicipios = makeEmptySelectItem();
		}

	}
	
	public void fillGruposMunicipiosListener(ActionEvent event) {
		String cicloSeleccionado = ((UIInput) event.getComponent().getParent())
		.getSubmittedValue().toString();
		SimpleQueryFilter sq = (SimpleQueryFilter) VariableResolverUtils
				.getObject(COMBO_GRUPOS_CICLOS_FILTER);
		Long l = new Long(-1);
		sq.setAttributeValue(new Long(cicloSeleccionado));
		if (!sq.getAttributeValue().equals(l)) {
			this.gruposMunicipios = ComboFactory.makeCombo(SRV_COMBO_GRUPOS,
					sq, COMBO_DEFAULT_LABEL, "id", null, l);
		} else {
			this.gruposMunicipios = makeEmptySelectItem();
		}

	}

	// combo de ordenes
	public SelectItem[] getOrdenes() {
		if (ordenes == null)
			this.ordenes = ComboFactory.makeCombo(SRV_COMBO_ORDENES,
					"comboQueryFilter", COMBO_DEFAULT_LABEL,
					COMBO_DEFAULT_VALUE);
		return ordenes;
	}

	// combo de sucursales
	public SelectItem[] getSucursales() {
		if (sucursales == null)
			this.sucursales = ComboFactory.makeCombo(SRV_COMBO_SUCURSALES,
					COMBO_DEFAULT_FILTER, COMBO_DEFAULT_LABEL, "id", null,
					new Long(-1));
		return sucursales;
	}

	// combo de departamentos
	public SelectItem[] getDepartamentos() {
		if (departamentos == null)
			this.departamentos = ComboFactory.makeCombo(
					SRV_COMBO_DEPARTAMENTOS, COMBO_DEFAULT_FILTER,
					COMBO_DEFAULT_LABEL, "id", null, new Long(-1));
		return this.departamentos;
	}

	// combo de manzanas
	public SelectItem[] getManzanas() {
		if (this.manzanas == null)
			this.manzanas = ComboFactory.makeCombo(SRV_COMBO_MANZANAS,
					COMBO_DEFAULT_FILTER, COMBO_DEFAULT_VALUE,
					COMBO_DEFAULT_VALUE);
		return this.manzanas;
	}

	public void setManzanas(SelectItem[] manzanas) {
		this.manzanas = manzanas;
	}

	// devuelve una array de SelectItem con un solo elemento vacio
	private SelectItem[] makeEmptySelectItem() {
		SelectItem[] select = new SelectItem[1];
		select[0] = new SelectItem("", "");
		return select;
	}

	// *******************************************************************
	// ** METODOS PARA LA NAVEGACION
	// ********************************************************************

	/**
	 * navegacion para volver a la pagina de busca cuenta
	 * 
	 */
	public String comeBackToSearch() {
		return OUTCOME_AVANZADA;
	}

	/**
	 * Navegacion a la busqueda avanzada
	 * 
	 * @return outcome para la navegacion de faces
	 */
	public String irBusquedaAvanzadaAction() {
		return OUTCOME_BUSQUEDA_AVANZADA;
	}

	/**
	 * Navegacion a la busqueda basic
	 * 
	 * @return outcome para la navegacion de faces
	 */
	public String irABusquedaBasicaAction() {
		return OUTCOME_BUSQUEDA_BASICA;
	}

	/**
	 * navegacion para la busqueda urbana
	 * 
	 * @return outcome para la navegacion de faces
	 */
	public String irABusquedaUrbanaAction() {
		return OUTCOME_BUSQUEDA_URBANA;
	}

	/**
	 * navegacion para la busqueda rural
	 * 
	 * @return outcome para la navegacion de faces
	 */
	public String irABusquedaRuralAction() {
		return OUTCOME_BUSQUEDA_RURAL;
	}

	// ****************************************************
	// *** GETTER AND SETTER
	// *****************************************************

	// combo de tipos de servicio
	public SelectItem[] getItemsTipoServicio() {
		List result = new ArrayList();

		Map element0 = new HashMap();
		element0.put("id", "Electrico");
		element0.put("descripcion", "Electrico");

		Map element1 = new HashMap();
		element1.put("id", "Venta");
		element1.put("descripcion", "Venta");

		Map element2 = new HashMap();
		element2.put("id", "CompraCartera");
		element2.put("descripcion", "Servicio Financiero");

		Map element3 = new HashMap();
		element3.put("id", "Convenio");
		element3.put("descripcion", "Convenio");

		Map element4 = new HashMap();
		element4.put("id", "EncargoCobranza");
		element4.put("descripcion", "Encargo de Cobranza");
		
		Map element5 = new HashMap();
		element5.put("id", "CuotaManejo");
		element5.put("descripcion", "Cuota de Manejo");

		Map element6 = new HashMap();
		element6.put("id", "Infraestructura");
		element6.put("descripcion", "Infraestructura");

		result.add(element0);
		result.add(element1);
		result.add(element2);
		result.add(element3);
		result.add(element4);
		result.add(element5);
		result.add(element6);
		
		return new ComboUtils("id", "descripcion").createSelecItems(result);
	}

	// combo de municipios
	public SelectItem[] getMunicipios() {
		if (municipios == null)
			this.municipios = this.makeEmptySelectItem();
		return municipios;
	}

	// combo de ciclos
	public SelectItem[] getCiclos() {
		if (this.ciclos == null)
			this.ciclos = this.makeEmptySelectItem();
		return this.ciclos;
	}

	// combo de zonas
	public SelectItem[] getZonas() {
		if (this.zonas == null)
			this.zonas = this.makeEmptySelectItem();
		return this.zonas;
	}

	// combo de grupos
	public SelectItem[] getGrupos() {
		if (this.grupos == null)
			this.grupos = this.makeEmptySelectItem();
		return this.grupos;
	}
	
//	 combo de tipos de documento
	public SelectItem[] getItemsTipoDocumento() {
		List result = new ArrayList();

		Map element0 = new HashMap();
		element0.put("id", "Adelanto de Cuotas de Convenio");
		element0.put("descripcion", "Adelanto de Cuotas de Convenio");

		Map element1 = new HashMap();
		element1.put("id", "Comprobante");
		element1.put("descripcion", "Comprobante");

		Map element2 = new HashMap();
		element2.put("id", "Comprobante de pago");
		element2.put("descripcion", "Comprobante de pago");

		Map element3 = new HashMap();
		element3.put("id", "Factura");
		element3.put("descripcion", "Factura");

		Map element4 = new HashMap();
		element4.put("id", "Factura Resumen");
		element4.put("descripcion", "Factura Resumen");
		
//		Map element5 = new HashMap();
//		element5.put("id", "CuotaManejo");
//		element5.put("descripcion", "Cuota de Manejo");

		result.add(element0);
		result.add(element1);
		result.add(element2);
		result.add(element3);
		result.add(element4);
//		result.add(element5);

		return new ComboUtils("id", "descripcion").createSelecItems(result);
	}

	public SelectItem[] getCiclosMunicipios() {
		if (this.ciclosMunicipios == null)
			this.ciclosMunicipios = this.makeEmptySelectItem();
		return this.ciclosMunicipios;

	}

	public void setCiclosMunicipios(SelectItem[] ciclosMunicipios) {
		this.ciclosMunicipios = ciclosMunicipios;
	}

	public SelectItem[] getGruposMunicipios() {
		if (this.gruposMunicipios == null)
			this.gruposMunicipios = this.makeEmptySelectItem();
		return this.gruposMunicipios;

	}

	public void setGruposMunicipios(SelectItem[] gruposMunicipios) {
		this.gruposMunicipios = gruposMunicipios;
	}

	public SelectItem[] getZonasMunicipios() {
		if (this.zonasMunicipios == null)
			this.zonasMunicipios = this.makeEmptySelectItem();
		return this.zonasMunicipios;

	}

	public void setZonasMunicipios(SelectItem[] zonasMunicipios) {
		this.zonasMunicipios = zonasMunicipios;
	}

	// getter and setter de las otras properties de la clase
	public QueryFilter getComboQueryFilter() {
		return comboQueryFilter;
	}

	public void setComboQueryFilter(QueryFilter comboQueryFilter) {
		this.comboQueryFilter = comboQueryFilter;
	}

	public String getServiceCombo() {
		return serviceCombo;
	}

	public void setServiceCombo(String serviceCombo) {
		this.serviceCombo = serviceCombo;
	}

	// setter de los combos
	public void setCiclos(SelectItem[] fooCicItems) {
		this.ciclos = fooCicItems;
	}

	public void setZonas(SelectItem[] fooZonItems) {
		this.zonas = fooZonItems;
	}

	public void setOrdenes(SelectItem[] ordenes) {
		this.ordenes = ordenes;
	}

	public void setSucursales(SelectItem[] sucursales) {
		this.sucursales = sucursales;
	}

	public void setDepartamentos(SelectItem[] departamentos) {
		this.departamentos = departamentos;
	}

	public void setGrupos(SelectItem[] grupos) {
		this.grupos = grupos;
	}

	public void setMunicipios(SelectItem[] municipios) {
		this.municipios = municipios;
	}

	public UIMainGroup getGroup() {
		return group;
	}

	public void setGroup(UIMainGroup group) {
		this.group = group;
	}



	public Object getQueryFilters() {
		return queryFilters;
	}

	public void setQueryFilters(Object queryFilters) {
		QueryFilter q = super.getQueryFilter();
		if (q!=null) {
			super.setQueryFilters(q);
			
		} else
		super.setQueryFilters(queryFilter);
		

	}



	public QueryFilter getQueryFilter() {
		QueryFilter q = super.getQueryFilter();
		if (q!=null) {
			super.setQueryFilters(q);
			return q;
		}
		super.setQueryFilters(queryFilter);
		return queryFilter;
	}


	public void setQueryFilter(QueryFilter queryFilter) {
		this.queryFilter = queryFilter;
	}

	public Object getServiceNames() {
		return serviceNames;
	}

	public void setServiceNames(Object serviceNames) {
		if (queryFilter != null) {
		super.setServiceNames(((CustomQueryFilter) queryFilter).getServiceName());
		this.serviceNames = ((CustomQueryFilter) queryFilter).getServiceName();
		} else {
			super.setServiceNames(serviceNames);
		this.serviceNames = serviceNames;
		}
	}


}