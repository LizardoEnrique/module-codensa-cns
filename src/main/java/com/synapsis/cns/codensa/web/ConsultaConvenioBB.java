package com.synapsis.cns.codensa.web;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.ajax4jsf.ajax.html.HtmlAjaxCommandButton;
import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.impl.ConsultaConvenioImpl;
import com.synapsis.integration.codensa.integration.kendari.URLHelper;

/**
 * Heredo de ListableBB para mantener la funcionalidad original, agrego la
 * accion para el boton que permite reimprimir el convenio.
 * 
 * @author svalls (ar31059727) - Modificacion por evolutivo SJCOD-538.
 */
public class ConsultaConvenioBB extends ListableBB {
	public static final String NOMBRE_CASO_DE_USO_REIMPRESION_CONVENIO = "ReimprimeContratoConvenioUseCaseFactory";

	public static final String NOMBRE_PARAMETRO_REIMPRESION_CONVENIO = "NAS_nroServicio";

	public static final String SEPARADOR_PARAMETROS_GET = "&";

	public static final String CONECTOR_PARAMETRO_VALOR_GET = "=";

	/**
	 * bean que se encarga de armar la url para navegar a kendari
	 */
	private URLHelper urlHelper;
	

	private String uri;
	private String errorMessages;
	private int cantConvenios;
	private HtmlAjaxCommandButton botonReimpresion;

	public void execKndUseCase(ActionEvent event) {
		
		String idButtonReimpresion = "formConsultaConvenio:kendariActionURL2";
		// Tomo el convenio selecccionado del BB certificaConvenio desde la
		// memoria. Si bien, no es lo mas
		// prolijo es lo que menos deforma el codigo original.
		ConsultaConvenioImpl convenioSeleccionado = (ConsultaConvenioImpl) VariableResolverUtils
				.getObject("certificaConvenioBB.convenio");
		Long numeroConvenioSeleccionado = convenioSeleccionado
				.getNroConvenio();
		if (numeroConvenioSeleccionado != null) {
			if (convenioSeleccionado.getEstado().equalsIgnoreCase("Aprobado") ||
					(convenioSeleccionado.getEstado().equalsIgnoreCase("Activo") 
							&& convenioSeleccionado.getValorCuotaInicial().doubleValue() == 0.0)) {
					String kendHelp = urlHelper
							.getUrlComplete(NOMBRE_CASO_DE_USO_REIMPRESION_CONVENIO);
					StringBuffer url = new StringBuffer();
					url.append(kendHelp).append(SEPARADOR_PARAMETROS_GET).append(
							NOMBRE_PARAMETRO_REIMPRESION_CONVENIO).append(
							CONECTOR_PARAMETRO_VALOR_GET).append(
							numeroConvenioSeleccionado);
					this.setUri(url.toString());
			}
			else
				this.errorMessages =
				MessageUtils.getMessage("cuenta.activa.con.cuota.inicial",null).getDetail();
			

		}
		else 
			this.errorMessages = 
				MessageUtils.getMessage("seleccione.un.convenio",null).getDetail();
	}

	public URLHelper getUrlHelper() {
		return urlHelper;
	}

	public void setUrlHelper(URLHelper urlHelper) {
		this.urlHelper = urlHelper;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the errorMessages
	 */
	public String getErrorMessages() {
		return errorMessages;
	}

	/**
	 * @param errorMessages the errorMessages to set
	 */
	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}
	
	public void buildScrolleableList() {
		String idButtonReimpresion;
		this.getMultiList().getUniqueList().buildScrolleableList();
		this.setCantConvenios(this.getMultiList().getUniqueList().getListResults().size());
		if(this.getCantConvenios()==0){ 
			this.errorMessages = "No se encontraron convenios para la Cuenta.";
			idButtonReimpresion = "formConsultaConvenio:kendariActionURL2";
			this.botonReimpresion = (HtmlAjaxCommandButton)JSFUtils.getComponentFromTree(idButtonReimpresion);
			if(this.botonReimpresion != null) {
				this.botonReimpresion.setDisabled(true);
			}
				
		}
		//else
			//this.errorMessages = "";
		
	}

	/**
	 * @return the cantConvenios
	 */
	public int getCantConvenios() {
		return cantConvenios;
	}

	/**
	 * @param cantConvenios the cantConvenios to set
	 */
	public void setCantConvenios(int cantConvenios) {
		this.cantConvenios = cantConvenios;
	}


	/**
	 * @return the botonReimpresion
	 */
	public HtmlAjaxCommandButton getBotonReimpresion() {
		return botonReimpresion;
	}


	/**
	 * @param botonReimpresion the botonReimpresion to set
	 */
	public void setBotonReimpresion(HtmlAjaxCommandButton botonReimpresion) {
		this.botonReimpresion = botonReimpresion;
	}


}