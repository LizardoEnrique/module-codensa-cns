/**
 * 
 */
package com.synapsis.cns.codensa.web;

import javax.faces.component.html.HtmlDataTable;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.impl.ConsultaHistoriaEstadosConvenioDetalleServicioImpl;
import com.synapsis.synergia.core.components.dataTable.ScrollableList;

/**
 * @author ar30557486
 *
 */
public class ConsultaHistoriaEstadosConvenioBB extends ListableBB {

	private static final long serialVersionUID = 1L;
	
	private Long nroServicio;
	private Long nroServicioSeleccionado;
	private ConsultaHistoriaEstadosConvenioDetalleServicioImpl consultaConvenioDetalleServicio;
	
		
	public void doFillDataTableAction() {
		HtmlDataTable dataTable = (HtmlDataTable)JSFUtils.getComponentFromTree("formHistoriaEstado:historiaEstadoTable");
		if (dataTable != null) {
			EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils.getObject("nroServicioQueryFilter");
			dataTable.setValue(new ScrollableList(queryFilter,super.getMultiList().getUniqueList().getServiceName()));
			dataTable.setFirst(0);
		}
	}
	
	
	/**
	 * Navegacion a pantalla de detalle
	 * @return String de navegacion
	 */
	public String detalleServicio() {
		return "detalleServicio";
	}


	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the nroServicioSeleccionado
	 */
	public Long getNroServicioSeleccionado() {
		return nroServicioSeleccionado;
	}
	/**
	 * @param nroServicioSeleccionado the nroServicioSeleccionado to set
	 */
	public void setNroServicioSeleccionado(Long nroServicioSeleccionado) {
		this.nroServicioSeleccionado = nroServicioSeleccionado;
	}


	/**
	 * @return the consultaConvenioDetalleServicio
	 */
	public ConsultaHistoriaEstadosConvenioDetalleServicioImpl getConsultaConvenioDetalleServicio() {
		return consultaConvenioDetalleServicio;
	}


	/**
	 * @param consultaConvenioDetalleServicio the consultaConvenioDetalleServicio to set
	 */
	public void setConsultaConvenioDetalleServicio(
			ConsultaHistoriaEstadosConvenioDetalleServicioImpl consultaConvenioDetalleServicio) {
		this.consultaConvenioDetalleServicio = consultaConvenioDetalleServicio;
	}
	
}