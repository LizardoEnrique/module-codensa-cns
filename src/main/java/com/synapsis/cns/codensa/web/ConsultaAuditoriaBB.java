package com.synapsis.cns.codensa.web;

import java.util.List;
import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

public class ConsultaAuditoriaBB {
	private String usuario;
	private String mesEmision;
	private Long nroServicio;
	private Long nroFactura;
	private Long nroCuenta;
	
	private List auditoria;
	private UIService consultaAuditoriaFacturacionService;
	private HtmlDataTable dataTableAuditoria;
	
	
	public void search(){
		this.getConsultaAuditoriaFacturacionService().execute();
		this.getDataTableAuditoria().setFirst(0);
	}
	

	/**
	 * @return the consultaAuditoriaFacturacionService
	 */
	public UIService getConsultaAuditoriaFacturacionService() {
		return consultaAuditoriaFacturacionService;
	}


	/**
	 * @param consultaAuditoriaFacturacionService the consultaAuditoriaFacturacionService to set
	 */
	public void setConsultaAuditoriaFacturacionService(
			UIService consultaAuditoriaFacturacionService) {
		this.consultaAuditoriaFacturacionService = consultaAuditoriaFacturacionService;
	}


	/**
	 * @return the dataTableAuditoria
	 */
	public HtmlDataTable getDataTableAuditoria() {
		return dataTableAuditoria;
	}


	/**
	 * @param dataTableAuditoria the dataTableAuditoria to set
	 */
	public void setDataTableAuditoria(HtmlDataTable dataTableAuditoria) {
		this.dataTableAuditoria = dataTableAuditoria;
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return the mesEmision
	 */
	public String getMesEmision() {
		return mesEmision;
	}
	/**
	 * @param mesEmision the mesEmision to set
	 */
	public void setMesEmision(String mesEmision) {
		this.mesEmision = mesEmision;
	}
	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}
	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}
	/**
	 * @return the nroFactura
	 */
	public Long getNroFactura() {
		return nroFactura;
	}
	/**
	 * @param nroFactura the nroFactura to set
	 */
	public void setNroFactura(Long nroFactura) {
		this.nroFactura = nroFactura;
	}
	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}
	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	/**
	 * @return the auditoria
	 */
	public List getAuditoria() {
		return auditoria;
	}
	/**
	 * @param auditoria the auditoria to set
	 */
	public void setAuditoria(List auditoria) {
		this.auditoria = auditoria;
	}

}
