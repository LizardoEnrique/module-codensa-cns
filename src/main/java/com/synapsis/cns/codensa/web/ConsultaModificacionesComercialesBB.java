/**
 * $Id: ConsultaModificacionesComercialesBB.java,v 1.1 2008/07/28 20:14:42 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

/**
 * @author Paola Attadio (se agrego seleccion en caso de invocación directa desde kendari)
 *
 */
public class ConsultaModificacionesComercialesBB extends ListableBB{
	
	private Long numeroCuentaQueryFilter;
	

	public ConsultaModificacionesComercialesBB (){
		
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			this.numeroCuentaQueryFilter = new Long(parameter.toString());
			queryFilter.setAttributeValue(this.numeroCuentaQueryFilter);
			
		} else 
			this.numeroCuentaQueryFilter = (Long) queryFilter.getAttributeValue();
	}


	public Long getNumeroCuentaQueryFilter() {
		return numeroCuentaQueryFilter;
	}


	public void setNumeroCuentaQueryFilter(Long numeroCuentaQueryFilter) {
		this.numeroCuentaQueryFilter = numeroCuentaQueryFilter;
	}
}
