package com.synapsis.cns.codensa.web;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;
import org.apache.myfaces.shared_tomahawk.util.MessageUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import com.suivant.arquitectura.core.exception.ExporterException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.CompositeQueryFilter;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
//import com.suivant.arquitectura.presentation.common.ExportPdf;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.AuditoriaFacturacion;
import com.synapsis.cns.codensa.model.CargoFactura;
import com.synapsis.cns.codensa.model.ReporteAnexoFactura;
import com.synapsis.cns.codensa.model.ReporteFactura;
import com.synapsis.cns.codensa.model.ReporteFacturaComun;
import com.synapsis.cns.codensa.model.ReporteFacturaDuplicadoLegal;
import com.synapsis.cns.codensa.model.ReporteServicioAseo;
import com.synapsis.cns.codensa.model.ServicioElectrico;
import com.synapsis.cns.codensa.model.impl.ConsumosUltimos6MesesImpl;
import com.synapsis.cns.codensa.model.impl.DetalleDatosCuentaImpl;
import com.synapsis.cns.codensa.model.impl.MotivoImpl;
import com.synapsis.cns.codensa.model.impl.ReporteFacturaImpl;
import com.synapsis.cns.codensa.model.impl.ReporteServicioAseoImpl;
import com.synapsis.cns.codensa.service.ReporteAnexoFacturaService;
import com.synapsis.cns.codensa.service.ReporteFacturaService;
import com.synapsis.cns.codensa.util.ExportPdf;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.commons.types.datetime.CalendarDate;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.presentation.model.NasServiceHelper;



/**
 * @author adambrosio
 * 
 * Modificado Lizardo Mamani - Ayesa - 29/09/2017
 */
public class ImpresionFacturaBB {
	
	Logger LOG = Logger.getLogger(ImpresionFacturaBB.class);
	private static final String PERMITIDAS = "max. permitidas";

	private static final String ACUMULADAS = "acumuladas";

	private Long copias;

	private String observacion;

	private Long cli;

	private Long nroCuenta;

	private UIService findPeriodosService;

	private HtmlDataTable dataTablePeriodos;

	private List periodosListResult;

	private List motivos;

	private ReporteFacturaService reporteFacturaService;

	private ReporteAnexoFacturaService reporteAnexoFacturaService;

	private FinderService servicioFinder;

	private String uri;

	// valor de la fecha del combo
	//
	/**
	 * afalduto: este valor pasa por el dataTime converter de faces, por lo tanto nunca retorna un
	 * com.synapsis.commons.types.datetime.CalendarDate como esta mapeado en el periodo. Lo que se podria
	 * hacer es definir un conversor para este tipo de datos en el caso de hacer falta, io subi una impl muy
	 * peque�a que no se si funca ... pero ya ta configurada para hacerla andar. Nota: en el
	 * imprimirFactura.xhtml vamos a ver un conversor con esta forma <f:convertDateTime type="both" />,
	 * basicamente el type both lo que hace es generar un date que sepa manejar un timestamp (por llamarlo de
	 * alguna forma).
	 * 
	 * @uml.property name="fechaProceso"
	 */
	private Date fechaProceso;

	private String periodoFacturacion;

	private Long motivo;

	// Los par�metros del reporte que est�n en cns-jasper-map.xml
	Map map;
	// Los par�metros del reporte que est�n en cns-jasper-map.xml
	Map mapDuplicadoLegal;
	// Los par�metros del reporte Anexo que est�n en cns-jasper-map.xml
	Map mapAnexo;
	/******Lizardo Mamani *****/
	// Los parametros del reporte ASEO
	Map map1;

	private Long idDocumento;

	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	// ******************************************************
	// metodos expuestos a presentacion
	// *****************************************************
	public void search() {
		if (this.periodoFacturacion != null && this.periodoFacturacion.length() > 0
			&& !isPeriodo(this.periodoFacturacion)) {
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("fac.impresion.periodo", new Object[] { (this.periodoFacturacion) }));
		}
		else {
			this.getFindPeriodosService().execute();
			this.getDataTablePeriodos().setFirst(0);
		}
	}

	public void init() {
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
		String clean = request.getParameter("action");
		if (StringUtils.equalsIgnoreCase(clean, "clean")) {
			this.cleanFields();
		}
	}

	private void cleanFields() {
		this.observacion = null;
		this.cli = null;
		this.periodoFacturacion = null;
		this.copias = null;
		this.nroCuenta = null;
	}

	/**
	 * PARA LA IMPRESION DE FACTURA (imprimirFactura.xhtml)<BR>
	 * <B>M�DULO CNS - ��� NO es el Duplicado LEGAL de Factura !!! NO es copia fiel de la Factura</B>
	 * 
	 * @param event
	 */
	public void imprimir(ActionEvent event) {

		ReporteFacturaImpl reporteFactura = (ReporteFacturaImpl) NasServiceHelper.getResponse(
			"impresionFacturaService", "findByCriteriaUnique", new Object[] { this.getIdDocumento() });
		LOG.info("Entro a Imprimir");
		LOG.info("Cantidad en consumo 6 meses: " + reporteFactura.getConsumosUltimos6Meses().size());
		LOG.info("Cantidad en consumo 6  ASEO: " + reporteFactura.getConsumosServicioAseo().size());
		LOG.info("el id documento es: " + this.getIdDocumento());
		Integer b = reporteFactura.getConsumosServicioAseo().size();
		if ("N".equals(reporteFactura.getDuplicado())) {
			FacesContext.getCurrentInstance().addMessage(
				null,
				MessageUtils.getMessage("fac.impresion.duplicado.error.marca", new Object[] { (reporteFactura
					.getDesMarca()) }));
			return;
		}
		else {
			int i1 = reporteFactura.getCargos().size();

			// ANS-169, SOLO QUIEREN LOS CARGOS. Dejo todo como esta por si quieren volver atras...cosa
			// muy probable
			reporteFactura.getOtrosCargos().clear();
			reporteFactura.getCargosDescuento().clear();
			reporteFactura.getCargosOtrosServPort().clear();

			int i2 = reporteFactura.getOtrosCargos().size();
			int i3 = reporteFactura.getCargosDescuento().size();
			int i4 = reporteFactura.getCargosOtrosServPort().size();

			// HARCODE PORQUE EXPLOTA EL REPORTE POR OVERFLOW DE BANDA:
			// SE MUESTRAN LOS SUBTOTALES UNICAMENTE Y NO LOS DETALLES
			// GRACIAS POR HACER T O D O EN EL TITLE!!!!!
			if (i1 + i2 + i3 + i4 > 22) {
				this.clearItems(reporteFactura.getCargos());
				this.clearItems(reporteFactura.getOtrosCargos());
				this.clearItems(reporteFactura.getCargosDescuento());
				this.clearItems(reporteFactura.getCargosOtrosServPort());
			}

			List listParaEnviarAJasperReports = new ArrayList();
			listParaEnviarAJasperReports.add(reporteFactura);

			reporteFactura.cargarLaManija();
			if(b>0){
				this.addGenericParameters(this.getMap());
				generarGraficosReporte(reporteFactura,this.getMap());
			}else{
				this.addGenericParameters(this.getMap1());
				generarGraficosReporte(reporteFactura,this.getMap1());
			}
			try {
				ExportPdf.generatePdf(listParaEnviarAJasperReports, (b>0)?getMap():getMap1());
				this.init();
			}
			catch (ExporterException e) {
				ErrorsManager.addInfoError(e.getMessage());
			}
		}
	}

	private Map addGenericParameters(Map maperoInicial) {
		Map map = maperoInicial;
		String company = SynergiaApplicationContext.getCurrentImplementationCompany();
		
		map.put("empresa", company);		
		map.put("TEXTO_01", "CODENSA S.A. ESP  NIT.: 830.037.248-0");
		map.put("TEXTO_02", company.equals(SynergiaApplicationContext.CODENSA)
			? "Cra. 13A No. 93-66"
			: "Cra. 11A No. 93-52 Piso 5");
		map.put("TEXTO_03", company.equals(SynergiaApplicationContext.CODENSA)
			? "www.CODENSA.com.co"
			: "www.EEC.com.co");
		map
			.put(
				"TEXTO_04",
				company.equals(SynergiaApplicationContext.CODENSA)
					? "Somos agentes retenedores de IVA e ICA. Somos grandes contribuyentes seg�n Res. No. 3371 de diciembre 1997. Somos autorretenedores seg�n Res. No. 3305 de Diciembre de 1997."
					: "Somos grandes contribuyentes seg�n Res. No. 3371 de diciembre de 1997. Somos autorretenedores seg�n Res. No. 3305 de diciembre de 1997.");
		
		map
			.put(
				"TEXTO_05",
				company.equals(SynergiaApplicationContext.CODENSA)
					? "CUALQUIER INQUIETUD COMUNICARSE A NUESTRA LINEA DE ATENCION. EN BOGOTA FONOSERVICIO 115 � CUNDINAMARCA 01-8000-912-115 � 4-19-72-72"
					: "CUALQUIER INQUIETUD COMUNICARSE A NUESTRO FONOSERVICIO L�NEA GRATUITA 01-8000-912-115 O A LA L�NEA FIJA (1) 5 115 115");
		map
			.put(																			 
				"TEXTO_06",
				company.equals(SynergiaApplicationContext.CODENSA)
					? "IVA r�gimen com�n. CUMPLIMIENTO RESOLUCI�N CREG 015 DE 1999. VIGILADO SUPERINTENDENCIA DE SERVICIOS P�BLICOS NIT 830.037.248-0"
					: "Vigilada Superservicios");
		map
		.put(
			"TEXTO_07",
			company.equals(SynergiaApplicationContext.CODENSA)
				? "SI LA FECHA DE PAGO ESTA VENCIDA PAGA EN CUALQUIERA DE LOS SIGUIENTES PUNTOS" + "                                    " +
						"CALLE 80: Calle 80 No. 89 15" + " 		    " + 
						"GALER�AS: Calle 53 No. 22 53" + "	 	    " + 
						"SAN DIEGO: Cra. 7 No. 32 84" + "	 	    " +
						"SUBA: Tr. 60 No. 128A 26" + "	 	   " +
						"SUBA RINC�N: Tr. 91 No. 127 10" + "	     	" +
						"SOACHA DESPENSA: Calle 12 No. 9 05" + "   	 	" +
						"VENECIA: Autopista Sur No. 54A 07" + "	    	    " +
						"SANTA LIBRADA: Calle 70 No. 00 79 sur" + "	 	    " +
						"KENNEDY: Cra 78B No. 38B 30 sur" + "	     	" +	
						"RESTREPO: Cra 21 No. 15 28 SUR"
				: "");
		return map;
	}

	private void clearItems(Collection col) {
		CargoFactura nullCargoFactura = null;

		for (Iterator iter = col.iterator(); iter.hasNext();) {
			CargoFactura cargoFactura = (CargoFactura) iter.next();
			cargoFactura.setCodigo("");
			cargoFactura.setConcepto("");
			cargoFactura.setValor(null);
			nullCargoFactura = cargoFactura;
		}

		if (col.size() > 0) {
			col.clear();
			col.add(nullCargoFactura);
		}
	}

	public void testDuplicadoLegal(ActionEvent event) {
		ReporteFacturaDuplicadoLegal reporteFactura = reporteFacturaService.getReporteFacturaForTest();
		copias = new Long(3);
		this.imprimirReporte(reporteFactura);
	}

	/**
	 * Llamado desde la IMPRESI�N DEL DUPLICADO LEGAL DE FACTURA (imprimeDuplicadoLegalFactura.xhtml)<BR>
	 * <B>MODULO COBRANZAS (NO CNS) - Es copia FIEL de la Factura emitida en su momento</B>
	 * 
	 * @param event
	 */
	public void imprimirReporteFactura(ActionEvent event) {
		if (!isDocumentoMigrado()) {
			if (this.copias.intValue() > 3) {
				FacesContext.getCurrentInstance().addMessage(null,
					MessageUtils.getMessage("fac.impresion.cantCopias", new Object[] { (copias) }));
			}
			else {

				QueryFilter qf = new EqualQueryFilter("nroCuenta", this.nroCuenta);
				ServicioElectrico servicio = null;
				try {
					servicio = (ServicioElectrico) servicioFinder.findByCriteriaUnique(qf);
				}
				catch (ObjectNotFoundException e1) {
					ErrorsManager.addInfoError(e1.getMessage());
				}

				CalendarDate fechaProcesoSinHora = (CalendarDate) this.getFechaProceso();

				// Esta forma alternativa inyectando el servicio en el bean en
				// faces-config-managed-bean.xml, funciona.
				Map parameters = new HashMap();

				Date fechaProcesoSinHora2 = (Date) this.getFechaProceso();

				parameters.put("nroCuenta", this.nroCuenta);
				parameters.put("fechaProceso", fechaProcesoSinHora2);

				ReporteFacturaDuplicadoLegal reporteFactura = obtenerReporteFacturaFromLineaFactura(fechaProcesoSinHora);

				if (reporteFactura != null) {
					int i1 = reporteFactura.getOtrosCargos().size();
					int i2 = reporteFactura.getCargosDescuento().size();
					int i3 = reporteFactura.getCargosServicioElectrico().size();
					int i4 = reporteFactura.getCargosOtrosServPort().size();

					if (i1 > 9) {
						reporteFactura.getOtrosCargos().clear();
					}
					if (i2 > 2) {
						reporteFactura.getCargosDescuento().clear();
					}
					if (i3 > 4) {
						reporteFactura.getCargosServicioElectrico().clear();
					}
					if (i4 > 7) {
						reporteFactura.getCargosOtrosServPort().clear();
					}

					this.imprimirReporte(reporteFactura);
					this.generarAuditoria(reporteFactura, servicio);
				}
				else {
					FacesContext.getCurrentInstance().addMessage(
						null,
						MessageUtils.getMessage("fac.impresionDuplicadoFactura.no.hay.datos",
							new Object[] { (fechaProcesoSinHora.toString()) }));
				}

			}
		}
		else {
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("fac.impresion.documento.migrado.no.imprime", null));
		}
	}

	private void imprimirReporte(ReporteFacturaDuplicadoLegal reporteFactura) {
		List listParaEnviarAJasperReports = new ArrayList();
		generarGraficosReporteDuplicadoLegal(reporteFactura, this.getMapDuplicadoLegal());

		listParaEnviarAJasperReports.add(reporteFactura);
		for (int i = 1; i < this.copias.intValue(); i++) {
			listParaEnviarAJasperReports.add(reporteFactura);
		}

		getMapDuplicadoLegal().put("reportesAImprimir", listParaEnviarAJasperReports);
		try {
			ExportPdf.generatePdf(listParaEnviarAJasperReports, getMapDuplicadoLegal());
		}
		catch (ExporterException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	private ReporteFacturaDuplicadoLegal obtenerReporteFacturaFromLineaFactura(CalendarDate fechaProcesoSinHora) {
		ReporteFacturaDuplicadoLegal reporteFactura = null;
		CompositeQueryFilter compo = this.createCompositeFilter(fechaProcesoSinHora);
		try {
			reporteFactura = reporteFacturaService.getReporteFactura(compo);
		}
		catch (ObjectNotFoundException e) {
			FacesContext.getCurrentInstance().addMessage(
				null,
				MessageUtils.getMessage("fac.impresionDuplicadoFactura.no.hay.datos",
					new Object[] { (fechaProcesoSinHora.toString()) }));
		}
		return reporteFactura;
	}

	private CompositeQueryFilter createCompositeFilter(CalendarDate fechaProcesoSinHora) {
		CompositeQueryFilter compo = new CompositeQueryFilter();
		compo.addFilter(new EqualQueryFilter("nroCuenta", this.nroCuenta));
		compo.addFilter(new EqualQueryFilter("fechaFactura", fechaProcesoSinHora));
		return compo;
	}

	private boolean isDocumentoMigrado() {
		return this.getCli().longValue() > 0;
	}

	/**
	 * obtiene los datos y genera el registro de auditoria
	 * 
	 * @param reporteFactura
	 */
	private void generarAuditoria(ReporteFacturaComun reporteFactura, ServicioElectrico servicio) {
		AuditoriaFacturacion audit = (AuditoriaFacturacion) NasServiceHelper.getResponse("auditoriaFacturacionService",
			"createNewInstance", new Object[] {});
		// Obtengo el usuario logeado
		String userName = "";
		// try and catch only for test
		try {
			userName = (String)SynergiaApplicationContext.getCurrentUser().getUsername();
		}
		catch (Exception e) {
			userName = "codensa";
		}
		audit.setUsuarioNombre(userName);
		audit.setNroCuenta(this.nroCuenta);
		audit.setPeriodo(this.periodoFacturacion);
		if (this.motivo != null) {
			MotivoImpl motivo = new MotivoImpl();
			motivo.setId(this.motivo);
			audit.setMotivo(motivo);
		}
		if (servicio != null) {
			audit.setIdServicio(servicio.getIdServicio());
			audit.setIdCuenta(servicio.getId());
			audit.setNroServicio(servicio.getNumeroServicio());
		}
		else {
			// recupero el id de cuenta
			Map parametro = new HashMap();
			parametro.put("nroCuenta", this.nroCuenta);
			DetalleDatosCuentaImpl cuenta = (DetalleDatosCuentaImpl) NasServiceHelper.getResponse(
				"detalleDatosCuentaService", "findByCriteriaUnique", parametro);
			audit.setIdCuenta(cuenta.getId());
		}
		audit.setNroFactura(reporteFactura.getNroFactura());
		audit.setCantCopias(this.getCopias());
		audit.setFecha(DateUtils.truncate(new Date(), Calendar.DATE));
		audit.setObservacion(this.getObservacion());
		Map parameters = new HashMap();
		parameters.put("objectToSaveOrUpdate", audit);
		NasServiceHelper.getResponse("auditoriaFacturacionService", "saveOrUpdate", parameters);
	}

	/**
	 * Llamado desde la IMPRESI�N DEL DUPLICADO LEGAL DE FACTURA (imprimeDuplicadoLegalFactura.xhtml)<BR>
	 * <B>MODULO COBRANZAS (NO CNS)</B>
	 * 
	 * @param event
	 */
	public void imprimirReporteAnexoFactura(ActionEvent event) {
		if (!isDocumentoMigrado()) {

			CalendarDate fechaProcesoSinHora = (CalendarDate) this.getFechaProceso();

			// Esta forma alternativa inyectando el servicio en el bean en
			// faces-config-managed-bean.xml, funciona.
			// Luego de la entrega lo arreglamos y acomodamos a AME.
			ReporteAnexoFactura reporteAnexoFactura;
			try {
				CompositeQueryFilter compo = createCompositeFilter(fechaProcesoSinHora);
				reporteAnexoFactura = this.getReporteAnexoFacturaService().getReporteAnexoFactura(compo);
				if (reporteAnexoFactura != null) {
					List listParaEnviarAJasperReports = new ArrayList();
					listParaEnviarAJasperReports.add(reporteAnexoFactura);

					// generarGraficosReporte(reporteFactura);
					try {
						ExportPdf.generatePdf(listParaEnviarAJasperReports, getMapAnexo());
					}
					catch (ExporterException e) {
						ErrorsManager.addInfoError(e.getMessage());
					}
					catch (NullPointerException e) {
						e.printStackTrace();
					}
					// this.generarAuditoria(reporteFactura);
				}
				else {
					FacesContext.getCurrentInstance().addMessage(
						null,
						MessageUtils.getMessage("fac.impresionDuplicadoFactura.no.hay.datos",
							new Object[] { (fechaProcesoSinHora.toString()) }));
				}
			}
			catch (ObjectNotFoundException e1) {
				FacesContext.getCurrentInstance().addMessage(null,
					MessageUtils.getMessage("fac.impresion.no.hay.facturas", null));
			}
		}
		else {
			FacesContext.getCurrentInstance().addMessage(null,
				MessageUtils.getMessage("fac.impresion.documento.migrado.no.imprime", null));
		}
	}

	// *******************************************************************
	// ** metodos privados para la generacion del grafico
	// *******************************************************************
	private void generarGraficosReporte(ReporteFacturaComun reporteFactura, Map parametros) {
		parametros.put("graficoConsumos", crearGraficoConsumos(reporteFactura));
		parametros.put("graficoConsumosAseo", crearGraficoConsumosAseo(reporteFactura));
		parametros.put("graficoHorasInterrumpidas", crearGraficoHorasInterrumpidas(reporteFactura, false));
		parametros.put("graficoNumeroInterrupciones", crearGraficoNumeroInterrupciones(reporteFactura, false));
		/***********A�adiendo meses*************/
		Object[] l = reporteFactura.getConsumosServicioAseo().toArray();
		Map<Object, Object> reporte = new HashMap();
		for(Object o:l){
	    	LOG.info("Entro a for de consumo aseo");
	    	ReporteServicioAseoImpl r = (ReporteServicioAseoImpl)o;
	    	LOG.info(r.getDireccion());
	    	LOG.info(r.getPuntoatencion());
	    	LOG.info(r.getNit());
	    	LOG.info(r.getDireccionweb());
	    	LOG.info(r.getBarrido());
	    	LOG.info(r.getRecoleccion());
	    	LOG.info(r.getDensidad());
	    	LOG.info(r.getVolumen());
	    	LOG.info(r.getParticipacion());
	    	reporte.put("mes1",(r.getMes_uno()!=null && r.getMes_uno().length()>0)?Double.valueOf(r.getMes_uno()):0.0);
	    	reporte.put("mes2",(r.getMes_dos()!=null && r.getMes_dos().length()>0)?Double.valueOf(r.getMes_dos()):0.0);
	    	reporte.put("mes3",(r.getMes_tres()!=null && r.getMes_tres().length()>0)?Double.valueOf(r.getMes_tres()):0.0);
	    	reporte.put("mes4",(r.getMes_cuatro()!=null && r.getMes_cuatro().length()>0)?Double.valueOf(r.getMes_cuatro()):0.0);
	    	reporte.put("mes5",(r.getMes_cinco()!=null && r.getMes_cinco().length()>0)?Double.valueOf(r.getMes_cinco()):0.0);
	    	reporte.put("mes6",(r.getMes_seis()!=null && r.getMes_seis().length()>0)?Double.valueOf(r.getMes_seis()):0.0);
	    	reporte.put("direccion",(r.getDireccion()!=null)?r.getDireccion():"");
	    	reporte.put("puntosatencion",(r.getPuntoatencion()!=null && r.getPuntoatencion().length()>0)?r.getPuntoatencion():"");
	    	reporte.put("operador",(r.getNombre()!=null && r.getNombre().length()>0)?r.getNombre():"");
	    	reporte.put("nit",(r.getNit()!=null && r.getNit().length()>0)?r.getNit():"");
	    	reporte.put("paginaweb",(r.getDireccionweb()!=null && r.getDireccionweb().length()>0)?r.getDireccionweb():"");
	    	reporte.put("barrido",(r.getBarrido()!=null && r.getBarrido().length()>0)?r.getBarrido():"");
	    	reporte.put("recoleccion",(r.getRecoleccion()!=null && r.getRecoleccion().length()>0)?r.getRecoleccion():"");
	    	reporte.put("densidad",(r.getDensidad()!=null && r.getDensidad().length()>0)?r.getDensidad():"");
	    	reporte.put("volumen",(r.getVolumen()!=null && r.getVolumen().length()>0)?r.getVolumen():"");
	    	reporte.put("parti",(r.getParticipacion()!=null && r.getParticipacion().length()>0)?r.getParticipacion():"");
	    	
		}	
		
		for(Map.Entry<Object,Object> d: reporte.entrySet()){
			parametros.put(d.getKey(), d.getValue());
		}
	}

	private void generarGraficosReporteDuplicadoLegal(ReporteFacturaComun reporteFactura, Map parametros) {
		parametros.put("graficoConsumos", crearGraficoConsumos(reporteFactura));
		parametros.put("graficoHorasInterrumpidas", crearGraficoHorasInterrumpidas(reporteFactura, true));
		parametros.put("graficoNumeroInterrupciones", crearGraficoNumeroInterrupciones(reporteFactura, true));
	}

	private BufferedImage crearGraficoConsumosAseo(ReporteFacturaComun rf) {
		DefaultCategoryDataset defaultCategoryDataset = null;
		final String CONSUMO = "consumoAseo";
			LOG.info("Entr� a consumo aseo");
			defaultCategoryDataset = new DefaultCategoryDataset();
	
			Object[] l = rf.getConsumosServicioAseo().toArray();
		    
		    for(Object o:l){
		    	LOG.info("Entro a for de consumo aseo");
		    	ReporteServicioAseoImpl r = (ReporteServicioAseoImpl)o;
		    	Double mes1 = (r.getMes_uno()!=null && r.getMes_uno().length()>0)?Double.valueOf(r.getMes_uno()):0.0;
		    	Double mes2 = (r.getMes_dos()!=null && r.getMes_dos().length()>0)?Double.valueOf(r.getMes_dos()):0.0;
		    	Double mes3 = (r.getMes_tres()!=null && r.getMes_tres().length()>0)?Double.valueOf(r.getMes_tres()):0.0;
		    	Double mes4 = (r.getMes_cuatro()!=null && r.getMes_cuatro().length()>0)?Double.valueOf(r.getMes_cuatro()):0.0;
		    	Double mes5 = (r.getMes_cinco()!=null && r.getMes_cinco().length()>0)?Double.valueOf(r.getMes_cinco()):0.0;
		    	Double mes6 = (r.getMes_seis()!=null && r.getMes_seis().length()>0)?Double.valueOf(r.getMes_seis()):0.0;
		    	
		    	defaultCategoryDataset.addValue(mes1, CONSUMO,"Mes 1");
				defaultCategoryDataset.addValue(mes2, CONSUMO,"Mes 2");
				defaultCategoryDataset.addValue(mes3, CONSUMO,"Mes 3");
				defaultCategoryDataset.addValue(mes4, CONSUMO,"Mes 4");
				defaultCategoryDataset.addValue(mes5, CONSUMO,"Mes 5");
				defaultCategoryDataset.addValue(mes6, CONSUMO,"Mes 6");
				
		    }

			// create a chart with the dataset
			JFreeChart chart = ChartFactory.createBarChart(null, // Title
			null, // X-Axis label
			null, // Y-Axis label
			defaultCategoryDataset, // Dataset
			PlotOrientation.VERTICAL, // Orientation
			false, // Show legend
			false, // Tooltip
			false // URLs
			);

		// COMENTO ESTA LINEA PORQUE AL FINAL NO HIZO FALTA, PERO DEJARLA AS� SE SABE C�MO HACERLO!
		// this.cambiarFuentePeriodos(chart);
		return chart.createBufferedImage(468, 84);
	}
	
	
	private BufferedImage crearGraficoConsumos(ReporteFacturaComun rf) {
		DefaultCategoryDataset defaultCategoryDataset = null;
		final String CONSUMO = "Consumo";

		if (rf.getConsumosUltimos6Meses().size() > 0) {
			defaultCategoryDataset = new DefaultCategoryDataset();
			Double kwh = null;
			String periodo = null;
			for (Iterator i = rf.getConsumosUltimos6Meses().iterator(); i.hasNext();) {
				ConsumosUltimos6MesesImpl consumo = (ConsumosUltimos6MesesImpl) i.next();
				kwh = consumo.getCons_KWH();
				periodo = consumo.getMes() != null ? consumo.getMes().trim() : consumo.getMes();

				defaultCategoryDataset.addValue(kwh, CONSUMO, periodo);
			}

			// kwh y periodo tienen la info del �ltimo per�odo (el actual)
			defaultCategoryDataset.addValue(kwh, CONSUMO, periodo);
		}

		// create a chart with the dataset
		JFreeChart chart = ChartFactory.createBarChart(null, // Title
			null, // X-Axis label
			null, // Y-Axis label
			defaultCategoryDataset, // Dataset
			PlotOrientation.VERTICAL, // Orientation
			false, // Show legend
			false, // Tooltip
			false // URLs
			);

		// COMENTO ESTA LINEA PORQUE AL FINAL NO HIZO FALTA, PERO DEJARLA AS� SE SABE C�MO HACERLO!
		// this.cambiarFuentePeriodos(chart);
		return chart.createBufferedImage(468, 84);
	}

	/**
	 * Cambia el tama�o de la fuente y el color de las columnas <BR>
	 * 
	 * @param chart
	 */
	private void cambiarFuentePeriodos(JFreeChart chart) {
		CategoryPlot categoryPlot = chart.getCategoryPlot();
		categoryPlot.getDomainAxis().setTickLabelFont(
			categoryPlot.getDomainAxis().getTickLabelFont().deriveFont((float) 10));

		BarRenderer renderer = (BarRenderer) categoryPlot.getRenderer();

		// set up gradient paints for series...
		GradientPaint gp = new GradientPaint(0.0f, 0.0f, Color.WHITE, 0.0f, 0.0f, Color.WHITE);
		renderer.setSeriesPaint(0, gp);
		renderer.setSeriesPaint(1, gp);

	}

	private BufferedImage crearGraficoHorasInterrumpidas(ReporteFacturaComun rfc, boolean duplicadoLegal) {
		DefaultCategoryDataset defaultCategoryDataset = null;
		Number maxHoras = new Double("0");
		Number cantHoras = new Double("0");

		if (rfc instanceof ReporteFactura) {
			ReporteFactura rf = (ReporteFactura) rfc;
			if (rf.getInfoCons() != null) {
				maxHoras = rf.getInfoCons().getMaximoPermitidoHoras();
				cantHoras = rf.getInfoCons().getNumeroHorasInterrupcionesAcumuladas();
			}
		}
		else if (rfc instanceof ReporteFacturaDuplicadoLegal) {
			ReporteFacturaDuplicadoLegal rfdl = (ReporteFacturaDuplicadoLegal) rfc;
			maxHoras = new Double(rfdl.getMaximoPermitidoHoras());
			cantHoras = new Double(rfdl.getNumeroHorasInterrupcionesAcumuladas());
		}

		defaultCategoryDataset = crearDataSetNumeroInterrupciones(maxHoras, cantHoras, "Horas Interrumpidas");

		return crearGraficoInterrupciones(defaultCategoryDataset, duplicadoLegal);
	}

	private BufferedImage crearGraficoNumeroInterrupciones(ReporteFacturaComun rfc, boolean duplicadoLegal) {
		DefaultCategoryDataset defaultCategoryDataset = null;

		Number maxInt = new Double("0");
		Number cantInt = new Double("0");

		if (rfc instanceof ReporteFactura) {
			ReporteFactura rf = (ReporteFactura) rfc;
			if (rf.getInfoCons() != null) {
				maxInt = rf.getInfoCons().getMaximoPermitidoInterrupciones();
				cantInt = rf.getInfoCons().getNumeroInterrupcionesAcumuladas();
			}
		}
		else if (rfc instanceof ReporteFacturaDuplicadoLegal) {
			ReporteFacturaDuplicadoLegal rfdl = (ReporteFacturaDuplicadoLegal) rfc;
			maxInt = new Double(rfdl.getMaximoPermitidoInterrupciones());
			cantInt = new Double(rfdl.getNumeroInterrupcionesAcumuladas());
		}

		defaultCategoryDataset = crearDataSetNumeroInterrupciones(maxInt, cantInt, "N�mero de Interrupciones");

		return crearGraficoInterrupciones(defaultCategoryDataset, duplicadoLegal);
	}

	private DefaultCategoryDataset crearDataSetNumeroInterrupciones(Number max, Number acum, String label) {
		DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
		defaultCategoryDataset.addValue(max, label, PERMITIDAS);
		defaultCategoryDataset.addValue(acum, label, ACUMULADAS);

		return defaultCategoryDataset;
	}

	private BufferedImage crearGraficoInterrupciones(DefaultCategoryDataset defaultCategoryDataset,
			boolean duplicadoLegal) {
		// create a chart with the dataset
		JFreeChart chart = ChartFactory.createBarChart(null, // Title
			null, // X-Axis label
			null, // Y-Axis label
			defaultCategoryDataset, // Dataset
			PlotOrientation.VERTICAL, // Orientation
			false, // Show legend
			false, // Tooltip
			false // URLs
			);

		if (duplicadoLegal) {
			chart.setBackgroundPaint(Color.white);
			this.cambiarFuentePeriodos(chart);
			return chart.createBufferedImage(300, 108);
		}

		return chart.createBufferedImage(228, 108);
	}

	private boolean isPeriodo(String periodo) {
		return Pattern.matches("\\d\\d/\\d\\d\\d\\d", periodo);
	}

	// ***************************************************
	// getters & setters
	// ********************************************
	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public Map getMap1() {
		return map1;
	}

	public void setMap1(Map map1) {
		this.map1 = map1;
	}

	public Map getMapAnexo() {
		return this.mapAnexo;
	}

	public void setMapAnexo(Map mapAnexo) {
		this.mapAnexo = mapAnexo;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Long getCopias() {
		return this.copias;
	}

	public void setCopias(Long copias) {
		this.copias = copias;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	/**
	 * @return the cli
	 */
	public Long getCli() {
		return cli;
	}

	/**
	 * @param cli the cli to set
	 */
	public void setCli(Long cli) {
		this.cli = cli;
	}

	/**
	 * @return the nroCuenta
	 */
	public Long getNroCuenta() {
		return nroCuenta;
	}

	/**
	 * @param nroCuenta the nroCuenta to set
	 */
	public void setNroCuenta(Long nroCuenta) {
		this.nroCuenta = nroCuenta;
	}

	/**
	 * @return the findPeriodosService
	 */
	public UIService getFindPeriodosService() {
		return findPeriodosService;
	}

	/**
	 * @param findPeriodosService the findPeriodosService to set
	 */
	public void setFindPeriodosService(UIService findPeriodosService) {
		this.findPeriodosService = findPeriodosService;
	}

	/**
	 * @return the dataTablePeriodos
	 */
	public HtmlDataTable getDataTablePeriodos() {
		return dataTablePeriodos;
	}

	/**
	 * @param dataTablePeriodos the dataTablePeriodos to set
	 */
	public void setDataTablePeriodos(HtmlDataTable dataTablePeriodos) {
		this.dataTablePeriodos = dataTablePeriodos;
	}

	/**
	 * @return the periodoFacturacion
	 */
	public String getPeriodoFacturacion() {
		return periodoFacturacion;
	}

	/**
	 * @param periodoFacturacion the periodoFacturacion to set
	 */
	public void setPeriodoFacturacion(String periodoFacturacion) {
		this.periodoFacturacion = periodoFacturacion;
	}

	/**
	 * @return the periodosListResult
	 */
	public List getPeriodosListResult() {
		return periodosListResult;
	}

	/**
	 * @param periodosListResult the periodosListResult to set
	 */
	public void setPeriodosListResult(List periodosListResult) {
		this.periodosListResult = periodosListResult;
	}

	/**
	 * @return the motivo
	 */
	public Long getMotivo() {
		return motivo;
	}

	/**
	 * @param motivo the motivo to set
	 */
	public void setMotivo(Long motivo) {
		this.motivo = motivo;
	}

	/**
	 * @return the motivos
	 */
	public List getMotivos() {
		return motivos;
	}

	/**
	 * @param motivos the motivos to set
	 */
	public void setMotivos(List motivos) {
		this.motivos = motivos;
	}

	/**
	 * @return the servicioFinder
	 */
	public FinderService getServicioFinder() {
		return servicioFinder;
	}

	/**
	 * @param servicioFinder the servicioFinder to set
	 */
	public void setServicioFinder(FinderService servicioFinder) {
		this.servicioFinder = servicioFinder;
	}

	/**
	 * @return the reporteFacturaService
	 */
	public ReporteFacturaService getReporteFacturaService() {
		return reporteFacturaService;
	}

	/**
	 * @param reporteFacturaService the reporteFacturaService to set
	 */
	public void setReporteFacturaService(ReporteFacturaService reporteFacturaService) {
		this.reporteFacturaService = reporteFacturaService;
	}

	/**
	 * @return the reporteAnexoFacturaService
	 */
	public ReporteAnexoFacturaService getReporteAnexoFacturaService() {
		return reporteAnexoFacturaService;
	}

	/**
	 * @param reporteAnexoFacturaService the reporteAnexoFacturaService to set
	 */
	public void setReporteAnexoFacturaService(ReporteAnexoFacturaService reporteAnexoFacturaService) {
		this.reporteAnexoFacturaService = reporteAnexoFacturaService;
	}

	public String getTituloConsulta() {
		if (this.nroCuenta != null && this.fechaProceso != null) {
			CalendarDate fechaProcesoSinHora = (CalendarDate) this.getFechaProceso();
			return fechaProcesoSinHora + " - Cuenta " + this.nroCuenta;
		}
		else {
			return "";
		}
	}

	public String execKndUseCase(ActionEvent event) {
		CUOrigenImpresionDuplicadoFacturaBB bb = (CUOrigenImpresionDuplicadoFacturaBB) VariableResolverUtils
			.getObject("cuOrigenImpresionDuplicadoFacturaBB");

		this.uri = bb.execKndUseCase(event);
		return this.uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Map getMapDuplicadoLegal() {
		return mapDuplicadoLegal;
	}

	public void setMapDuplicadoLegal(Map mapDuplicadoLegal) {
		this.mapDuplicadoLegal = mapDuplicadoLegal;
	}

}
