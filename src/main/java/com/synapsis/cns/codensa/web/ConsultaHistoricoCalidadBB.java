package com.synapsis.cns.codensa.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

/**
 * 
 * @author adambrosio
 */
public class ConsultaHistoricoCalidadBB {

	private String nroTransformador;
	private Date fechaIncidencia;
	private Date fechaIncidenciaSinHora;
	
	private UIService findHistoricoCalidadByCriteriaService;
	private HtmlDataTable dataTableHistoricoCalidad;	
	
	public void search() {
	  this.getFindHistoricoCalidadByCriteriaService().execute();
      this.getDataTableHistoricoCalidad().setFirst(0);   
	}

	public HtmlDataTable getDataTableHistoricoCalidad() {
		return this.dataTableHistoricoCalidad;
	}

	public void setDataTableHistoricoCalidad(HtmlDataTable dataTableHistoricoCalidad) {
		this.dataTableHistoricoCalidad = dataTableHistoricoCalidad;
	}

	public Date getFechaIncidencia() {
		return this.fechaIncidencia;
	}

	public void setFechaIncidencia(Date fechaIncidencia) {
		this.fechaIncidencia  = fechaIncidencia;
	}

	public String getNroTransformador() {
		return this.nroTransformador;
	}

	public void setNroTransformador(String nroTransformador) {
		this.nroTransformador = nroTransformador;
	}

	public UIService getFindHistoricoCalidadByCriteriaService() {
		return this.findHistoricoCalidadByCriteriaService;
	}

	public void setFindHistoricoCalidadByCriteriaService(UIService findHistoricoCalidadByCriteriaService) {
		this.findHistoricoCalidadByCriteriaService = findHistoricoCalidadByCriteriaService;
	}

	public Date getFechaIncidenciaSinHora() {
        try {
              SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
              // a la fecha le sumo 1 por el quilombo del componente que le resta 1.
              // al  mes le sumo uno porque los meses se toman arrancando desde el cero.
              // al ano tmb le sumo 1900 pq arranca desde 1900
              fechaIncidenciaSinHora = format.parse((fechaIncidencia.getDate()+1) + "-"
                         + (fechaIncidencia.getMonth() +1) + "-"
                         + (fechaIncidencia.getYear()+1900));              
              return fechaIncidenciaSinHora;
        } catch (ParseException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
        }
        return fechaIncidencia;
	}

	public void setFechaIncidenciaSinHora(Date fechaIncidenciaSinHora) {
		this.fechaIncidenciaSinHora = fechaIncidenciaSinHora;
	}	
}