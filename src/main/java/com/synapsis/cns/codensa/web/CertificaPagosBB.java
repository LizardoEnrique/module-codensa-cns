package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.exception.ExporterException;
import com.suivant.arquitectura.core.exception.SystemException;
import com.suivant.arquitectura.core.security.model.User;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.common.ExportPdf;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.InformacionPago;
import com.synapsis.cns.codensa.model.ReporteDatosGenerales;
import com.synapsis.cns.codensa.model.impl.DetalleCertificadoPago;
import com.synapsis.cns.codensa.model.impl.ReporteCertificaPagosImpl;
import com.synapsis.components.UIMainGroup;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * 
 * 
 * @author Paola Attadio
 * @version $Id: CertificaPagosBB.java,v 1.24 2016/10/14 12:17:24 syacac Exp $
 */
public class CertificaPagosBB extends ViewBB {

	// Mapa con los par�metros del reporte
	private Map map;

	private String obsFechaYJuzgado;

	// Propiedades que son ingresadas por el usuario
	private String nombreSolicitantePersona;

	private String tipoDocumentoPersona;

	private String numeroDocumentoPersona;

	private String nombreJuzgado;

	private String nombreSolicitanteJuzgado;

	private String tipoDocumentoJuzgado;

	private String numeroDocumentoJuzgado;

	private String errorListaVacia = "";

	// En esta variable se almacenan los objetos seleccionados por los check
	private List certificadoPagoSelected = new ArrayList();

	private UIMainGroup group;

	private String uri;

	public List getCertificadoPagoSelected() {
		return certificadoPagoSelected;
	}

	public String setSelectedAction() {
		return "certificaPagos";

	}

	public void setCertificadoPagoSelected(List certificadoPagoSelected) {
		this.certificadoPagoSelected = certificadoPagoSelected;
	}

	// obtiene un servicio pidiendoselo al ApplicationContext
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext.findService(service);
	}

	/**
	 * genera el PDF del reporte CertificaPago
	 */
	public void emitirCertificadoAction(ActionEvent event) {

		try {
			List listToExport = getService().findByCriteria(getQueryFilter());
			if (listToExport.size() != 0) {
				ReporteCertificaPagosImpl reporte = completarDatosReporte(listToExport);

				ExportPdf.generatePdf(listToExport, getParametros(reporte));
			}
			else
				setErrorListaVacia("No se puede mostrar el reporte porque no hay datos para la cuenta seleccionada");
		}
		catch (ExporterException ioe) {
			throw new SystemException("certificaBuenCliente.error.cuenta.no.valida");
		}
	}

	/**
	 * Agrega al reporte los datos que no se obtienen de la BD
	 */
	private ReporteCertificaPagosImpl completarDatosReporte(List listToExport) {

		ReporteCertificaPagosImpl reporte = (ReporteCertificaPagosImpl) listToExport.get(0);
		Long nroCuenta = new Long(0);

		// determinacion
		String idGrupoSelected = this.getGroup().getGroupSelected().getId();

		if (idGrupoSelected.equals("grupoJuzgado")) {
			reporte.setNombreSolicitante(getNombreSolicitanteJuzgado());
			reporte.setTipoIdentificacion(getTipoDocumentoJuzgado());
			reporte.setNumeroIdentificacion(getNumeroDocumentoJuzgado());
		}
		else {
			reporte.setNombreSolicitante(getNombreSolicitantePersona());
			reporte.setTipoIdentificacion(getTipoDocumentoPersona());
			reporte.setNumeroIdentificacion(getNumeroDocumentoPersona());
		}

		// TODO validar que existan pagos seleccionados.
		Iterator itPagos = this.getCertificadoPagoSelected().iterator();
		while (itPagos.hasNext()) {
			InformacionPago infoPago = (InformacionPago) itPagos.next();
			DetalleCertificadoPago detalle = new DetalleCertificadoPago();

			detalle.setConcepto(infoPago.getTipoDocumento());
			detalle.setNumeroDocumento(infoPago.getNroDocumento());
			detalle.setFecha(infoPago.getFechaPagoEntidad());
			detalle.setEstado(infoPago.getEstadoPago());
			detalle.setSubtotal(this.convert(infoPago.getValorPago()));
			nroCuenta = infoPago.getNroCuenta();
			reporte.getDetalles().add(detalle);
		}
		double total = 0;
		for (Iterator i = reporte.getDetalles().iterator(); i.hasNext();) {
			total += ((DetalleCertificadoPago) i.next()).getSubtotal().doubleValue();
		}
		reporte.setTotal(new Double(total));
		reporte.setNroCuenta(nroCuenta);
		reporte.setFechaExpedicion(new Date());
		reporte.setUsuarioSistema(obtainUserName());
		setObsFechaYJuzgado(reporte);
		return reporte;
	}

	// No quieren modificar la vista.
	private Double convert(String val) {
		try {
			return new Double(val);
		}
		catch (NumberFormatException e) {
			StringBuffer buff = new StringBuffer();
			val = val.trim();
			for (int i = 0; i < val.length(); i++) {
				char c = val.charAt(i);
				switch (c) {
					case '$':
					case ',':
						break;
					default:
						buff.append(c);
				}
			}
			return new Double(buff.toString());
		}
	}

	/*
	 * Esto esta re mal, no deberia devolver nunca user_codensa cuando tira la exception
	 */
	private String obtainUserName() {
		try {
			User user = SynergiaApplicationContext.getCurrentUser();
			return (user.getFirstname() + " " + user.getLastName());
		}
		catch (Exception e) {
			return "user_codensa";
		}
	}

	public Map getMap() {
		return map;
	}

	/**
	 * Agrega los par�metros necesarios al mapa definido en cns-jasper-map.xml
	 */
	public Map getParametros(ReporteDatosGenerales reporte) {
		Map mapa = getMap();
		mapa.put("diaExpedicion", reporte.getDiaExpedicion());
		mapa.put("mesExpedicion", reporte.getMesExpedicion());
		mapa.put("anioExpedicion", reporte.getAnioExpedicion());
		mapa.put("nombreJuzgado", nombreJuzgado());

		if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.CODENSA)) {
			mapa.put("texto", "SERVICIO AL CLIENTE DE CODENSA");
			mapa.put("empresa", "CODENSA S.A. ESP");
		}
		else if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.EEC)) {
			mapa.put("texto", "SERVICIO AL CLIENTE");
			mapa.put("empresa", "CODENSA S.A. ESP");
		}

		return mapa;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public String getObsFechaYJuzgado() {
		return obsFechaYJuzgado;
	}

	public void setObsFechaYJuzgado(ReporteDatosGenerales reporte) {
		String obsFechaYJuzgado = "Este certificado se emite a los " + reporte.getDiaExpedicion() + " d�as de "
			+ reporte.getMesExpedicion() + " de " + reporte.getAnioExpedicion();
		obsFechaYJuzgado = nombreJuzgado();
		this.obsFechaYJuzgado = obsFechaYJuzgado + ".";
	}

	private String nombreJuzgado() {
		String nombreJuzgado = "";
		if (getNombreJuzgado() != null && getNombreJuzgado().length() != 0) {
			nombreJuzgado += " a solicitud del juzgado " + getNombreJuzgado();
		}
		return nombreJuzgado;
	}

	public String getNombreJuzgado() {
		return nombreJuzgado;
	}

	public void setNombreJuzgado(String nombreJuzgado) {
		this.nombreJuzgado = nombreJuzgado;
	}

	/**
	 * @return the nombreSolicitanteJuzgado
	 */
	public String getNombreSolicitanteJuzgado() {
		return this.nombreSolicitanteJuzgado;
	}

	/**
	 * @param nombreSolicitanteJuzgado the nombreSolicitanteJuzgado to set
	 */
	public void setNombreSolicitanteJuzgado(String nombreSolicitanteJuzgado) {
		this.nombreSolicitanteJuzgado = nombreSolicitanteJuzgado;
	}

	/**
	 * @return the nombreSolicitantePersona
	 */
	public String getNombreSolicitantePersona() {
		return this.nombreSolicitantePersona;
	}

	/**
	 * @param nombreSolicitantePersona the nombreSolicitantePersona to set
	 */
	public void setNombreSolicitantePersona(String nombreSolicitantePersona) {
		this.nombreSolicitantePersona = nombreSolicitantePersona;
	}

	/**
	 * @return the numeroDocumentoJuzgado
	 */
	public String getNumeroDocumentoJuzgado() {
		return this.numeroDocumentoJuzgado;
	}

	/**
	 * @param numeroDocumentoJuzgado the numeroDocumentoJuzgado to set
	 */
	public void setNumeroDocumentoJuzgado(String numeroDocumentoJuzgado) {
		this.numeroDocumentoJuzgado = numeroDocumentoJuzgado;
	}

	/**
	 * @return the numeroDocumentoPersona
	 */
	public String getNumeroDocumentoPersona() {
		return this.numeroDocumentoPersona;
	}

	/**
	 * @param numeroDocumentoPersona the numeroDocumentoPersona to set
	 */
	public void setNumeroDocumentoPersona(String numeroDocumentoPersona) {
		this.numeroDocumentoPersona = numeroDocumentoPersona;
	}

	/**
	 * @return the tipoDocumentoJuzgado
	 */
	public String getTipoDocumentoJuzgado() {
		return this.tipoDocumentoJuzgado;
	}

	/**
	 * @param tipoDocumentoJuzgado the tipoDocumentoJuzgado to set
	 */
	public void setTipoDocumentoJuzgado(String tipoDocumentoJuzgado) {
		this.tipoDocumentoJuzgado = tipoDocumentoJuzgado;
	}

	/**
	 * @return the tipoDocumentoPersona
	 */
	public String getTipoDocumentoPersona() {
		return this.tipoDocumentoPersona;
	}

	/**
	 * @param tipoDocumentoPersona the tipoDocumentoPersona to set
	 */
	public void setTipoDocumentoPersona(String tipoDocumentoPersona) {
		this.tipoDocumentoPersona = tipoDocumentoPersona;
	}

	public String getErrorListaVacia() {
		return errorListaVacia;
	}

	public void setErrorListaVacia(String errorListaVacia) {
		this.errorListaVacia = errorListaVacia;
	}

	/**
	 * @return the group
	 */
	public UIMainGroup getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(UIMainGroup group) {
		this.group = group;
	}

	public String execKndUseCase(ActionEvent event) {
		CUOrigenCertificaPagosBB bb = (CUOrigenCertificaPagosBB) VariableResolverUtils
			.getObject("cuOrigenCertificaPagosBB");

		this.uri = bb.execKndUseCase(event);
		return this.uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the comboQueryFilter
	 */
	// public QueryFilter getComboQueryFilter() {
	// return this.comboQueryFilter;
	// }
	/**
	 * @param comboQueryFilter the comboQueryFilter to set
	 */
	// public void setComboQueryFilter(QueryFilter comboQueryFilter) {
	// this.comboQueryFilter = comboQueryFilter;
	// }
	/**
	 * @return the serviceCombo
	 */
	// public String getServiceCombo() {
	// return this.serviceCombo;
	// }
	/**
	 * @param serviceCombo the serviceCombo to set
	 */
	// public void setServiceCombo(String serviceCombo) {
	// this.serviceCombo = serviceCombo;
	// }
}
