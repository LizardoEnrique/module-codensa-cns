package com.synapsis.cns.codensa.web;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.apache.myfaces.component.html.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.nuc.codensa.validation.NumeroCuentaQueryFilterValidation;

public class ConsultaHistoricaOtrosNegociosBB {

	//filtros 
	private String tipoServicio;
	private String tipoIdentificacionTitular;
	private Long lineaNegocio;
	private Date fechaInicialProducto;
	private Date fechaFinalProducto;
	private Long nroServicio;
	private String nroIdentificacionTitular;
	private Long producto;
	
	private String nombreTitularProducto;
	private Long plan;
	private Long nroSrvFinEco;
	
	// fin filtros
	private HtmlDataTable dataTableCodensaHogar; 
	private HtmlDataTable dataTableEncargosCobranza;
	private HtmlCommandButton buttonConsultaModificaciones;
	
	/**
	 * Comment for <code>findByCriteriaService</code> Servicio para realizar
	 * la busqueda
	 */
	private UIService svcFinancieroCodensaHogarService;
	private UIService svcEncargosCobranzaService;
	
	private Long idServicio;
	
	/*
	 * Se realiza la busqueda solo si se ingresaron los parametros obligatorios y ademas si
	 * se eligio alguna de las fechas entonces tienen que estar las dos fechas para el filtro,
	 * es decir no puede pasar que seleccion fecha inicio y no fecha final, ni viceversa
	 *  
	 */
	public void search() {
		this.limpiarResultados();
		// ANSCOD-183: Pidieron sacar esta restriccion, la dejo comentada por si hay que volver atras por
		// cuestiones de performance (van a intentar optimizar la vista)
		/*
		if(tieneParametrosObligatorios() && !fechasValidas()){
				FacesContext.getCurrentInstance().addMessage(
						null, new FacesMessage("Por favor para utilizar el filtro de fechas, ingresar ambos valores: fecha Inicial y fecha Final"));
		}else
		*/
			if(!tieneParametrosObligatorios()){
				FacesContext.getCurrentInstance().addMessage(
						null, new FacesMessage("Por favor verificar que se ingreso alguno de los siguientes filtros: Nro.Servicio o Tipo y Nro. de identificación del titular"));
			}else{
				this.getSvcFinancieroCodensaHogarService().execute();
				this.getSvcEncargosCobranzaService().execute();
			}
		this.getDataTableCodensaHogar().setFirst(0);
		this.getDataTableEncargosCobranza().setFirst(0);
		}


	private void limpiarResultados() {
				
	}


	private boolean fechasValidas() {
		return (this.fechaInicialProducto != null && this.fechaFinalProducto !=null) || (this.fechaInicialProducto == null && this.fechaFinalProducto == null);
	}


	private boolean tieneParametrosObligatorios() {
		return (this.nroServicio != null) || StringUtils.isNotEmpty(this.tipoIdentificacionTitular) && StringUtils.isNotEmpty(this.nroIdentificacionTitular);
	}


	/**
	 * @return the buttonConsultaModificaciones
	 */
	public HtmlCommandButton getButtonConsultaModificaciones() {
		return buttonConsultaModificaciones;
	}


	/**
	 * @param buttonConsultaModificaciones the buttonConsultaModificaciones to set
	 */
	public void setButtonConsultaModificaciones(
			HtmlCommandButton buttonConsultaModificaciones) {
		this.buttonConsultaModificaciones = buttonConsultaModificaciones;
	}


	/**
	 * @return the dataTableCodensaHogar
	 */
	public HtmlDataTable getDataTableCodensaHogar() {
		return dataTableCodensaHogar;
	}


	/**
	 * @param dataTableCodensaHogar the dataTableCodensaHogar to set
	 */
	public void setDataTableCodensaHogar(HtmlDataTable dataTableCodensaHogar) {
		this.dataTableCodensaHogar = dataTableCodensaHogar;
	}


	/**
	 * @return the dataTableEncargosCobranza
	 */
	public HtmlDataTable getDataTableEncargosCobranza() {
		return dataTableEncargosCobranza;
	}


	/**
	 * @param dataTableEncargosCobranza the dataTableEncargosCobranza to set
	 */
	public void setDataTableEncargosCobranza(HtmlDataTable dataTableEncargosCobranza) {
		this.dataTableEncargosCobranza = dataTableEncargosCobranza;
	}


	/**
	 * @return the fechaFinalProducto
	 */
	public Date getFechaFinalProducto() {
		return fechaFinalProducto;
	}


	/**
	 * @param fechaFinalProducto the fechaFinalProducto to set
	 */
	public void setFechaFinalProducto(Date fechaFinalProducto) {
		this.fechaFinalProducto = fechaFinalProducto;
	}


	/**
	 * @return the fechaInicialProducto
	 */
	public Date getFechaInicialProducto() {
		return fechaInicialProducto;
	}


	/**
	 * @param fechaInicialProducto the fechaInicialProducto to set
	 */
	public void setFechaInicialProducto(Date fechaInicialProducto) {
		this.fechaInicialProducto = fechaInicialProducto;
	}


	/**
	 * @return the lineaNegocio
	 */
	public Long getLineaNegocio() {
		return lineaNegocio;
	}


	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(Long lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}


	/**
	 * @return the nombreTitularProducto
	 */
	public String getNombreTitularProducto() {
		return nombreTitularProducto;
	}


	/**
	 * @param nombreTitularProducto the nombreTitularProducto to set
	 */
	public void setNombreTitularProducto(String nombreTitularProducto) {
		this.nombreTitularProducto = nombreTitularProducto;
	}


	/**
	 * @return the nroIdentificacionTitular
	 */
	public String getNroIdentificacionTitular() {
		return nroIdentificacionTitular;
	}


	/**
	 * @param nroIdentificacionTitular the nroIdentificacionTitular to set
	 */
	public void setNroIdentificacionTitular(String nroIdentificacionTitular) {
		this.nroIdentificacionTitular = nroIdentificacionTitular;
	}

	/**
	 * @return the nroServicio
	 */
	public Long getNroServicio() {
		return nroServicio;
	}


	/**
	 * @param nroServicio the nroServicio to set
	 */
	public void setNroServicio(Long nroServicio) {
		this.nroServicio = nroServicio;
	}


	/**
	 * @return the plan
	 */
	public Long getPlan() {
		return plan;
	}


	/**
	 * @param plan the plan to set
	 */
	public void setPlan(Long plan) {
		this.plan = plan;
	}


	/**
	 * @return the producto
	 */
	public Long getProducto() {
		return producto;
	}


	/**
	 * @param producto the producto to set
	 */
	public void setProducto(Long producto) {
		this.producto = producto;
	}


	/**
	 * @return the svcEncargosCobranzaService
	 */
	public UIService getSvcEncargosCobranzaService() {
		return svcEncargosCobranzaService;
	}


	/**
	 * @param svcEncargosCobranzaService the svcEncargosCobranzaService to set
	 */
	public void setSvcEncargosCobranzaService(UIService svcEncargosCobranzaService) {
		this.svcEncargosCobranzaService = svcEncargosCobranzaService;
	}


	/**
	 * @return the svcFinancieroCodensaHogarService
	 */
	public UIService getSvcFinancieroCodensaHogarService() {
		return svcFinancieroCodensaHogarService;
	}


	/**
	 * @param svcFinancieroCodensaHogarService the svcFinancieroCodensaHogarService to set
	 */
	public void setSvcFinancieroCodensaHogarService(
			UIService svcFinancieroCodensaHogarService) {
		this.svcFinancieroCodensaHogarService = svcFinancieroCodensaHogarService;
	}


	/**
	 * @return the tipoIdentificacionTitular
	 */
	public String getTipoIdentificacionTitular() {
		return tipoIdentificacionTitular;
	}


	/**
	 * @param tipoIdentificacionTitular the tipoIdentificacionTitular to set
	 */
	public void setTipoIdentificacionTitular(String tipoIdentificacionTitular) {
		this.tipoIdentificacionTitular = tipoIdentificacionTitular;
	}


	/**
	 * @return the tipoServicio
	 */
	public String getTipoServicio() {
		return tipoServicio;
	}


	/**
	 * @param tipoServicio the tipoServicio to set
	 */
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	/**
	 * @return the nroSrvFinEco
	 */
	public Long getNroSrvFinEco() {
		return nroSrvFinEco;
	}

	/**
	 * @param nroSrvFinEco the nroSrvFinEco to set
	 */
	public void setNroSrvFinEco(Long nroSrvFinEco) {
		this.nroSrvFinEco = nroSrvFinEco;
	}


	public Long getIdServicio() {
		return idServicio;
	}


	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	
	
}
