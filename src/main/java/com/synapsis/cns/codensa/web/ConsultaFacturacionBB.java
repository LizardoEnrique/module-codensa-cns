/**
 * 
 */
package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;

/**
 * @author ar30557486
 *
 */
public class ConsultaFacturacionBB extends ListableBB {
	
	public ConsultaFacturacionBB(){
		
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			Long nroCuentaaa = new Long(parameter.toString());
			queryFilter.setAttributeValue(nroCuentaaa);
		}
	}
	
	
	/*
	 * Detalle de la facturacion total
	 */
	private Object detalleValorTotal; 
	
	
	/**
	 * @return the detalleValorTotal
	 */
	public Object getDetalleValorTotal() {
		return detalleValorTotal;
	}
	/**
	 * @param detalleValorTotal the detalleValorTotal to set
	 */
	public void setDetalleValorTotal(Object detalleValorTotal) {
		this.detalleValorTotal = detalleValorTotal;
	}	
}
