/**
 * 
 */
package com.synapsis.cns.codensa.web;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.JSFUtils;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.integration.codensa.backingBeans.helpers.DataTableFactoryRefresh;
import com.synapsis.synergia.core.integration.ListableData;

/**
 * @author outaTiME
 * 
 */
public class ConsultaMedidoresBB extends ListableBB {
	
	
	private Long nroComponente;
	
	
	public ConsultaMedidoresBB() {
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils
		.getObject("numeroCuentaQueryFilter");
		Object parameter = JSFUtils.getParameter("nroCuenta");
		if (parameter != null) {
			Long nroCuentaaa = new Long(parameter.toString());
			queryFilter.setAttributeValue(nroCuentaaa);
		}		
	}	

	/**
	 * @return
	 */
	public void medidorQueryFilter(ActionEvent event) {
		// retain the idMedidor specified ...
		String idMedidor = (String) JSFUtils.getParameter("idMedidor");
		
		//obtengo el filtro del contexto y le seteo el valor
		SimpleQueryFilter queryFilter = (SimpleQueryFilter) VariableResolverUtils.getObject("eventoMedidorQueryFilter");
		queryFilter.setAttributeValue(new Long(idMedidor));	
		
		//realizo el refresco de la tabla 
		DataTableFactoryRefresh.makeDataTable("consultaComponentesForm:eventosComponentes", queryFilter,
				 ((ListableData)getMultiList().getData().get("eventosComponentes")).getServiceName(), 
				 ((ListableData)getMultiList().getData().get("eventosComponentes")).getMetadata());
	}

	public Long getNroComponente() {
		return nroComponente;
	}

	public void setNroComponente(Long nroComponente) {
		this.nroComponente = nroComponente;
	}
	
	
}
