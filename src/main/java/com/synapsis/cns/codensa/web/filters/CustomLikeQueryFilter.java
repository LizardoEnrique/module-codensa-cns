package com.synapsis.cns.codensa.web.filters;

import com.suivant.arquitectura.core.queryFilter.LikeQueryFilter;
/**
 * Sobrecarga del likeQueryFilter para poder trabajar con diff servicios para
 * un mismo BB
 * 
 * @author Emiliano Arango (ar30557486)
 * 
 */
public class CustomLikeQueryFilter extends LikeQueryFilter implements
		CustomQueryFilter {

	private String serviceName;

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
