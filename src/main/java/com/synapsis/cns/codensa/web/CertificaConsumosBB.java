package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.event.ActionEvent;

import com.suivant.arquitectura.core.exception.ExporterException;
import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.security.model.User;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.common.ExportPdf;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.ReporteCertificaBuenCliente;
import com.synapsis.cns.codensa.model.impl.ConsultaLecturaConsumoImpl;
import com.synapsis.components.UIMainGroup;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author outaTiME (afalduto at gmail dot com)
 * @version $Revision: 1.19 $ $Date: 2011/07/27 16:39:33 $
 * 
 */
public class CertificaConsumosBB extends ViewBB {

	/**
	 * Comment for <code>valoresConsumos</code>
	 */
	private List valoresConsumos = new ArrayList();

	// map con los datos del archivo pdf para generar Certificado de Consumo
	private Map reportMapConsumo;

	// map para generar certificado de Consumo para empresa de ASEO
	private Map reportMapConsumoAseo;

	// datos de la pantalla necesarios para generar el archivo PDF
	private String nombreSolicitante;

	private String tipoDocumentoPersona;

	private String nroDocumento;

	private String telefono;

	private String empresaExpideCertificacion;

	private String observaciones;

	private String nombreQuienEmite;

	private UIMainGroup group;

	private String uri;

	private List certificadoConsumosSelected = new ArrayList();

	public List getCertificadoConsumosSelected() {
		return certificadoConsumosSelected;
	}

	public void setCertificadoConsumosSelected(List certificadoConsumosSelected) {
		this.certificadoConsumosSelected = certificadoConsumosSelected;
	}

	public String execAction() {
		this.setValoresConsumos(this.getCertificadoConsumosSelected());
		return "emitirPdf";
	}

	public void generatePdfConsumo() throws ObjectNotFoundException {
		// determinacion
		String idGrupoSelected = this.getGroup().getGroupSelected().getId();

		if (idGrupoSelected.equals("grupoConsumo")) {
			this.generateClientData(getReportMapConsumo());
			this.putTextoEmpresa(this.getReportMapConsumo());
			this.doGeneratePDF();
		}
		else {
			this.generateClientData(getReportMapConsumoAseo());
			this.putTextoEmpresa(this.getReportMapConsumoAseo());
			this.doGeneratePDFAseo();
		}
	}

	private void doGeneratePDF() {
		try {
			ExportPdf.generatePdf(this.generateConsumos(), this.getReportMapConsumo());
		}
		catch (ExporterException e) {
			e.printStackTrace();
		}
	}

	private void doGeneratePDFAseo() {
		try {
			ExportPdf.generatePdf(this.generateConsumos(), this.getReportMapConsumoAseo());
		}
		catch (ExporterException e) {
			e.printStackTrace();
		}
	}

	// todo: estos datos se deben levantar de la base
	private void generateClientData(Map map) throws ObjectNotFoundException {

		map.put("nombreSolicitante", this.getNombreSolicitante());
		map.put("tipoIdentificacion", this.getTipoDocumentoPersona());
		map.put("numeroIdentificacion", this.getNroDocumento());
		map.put("nombreQuienEmite", this.getNombreSolicitante());
		map.put("empresaExpideCertificacion", this.getEmpresaExpideCertificacion());

		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("numeroCuentaQueryFilter");
		FinderService finderService = (FinderService) SynergiaApplicationContext
			.findService("certificaBuenClienteServiceFinder");
		ReporteCertificaBuenCliente datosCuenta = (ReporteCertificaBuenCliente) finderService
			.findByCriteriaUnique(queryFilter);

		map.put("nroCuenta", datosCuenta.getNroCuenta());
		datosCuenta.setFechaExpedicion(new Date());
		map.put("diaExpedicion", datosCuenta.getDiaExpedicion().toString());
		map.put("mesExpedicion", datosCuenta.getMesExpedicion().toString());
		map.put("anioExpedicion", datosCuenta.getAnioExpedicion().toString());
		map.put("nombreTitularCuenta", datosCuenta.getNombreTitularCuenta().toString());
		if (datosCuenta.getApellidoTitularCuenta() != null) {
			map.put("apellidoTitularCuenta", datosCuenta.getApellidoTitularCuenta().toString());
		}
		if (datosCuenta.getDireccion() != null) {
			map.put("direccion", datosCuenta.getDireccion().toString());
		}
		if (datosCuenta.getLocalizacion() != null) {
			map.put("localizacion", datosCuenta.getLocalizacion().toString());
		}
		if (datosCuenta.getMunicipio() != null) {
			map.put("municipio", datosCuenta.getMunicipio().toString());
		}
		if (datosCuenta.getBarrio() != null) {
			map.put("barrio", datosCuenta.getBarrio().toString());
		}
		if (datosCuenta.getTelefono() != null) {
			map.put("telefono", datosCuenta.getTelefono().toString());
		}
		map.put("direccionRepartoEspecial", datosCuenta.getDireccionRepartoEspecial().toString());
		map.put("usuarioSistema", this.obtainUserName());
	}

	private String obtainUserName() {
		try {
			User user = SynergiaApplicationContext.getCurrentUser();
			return (user.getFirstname() + " " + user.getLastName());
		}
		catch (Exception e) {
			return "user_codensa";
		}
	}

	private List generateConsumos() {
		List l = new ArrayList();
		Iterator itValoresConsumos = this.valoresConsumos.iterator();
		while (itValoresConsumos.hasNext()) {
			ConsultaLecturaConsumoImpl consumo = (ConsultaLecturaConsumoImpl) itValoresConsumos.next();
			ConsultaLecturaConsumoImpl lc = new ConsultaLecturaConsumoImpl();
			lc.setPeriodoFacturacion(consumo.getPeriodoFacturacion());
			lc.setConsumoDemandaFP(consumo.getConsumoFacturadoActivaFP().toString());
			l.add(lc);
		}
		return l;
	}

	/**
	 * @return the valoresConsumos
	 */
	public List getValoresConsumos() {
		return this.valoresConsumos;
	}

	/**
	 * @param valoresConsumos the valoresConsumos to set
	 */
	public void setValoresConsumos(List valoresConsumos) {
		this.valoresConsumos = valoresConsumos;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Map getReportMapConsumo() {
		return reportMapConsumo;
	}

	public void setReportMapConsumo(Map reportMapConsumo) {
		this.reportMapConsumo = reportMapConsumo;
	}

	public Map getReportMapConsumoAseo() {
		return reportMapConsumoAseo;
	}

	public void setReportMapConsumoAseo(Map reportMapConsumoAseo) {
		this.reportMapConsumoAseo = reportMapConsumoAseo;
	}

	public String getTipoDocumentoPersona() {
		return tipoDocumentoPersona;
	}

	public void setTipoDocumentoPersona(String tipoDocumentoPersona) {
		this.tipoDocumentoPersona = tipoDocumentoPersona;
	}

	public String getEmpresaExpideCertificacion() {
		return empresaExpideCertificacion;
	}

	public void setEmpresaExpideCertificacion(String empresaExpideCertificacion) {
		this.empresaExpideCertificacion = empresaExpideCertificacion;
	}

	public String getNombreQuienEmite() {
		return nombreQuienEmite;
	}

	public void setNombreQuienEmite(String nombreQuienEmite) {
		this.nombreQuienEmite = nombreQuienEmite;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	// me devuleve un servicio pidiendoselo al SynergiaApplicacontext
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext.findService(service);
	}

	public UIMainGroup getGroup() {
		return group;
	}

	public void setGroup(UIMainGroup group) {
		this.group = group;
	}

	public String execKndUseCase(ActionEvent event) {
		CUOrigenCertificaConsumosBB bb = (CUOrigenCertificaConsumosBB) VariableResolverUtils
			.getObject("cuOrigenCertificaConsumosBB");

		this.uri = bb.execKndUseCase(event);
		return this.uri;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	private void putTextoEmpresa(Map map) {
		StringBuffer buff = new StringBuffer("SERVICIO AL CLIENTE");
		if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.CODENSA))
			buff.append(" DE CODENSA");

		map.put("texto", buff.toString());
	}
}