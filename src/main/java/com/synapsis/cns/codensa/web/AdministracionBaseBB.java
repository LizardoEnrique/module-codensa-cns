package com.synapsis.cns.codensa.web;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlPanelGrid;

import org.apache.myfaces.custom.service.UIService;


public abstract class AdministracionBaseBB {

	private HtmlDataTable dataTable;
	private HtmlPanelGrid panelGrid;

	private UIService consultaService;
	

	/**
     * - CONSULTA HISTORICA DE CALIDAD. 
     * - CONSULTA CLIENTE RED PARA SERVICIO ELECTRICO, NO REGULADO Y PEAJES
     * - CONSULTA X
     * 
     * @throws Exception
     */
    protected void prepararColeccionesConsulta() throws Exception {
    	
    }

	public HtmlDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlDataTable dataTable) {
		this.dataTable = dataTable;
	}

	public UIService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(UIService consultaService) {
		this.consultaService = consultaService;
	}

	public HtmlPanelGrid getPanelGrid() {
		return panelGrid;
	}

	public void setPanelGrid(HtmlPanelGrid panelGrid) {
		this.panelGrid = panelGrid;
	}

}
