package com.synapsis.cns.codensa.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.myfaces.custom.service.UIService;

import com.suivant.arquitectura.core.model.BusinessObject;
import com.synapsis.commons.faces.messages.ErrorsManager;

public class ConsultaIndidividualClienteRedUseCaseBB extends AdministracionBaseBB {

	private String periodoActual;
	private String periodoDesde;	
	private String periodoHasta;
	private Long nroTransformador;
	
	private Long nroSrvElectrico;
	
	private BusinessObject businessObject1;
	
	private static DateFormat df = new SimpleDateFormat("ddMMyyyy");
	
	private UIService consultaTransfListService;
	
	public ConsultaIndidividualClienteRedUseCaseBB() {
		//iniciarUseCase();
	}
	
	public String execAction() {
	  return "dispatchToAccion";
	}

	/**
	 * 
	 */
	public void search() {
		prepararColeccionesConsulta();
		this.getDataTable().setFirst(0);

	}

	protected void prepararColeccionesConsulta() {
	
		try {
		getConsultaService().execute();
		} catch (Exception e) {
			ErrorsManager.addInfoError(e.getMessage());
		}

	}

	public void findConsultaIndividual() {
		prepararColeccionesConsulta();
		this.getPanelGrid();
	}

	public void findByDetail() {
		
		prepararColeccionesConsulta();
		if(this.getDataTable()!=null)
		this.getDataTable().setFirst(0);

	}

	
	public void searchDetail() {
		this.consultaTransfListService.execute();
		if(this.getDataTable()!=null)
			this.getDataTable().setFirst(0);		
	}
	
	
	private void iniciarUseCase() {

		/* esto es para ver como quedaria el requerimiento 
		 *
		 *COD-CU-AIC075 Consulta Vinculo Cliente Red v4.0
		 *En el punto... 
		 * .	El Operador Consulta AIC selecciona la forma del consulta del periodo as�:
		 * 	-	Consulta por per�odo actual
		 * 	-	Consulta de per�odos anteriores
		 *  
		 *  Las preguntas que quedan pendientes es de donde sacara el periodo.
		 *  Por el momento hoyBean tiene el periodo actual.
		 * 
		 */
		this.periodoActual = retrievePeriodoActual();
		
	}
	

	private String retrievePeriodoActual() {

		// [HARDCODE] No se sabe a�n de donde sale la fecha
		Date fecha = new Date(1,11,2007);
		
		if (fecha != null) {
			GregorianCalendar calendar = (GregorianCalendar) df.getCalendar().getInstance();	
			calendar.setTime(fecha);
			return df.format(calendar);
		}
		
//		try {		
			// [adambrosio] qu� es esta negrada try-catch???????? AWFUL!!!!!
//			Date hoyBean = new Date(1,11,2007);
//			String periodo = df.format(strToCalendar(hoyBean.toString()));
			//this.periodoActual = df.format(strToCalendar(hoyBean.toGMTString()).getTime());
//			return periodo;
//		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return null;
	}


	public Long getNroTransformador() {
		return nroTransformador;
	}

	public void setNroTransformador(Long nroTransformador) {
		this.nroTransformador = nroTransformador;
	}

	//[adambrosio] Por qu� public???
	public static Calendar strToCalendar( String data ) throws Exception {
		GregorianCalendar result = (GregorianCalendar) df.getCalendar().getInstance();	
  		result.setTime(df.parse(data));
	  return result;
	}

	public Long getNroSrvElectrico() {
		return nroSrvElectrico;
	}

	public void setNroSrvElectrico(Long nroSrvElectrico) {
		this.nroSrvElectrico = nroSrvElectrico;
	}

	public String getPeriodoActual() {
		return periodoActual;
	}

	public void setPeriodoActual(String periodoActual) {
		this.periodoActual = periodoActual;
	}

	public String getPeriodoDesde() {
		return periodoDesde;
	}

	public void setPeriodoDesde(String periodoDesde) {
		this.periodoDesde = periodoDesde;
	}

	public String getPeriodoHasta() {
		return periodoHasta;
	}

	public void setPeriodoHasta(String periodoHasta) {
		this.periodoHasta = periodoHasta;
	}

	public BusinessObject getBusinessObject1() {
		return businessObject1;
	}

	public void setBusinessObject1(BusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}

	public UIService getConsultaTransfListService() {
		return consultaTransfListService;
	}

	public void setConsultaTransfListService(UIService consultaTransfListService) {
		this.consultaTransfListService = consultaTransfListService;
	}



}
