/**
 * 
 */
package com.synapsis.cns.codensa.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import com.suivant.arquitectura.core.exception.BusinessException;
import com.suivant.arquitectura.core.queryFilter.EqualQueryFilter;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.common.ExportPdf;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.suivant.arquitectura.presentation.utils.ComboFactory;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.cns.codensa.model.CertificaBuenCliente;
import com.synapsis.cns.codensa.model.ReporteCertificaBuenCliente;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

/**
 * @author dBraccio 27/11/2006
 * @author m3.MarioRoss
 */
public class CertificaBuenClienteBB extends ViewBB {

	/**
	 * este es el map con los datos para emitir el archivo pdf
	 */
	private Map map;

	HtmlSelectOneMenu componente;

	boolean reImprimir = false;

	/**
	 * Propiedades que son ingresadas por el usuario
	 */
	private String nombreSolicitante;

	String tipoDocumento;

	String numeroDocumento;

	String errorListaVacia = "";

	private String serviceCombo;

	private QueryFilter comboQueryFilter;

	// private static final String Service_Name = "aICContactoService";
	//
	// private static final String Service_Method = "generarContactoCertificaBuenCliente";

	/**
	 * Validacion que identifica si un cliente paso todas las validaciones o no.
	 */
	private boolean certifica;

	private List validations;

	private String uri;

	/**
	 * Constuctor
	 * 
	 */
	public CertificaBuenClienteBB() {
	}

	/**
	 * 
	 * @return SelectItem[] Con los tipos de documentos
	 */
	public SelectItem[] getTipoDocumentoList() {
		return ComboFactory.makeCombo(serviceCombo, comboQueryFilter, "descripcion", "codigo");

	}

	/**
	 * Ejecuta la busqueda.
	 */
	public String buscarAction() {
		this.setValidations(new ArrayList());
		this.doValidation();
		return "success";
	}

	/**
	 * Realiza las validaciones y setea si el cliente certifica o no
	 * 
	 */
	private void doValidation() {
		List bos = this.getValidationList();
		for (Iterator iter = bos.iterator(); iter.hasNext();) {
			CertificaBuenCliente cc = (CertificaBuenCliente) iter.next();
			this.setCertifica(cc.getCertifica().booleanValue());
			this.makeStringValidations(cc);
		}
	}

	/*
	 * Genero los String con las validaciones
	 */
	private void makeStringValidations(CertificaBuenCliente cc) {
		if (!cc.getDeuda().booleanValue())
			this.getValidations().add(this.getBundle("certificaBuenCliente.error.cuenta.no.certifica.deuda"));
		if (!cc.getExpedientes().booleanValue())
			this
				.getValidations()
				.add(this.getBundle("certificaBuenCliente.error.cuenta.no.certifica.poseeExpedientes"));
		if (!cc.getHabitos().booleanValue())
			this.getValidations().add(this.getBundle("certificaBuenCliente.error.cuenta.no.certifica.habitos"));
	}

	/**
	 * tomo los bundles.
	 * 
	 * @param exp
	 * @return
	 */
	private String getBundle(String exp) {
		StringBuffer sb = new StringBuffer().append("#{bundle['").append(exp).append("']}");
		FacesContext fc = FacesContext.getCurrentInstance();
		return (String) fc.getApplication().createValueBinding(sb.toString()).getValue(fc);
	}

	/**
	 * trae la lista de las validaciones realizadas por el servicio
	 * 
	 * @return Lista con las validaciones.
	 */
	private List getValidationList() {
		EqualQueryFilter queryFilter = (EqualQueryFilter) VariableResolverUtils.getObject("numeroCuentaQueryFilter");
		List out = getService("certificaBuenClienteValidacionServiceFinder").findByCriteria(queryFilter);
		return out;
	}

	// obtiene el servicio pidiendoselo al ApplicationContext
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext.findService(service);
	}

	/**
	 * Dispara la generacion del certificado
	 * 
	 * @throws BusinessException
	 * 
	 */
	public void emitirCertificadoAction() throws BusinessException {
		EqualQueryFilter eqf = (EqualQueryFilter) getQueryFilter();
		if (eqf.getAttributeValue() != null) {
			List listToExport = getService().findByCriteria(getQueryFilter());
			if (!listToExport.isEmpty()) {
				if (!reImprimir) {
					try {
						String nroCuenta = eqf.getAttributeValue().toString();
						// NasServiceHelper.invoke(Service_Name, Service_Method,
						// generateWebServicesParameters(eqf));
					}
					catch (Exception t) {
						reImprimir = true;
						ErrorsManager
							.addError("No se pudo generar el contacto Automaticamente, si desea imprimir el certificado sin generar un contacto seleccione Imprimir");
						return;
					}
				}
				completarDatosReporte(listToExport);
				try {
					ExportPdf.generatePdf(listToExport, getParametros());
				}
				catch (Exception e) {
					System.out.println("aca paso");
				}
			}
			else {
				setErrorListaVacia("No se puede mostrar el reporte porque no hay datos para la cuenta seleccionada");
			}
		}
		else {
			setErrorListaVacia("No se puede generar el contacto en Kendari si no selecciona una cuenta");
		}
	}

	private Map generateWebServicesParameters(EqualQueryFilter eqf) {
		Map parameters = new HashMap();
		parameters.put("cuenta", eqf.getAttributeValue());
		parameters.put("nombreSolicitante", getNombreSolicitante());
		parameters.put("tipoDocumento", getTipoDocumento());
		parameters.put("nroDocumento", Long.valueOf(getNumeroDocumento()));
		parameters.put("userName", getUserName());
		return parameters;
	}

	/**
	 * Agrega al reporte los datos que no se obtienen de la BD
	 * 
	 * NO TOMAR ESTA FORMA COMO EJEMPLO
	 */
	private void completarDatosReporte(List listToExport) {
		ReporteCertificaBuenCliente reporte = (ReporteCertificaBuenCliente) listToExport.get(0);
		// Buscamos el valor del combo para obtener el label en vez del id
		// y pasarle eso al PDF
		ListIterator li = componente.getChildren().listIterator();
		String doc = null;
		while (li.hasNext()) {
			UISelectItems usi = (UISelectItems) li.next();
			SelectItem[] si = (SelectItem[]) usi.getValue();
			for (int i = 0; i < si.length; i++) {
				SelectItem selectItem = si[i];
				if (selectItem.getValue().toString().equals(tipoDocumento)) {
					doc = selectItem.getLabel();
					break;
				}

			}
		}
		reporte.setNombreSolicitante(getNombreSolicitante());
		reporte.setTipoIdentificacion(doc);
		reporte.setNumeroIdentificacion(getNumeroDocumento());

		reporte.setFechaExpedicion(new Date());
		reporte.setUsuarioSistema(getUserName());
	}

	/**
	 * @return Map con los parametros.
	 */
	public Map getParametros() {
		Map mapa = this.getMap();

		if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.CODENSA)) {
			mapa.put("texto", "SERVICIO AL CLIENTE DE CODENSA");
			mapa.put("detalleEmpresa", "cliente de CODENSA");
		}
		else if (SynergiaApplicationContext.getCurrentImplementationCompany().equals(SynergiaApplicationContext.EEC)) {
			mapa.put("texto", "SERVICIO AL CLIENTE");
			mapa.put("detalleEmpresa", "nuestro cliente");
		}

		return mapa;
	}

	// --- Getters & Setters

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public QueryFilter getComboQueryFilter() {
		return comboQueryFilter;
	}

	public void setComboQueryFilter(QueryFilter comboQueryFilter) {
		this.comboQueryFilter = comboQueryFilter;
	}

	public String getServiceCombo() {
		return serviceCombo;
	}

	public void setServiceCombo(String serviceCombo) {
		this.serviceCombo = serviceCombo;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getErrorListaVacia() {
		return errorListaVacia;
	}

	public void setErrorListaVacia(String errorListaVacia) {
		this.errorListaVacia = errorListaVacia;
	}

	public HtmlSelectOneMenu getComponente() {
		return componente;
	}

	public void setComponente(HtmlSelectOneMenu componente) {
		this.componente = componente;
	}

	public boolean isCertifica() {
		return certifica;
	}

	public boolean getCertifica() {
		return certifica;
	}

	public void setCertifica(boolean certifica) {
		this.certifica = certifica;
	}

	public List getValidations() {
		return validations;
	}

	public void setValidations(List validations) {
		this.validations = validations;
	}

	private String getUserName() {

		String user_name = (String) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(
			false)).getAttribute("userName");
		if (user_name == null) {
			user_name = "user_codensa";
		}
		System.out.println("user_name: " + user_name);
		return user_name;

	}

	public boolean isReImprimir() {
		return reImprimir;
	}

	public void setReImprimir(boolean reImprimir) {
		this.reImprimir = reImprimir;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String execKndUseCase(ActionEvent event) {
		CUOrigenCertificaBuenClienteBB bb = (CUOrigenCertificaBuenClienteBB) VariableResolverUtils
			.getObject("cuOrigenCertificaBuenClienteBB");

		this.uri = bb.execKndUseCase(event);
		return this.uri;
	}

}
