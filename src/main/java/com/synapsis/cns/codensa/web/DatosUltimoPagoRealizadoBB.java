package com.synapsis.cns.codensa.web;

/**
 * En este Backing bean solo dejo el resultado de la busqueda y luego lo meto en la lista para que lo pueda
 * leer la tabla
 * 
 * @author dbraccio - Suivant 22/04/2008
 * @egrande REFACTOR: creo ConsultaPrincipalUniqueBB (13/08/2009)
 */
public class DatosUltimoPagoRealizadoBB extends ConsultaPrincipalUniqueBB {

	public void buscarDatosUltimoPago() {
		findObject("datosUltimoPagoRealizadoService");
	}

}
