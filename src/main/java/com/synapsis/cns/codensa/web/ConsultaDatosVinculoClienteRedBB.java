package com.synapsis.cns.codensa.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.myfaces.custom.service.UIService;
import org.apache.myfaces.shared_impl.util.MessageUtils;

import com.synapsis.cns.codensa.model.impl.ConsultaDatosTransformadorBySEImpl;
import com.synapsis.cns.codensa.model.impl.ConsultaDatosVinculoClienteRedByParamsImpl;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;
import com.synapsis.synergia.presentation.model.NasServiceHelper;

public class ConsultaDatosVinculoClienteRedBB {	
	private String nroServicioElectrico;
	private Long nroServicioElectricoValue;
	private Date fechaDesde;
	private Date fechaHasta;
	private UIService findDatosVinculoClienteRedByParamsService;
	private Date fechaProceso;
	private ConsultaDatosVinculoClienteRedByParamsImpl datosVinculoClienteRed;
	private ConsultaDatosTransformadorBySEImpl consultaDatosTranformador;
	
	public ConsultaDatosTransformadorBySEImpl getConsultaDatosTranformador() {
		return consultaDatosTranformador;
	}
	public void setConsultaDatosTranformador(
			ConsultaDatosTransformadorBySEImpl consultaDatosTranformador) {
		this.consultaDatosTranformador = consultaDatosTranformador;
	}
	public void search() {	
		if(validateFields()){
			Map parametroVinculo = new HashMap();
			parametroVinculo.put("servicio", this.nroServicioElectricoValue);
			parametroVinculo.put("fechaProceso", DateConverterUtils.getDateFormatddmmyyyy(this.fechaProceso));
			List datosVinculo = (List)NasServiceHelper.getResponse(
			"datosVinculoClienteRedByParamsService", "findByCriteria",parametroVinculo);
			
			
			Map parametroTrafo = new HashMap();
			parametroTrafo.put("servicio", this.nroServicioElectricoValue);
			parametroTrafo.put("fechaProceso", this.fechaProceso);
			
			List datosTrafo = (List)NasServiceHelper.getResponse(
				"datosVinculoClienteRedTrafoBySE", "findByCriteria",parametroTrafo);
			
			if(datosVinculo.size() != 0) {
				Iterator datosIter = datosVinculo.iterator();
				if(datosIter.hasNext()){
					datosVinculoClienteRed = (ConsultaDatosVinculoClienteRedByParamsImpl)datosIter.next();
					consultaDatosTranformador = (ConsultaDatosTransformadorBySEImpl)datosTrafo.get(0);
				}
			}else{
				FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.no.hay.registros", null));
			}
			
			
		//	this.getFindDatosVinculoClienteRedByParamsService().execute();
		}	
	}
	private boolean validateFields(){
		boolean validate = true;
		if (this.getNroServicioElectrico()==null){ //hacemos esto aca por que este BB pertenece a un subview y no podesmos usar commons validator
			FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.nroServico.cns.requerido.error", null));
			validate = false;
		}
		if (this.getFechaProceso() == null) {
			FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.periodo.cns.requerido.error", null));
			validate = false;
		}
		try{
			nroServicioElectricoValue = new Long(this.getNroServicioElectrico());
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,MessageUtils.getMessage("servicio.nroServicio.cns.number.error", null));
			validate = false;
		}
		return validate;
	}
	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	
	public Date getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public UIService getFindDatosVinculoClienteRedByParamsService() {
		return findDatosVinculoClienteRedByParamsService;
	}
	public void setFindDatosVinculoClienteRedByParamsService(
			UIService findDatosVinculoClienteRedByParamsService) {
		this.findDatosVinculoClienteRedByParamsService = findDatosVinculoClienteRedByParamsService;
	}
	
	/**
	 * @return the nroServicioElectrico
	 */
	public String getNroServicioElectrico() {
		return nroServicioElectrico;
	}
	/**
	 * @param nroServicioElectrico the nroServicioElectrico to set
	 */
	public void setNroServicioElectrico(String nroServicioElectrico) {
		this.nroServicioElectrico = nroServicioElectrico;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	/**
	 * @return the datosVinculoClienteRed
	 */
	public ConsultaDatosVinculoClienteRedByParamsImpl getDatosVinculoClienteRed() {
		return datosVinculoClienteRed;
	}
	/**
	 * @param datosVinculoClienteRed the datosVinculoClienteRed to set
	 */
	public void setDatosVinculoClienteRed(
			ConsultaDatosVinculoClienteRedByParamsImpl datosVinculoClienteRed) {
		this.datosVinculoClienteRed = datosVinculoClienteRed;
	}
	/**
	 * @return the nroServicioElectricoValue
	 */
	public Long getNroServicioElectricoValue() {
		return nroServicioElectricoValue;
	}
	/**
	 * @param nroServicioElectricoValue the nroServicioElectricoValue to set
	 */
	public void setNroServicioElectricoValue(Long nroServicioElectricoValue) {
		this.nroServicioElectricoValue = nroServicioElectricoValue;
	}
}

