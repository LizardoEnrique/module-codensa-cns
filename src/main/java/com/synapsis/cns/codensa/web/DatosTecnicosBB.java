package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.model.BusinessObject;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ViewBB;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;

public class DatosTecnicosBB extends ViewBB {
	
	private BusinessObject datosTecnicos;
	private BusinessObject atributos;

	private String serviceNameDatosTecnicos;
	private String serviceNameAtributos;
	

	/**
	 * @return the atributos
	 */
	public BusinessObject getAtributos() {
		return atributos;
	}


	/**
	 * @param atributos the atributos to set
	 */
	public void setAtributos(BusinessObject atributos) {
		this.atributos = atributos;
	}


	/**
	 * @return the datosTecnicos
	 */
	public BusinessObject getDatosTecnicos() {
		return datosTecnicos;
	}


	/**
	 * @param datosTecnicos the datosTecnicos to set
	 */
	public void setDatosTecnicos(BusinessObject datosTecnicos) {
		this.datosTecnicos = datosTecnicos;
	}


	/**
	 * @return the serviceNameAtributs
	 */
	public String getServiceNameAtributos() {
		return serviceNameAtributos;
	}


	/**
	 * @param serviceNameAtributs the serviceNameAtributs to set
	 */
	public void setServiceNameAtributos(String serviceNameAtributos) {
		this.serviceNameAtributos = serviceNameAtributos;
	}


	/**
	 * @return the serviceNameDatosTecnicos
	 */
	public String getServiceNameDatosTecnicos() {
		return serviceNameDatosTecnicos;
	}


	/**
	 * @param serviceNameDatosTecnicos the serviceNameDatosTecnicos to set
	 */
	public void setServiceNameDatosTecnicos(String serviceNameDatosTecnicos) {
		this.serviceNameDatosTecnicos = serviceNameDatosTecnicos;
	}


	private void searchBusiness() {
		try {
			setDatosTecnicos(getService(getServiceNameDatosTecnicos()
				).findByCriteriaUnique(getQueryFilter()));
		}catch (ObjectNotFoundException e) {
		//ErrorsManager.addInfoMessage(e.getMessage());
		}
		try {
			setAtributos(getService(getServiceNameAtributos()
					).findByCriteriaUnique(getQueryFilter()));
		}catch (ObjectNotFoundException e) {
		//ErrorsManager.addInfoMessage(e.getMessage());
			
		}
	}
	

	public DatosTecnicosBB() {
	}
	

	public void buscarDatosTecnicosAction() {
			searchBusiness();
	}
	
	private FinderService getService(String serviceFinder){
		return (FinderService)SynergiaApplicationContext.findService(
				serviceFinder);
	}
	
	
}
