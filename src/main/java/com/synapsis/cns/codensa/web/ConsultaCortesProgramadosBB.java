package com.synapsis.cns.codensa.web;

import java.util.Date;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;
import com.synapsis.nuc.codensa.utils.DateConverterUtils;


public class ConsultaCortesProgramadosBB  {
	/**
	*
	* @author Alex Valencia
	*
	*/
	private static final long serialVersionUID = 1L;
	
	private String tipoServicioEmpresarial;
	private String cicloFacturacion;
	private String comercializador;
	private String regional;
	private String transformador;
	private String codCircuito;
	private Date fechaInicioCorte;
	private Date fechaFinCorte;
	private UIService findCortesProgramadosSDAService;	                  
	private HtmlDataTable dataTableCortesSDA;

	// esto es un patch
	private Date fechaHastaFormatted;
	// esto es un patch
	private Date fechaDesdeFormatted;


	
	public void search() {
	  this.getFindCortesProgramadosSDAService().execute();
	  this.getDataTableCortesSDA().setFirst(0);	      
	}

	public String getCicloFacturacion() {
		return cicloFacturacion;
	}
	public void setCicloFacturacion(String cicloFacturacion) {
		this.cicloFacturacion = cicloFacturacion;
	}
	public String getComercializador() {
		return comercializador;
	}
	public void setComercializador(String comercializador) {
		this.comercializador = comercializador;
	}
	public Date getFechaFinCorte() {
		return fechaFinCorte;
	}
	public void setFechaFinCorte(Date fechaFinCorte) {
		this.fechaFinCorte = fechaFinCorte;
	}
	public Date getFechaInicioCorte() {
		return fechaInicioCorte;
	}
	public void setFechaInicioCorte(Date fechaInicioCorte) {
		this.fechaInicioCorte = fechaInicioCorte;
	}

	public String getTipoServicioEmpresarial() {
		return tipoServicioEmpresarial;
	}
	public void setTipoServicioEmpresarial(String tipoServicioEmpresarial) {
		this.tipoServicioEmpresarial = tipoServicioEmpresarial;
	}
	public String getTransformador() {
		return transformador;
	}
	public void setTransformador(String transformador) {
		this.transformador = transformador;
	}
	public UIService getFindCortesProgramadosSDAService() {
		return findCortesProgramadosSDAService;
	}
	public void setFindCortesProgramadosSDAService(
			UIService findCortesProgramadosSDAService) {
		this.findCortesProgramadosSDAService = findCortesProgramadosSDAService;
	}

	public HtmlDataTable getDataTableCortesSDA() {
		return dataTableCortesSDA;
	}

	public void setDataTableCortesSDA(HtmlDataTable dataTableCortesSDA) {
		this.dataTableCortesSDA = dataTableCortesSDA;
	}

	public String getRegional() {
		return regional;
	}

	public void setRegional(String regional) {
		this.regional = regional;
	}

	public Date getFechaHastaFormatted() {
		return DateConverterUtils.getDateFormatddmmyyyy(this.fechaFinCorte);
	}
	
	public void setFechaHastaFormatted(Date fechaHastaFormatted) {
		this.fechaHastaFormatted = fechaHastaFormatted;
	}

	public Date getFechaDesdeFormatted() {
		return DateConverterUtils.getDateFormatddmmyyyy(this.fechaInicioCorte);
	}
	
	public void setFechaDesdeFormatted(Date fechaDesdeFormatted) {
		this.fechaDesdeFormatted = fechaDesdeFormatted;
	}

	/**
	 * @return the codCircuito
	 */
	public String getCodCircuito() {
		return codCircuito;
	}

	/**
	 * @param codCircuito the codCircuito to set
	 */
	public void setCodCircuito(String codCircuito) {
		this.codCircuito = codCircuito;
	}

}

