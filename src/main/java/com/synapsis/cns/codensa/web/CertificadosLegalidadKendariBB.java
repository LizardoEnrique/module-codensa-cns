/**
 * $Id: CertificadosLegalidadKendariBB.java,v 1.6 2008/03/15 20:27:44 ar26557682 Exp $
 */
package com.synapsis.cns.codensa.web;

import java.util.Date;

import javax.faces.event.ActionEvent;

import org.apache.myfaces.custom.datatable.ext.HtmlDataTable;
import org.apache.myfaces.custom.service.UIService;

import com.synapsis.cns.codensa.model.impl.CertificadoLegalidadImpl;
import com.synapsis.integration.codensa.integration.kendari.URLHelper;
import com.synapsis.integration.codensa.integration.kendari.URLParameter;

/**
 * @author Emiliano Arango (ar30557486)
 * @author Paola Attadio (refactor por fix rotura de este CU realizada por Emiliano Arango)
 * 
 */
public class CertificadosLegalidadKendariBB {

	/**
	 * Helper que resuelve la url para realizar la llamada a KND
	 */
	private URLHelper urlHelper;
	private UIService certificadoLegalidadService;
	private HtmlDataTable certificadoLegalidadDataTable;
	private CertificadoLegalidadImpl certificadosLegalidadSeleccionados;
	private String uri;
	private Date fechaDesde;
	private Date fechaHasta;

	public void makeAction(ActionEvent event) {
		URLParameter param1 = null;
		if (certificadosLegalidadSeleccionados != null) {
			param1 = new URLParameter("NAS_nro_orden",
					certificadosLegalidadSeleccionados.getNroOrdenGarantia());
			uri = urlHelper.getUrlComplete(getUseCaseName(),
					new URLParameter[] { param1 });
		}
	}

	protected String getUseCaseName() {
		return "InformaDocumentoGtiaElectricaUseCaseFactory";
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public URLHelper getUrlHelper() {
		return urlHelper;
	}

	public void setUrlHelper(URLHelper urlHelper) {
		this.urlHelper = urlHelper;
	}

	public void search() {
		this.getCertificadoLegalidadService().execute();
		this.getCertificadoLegalidadDataTable().setFirst(0);
	}

	public HtmlDataTable getCertificadoLegalidadDataTable() {
		return certificadoLegalidadDataTable;
	}

	public void setCertificadoLegalidadDataTable(
			HtmlDataTable certificadoLegalidadDataTable) {
		this.certificadoLegalidadDataTable = certificadoLegalidadDataTable;
	}

	public UIService getCertificadoLegalidadService() {
		return certificadoLegalidadService;
	}

	public void setCertificadoLegalidadService(
			UIService certificadoLegalidadService) {
		this.certificadoLegalidadService = certificadoLegalidadService;
	}

	public CertificadoLegalidadImpl getCertificadosLegalidadSeleccionados() {
		return certificadosLegalidadSeleccionados;
	}

	public void setCertificadosLegalidadSeleccionados(
			CertificadoLegalidadImpl certificadosLegalidadSeleccionados) {
		this.certificadosLegalidadSeleccionados = certificadosLegalidadSeleccionados;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	
}
