package com.synapsis.cns.codensa.web;

import com.suivant.arquitectura.core.exception.ObjectNotFoundException;
import com.suivant.arquitectura.core.queryFilter.QueryFilter;
import com.suivant.arquitectura.core.queryFilter.SimpleQueryFilter;
import com.suivant.arquitectura.core.service.FinderService;
import com.suivant.arquitectura.presentation.model.ListableBB;
import com.suivant.arquitectura.presentation.utils.VariableResolverUtils;
import com.synapsis.commons.faces.messages.ErrorsManager;
import com.synapsis.synergia.core.context.SynergiaApplicationContext;
import com.synapsis.synergia.core.model.SynergiaBusinessObject;

public class ConsultaItemDisposicionLegalBB extends ListableBB {

	private SynergiaBusinessObject businessObject2;
	/* ServicioBOMovimientos Refacturacion Ajuste */
	private String serviceNameBOItemDispLegal;	
	private QueryFilter queryFilterForItemsDispLegal;
	private SynergiaBusinessObject businessObject1;
	
	private Long idOrdenQueryFilter;
	private Long idMovRefaQueryFilter;
	
	
	public void buscarDatosItemsDisposicionLegal() {
		try {
			buscarDatosItemsDisposicionLegalImpl();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	public void buscarDetailDatos() {
		try {
			searchDetailBusiness();
		} catch (ObjectNotFoundException e) {
			ErrorsManager.addInfoError(e.getMessage());
		}
	}

	private void searchDetailBusiness() throws ObjectNotFoundException {
		setBusinessObject1((SynergiaBusinessObject) getService(getServiceNameBOItemDispLegal()).findByCriteriaUnique(getQueryFilterForItemsDispLegal()));
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("idMovRefaQueryFilter");
		//consultaMovimientosDisposicionLegalDAO
		setBusinessObject2((SynergiaBusinessObject) this.getService("consultaItemsDispLegalDetailServiceFinder").findByCriteriaUnique(queryFilter));		
	}

	private void buscarDatosItemsDisposicionLegalImpl() throws ObjectNotFoundException {
		
		QueryFilter queryFilter = (QueryFilter) VariableResolverUtils.getObject("numeroAjusteQueryFilter");
		Long nroAjuste = (Long) ((SimpleQueryFilter) queryFilter).getAttributeValue();
		((SimpleQueryFilter) queryFilter).setAttributeName("idOrden");
		((SimpleQueryFilter) queryFilter).setAttributeValue(nroAjuste);

		try { 
		setBusinessObject1((SynergiaBusinessObject) getService(getServiceNameBOItemDispLegal()).findByCriteriaUnique(queryFilter));

		} catch(ObjectNotFoundException e) {
			((SimpleQueryFilter) queryFilter).setAttributeName("nroAjuste");
			((SimpleQueryFilter) queryFilter).setAttributeValue(nroAjuste);
			throw new ObjectNotFoundException(e); 
		}


		//consultaMovimientosDisposicionLegalDAO
		//setBusinessObject2((SynergiaBusinessObject) this.getService("consultaItemsDispLegalServiceFinder").findByCriteriaUnique(queryFilter));		
	}

	/** 
	 * Devuleve un servicio pidiendoselo al SynergiaApplicacontext
	 */
	private FinderService getService(String service) {
		return (FinderService) SynergiaApplicationContext
				.findService(service);
	}
	
	
	
	//______________________________________________ SETTERS && GETTERS ____________________________________________

	public SynergiaBusinessObject getBusinessObject1() {
		return businessObject1;
	}
	public void setBusinessObject1(SynergiaBusinessObject businessObject1) {
		this.businessObject1 = businessObject1;
	}
	public SynergiaBusinessObject getBusinessObject2() {
		return businessObject2;
	}
	public void setBusinessObject2(SynergiaBusinessObject businessObject2) {
		this.businessObject2 = businessObject2;
	}
	public Long getIdOrdenQueryFilter() {
		return idOrdenQueryFilter;
	}
	public void setIdOrdenQueryFilter(Long idOrdenQueryFilter) {
		this.idOrdenQueryFilter = idOrdenQueryFilter;
	}
	public QueryFilter getQueryFilterForItemsDispLegal() {
		return queryFilterForItemsDispLegal;
	}
	public void setQueryFilterForItemsDispLegal(
			QueryFilter queryFilterForItemsDispLegal) {
		this.queryFilterForItemsDispLegal = queryFilterForItemsDispLegal;
	}
	public String getServiceNameBOItemDispLegal() {
		return serviceNameBOItemDispLegal;
	}
	public void setServiceNameBOItemDispLegal(String serviceNameBOItemDispLegal) {
		this.serviceNameBOItemDispLegal = serviceNameBOItemDispLegal;
	}

	public Long getIdMovRefaQueryFilter() {
		return idMovRefaQueryFilter;
	}

	public void setIdMovRefaQueryFilter(Long idMovRefaQueryFilter) {
		this.idMovRefaQueryFilter = idMovRefaQueryFilter;
	}


}
