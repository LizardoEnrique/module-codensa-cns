package com.synapsis.cns.codensa;

import com.suivant.arquitectura.support.tests.common.SpringTestCase;

/**
 * clase de la cual deben extender los testcase de cns-codensa
 * @author ar29261698
 *
 */

public abstract class CnsCodensaTestCase extends SpringTestCase{
     
	public CnsCodensaTestCase() {
		super(CnsCodensaTestSuite.SPRING_TEST_CONFIG);
	}
	

}
