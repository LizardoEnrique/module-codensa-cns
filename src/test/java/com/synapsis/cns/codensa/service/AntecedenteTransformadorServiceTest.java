/**
 * 
 */
package com.synapsis.cns.codensa.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.cns.codensa.CnsCodensaTestService;
import com.synapsis.cns.codensa.model.impl.AntecedenteTransformadorImpl;




/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public class AntecedenteTransformadorServiceTest extends CnsCodensaTestService {


	public AntecedenteTransformadorServiceTest() {
		super();
	}


	protected Class getBeanClass() {
		return AntecedenteTransformadorImpl.class;
	}


	protected String getServiceName() {
		return "antecedenteTransformadorServiceFinder";
	}
	


}
