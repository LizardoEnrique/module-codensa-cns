/**
 * 
 */
package com.synapsis.cns.codensa.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.suivant.arquitectura.core.model.PersistentObject;
import com.synapsis.cns.codensa.CnsCodensaTestService;
import com.synapsis.cns.codensa.model.impl.CaracteristicasGeneralImpl;

/**
 * @author dBraccio
 *
 * 16/11/2006
 */
public class CaracteristicasGeneralServiceTest extends
	CnsCodensaTestService {

	protected final Log log = LogFactory.getLog(getClass());

	
	protected String getServiceName() {
		return "caracteristicasGeneralServiceFinder";
	}



	protected void assertsUpdate(PersistentObject oldBean, PersistentObject newBean) {
		// TODO Auto-generated method stub
		
	}


	protected Class getBeanClass() {
		return CaracteristicasGeneralImpl.class;
	}


	protected PersistentObject getMockBean() {
		// TODO Auto-generated method stub
		return null;
	}


	protected PersistentObject getMockModifiedBean(PersistentObject bean) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
