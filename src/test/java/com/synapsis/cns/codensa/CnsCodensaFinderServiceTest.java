/**
 * 
 */
package com.synapsis.cns.codensa;

import com.suivant.arquitectura.support.tests.service.SpringFinderServiceTestCase;

/**
 * @author ar18799631
 *
 */ 
public abstract class CnsCodensaFinderServiceTest extends SpringFinderServiceTestCase {

	public CnsCodensaFinderServiceTest(){
		super(CnsCodensaTestSuite.SPRING_TEST_CONFIG);
	}
	
}
