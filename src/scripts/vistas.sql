CREATE OR REPLACE VIEW CNS_DATOS_TITULAR_CUENTA_V ( ID_EMPRESA, 
NRO_CUENTA, NOMBRE, APELLIDO_PAT, APELLIDO_MAT, 
COD_TIP_DOC_PERSONA, DES_TIP_DOC_PERSONA, NRO_DOCTO_IDENT, TELEFONO, 
TELEFONO_MOVIL, TELEFONO_AUXILIAR, EMAIL, ID_CUENTA
 ) AS select  
          cu.id_empresa 
        , cu.nro_cuenta 
        , p.nombre 
        , p.apellido_pat 
        , p.apellido_mat 
        , t.cod_tip_doc_persona 
        , t.des_tip_doc_persona 
        ,p.nro_docto_ident 
        , p.telefono 
        , p.telefono_movil 
        , p.telefono_auxiliar 
        , p.email 
        , cu.id_cuenta 
from nuc_cuenta cu 
       , nuc_cliente cli 
       , nuc_persona p 
       , nuc_tip_doc_persona t 
where 
cu.id_cliente = cli.id_cliente and 
cli.id_persona = p.id_persona and 
t.id_tip_doc_persona = p.id_tip_doc_persona;


--1 clientes aic contactos
CREATE OR REPLACE VIEW CNS_AIC_CONTACTOS_V ( ID_CUENTA, 
NRO_CUENTA, ID_EMPRESA, ID_CONTACTO_DIST, ORIGEN_CONTACTO, 
NUMERO_CONTACTO, FECHA_HORA_CREACION, CANAL_COMUNICACION_CONTACTO, USUARIO_GENERADOR_CONTACTO, 
NOMBRE_GENERADOR_CONTACTO, GRUPO_USUARIO_GENERADOR, OBSERVACIONES_CONTACTO, TIPO_RADICACION, 
NUMERO_RADICACION ) AS select a.id_cuenta                        id_cuenta, 
       a.nro_cuenta                       nro_cuenta, 
       a.id_empresa                       id_empresa, 
       b.id_contacto_dist                 id_contacto_dist, 
       b.origen_contacto	              origen_contacto, 
       b.nro                              numero_contacto, 
	   b.fecha_hora_inicio                fecha_hora_creacion,           
	   c.des_canal_comunica               canal_comunicacion_contacto,   
	   d.username                         usuario_generador_contacto,  
	   d.apellido || ', ' || d.nombre     nombre_generador_contacto,   
	   b.grupo_trabajo                    grupo_usuario_generador, 
	   b.observ                           observaciones_contacto,  
	   b.tipo_radic                       tipo_radicacion, 
	   b.nro_radic                        numero_radicacion 
from  
     nuc_cuenta         a, 
     aic_contacto_dist  b, 
     aic_canal_comunica c, 
	 seg_usuario        d 
where  a.id_cuenta         = b.id_cuenta 
and    b.id_canal_comunica = c.id_canal_comunica 
and    b.id_usuario        = d.id_usuario;

--2 clientes aic atenciones
CREATE OR REPLACE VIEW CNS_AIC_ATENCIONES_V ( NRO_CUENTA, 
NUMERO_CONTACTO, ID_EMPRESA, ID_CONTACTO_DIST, ID_ATENCION, 
ESTADO_ATENCION, NUMERO_ATENCION, TIPO_SERVICIO, NUMERO_SERVICIO, 
TIPO_DE_ATENCION, DESCRIPCION_AREA, MOTIVO_CLIENTE, FECHA_HORA_CREACION_ATENCION, 
FECHA_FINALIZACION_ATENCION, FECHA_VENCIMIENTO_INTERNO, FECHA_VENCIMIENTO_LEGAL, USUARIO_RESPONSABLE_ATENCION, 
NOMBRE_RESPONSABLE_ATENCION, TIPO_RESPUESTA_ATENCION ) AS select  
    a.nro_cuenta                   nro_cuenta,  
    b.nro                          numero_contacto, 
	a.id_empresa                   id_empresa,     
    c.id_contacto_dist             id_contacto_dist,   
    d.id_atencion                  id_atencion,   
    d.estado                       estado_atencion,   
    d.nro_atenc                    numero_atencion,   
    j.tipo_servicio                tipo_servicio,   
    j.nro_servicio                 numero_servicio,   
    e.description                  tipo_de_atencion,   
    f.descripcion                  descripcion_area,   
    g.description                  motivo_cliente,   
    d.fecha_reg                    fecha_hora_creacion_atencion,   
    d.fecha_cierre                 fecha_finalizacion_atencion,   
    d.fecha_vi                     fecha_vencimiento_interno,   
    d.fecha_vl                     fecha_vencimiento_legal,   
    h.username                     usuario_responsable_atencion,   
    h.apellido || ', ' || h.nombre nombre_responsable_atencion,   
    i.des_tipo_res                 tipo_respuesta_atencion   
from   
  nuc_cuenta         a,   
  aic_contacto_dist  b,     
  aic_tem_cont_dist  c,   
  aic_atencion_dist  d,   
  aic_tp_atencion    e,   
  seg_area           f,   
  aic_mot_clien_dist g,   
  seg_usuario        h,   
  aic_tipo_res_dist  i,   
  nuc_servicio       j   
where  a.id_cuenta         = b.id_cuenta   
and    b.id_contacto_dist  = c.id_contacto_dist   
and    c.id_tp_atencion    = e.id_tp_atencion   
and    c.id_atencion       = d.id_atencion  (+)   
and    d.id_area           = f.id_area      (+)   
and    c.id_mot_clien_dist = g.id_mot_clien_dist   
and    d.id_user_resp      = h.id_usuario   (+)   
and    d.id_tipo_resp_dist = i.id_tipo_res  (+)   
and    c.id_servicio       = j.id_servicio  (+);

--3 clientes aic observaciones
CREATE OR REPLACE VIEW CNS_AIC_OBSERVACIONES_V ( ID_OBSERVACION, 
ID_ATENCION, ID_EMPRESA, OBSERVACION ) AS select id_observacion, id_atencion, id_empresa,  
       texto observacion  
from  aic_obs_at_dist;

------despues vemos
  CREATE OR REPLACE FORCE VIEW CNS_CON_DEUDACONDONADA_V ("ID_EMPRESA", "ID_CUENTA", "NRO_CUENTA", "DEUDA_CONDONADA") AS 
  SELECT nc.id_empresa, nc.id_cuenta,nc.nro_cuenta, SUM(nid.saldo) 
FROM NUC_CUENTA nc, NUC_SERVICIO ns, CON_CONVENIO cc, CON_ITEM_CONVENIDO cic, 
	 NUC_DOCUMENTO nd, NUC_ITEM_DOC nid , NUC_CARGO ncar 
WHERE 
nid.saldo <> 0 
AND ncar.ind_condonable ='S' 
AND cc.monto_condonacion >0 
AND nid.id_cargo = ncar.id_cargo 
AND cic.id_item_doc = nid.id_item_doc 
AND nd.id_documento = nid.id_documento 
AND ns.id_servicio = cc.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND ns.id_cuenta= nc.id_cuenta 
GROUP BY nc.id_empresa, nc.id_cuenta, nc.nro_cuenta
;

  CREATE OR REPLACE FORCE VIEW CNS_COB_DEUDACASTIGADA_V ("ID_EMPRESA", "ID_CUENTA", "NRO_CUENTA", "SALDO_CASTIGADO") AS 
  SELECT nc.id_empresa, nc.id_cuenta, NRO_CUENTA,SUM(nid.saldo) 
FROM NUC_CUENTA nc, NUC_SERVICIO ns, COB_ITEM_CASTIGO cic, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid 
WHERE 
nid.saldo <>0 
AND cic.id_item_doc = nid.id_item_doc 
AND nid.id_servicio = ns.id_servicio 
AND nid.id_documento = nd.id_documento 
AND nd.id_cuenta = nc.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
GROUP BY nc.id_empresa, nc.id_cuenta, nc.nro_cuenta
;

  CREATE OR REPLACE FORCE VIEW CNS_CON_CONVACTIVO_V ("ID_EMPRESA", "ID_CUENTA", "NRO_CUENTA", "ID_CONVENIO") AS 
  SELECT nc.id_empresa ,nc.id_cuenta ,nc.nro_cuenta,  CC.ID_CONVENIO 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, CON_CONVENIO cc, WKF_WORKFLOW wkf 
WHERE 
ns.tipo='CONVENIO' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = cc.id_workflow 
AND cc.id_servicio = ns.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa
;

--4 clientes aic ordenes
CREATE OR REPLACE VIEW CNS_AIC_ORDENES_V ( NUMERO_ORDEN, 
ID_ATENCION, ID_ORDEN, ID_EMPRESA, TIPO_ORDEN, 
ESTADO_ORDEN, FECHA_INGRESO_ORDEN ) AS SELECT  
c.nro_orden        numero_orden,  
a.id_atencion      id_atencion,  
b.id_orden		 id_orden,  
a.id_empresa       id_empresa,  
d.des_tipo_orden   tipo_orden,  
e.id_state         estado_orden,  
c.fecha_creacion   fecha_ingreso_orden  
FROM  
aic_atencion_dist a,  
aic_atenc_orden b,  
ord_orden       c,  
ord_tipo_orden  d,  
wkf_workflow    e  
WHERE  
a.id_atencion    = b.id_atencion   	and  
b.id_orden      = c.id_orden   	and  
c.id_tipo_orden = d.id_tipo_orden   	and  
c.id_workflow   = e.id_workflow;

--5 clientes clp datos de la cuenta.
CREATE OR REPLACE VIEW CNS_CLP_DATOS_CUENTA_V (ID_CUENTA, 
NRO_CUENTA, ID_EMPRESA, SE_EMPRESARIAL, CORPORATIVA, 
OFICIAL, VIP, EJECUTIVO_CUENTA ) AS select a.id_cuenta                     id_cuenta, 
       a.nro_cuenta  													   nro_cuenta,			  
       a.id_empresa                                                        id_empresa,  
       nvl(c.is_vip,'N')                                                   se_empresarial,  
       DECODE(nvl(c.tip_cli_emp,' '),'Corporativa','S','N')                corporativa,  
       DECODE(nvl(c.tip_cli_emp,' '),'Oficial','S','N')                    oficial,  
       nvl(c.is_vip,'N')                                                   vip,        
       decode(nvl(e.apellido,''),'','',e.apellido, + ',' + e.nombre)       ejecutivo_cuenta  
from nuc_cuenta    a, 
     nuc_servicio  b,   
     clp_serv_pref c,  
     clp_ejecutivo_sep d,  
     seg_usuario       e	  
where a.id_cuenta         = b.id_cuenta 
and   b.id_servicio       = c.id_serv_electrico (+)  
and   c.id_serv_electrico = d.id_clp_serv_pref(+)  
and   d.id_usuario        = e.id_usuario (+);

--6 ingresos fac datos cuenta 
CREATE OR REPLACE VIEW CNS_FAC_DATOS_CUENTA_V(ID_CUENTA, 
NRO_CUENTA, ID_SERVICIO, NRO_SERVICIO, EN_FACTURACION, 
COD_INTERNO, DESCRIPCION, RUTA_FACTURACION, DESCRIPCION_SUCURSAL, 
NIVEL_NODO, CODIGO_RETENCION, FECHA_INCORPORACION, ID_EMPRESA, 
ESTADO_CLIENTE, DIR_REP_ESPECIAL ) AS SELECT NS.ID_CUENTA, NC.NRO_CUENTA, SE.ID_SERVICIO, NS.NRO_SERVICIO, NS.EN_FACTURACION ENFACTURACION, SEE.COD_INTERNO,  
SEE.DESCRIPCION, UR.COD_RUTA RUTA_FACTURACION, UNR.DES_NODO DESCRIPCION_NSUCURSAL, UNIR.NRO_NIVEL NIVEL_NODO,  
'CODRET' CODIGO_RETENCION, SE.FEC_ACTIVACION FECHA_INCORPORACION, NS.ID_EMPRESA, NCLI.ESTADO ESTADO_CLIENTE,  
'DIRECCION_REPARTO_ESPECIAL' DIR_REP_ESPECIAL  
FROM NUC_SERVICIO NS  
INNER JOIN NUC_CUENTA NC ON (NS.ID_CUENTA=NC.ID_CUENTA)  
INNER JOIN SRV_ELECTRICO SE ON (NS.ID_SERVICIO=SE.ID_SERVICIO)  
INNER JOIN SRV_ELECT_ESTADO SEE ON (SE.ID_ESTADO=SEE.ID_ESTADO)  
INNER JOIN FAC_CONFIG_DOC FCD ON (NS.ID_CUENTA=FCD.ID_CUENTA)  
INNER JOIN UBI_RUTA UR ON (FCD.ID_RUTA_FACT=UR.ID_RUTA)  
INNER JOIN UBI_NODO_RUTA UNR ON (UR.ID_NODO=UNR.ID_NODO)  
INNER JOIN UBI_NIVEL_RUTA UNIR ON (UNR.ID_NIVEL=UNIR.ID_NIVEL)  
INNER JOIN NUC_CLIENTE NCLI ON (NCLI.ID_CLIENTE=NC.ID_CLIENTE);

--6.1 lar denuncia ley arrendamiento 
CREATE OR REPLACE VIEW CNS_LAR_DENUNCIA_V AS
select a.id_empresa id_empresa, 
       c.id_orden   id_orden, 
	   a.id_cuenta  id_cuenta,
	   a.nro_cuenta nro_cuenta,
	   b.id_servicio id_servicio,
	   b.nro_servicio nro_servicio, 
	   decode(e.id_state,'APROBADA','S','N') denuncia_lar	   
from nuc_cuenta       a,
     nuc_servicio     b, 
     lar_orden        c,
     ord_orden        d,
     wkf_workflow     e,
	 lar_contrato     f,
	 lar_arrendatario g
where a.id_cuenta       = b.id_cuenta
  and b.id_servicio     = c.id_servicio
  and c.id_orden        = d.id_orden
  and d.id_workflow     = e.id_workflow 
  and c.id_contrato     = f.id_contrato
  and f.arrendatario_id = g.id_arrendatario
  and e.id_state        = 'APROBADA';

--7 caso de uso 0005 - datos cuenta. 
CREATE OR REPLACE VIEW CNS_DATOS_CUENTA_V ( ID_CUENTA, 
NRO_CUENTA, ID_SERVICIO, NRO_SERVICIO, ENFACTURACION, 
COD_INTERNO, DESCRIPCION, RUTA_FACTURACION, DESCRIPCIONSUCURSAL, 
NIVEL_NODO, CODIGORETENCION, DIRECCIONREPARTOESPECIAL, FECHAINCORPORACION, 
ID_EMPRESA, CODIGOENTIDADCOMERCIAL, DESCRIPCIONENTIDADCOMERCIAL, NUMEROCUENTAPADRE, 
ESTADOCLIENTE, PRESENTACONVENIOACTIVO, PRESENTAEXPEDIENTE, PRESENTADEUDACASTIGADA, 
PRESENTADEUDACONDONADA, CLIENTEVIP, SERVICIOELECTRICOEMPRESARIAL, OTROCOMERCIALIZADOR, 
CUENTACORPORATIVA, CUENTAOFICIAL, DENUNCIALEYARRENDAMIENTO ) AS 
SELECT DCFV.ID_CUENTA, DCFV.NRO_CUENTA, DCFV.ID_SERVICIO, DCFV.NRO_SERVICIO, DCFV.EN_FACTURACION ENFACTURACION,       
DCFV.COD_INTERNO, DCFV.DESCRIPCION, DCFV.RUTA_FACTURACION, DCFV.DESCRIPCION_SUCURSAL, DCFV.NIVEL_NODO,       
DCFV.CODIGO_RETENCION, DCFV.DIR_REP_ESPECIAL, DCFV.FECHA_INCORPORACION, DCFV.ID_EMPRESA,       
'COD38482910' CODIGOENTIDADCOMERCIAL, 'Descripcion Entidad Comercial' DESCRIPCIONENTIDADCOMERCIAL,       
'48299' NUMEROCUENTAPADRE, DCFV.ESTADO_CLIENTE, INDEU.IND_CONVENIO, 'N' PRESENTAEXPEDIENTE,       
INDEU.IND_DEUDACASTIGADA, INDEU.IND_DEUDACONDONADA, DCCLP.VIP CLIENTEVIP,  DCCLP.SE_EMPRESARIAL SERVICIOELECTRICOEMPRESARIAL,       
'N' OTROCOMERCIALIZADOR      , DCCLP.CORPORATIVA CUENTACORPORATIVA, DCCLP.OFICIAL CUENTAOFICIAL , DELAR.DENUNCIA_LAR DENUNCIALEYARRENDAMIENTO       
FROM CNS_FAC_DATOS_CUENTA_V  DCFV,      
     CNS_CLP_DATOS_CUENTA_V  DCCLP,
	 CNS_INDICADORDEUDA_V    INDEU,
	 CNS_LAR_DENUNCIA_V      DELAR       
WHERE dcfv.id_cuenta = dcclp.id_cuenta
AND   dcfv.id_cuenta = indeu.id_cuenta (+)
AND   dcfv.id_cuenta = delar.id_cuenta (+);  

--8 ingresos fac datos del beneficiario. 
CREATE OR REPLACE VIEW CNS_FAC_DATOS_BENEFICIARIO_V ( ID_CUENTA, 
NRO_CUENTA, ID_SERVICIO, NRO_SERVICIO, ID_BENEFICIARIO, 
NOMBRE_BENEFICIARIO, APELLIDO_BENEFICIARIO, TIPO_DOCUMENTO_BENEFICIARIO, NRO_DOC_BENEFICIARIO, 
TELEFONO_BENEFICIARIO, DIRECCION_BENEFICIARIO, EMAIL_BENEFICIARIO ) AS SELECT NCU.ID_CUENTA, NCU.NRO_CUENTA, NSE.ID_SERVICIO, NSE.NRO_SERVICIO,  
AB.ID_BENEFICIARIO, AB.NOMBRE, AB.APELLIDO, NTDOC.DES_TIP_DOC_PERSONA, AB.NRO_IDENTIFICACION,  
AB.TEL_UNO TELEFONO, AB.DIRECCION, AB.DIR_EMAIL EMAIL  
FROM AIC_BENEFICIARIO AB, FAC_CONFIG_DOC FCD, NUC_CUENTA NCU, NUC_SERVICIO NSE,  
SRV_ELECTRICO SEL, NUC_TIP_DOC_PERSONA NTDOC  
WHERE AB.ID_CONFIG_FACT=FCD.ID_CONFIG_FACT  
AND FCD.ID_CUENTA=NCU.ID_CUENTA  
AND NCU.ID_CUENTA=NSE.ID_CUENTA  
AND NSE.ID_SERVICIO=SEL.ID_SERVICIO  
AND AB.ID_TIP_DOC_PERSONA=NTDOC.ID_TIP_DOC_PERSONA;

--9 caso de uso 0005 - datos servicio electrico. 
  CREATE OR REPLACE FORCE VIEW CNS_DATOS_SERVICIO_ELECTRICO_V ("ID_SERVICIO", "ID_EMPRESA", "ID_CUENTA", "NRO_SERVICIO", "NRO_CUENTA", "DIRECCION", "MUNICIPIO", "BARRIO", "ID_STATE", "MANZANA", "RUTA_LECTURA", "RUTA_REPARTO", "TIPO_CLIENTE", "CODIGO_TARIFA", "DESC_TARIFA", "ESTRATO_SOC", "CONTADOR_GRADUAL", "LOCALIZACION", "ESTADO_SRV_ELECTRICO", "NOMBRE_BENEFICIARIO", "TIPO_DOC_BENEFICIARIO", "NUM_DOC_BENEFICIARIO", "TELEFONO_BENEFICIARIO", "MAIL_BENEFICIARIO", "ACREDITACION_BENEFICIARIO", "ESTADO_COB", "EJECUTIVO_CUENTA") AS 
  (     
SELECT     
N_SERV.ID_SERVICIO,     
N_SERV.ID_EMPRESA,     
N_CTA.ID_CUENTA,     
N_SERV.NRO_SERVICIO,     
N_CTA.NRO_CUENTA,     
U_DIR.DIRECCION DIRECCION,     
U_MUNIC_V.DES_VALOR MUNICIPIO,     
U_BARRIO_V.DES_VALOR BARRIO,     
WFK.ID_STATE,     
U_MANZ.CODIGO_MANZANA MANZANA,     
U_RUTA_LECT.COD_RUTA RUTA_LECTURA,     
U_RUTA_REP.COD_RUTA RUTA_REPARTO,     
N_TIP_CLI.DES_TIP_CLIENTE TIPO_CLIENTE,     
F_TAR.COD_TARIFA CODIGO_TARIFA,     
F_TAR.DESCRIPCION DESC_TARIFA,     
S_ESTR.DESCRIPCION ESTRATO_SOC,     
S_ESTR.CODIGO CONTADOR_GRADUAL,     
U_LOCALIZACION_V.VALOR LOCALIZACION,     
S_EST.DESCRIPCION ESTADO_SRV_ELECTRICO,     
D_BEN.NOMBRE_BENEFICIARIO || ', ' || D_BEN.APELLIDO_BENEFICIARIO,     
D_BEN.TIPO_DOCUMENTO_BENEFICIARIO,     
D_BEN.NRO_DOC_BENEFICIARIO,     
D_BEN.TELEFONO_BENEFICIARIO,     
D_BEN.EMAIL_BENEFICIARIO,     
'ACREDITACION' ACREDITACION_BENEFICIARIO,   
E_COB.ESTADO_COB, 
D_CLP.EJECUTIVO_CUENTA     
FROM     
NUC_CUENTA N_CTA,     
NUC_SERVICIO N_SERV,     
SRV_ELECTRICO S_ELECT,     
CNS_FAC_DATOS_BENEFICIARIO_V D_BEN,     
NUC_CLIENTE N_CLI,     
NUC_TIP_CLIENTE N_TIP_CLI,     
CNS_CLP_DATOS_CUENTA_V D_CLP,    
CNS_ESTCOBSRVELECT_V   E_COB, 
( SELECT     
ID_DIRECCION,  DES_VALOR     
FROM     
UBI_VALOR_DIR,  UBI_LISTA_VALOR_DIR     
WHERE     
UBI_VALOR_DIR.ATRIBUTO = 'municipio' AND     
UBI_LISTA_VALOR_DIR.ID_LISTA_VALOR_DIR = UBI_VALOR_DIR.ID_LISTA_VALOR_DIR) U_MUNIC_V,     
( SELECT     
ID_DIRECCION,  DES_VALOR     
FROM     
UBI_VALOR_DIR,  UBI_LISTA_VALOR_DIR     
WHERE     
UBI_VALOR_DIR.ATRIBUTO = 'barrio' AND     
UBI_LISTA_VALOR_DIR.ID_LISTA_VALOR_DIR = UBI_VALOR_DIR.ID_LISTA_VALOR_DIR) U_BARRIO_V,     
( SELECT     
ID_DIRECCION,  VALOR     
FROM     
UBI_VALOR_DIR     
WHERE     
UBI_VALOR_DIR.ATRIBUTO = 'localizacion') U_LOCALIZACION_V,     
UBI_DIRECCION U_DIR,     
WKF_WORKFLOW WFK,     
FAC_TARIFA F_TAR,     
SRV_ESTRATOSOC S_ESTR,     
SRV_ELECT_ESTADO S_EST,     
UBI_RUTA U_RUTA_LECT,     
UBI_RUTA U_RUTA_REP,     
UBI_MANZANA U_MANZ,     
REP_CONFIG_REP R_CONF_REP     
WHERE     
N_CTA.ID_CUENTA = N_SERV.ID_CUENTA AND     
N_SERV.ID_SERVICIO = S_ELECT.ID_SERVICIO AND     
N_SERV.TIPO = 'ELECTRICO' AND     
D_BEN.ID_CUENTA(+) = N_CTA.ID_CUENTA AND     
S_ELECT.ID_DIRECCION = U_DIR.ID_DIRECCION AND     
U_MUNIC_V.ID_DIRECCION(+)=S_ELECT.ID_DIRECCION AND     
U_BARRIO_V.ID_DIRECCION(+)=S_ELECT.ID_DIRECCION AND     
U_LOCALIZACION_V.ID_DIRECCION(+) = S_ELECT.ID_DIRECCION AND     
S_ELECT.ID_WORKFLOW = WFK.ID_WORKFLOW AND     
S_ELECT.ID_TARIFA = F_TAR.ID_TARIFA(+) AND     
S_ELECT.ID_ESTRATOSOC = S_ESTR.ID_ESTRATOSOC(+) AND     
S_ELECT.ID_ESTADO = S_EST.ID_ESTADO AND     
S_ELECT.ID_RUTA_LECTURA = U_RUTA_LECT.ID_RUTA(+) AND     
U_RUTA_LECT.ID_MANZANA = U_MANZ.ID_MANZANA(+) AND     
R_CONF_REP.ID_SERVICIO(+)=S_ELECT.ID_SERVICIO AND     
R_CONF_REP.ID_RUTA = U_RUTA_REP.ID_RUTA(+) AND     
N_CLI.ID_CLIENTE=N_CTA.ID_CLIENTE AND     
N_CLI.ID_TIP_CLIENTE=N_TIP_CLI.ID_TIP_CLIENTE AND   
N_CTA.ID_CUENTA     = D_CLP.ID_CUENTA  AND 
N_CTA.ID_CUENTA     = E_COB.ID_CUENTA (+) 
)
;

--10 control rec deuda total actual. 
CREATE OR REPLACE VIEW CNS_REC_DEUDATOTALACT_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, DEUDA_TOTAL_ACTUAL ) AS SELECT empresa, idcuenta, nrocuenta ,SUM(saldo) FROM ( 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, SRV_ELECTRICO se, SRV_ELECT_ESTADO see, 
      NUC_DOCUMENTO nd, NUC_ITEM_DOC nid 
WHERE 
nid.saldo <>0 
AND see.cod_interno ='Habilitado' 
AND ns.tipo='ELECTRICO' 
AND see.id_estado = se.id_estado 
AND nd.id_documento = nid.id_documento 
AND ns.id_servicio = nid.id_servicio 
AND ns.id_servicio = se.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND nd.id_cuenta= nc.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, (fc.capital_no_afecto_interes + ncu.capital + ncu.interes) saldo 
FROM NUC_CUENTA nc, ECO_ENCARGO_COB eco, FIN_CUOTA fc,NUC_CUOTA ncu,  NUC_SERVICIO ns, WKF_WORKFLOW wkf 
WHERE 
wkf.id_state ='Habilitado' 
AND wkf.id_workflow = eco.id_workflow 
AND fc.id_item_capital IS NULL 
AND fc.id_item_interes IS NULL 
AND ns.tipo='ENCARGO_COB' 
AND fc.id_cuota=ncu.id_cuota 
AND fc.id_financiacion = eco.id_financiacion 
AND eco.id_servicio = ns.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,ECO_ENCARGO_COB eco, NUC_SERVICIO ns , WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='ENCARGO_COB' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = eco.id_workflow 
AND nd.id_documento = nid.id_documento 
AND eco.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, (fc.capital_no_afecto_interes+ ncu.capital+ ncu.interes) saldo 
FROM NUC_CUENTA nc, CCA_SERV_FINANC csf, FIN_CUOTA fc,NUC_CUOTA ncu,  NUC_SERVICIO ns , WKF_WORKFLOW wkf 
WHERE 
fc.id_item_capital IS NULL 
AND fc.id_item_interes IS NULL 
AND fc.id_cuota=ncu.id_cuota 
AND fc.id_financiacion = csf.id_financiacion 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = csf.id_workflow 
AND ns.tipo='FINANCIACCA' 
AND csf.id_servicio = ns.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, (nid.saldo) saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CCA_SERV_FINANC csf, NUC_SERVICIO ns, WKF_WORKFLOW wkf 
WHERE 
ns.tipo='FINANCIACCA' 
AND nid.saldo <>0 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = csf.id_workflow 
AND nd.id_documento = nid.id_documento 
AND csf.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta,nc.id_empresa empresa,(fc.capital_no_afecto_interes+ ncu.capital+ ncu.interes) saldo 
FROM NUC_CUENTA nc, FIN_CUOTA fc,NUC_CUOTA ncu,  NUC_SERVICIO ns, CON_CONVENIO cc 
WHERE 
fc.id_item_capital IS NULL 
AND fc.id_item_interes IS NULL 
AND fc.id_cuota=ncu.id_cuota 
AND fc.id_financiacion = cc.id_financiacion 
AND ns.tipo='CONVENIO' 
AND cc.id_servicio = ns.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa,(nid.saldo) saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CON_CONVENIO cc, WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='CONVENIO' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = cc.id_workflow 
AND nd.id_documento = nid.id_documento 
AND cc.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta= nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
)GROUP BY empresa, idcuenta, nrocuenta;

--11 control rec deuda a pagar. 
CREATE OR REPLACE VIEW CNS_REC_DEUDAAPAGAR_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, DEUDA_A_PAGAR ) AS SELECT empresa, idcuenta,nrocuenta, SUM(saldo) FROM ( 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, SRV_ELECTRICO se, 
      NUC_DOCUMENTO nd, NUC_ITEM_DOC nid, SRV_ELECT_ESTADO see 
WHERE 
nid.saldo <>0 
AND see.cod_interno ='Habilitado' 
AND ns.tipo='ELECTRICO' 
AND see.id_estado = se.id_estado 
AND nd.id_documento = nid.id_documento 
AND ns.id_servicio = nid.id_servicio 
AND ns.id_servicio = se.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND nd.id_cuenta= nc.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,ECO_ENCARGO_COB eco, NUC_SERVICIO ns , WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='ENCARGO_COB' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = eco.id_workflow 
AND nd.id_documento = nid.id_documento 
AND eco.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CCA_SERV_FINANC csf, NUC_SERVICIO ns, WKF_WORKFLOW wkf 
WHERE 
ns.tipo='FINANCIACCA' 
AND nid.saldo <>0 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = csf.id_workflow 
AND nd.id_documento = nid.id_documento 
AND csf.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, nc.id_empresa empresa, nid.saldo saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CON_CONVENIO cc, WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='CONVENIO' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = cc.id_workflow 
AND nd.id_documento = nid.id_documento 
AND cc.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta= nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa) 
GROUP BY empresa,idcuenta,nrocuenta;

--12 control rec saldo convenio vigente. 
CREATE OR REPLACE VIEW CNS_REC_SALDOCONVIG_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, SALDO_CONVIG ) AS SELECT  nc.id_empresa,nc.id_cuenta,nc.nro_cuenta,(fc.capital_no_afecto_interes+ ncu.capital+ ncu.interes) saldo 
FROM NUC_CUENTA nc, FIN_CUOTA fc,NUC_CUOTA ncu,  NUC_SERVICIO ns, CON_CONVENIO cc, WKF_WORKFLOW wkf 
WHERE 
wkf.id_state='Activo' 
AND wkf.id_workflow= cc.id_workflow 
AND fc.id_item_capital IS NULL 
AND fc.id_item_interes IS NULL 
AND fc.id_cuota=ncu.id_cuota 
AND fc.id_financiacion = cc.id_financiacion 
AND ns.tipo='CONVENIO' 
AND cc.id_servicio = ns.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa;

--13 control rec saldo ener actual. 
CREATE OR REPLACE VIEW CNS_REC_SALDOENERACT_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, SALDO_ENERACT ) AS SELECT   nc.id_empresa,nc.id_cuenta ,nc.nro_cuenta , SUM (nid.saldo) saldo 
FROM     NUC_CUENTA nc, 
         NUC_SERVICIO ns, 
         SRV_ELECTRICO se, 
         NUC_DOCUMENTO nd, 
         NUC_ITEM_DOC nid, 
         SRV_ELECT_ESTADO see 
WHERE    nid.tip_item_doc = 'Consumo' 
AND      ns.tipo = 'ELECTRICO' 
AND      see.cod_interno = 'Habilitado' 
AND      see.id_estado = se.id_estado 
AND      nid.saldo <> 0 
AND      ns.id_servicio = nid.id_servicio 
AND      ns.id_servicio = se.id_servicio 
AND      nd.id_documento = nid.id_documento 
AND      nc.id_cuenta = nd.id_cuenta 
AND      nc.id_cuenta = ns.id_cuenta 
AND      nc.id_empresa = ns.id_empresa 
GROUP BY nc.id_empresa, nc.id_cuenta ,nc.nro_cuenta;

--14 control rec valor ultima facturacion. 
CREATE OR REPLACE VIEW CNS_REC_VALORULTFACT_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, TOT_DOCUMENTO ) AS SELECT nc.id_empresa, nc.id_cuenta, nc.nro_cuenta,nd.tot_documento 
FROM NUC_CUENTA nc,NUC_SERVICIO ns, NUC_DOCUMENTO nd, HIS_AGRUP_CICLO hac 
WHERE 
ns.id_ult_agrup_ciclo= hac.id_agrup_ciclo 
AND nc.id_cuenta = nd.id_cuenta 
AND ns.id_cuenta = nc.id_cuenta 
AND nc.id_empresa=ns.id_empresa;

--15 caso de uso 0005 - resumen de deuda. 
CREATE OR REPLACE VIEW CNS_RESUMEN_DEUDA_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, DEUDATOTALACTUAL, DEUDAPAGAR, 
SALDOTOTALCONVENIOVIGENTE, SALDODEUDAENERGIAACTUAL, SALDODEUDASERVICIOACTUAL, VALORTOTALULTIMAFACTURA) AS
 SELECT a.id_empresa,
   a.id_cuenta, 
   a.nro_cuenta,
   a.deuda_total_actual,
   b.deuda_a_pagar,
   c.saldo_convig, 
   d.saldo_eneract,
   b.deuda_a_pagar, 
   e.tot_documento    
FROM  
CNS_REC_DEUDATOTALACT_V a,  
CNS_REC_DEUDAAPAGAR_V b,  
CNS_REC_SALDOCONVIG_V c,  
CNS_REC_SALDOENERACT_V d ,  
CNS_REC_VALORULTFACT_V e  
WHERE  
b.id_cuenta(+) = a.id_cuenta  
AND c.id_cuenta(+) = a.id_cuenta  
AND d.id_cuenta(+) = a.id_cuenta  
AND e.id_cuenta(+) = a.id_cuenta;

--16 caso de uso 0005 - resumen de deuda (popup detalle deuda servicio actual).
CREATE OR REPLACE VIEW CNS_REC_DEUDASERVICIOACT_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, NRO_SERVICIO, ID_TIPO_SERV, 
SALDO_SERVICIO ) AS SELECT empresa, idcuenta,nrocuenta, servicio,tipo_servicio , SUM(saldo) 
FROM ( 
SELECT nc.id_empresa empresa,nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta,ns.nro_servicio servicio, ns.tipo tipo_servicio, nid.saldo saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, SRV_ELECTRICO se, 
      NUC_DOCUMENTO nd, NUC_ITEM_DOC nid, SRV_ELECT_ESTADO see 
WHERE 
nid.saldo <>0 
AND see.cod_interno ='Habilitado' 
AND ns.tipo='ELECTRICO' 
AND see.id_estado = se.id_estado 
AND nd.id_documento = nid.id_documento 
AND ns.id_servicio = nid.id_servicio 
AND ns.id_servicio = se.id_servicio 
AND nc.id_cuenta = ns.id_cuenta 
AND nd.id_cuenta= nc.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_empresa empresa,nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, ns.nro_servicio servicio, ns.tipo tipo_servicio, nid.saldo saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,ECO_ENCARGO_COB eco, NUC_SERVICIO ns , WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='ENCARGO_COB' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = eco.id_workflow 
AND nd.id_documento = nid.id_documento 
AND eco.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_empresa empresa,nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, ns.nro_servicio servicio, ns.tipo tipo_servicio, nid.saldo saldo 
FROM NUC_CUENTA nc, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CCA_SERV_FINANC csf, NUC_SERVICIO ns, WKF_WORKFLOW wkf 
WHERE 
ns.tipo='FINANCIACCA' 
AND nid.saldo <>0 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = csf.id_workflow 
AND nd.id_documento = nid.id_documento 
AND csf.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta = nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
UNION ALL 
SELECT nc.id_empresa empresa,nc.id_cuenta idcuenta,nc.nro_cuenta nrocuenta, ns.nro_servicio servicio, ns.tipo tipo_servicio, nid.saldo saldo 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, NUC_DOCUMENTO nd, NUC_ITEM_DOC nid,CON_CONVENIO cc, WKF_WORKFLOW wkf 
WHERE 
nid.saldo <>0 
AND ns.tipo='CONVENIO' 
AND wkf.id_state ='Activo' 
AND wkf.id_workflow = cc.id_workflow 
AND nd.id_documento = nid.id_documento 
AND cc.id_servicio = ns.id_servicio 
AND ns.id_servicio = nid.id_servicio 
AND nc.id_cuenta= nd.id_cuenta 
AND nc.id_cuenta = ns.id_cuenta 
AND ns.id_empresa = nc.id_empresa 
) 
GROUP BY empresa,idcuenta ,nrocuenta ,servicio, tipo_servicio;

--17 caso de uso 0022 - certificados de legalidad.
CREATE OR REPLACE VIEW CNS_GEL_CERTIF_LEGALIDAD_V ( ID_DOCUMENTO, 
ID_CUENTA, NRO_CUENTA, ID_EMPRESA, NRO_SERVICIO, 
FECHA_SOLICITUD, NOM_SOLICITANTE, FECHA_GENERACION_DOC, NRO_DOCUMENTO, 
FECHA_IMPRESION, NRO_ORDEN ) AS select e.id_documento    id_documento, 
       a.id_cuenta       id_cuenta,  
       a.nro_cuenta      nro_cuenta,  
       a.id_empresa      id_empresa,   
   	   b.nro_servicio    nro_servicio, 
       d.fecha_creacion  fecha_solicitud, 
	   e.nom_solicitante nom_solicitante, 
	   e.fecha_impresion fecha_generacion_doc, 
	   e.nro_documento   nro_documento, 
	   e.fecha_impresion fecha_impresion, 
	   d.nro_orden       nro_orden    
from 
    NUC_CUENTA          a,  
	NUC_SERVICIO        b, 
	GEL_ORDEN_GTIA_ELEC c, 
	ORD_ORDEN           d, 
	GEL_DOCUMENTO       e   
where a.id_cuenta          = b.id_cuenta 
and   b.id_servicio        = c.id_servicio 
and   c.id_orden_gtia_elec = d.id_orden 
and   c.id_orden_gtia_elec = e.id_orden 
and   e.tipo               = 'CertificadoLegalidad'; 

--18 caso de uso 0005 - otros servicios.
CREATE OR REPLACE VIEW CNS_OTROSSERVICIOS_V (
   ID_EMPRESA,
   ID_SERVICIO, 
   ID_CUENTA, 
   NRO_CUENTA, 
   NRO_SERVICIO, 
   TIPO, 
   ESTADO,
   TITULAR,
   DOCUMENTO,
   FECHA_INICIO,
   FECHA_FINALIZACION
 ) AS 
SELECT nc.id_empresa, ns.id_servicio, nc.id_cuenta cuenta,nc.nro_cuenta,ns.nro_servicio,ns.tipo_servicio,cec.estado_cob, 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.nombre)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_PAT) ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_MAT)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'J', np.nombre) Titular, 
       DECODE(ntp.cod_tip_persona,'F',  ntdp.cod_interno||'-'||np.NRO_DOCTO_IDENT, NULL) tdoc, 
       ns.fec_inicio,
	   null 
FROM NUC_PERSONA np, NUC_CLIENTE ncli, NUC_CUENTA nc, NUC_SERVICIO ns, ECO_ENCARGO_COB eco, 
	 COB_SRV_CARTERA csc, COB_EST_SRV_CAR cesc, COB_ETAPA_COB cec, WKF_WORKFLOW wkf, 
 	 NUC_TIP_PERSONA ntp, NUC_TIP_DOC_PERSONA ntdp 
WHERE wkf.id_state ='Activo' 
AND eco.id_workflow=wkf.id_workflow 
AND cesc.id_etapa_cob = cec.id_etapa_cob 
AND csc.id_srv_cartera = cesc.id_est_srv_car 
AND ns.tipo='ENCARGO_COB' 
AND ntdp.id_tip_doc_persona = np.id_tip_doc_persona 
AND ntp.id_tip_persona = np.id_tip_persona 
AND csc.id_servicio = eco.id_servicio 
AND ns.id_servicio = eco.id_servicio 
AND ns.id_cuenta = nc.id_cuenta 
AND nc.id_cliente =ncli.id_cliente 
AND ncli.id_persona = np.id_persona 
AND ncli.id_empresa=nc.id_empresa 
UNION ALL 
SELECT ncli.id_empresa, ns.id_servicio, nc.id_cuenta cuenta,nc.nro_cuenta,ns.nro_servicio,ns.tipo_servicio,cec.estado_cob, 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.nombre)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_PAT) ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_MAT)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'J', np.nombre) Titular, 
       DECODE(ntp.cod_tip_persona,'F',ntdp.cod_interno||'-'||np.NRO_DOCTO_IDENT, NULL) tdoc, 
       ns.fec_inicio,
	   null 
FROM NUC_PERSONA np, NUC_CLIENTE ncli, NUC_CUENTA nc, NUC_SERVICIO ns, CCA_SERV_FINANC csf, 
	 COB_SRV_CARTERA csc, COB_EST_SRV_CAR cesc, COB_ETAPA_COB cec, WKF_WORKFLOW wkf, 
 	 NUC_TIP_PERSONA ntp, NUC_TIP_DOC_PERSONA ntdp 
WHERE wkf.id_state ='Activo' 
AND csf.id_workflow=wkf.id_workflow 
AND cesc.id_etapa_cob = cec.id_etapa_cob 
AND csc.id_srv_cartera = cesc.id_est_srv_car 
AND ns.tipo='FINANCIACCA' 
AND ntdp.id_tip_doc_persona = np.id_tip_doc_persona 
AND ntp.id_tip_persona = np.id_tip_persona 
AND csf.id_servicio = csc.id_servicio 
AND ns.id_servicio = csf.id_servicio 
AND ns.id_cuenta = nc.id_cuenta 
AND nc.id_cliente =ncli.id_cliente 
AND ncli.id_persona = np.id_persona 
AND ncli.id_empresa=nc.id_empresa 
UNION ALL 
SELECT nc.id_empresa, ns.id_servicio, nc.id_cuenta cuenta,nc.nro_cuenta,ns.nro_servicio,ns.tipo_servicio,cec.estado_cob, 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.nombre)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_PAT) ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'F', np.APELLIDO_MAT)  ||' '|| 
	   DECODE(ntp.COD_TIP_PERSONA, 'J', np.nombre) Titular, 
       DECODE(ntp.cod_tip_persona,'F',ntdp.cod_interno||'-'||np.NRO_DOCTO_IDENT, NULL) tdoc, 
       ns.fec_inicio,
	   null 
FROM NUC_PERSONA np, NUC_CLIENTE ncli, NUC_CUENTA nc, NUC_SERVICIO ns, CON_CONVENIO cc, 
	 COB_SRV_CARTERA csc, COB_EST_SRV_CAR cesc, COB_ETAPA_COB cec, WKF_WORKFLOW wkf, 
 	 NUC_TIP_PERSONA ntp, NUC_TIP_DOC_PERSONA ntdp 
WHERE wkf.id_state ='Activo' 
AND cc.id_workflow=wkf.id_workflow 
AND cesc.id_etapa_cob = cec.id_etapa_cob 
AND csc.id_srv_cartera = cesc.id_est_srv_car 
AND ns.tipo='CONVENIO' 
AND ntdp.id_tip_doc_persona = np.id_tip_doc_persona 
AND ntp.id_tip_persona = np.id_tip_persona 
AND cc.id_servicio = csc.id_servicio 
AND ns.id_servicio = cc.id_servicio 
AND ns.id_cuenta = nc.id_cuenta 
AND nc.id_cliente =ncli.id_cliente 
AND ncli.id_persona = np.id_persona 
AND nc.id_empresa=ncli.id_empresa; 

--19 caso de uso 0025 - consulta de ordenes.
CREATE OR REPLACE VIEW CNS_ORD_ORDENES_V ( ID_EMPRESA, 
ID_ORDEN, NRO_CUENTA, DES_TIPO_ORDEN, NRO_ORDEN, 
TIPO_SERVICIO, NRO_SERVICIO, ESTADO, FECHA_CREACION, 
FECHA_FINALIZACION, DIAS_HABILES_DIF, FECHA_VENC_LEGAL, FECHA_VENCIMIENTO, 
OBSERVACIONES, USUARIO_GENERADOR, NOMBRE_GENERADOR, USUARIO_FINALIZACION, 
NOMBRE_FINALIZACION, COD_AREA, DES_AREA ) AS
SELECT srv.id_empresa, 
       ord.id_orden, 
       cue.nro_cuenta, 
       tip.des_tipo_orden, 
       ord.nro_orden, 
       srv.tipo tipo_servicio, 
       srv.nro_servicio, 
       wkf.id_state estado, 
       ord.fecha_creacion, 
       ord.fecha_finalizacion, 
       DECODE(ord.fecha_finalizacion, NULL, NULL, 
              0) dias_habiles_dif, 
       ord.fecha_venc_legal, 
       ord.fecha_vencimiento, 
       '' observaciones, 
       usu.username usuario_generador, 
       usu.nombre nombre_generador, 
       '' usuario_finalizacion, 
       '' nombre_finalizacion, 
       are.cod_area, 
       are.descripcion des_area 
FROM   NUC_CUENTA      cue, 
       NUC_SERVICIO    srv, 
       ORD_ORDEN       ord, 
       ORD_TIPO_ORDEN  tip, 
       WKF_WORKFLOW    wkf, 
       SEG_USUARIO     usu, 
       SEG_AREA        are 
WHERE  cue.id_cuenta = srv.id_cuenta 
AND    srv.id_servicio = ord.id_servicio 
AND    ord.id_tipo_orden = tip.id_tipo_orden 
AND    ord.id_workflow = wkf.id_workflow 
AND    ord.id_usuario_creador = usu.id_usuario 
AND    usu.id_area_default = are.id_area; 

--20 caso de uso 0005 - anomalias.
CREATE OR REPLACE VIEW CNS_FAC_ANOMALIAS_V ( ID_EMPRESA, 
ID_SIT_TER_INFO, CODE, DESCRIPTION ) AS SELECT id_empresa, 
       id_sit_ter_info, 
       code, 
       description 
FROM   MED_SIT_TER_INFO;

--21 caso de uso 0026 - restriccionesBB.
CREATE OR REPLACE VIEW CNS_COM_RESTRICCION_V ( ID_EMPRESA, 
ID_RESTRICCION, NRO_CUENTA, TIPO_SERVICIO, NRO_SERVICIO, 
FEC_CREACION, DES_TIP_RESTRICCION, FEC_INI_RESTRICCION, FEC_FIN_RESTRICCION, 
USUARIO_GENERADOR, NOMBRE_GENERADOR, USERNAME_SOLIC_RESTRICCION, NOMBRE_SOLIC_RESTRICCION, 
DES_MOT_RESTRICCION, OBSERVACIONES, USUARIO_DESACTIVA, NOMBRE_DESACTIVA, 
FECHA_DESACTIVA, OBSERVACIONES_DESACTIVA,OBSERVACIONES_INGRESO) AS 
SELECT srv.id_empresa, 
       res.id_restriccion, 
       cue.nro_cuenta, 
       srv.tipo tipo_servicio, 
       srv.nro_servicio, 
       res.fec_creacion, 
       ctr.des_tip_restriccion, 
       res.fec_ini_restriccion, 
       res.fec_fin_restriccion, 
       usu.username usuario_generador, 
       usu.nombre nombre_generador, 
       '' username_solic_restriccion, 
       '' nombre_solic_restriccion, 
       cmr.des_mot_restriccion, 
       res.observaciones, 
       '' usuario_desactiva, 
       '' nombre_desactiva, 
       '' fecha_desactiva, 
       '' observaciones_desactiva,
	   '' observaciones_ingreso 
FROM   NUC_CUENTA          cue, 
       NUC_SERVICIO        srv, 
       COM_SRV_REST        csr, 
       COM_RESTRICCION     res, 
       COM_MOT_RESTRICCION cmr, 
       COM_TIP_RESTRICCION ctr, 
       SEG_USUARIO         usu 
WHERE  cue.id_cuenta = srv.id_cuenta 
AND    srv.id_servicio = csr.id_servicio 
AND    csr.id_restriccion = res.id_restriccion 
AND    res.id_mot_restriccion = cmr.id_mot_restriccion 
AND    cmr.id_tip_restriccion = ctr.id_tip_restriccion 
AND    res.id_usuario_creador = usu.id_usuario 
ORDER  BY res.fec_creacion DESC;

--22 caso de uso 0005 - tarifas.
CREATE OR REPLACE VIEW CNS_FAC_TARIFAS_V ( ID_TARIFA, 
ID_EMPRESA, COD_TARIFA, DESCRIPCION_TARIFA, TIPI, 
ACTIVO, HABILITADO ) AS 
SELECT ID_TARIFA, ID_EMPRESA, COD_TARIFA, DESCRIPCION DESCRIPCION_TARIFA, TIPO, ACTIVO, HABILITADO  
FROM FAC_TARIFA;

--23 caso de uso 0005 - sucursales.
CREATE OR REPLACE VIEW CNS_FAC_SUCURSALES_V ( ID_SUCURSAL, 
ID_EMPRESA, COD_SUCURSAL, DES_SUCURSAL, DIRECCION, 
TELEFONO, FECHA_ACTIVACION, FECHA_DESACTIVACION, ES_CENTRO_SERVICIO
 ) AS SELECT ID_OFICINA, ID_EMPRESA, COD_OFICINA, DES_OFICINA, DIRECCION, TELEFONO, FEC_ACTIVACION,  
FEC_DESACTIVACION, ES_CENTRO_SERVICIO  
FROM SEG_OFICINA;

--24 control indicador deuda.
CREATE OR REPLACE VIEW CNS_INDICADORDEUDA_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, IND_DEUDACONDONADA, IND_DEUDACASTIGADA, 
IND_CONVENIO ) AS 
SELECT a.id_empresa,d.id_cuenta ,d.nro_cuenta, DECODE(NVL(a.deuda_condonada,'N'),'N','N','S'), 
       DECODE(NVL(b.saldo_castigado,'N'),'N','N','S'), DECODE(c.id_convenio,'N','N','S') 
FROM   cns_con_deudacondonada_v a, 
       cns_cob_deudacastigada_v b, 
       cns_con_convactivo_v c, 
       (SELECT id_cuenta  , nro_cuenta 
       FROM CNS_CON_deudaCONDONADA_V 
       UNION 
       SELECT id_cuenta idcuenta , nro_cuenta 
       FROM CNS_COB_deudaCASTIGADA_V 
       UNION 
       SELECT id_cuenta  , nro_cuenta 
       FROM CNS_CON_CONVACTIVO_V 
       ) d 
WHERE a.id_cuenta(+) = d.id_cuenta 
AND b.id_cuenta(+) = d.id_cuenta 
AND c.id_cuenta(+) = d.id_cuenta;

--25 control cobranzas estado cobranza servicio electrico.
CREATE OR REPLACE VIEW CNS_ESTCOBSRVELECT_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, NRO_SERVICIO, ESTADO_COB
 ) AS 
SELECT nc.id_empresa, nc.id_cuenta cuenta,nc.nro_cuenta, ns.NRO_servicio,cec.estado_cob 
FROM  NUC_CUENTA nc, NUC_SERVICIO ns, SRV_ELECTRICO se, 
	 COB_SRV_CARTERA csc, COB_EST_SRV_CAR cesc, COB_ETAPA_COB cec, SRV_ELECT_ESTADO see 
WHERE see.cod_interno ='Habilitado' 
AND see.id_estado=se.id_estado 
AND cesc.id_etapa_cob = cec.id_etapa_cob 
AND csc.id_srv_cartera = cesc.id_est_srv_car 
AND ns.tipo='ELECTRICO' 
AND csc.id_servicio = se.id_servicio 
AND ns.id_servicio =se.id_servicio 
AND ns.id_cuenta = nc.id_cuenta;

--26 control operaciones de corte (caso de uso 24).
CREATE OR REPLACE VIEW CNS_ORD_OPECORTE_V ( ID_EMPRESA, 
ID_CUENTA, NRO_CUENTA, ID_SERVICIO, NRO_SERVICIO, 
NUMERO_OPERACION, NUMERO_REPROGRAMACION, TIPO_OPERACION, CONTRATISTA, FECHA_GENERACION, 
USUARIO_CREACION, NOMBRE_USUARIO_CREACION, ESTADO_OPERACION, MOTIVO_GENERACION, FECHA_EJECUCION, 
RESULTADO_OPERACION, SITUACION_ENCONTRADA, SUBTIPO_OPERACION, OBSERVACION
 ) AS SELECT nc.id_empresa, nc.id_cuenta, nc.nro_cuenta,ns.id_servicio, ns.nro_servicio, oo.id_orden,0, oto.cod_interno, 
       np1.nombre ||' '||np1.apellido_mat||' '||np1.apellido_mat, 
       co.fecha_emision,
	   'usuario_creacion',
	   np2.nombre ||' '||np2.apellido_mat||' '||np2.apellido_mat,	   
	   wkf.id_state, omc.des_motivo,co.fecha_ejecucion, 
       car.descripcion,  cst.descripcion,co.observaciones, cso.descripcion 
FROM NUC_CUENTA nc, NUC_SERVICIO ns, SRV_ELECTRICO se, COR_ORD_SRV_ELECT cose, 
     ORD_ORDEN oo, COR_ORDEN co, ORD_TIPO_ORDEN oto, COR_SUBTIPO_ORD cso, COM_CONTRATISTA cc, NUC_PERSONA np1, 
     COM_EJECUTOR ce,NUC_PERSONA np2, WKF_WORKFLOW wkf, ORD_MOTIVO_CREACION omc , COR_ACC_TERRENO cat, 
	 COR_ACC_REALIZADA car, COM_SIT_TERRENO cst 
WHERE 
omc.id_motivo = oo.id_motivo 
AND co.id_subtipo_orden = cso.id_subtipo_orden 
AND cst.id_sit_terreno(+) = cat.id_sit_terreno_pri 
AND wkf.id_workflow = oo.id_workflow 
AND car.id_acc_realizada(+) = cat.id_accion_realizada 
AND np2.id_persona(+) = ce.id_persona 
AND ce.id_ejecutor(+) = co.id_ejecutor 
AND np1.id_persona(+) = cc.id_persona 
AND cc.id_contratista(+)  = co.contratista 
AND ns.id_servicio = cose.id_servicio 
AND oo.id_tipo_orden = oto.id_tipo_orden 
AND cat.id_orden(+) = co.id_orden 
AND co.id_orden = cose.id_orden 
AND oo.id_orden = co.id_orden 
AND ns.id_servicio = se.id_servicio 
AND nc.id_cuenta=ns.id_cuenta;

 --27 control medidores (caso de uso 24).
CREATE OR REPLACE VIEW CNS_ORD_OPEMEDIDOR_V ( ID_EMPRESA, ID_COMPONENTE, 
ID_SERVICIO, NRO_SERVICIO, ID_CUENTA, NRO_CUENTA, MARCA_MEDIDOR, 
MODELO_MEDIDOR, NUMERO_MEDIDOR, LECTURA_MEDIDOR,TIPO_MEDIDOR,FECHA_HORA_DESCARGA) AS
SELECT nu.id_empresa, mc.id_componente, ns.id_servicio ,ns.nro_servicio, nu.id_cuenta, nu.nro_cuenta, mma.des_marca,mm.des_modelo,mc.nro_componente,mtm.des_tip_medida,'tipo_medidor',SYSDATE
FROM NUC_CUENTA nu, NUC_SERVICIO ns, SRV_ELECTRICO se, MED_COMPONENTE mc, MED_MARCA mma,MED_MODELO mm, MED_TIP_MEDIDA mtm 
WHERE 
mtm.id_tip_medida(+) = mc.id_tip_medida 
AND mm.id_marca      = mma.id_marca 
AND mc.id_modelo     = mm.id_modelo 
AND se.id_servicio   = mc.id_ubicacion 
AND ns.id_servicio   = se.id_servicio
AND nu.id_cuenta     = ns.id_cuenta;

--28 control sellos (caso de uso 24).
CREATE OR REPLACE VIEW CNS_ORD_OPESELLOMEDIDOR_V ( ID_EMPRESA, ID_SELLO, ID_COMPONENTE, 
NRO_SELLO, COLOR_SELLO, MATERIAL_SELLO, ESTADO_SELLO
 ) AS 
SELECT ss.id_empresa, ss.id_sello, ss.id_componente, ss.nro_sello,sc.des_color, sbs.serie, ses.des_est_sello 
FROM SEL_SELLO ss,SEL_BOLSA_SELLOS sbs, SEL_COLOR sc, SEL_MOTIVO_ESTADO sme, SEL_EST_SELLO ses 
WHERE 
ss.ID_MOTIVO_ESTADO = sme.ID_MOTIVO_ESTADO 
AND ses.cod_interno IN ('Instalado','Retirado') 
AND sme.ID_EST_SELLO = ses.ID_EST_SELLO 
AND sbs.id_color = sc.id_color 
AND ss.id_bolsa_sellos = sbs.id_bolsa_sellos;

--29 caso de uso 0005 - ley de arrendamiento. 
CREATE OR REPLACE VIEW CNS_LAR_INF_HISTORICA_V AS
select a.id_empresa id_empresa, 
       c.id_orden   id_orden, 
	   a.id_cuenta  id_cuenta,
	   a.nro_cuenta nro_cuenta,
	   b.id_servicio id_servicio,
	   b.nro_servicio nro_servicio, 
	   decode(e.id_state,'APROBADA','S','N') indicador_denuncia_lar,
	   ' '                                   tipo_residente,
	   'S'                                   indicador_propiedad_med,
	   'S'                                   factura_consumo,
	   'S'                                   emite_factura,
	   0                                     nro_cuenta_inquilino,
	   g.nombre                              nombre,
	   0                                     nro_contrato,
	   f. fecha_fin                          fecha_vencimiento
from nuc_cuenta       a,
     nuc_servicio     b, 
     lar_orden        c,
     ord_orden        d,
     wkf_workflow     e,
	 lar_contrato     f,
	 lar_arrendatario g
where a.id_cuenta       = b.id_cuenta
  and b.id_servicio     = c.id_servicio
  and c.id_orden        = d.id_orden
  and d.id_workflow     = e.id_workflow 
  and c.id_contrato     = f.id_contrato
  and f.arrendatario_id = g.id_arrendatario
  and e.id_state        = 'APROBADA';

--30 caso de uso 0021 - restriccionesBB de suspension. 
CREATE OR REPLACE VIEW CNS_COM_RESTRICCION_SUSP_V ( ID_EMPRESA, 
ID_RESTRICCION, NRO_CUENTA, ID_CUENTA, NRO_SERVICIO, 
ID_SERVICIO, USUARIO_GENERADOR, NOMBRE_GENERADOR, DES_MOT_RESTRICCION, 
USUARIO_SOLICITA_ANULACION, NOMBRE_USUARIO_SOLICITA_ANUL, FEC_SOLIC_RESTRICCION, DIAS_SOLIC_RESTRICCION, 
FEC_VTO_RESTRICCION, OBSERVACIONES, NUMERO_RADICACION ) AS SELECT srv.id_empresa, 
       res.id_restriccion, 
       cue.nro_cuenta, 
       cue.id_cuenta, 
       srv.nro_servicio, 
       srv.id_servicio,	    
       usu.username usuario_generador, 
       usu.nombre nombre_generador, 
       cmr.des_mot_restriccion, 
	   ' ' usuario_solicita_anulacion, 
	   ' ' nombre_usuario_solicita_anul,
	   res.fec_ini_restriccion fec_solic_restriccion,
       TRUNC(res.fec_fin_restriccion)-TRUNC(res.fec_ini_restriccion) dias_solic_restriccion,
       res.fec_fin_restriccion fec_vto_restriccion,
	   res.observaciones, 
	   0 numero_radicacion 
FROM   NUC_CUENTA          cue, 
       NUC_SERVICIO        srv, 
       COM_SRV_REST        csr, 
       COM_RESTRICCION     res, 
       COM_MOT_RESTRICCION cmr, 
       COM_TIP_RESTRICCION ctr, 
       SEG_USUARIO         usu 
WHERE  cue.id_cuenta = srv.id_cuenta 
AND    srv.id_servicio = csr.id_servicio 
AND    csr.id_restriccion = res.id_restriccion 
AND    res.id_usuario_creador = usu.id_usuario 
AND    res.id_mot_restriccion = cmr.id_mot_restriccion 
AND    cmr.id_tip_restriccion = ctr.id_tip_restriccion 
AND    ctr.cod_interno = 'CreacionOrdenCorte' 
ORDER  BY res.fec_creacion DESC; 
